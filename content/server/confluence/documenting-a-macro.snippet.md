---
aliases:
- /server/confluence/documenting-a-macro-39368924.html
- /server/confluence/documenting-a-macro-39368924.md
category: devguide
confluence_id: 39368924
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368924
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368924
date: '2017-12-08'
legacy_title: Documenting a Macro
platform: server
product: confluence
subcategory: other
title: Documenting a Macro
---
# Documenting a Macro

## Overview

HTML can be added to the macro i18n strings - this open the door to provide a hyperlink to documentation for the macro.  A simple approach is to create a public url and simply open a new tab or window to the page using an achor tag (ex: &lt;a href="&lt;url&gt;" target="\_blank"&gt;Documentation&lt;/a&gt; ); however, not all developers have access to a stable and public url to place the documentation.  A second approach is to embed the documentation as a velocity macro and use an ajax call to a rest interface for the plugin to get the information.  This second approach is described in more detail below.

The embedded documentation approach is to provide a link in the macro description i18n string which opens a new dialog window containing the documentation for the macro.

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/20185705/image2015-12-16%2014%3A26%3A19.png?version=1&amp;modificationDate=1450308117600&amp;api=v2" class="image-center confluence-external-resource" height="250" />

The macro browser with the Documentation link

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/20185705/image2015-12-16%2014%3A26%3A34.png?version=1&amp;modificationDate=1450308117570&amp;api=v2" class="image-center confluence-external-resource" height="250" />

The documentation displayed

The pieces to make this approach work are as follows:

## Velocity Templates To Contain the Documentation

The recomendation is to have a header and footer velocity templates in addition to one template per macro.  This is because the documentation needs to be wrapped in the aui-dialog2 section tags.  The classes and xml elements for the header and footer are taken directly from the <a href="https://docs.atlassian.com/aui/latest/docs/dialog2.html" class="external-link">AUI Dialog2 example</a>. 

**help-header.vm**  Expand source

``` xml
<!-- Render the dialog -->
<section role="dialog" class="aui-layer aui-dialog2 aui-dialog2-medium" data-aui-modal="true" aria-hidden="true">
    <!-- Dialog header -->
    <header class="aui-dialog2-header">
        <!-- The dialog's title -->
        <h2 class="aui-dialog2-header-main">$title</h2>
    </header>
    <!-- Main dialog content -->
    <div class="aui-dialog2-content">
```

**help-footer.vm**  Expand source

``` xml
    </div>
    <!-- Dialog footer -->
    <footer class="aui-dialog2-footer">
        <!-- Actions to render on the right of the footer -->
        <div class="aui-dialog2-footer-actions">
            <button id="dialog-submit-button" class="aui-button aui-button-primary" onclick="macroHelpDocumentation.closeMacroHelp();">OK</button>
        </div>
    </footer>
</section>
```

With the header.vm and footer.vm defined, another velocity macro can be defined with arbitrary html that will be presented in the new dialog box.  For example:

**Macro Help Velocity Macro**

``` xml
<h3>Introduction</h3>
<p>This is how to use the macro...</p>
```

## Javascript to Retrieve, Display and Hide the Documentation

### Generic Calls

There are two generic calls.  One to get and show the documentation; the other to hide and remove it.  The syntax for them is below:

``` javascript
macroHelpDocumentation.getMacroHelp( event, <my-plugin-id>, <my-plugins-rest-version>, <my-macro-id> );
macroHelpDocumentation.closeMacroHelp();
```

The actual code is shown below with embedded comments as to what it's doing.

**macro-help.js**  Expand source

``` javascript
// The pattern below is a 'module' pattern based upon iife (immediately invoked function expressions) closures.
// see: http://benalman.com/news/2010/11/immediately-invoked-function-expression/ for a nice discussion of the pattern
// The value of this pattern is to help us keep our variables to ourselves.
// 
macroHelpDocumentation = (function( $ ){

   // module variables:
   var methods = new Object();
   var initialAuiBlanketZIndex = "";
   var embeddedHelpMacroId     = "my-embedded-macro-help";
   var macroBrowserId          = "macro-browser-dialog";

   // module methods
   methods[ 'getMacroHelp' ] = function( e, pluginId, restVersion, macroId ){
      // Prevent any attached event handlers from executing as that can result
      // in opening unwanted windows or closing the macro browser
      e.preventDefault();
      e.stopPropagation();
      $( e.target ).unbind();
 
      // Use ajax to retrieve the documentation from the plugin's rest service, append
      // the returned html to the end of the document and show the dialog.
      // Note, the z index of the aui-blanket (the semi-transparent gray layer) needs
      // to be moved up so it's now in front of the macro browser but behind the 
      // documentation dialog. 
      var helpUrl = AJS.Data.get( "base-url" ) + "/rest/"+pluginId+"/"+restVersion+"/help/" + macroId;
      AJS.$.ajax({
         url: helpUrl,
         type: "GET",
         dataType: "json",
      }).done( function ( data ) { 
         //alert( data[ "message-body" ] );
         var macroBrowserZIndex;

         // append the returned html to the body of the page
         AJS.$( "body" ).append( data[ "message-body" ] );
      
         // give the last section an id
         AJS.$( "section" ).last().attr( "id", embeddedHelpMacroId );

         // show the help dialog
         AJS.dialog2( "#"+embeddedHelpMacroId ).show();

         // move the dialog on top of the macro browser dialog
         macroBrowserZIndex = AJS.$( "#"+macroBrowserId ).zIndex();
         AJS.$( "#"+embeddedHelpMacroId ).zIndex( macroBrowserZIndex + 500 );

         // move the aui-blanket between the macro browser and the help dialog 
         initialAuiBlanketZIndex = AJS.$( ".aui-blanket" ).zIndex();
         AJS.$( ".aui-blanket" ).zIndex( macroBrowserZIndex + 250 ); 

      }).fail( function (self, status, error ){ alert( error );
      });
   }
 
   methods[ 'closeMacroHelp' ] = function(){
      AJS.dialog2("#"+embeddedHelpMacroId).remove();
      // put the aui-blanket back
      AJS.$( ".aui-blanket" ).attr( 'style', 'z-index: '+initialAuiBlanketZIndex ); 
      AJS.$( ".aui-blanket" ).attr( 'aria-hidden', 'false' ); 
   }

   // return the object with the methods
   return methods;

// End closure
})( AJS.$ || jQuery );
```

### Plugin and Macro Specific Calls

For the macro specific methods, a second java-script file is used.  Having helper methods in javascript is advisable, but not necessary.  The call to the generic methods could be made directly in the i18n values.  However, the quoting of strings gets rather messy which is why the helper methods are being used.  For the example below, it would be advisable to change the following

-   Provide a unique name for the module (rather than myPluginMacroHelp)
-   Provide the correct value for the pluginId and restVersion
-   Add methods for each of the plugin macros  
      

**my-plugin.js**

``` javascript
// The pattern below is a 'module' pattern based upon iife (immediately invoked function expressions) closures.
// see: http://benalman.com/news/2010/11/immediately-invoked-function-expression/ for a nice discussion of the pattern
// The value of this pattern is to help us keep our variables to ourselves.
var myPluginMacroHelp = (function( $ ){
    
   // module variables
   var methods     = new Object();
   var pluginId    = "my-plugin";
   var restVersion = "1.0";

   // module methods
   methods[ 'showMyFirstMacroHelp' ] = function( e ){
      macroHelpDocumentation.getMacroHelp( e, pluginId, restVersion, "my-first-macro-id" );
   }
   methods[ 'showMySecondMacroHelp' ] = function( e ){
      macroHelpDocumentation.getMacroHelp( e, pluginId, restVersion, "my-second-macro-id" );
   }

   // return the object with the methods
   return methods;
// end closure
})( AJS.$ || jQuery )
```

As with the generic methods, calling the functions in the module pattern looks like the following

``` javascript
myPluginMacroHelp.showMyFirstMacroHelp( event );
```

## A Rest Service from which Javascript can Retrieve the Documentation

Two java classes are needed for this service.  One, the RestResponse.java, defines the return data structure and is quite generic.  The only modification needed is to update the namespace for the class.

**RestResponse.java**  Expand source

``` java
package com.mycompany.plugins.confluence.myplugin.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)

public class RestResponse {
    @XmlElement(name = "message-body")
    private String messageBody;
    public RestResponse() {
    }
    public RestResponse(String messageBody) {
        this.messageBody = messageBody;
    }
 
    public String getMessageBody() {
        return messageBody;
    }
 
    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
} 
```

The second class, RestService.java, is a more complex as it does the work.  Within the class a method is needed to provide the documentation for each of the macros. The name of the method is quite irrelevant - the true linkage comes from the @Path attribute.  Within each method, the following need to be customized:

-   The @Path attribute (the portion after "help/" needs to match the string passed into the getMyPluginHelp() javascript method. )
-   The method name to some unique value
-   The filename of the body template
-   The value for the title passed into the velocity template

 

``` javascript
package com.mycompany.plugins.confluence.myplugin.rest;

import java.util.Map;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.setup.settings.SettingsManager;

@Path("/")
public class RestService
{
   private final SettingsManager settingsManager;
   private final VelocityHelperService velocityHelperService;
   public RestService( SettingsManager settingsManager,
                       VelocityHelperService velocityHelperService
   ){
       this.settingsManager            = settingsManager;
       this.velocityHelperService      = velocityHelperService;
   }

   @GET
   @Path("help/my-first-macro")
   @Produces({MediaType.APPLICATION_JSON})
   @Consumes({MediaType.APPLICATION_JSON})
   public Response myFirstMacroHelp( ){
      String title = "My First Macro Help";
      String bodyTemplate = "/templates/my-first-macro-help.vm";

      return getMacroHelp( title, bodyTemplate );
   }

   @GET
   @Path("help/my-second-macro")
   @Produces({MediaType.APPLICATION_JSON})
   @Consumes({MediaType.APPLICATION_JSON})
   public Response mySecondMacroHelp( ){
      String title = "My Second Macro Help";
      String bodyTemplate = "/templates/my-second-macro-help.vm";
 
      return getMacroHelp( title, bodyTemplate );
   }
 
   private Response getMacroHelp( String title, String bodyTemplate ){
      StringBuilder html = new StringBuilder();
      String headerTemplate = "/templates/help-header.vm";
      String footerTemplate = "/templates/help-footer.vm";

      Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
      velocityContext.put( "title", title );
      velocityContext.put( "baseUrl", settingsManager.getGlobalSettings().getBaseUrl() );

      html.append( velocityHelperService.getRenderedTemplate( headerTemplate, velocityContext ) );
      html.append( velocityHelperService.getRenderedTemplate( bodyTemplate,   velocityContext ) );
      html.append( velocityHelperService.getRenderedTemplate( footerTemplate, velocityContext ) );

      return Response.ok( new RestResponse( html.toString() ) ).build();
   }
}
```

## Hooking All of the Pieces Together in the atlassian-plugin-xml file

Before diving in the atlassian-plugin.xml file, there are some modifications to the pom.xml file to include the java classes supporting the REST webservice.  In particular, the block below needs to be added:

``` javascript
       </dependency>
            <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <version>1.0.2</version>
            <scope>provided</scope>
        </dependency>
```

In terms of the atlassian-plugin.xml there are a number of modifications.  First, in the web resources for the plugin there are some dependencies that must be added:

``` javascript
<dependency>com.atlassian.auiplugin:dialog2</dependency>
```

There may also be a need to add some extra context and resource elements.  A full web resource block is provided below:

**plugin resources section**  Expand source

``` xml
   <!-- add our web resources -->
   <web-resource key="my-plugin-resources" name="My Plugin Web Resources">
      <dependency>com.atlassian.auiplugin:ajs</dependency>
      <dependency>com.atlassian.auiplugin:dialog2</dependency>
        
      <resource type="download" name="my-plugin.css"      location="/css/my-plugin.css"/>
      <resource type="download" name="my-plugin.js"       location="/js/my-plugin.js"/>
      <resource type="download" name="macro-help.js"      location="/js/macro-help.js"/>
      <resource type="download" name="images/" location="/images"/>

      <context>my-plugin</context>
      <context>atl.general</context>
      <context>preview</context>
      <context>macro-browser</context>
   </web-resource
```

The REST interface needs to be added...

**Rest Interface**

``` xml
   <rest name="My Plugin Rest Interface" key="my-plugin-rest-resource" path="/my-plugin" version="1.0">
      <description>This is a rest resource to provide macro documentation.</description>
   </rest>
```

## Calling the Documentation from the i18n Value

The macro description is contained in the value for the i18n key "&lt;plugin-id&gt;.&lt;macro-id&gt;.desc" where &lt;plugin-id&gt; and &lt;macro-id&gt; are the unique strings identifying the plugin and macro.  To add the documentation link, append the following to the value for the description:

**Link to the Macro Documentation**

``` xml
<br /><a href="#" onclick="myPluginMacroHelp.showMyFirstMacroHelp( event );" >Documentation</a>
```

## Adding Images to the documentation

Image can be added into the documentation by placing the image file in the "images" folder, then linking to it with the following syntax in the velocity template.  The sytax below assumes:

-   &lt;plugin-id&gt; is the id for your plugin defined in the pom.xml file
-   &lt;plugin-web-resources&gt; is the id for your web resources in the atlassian-plugin.xml file
-   &lt;image-file-name&gt; is the name of the file you placed in the images folder

``` javascript
#set( $images = "$baseUrl/download/resources/<plugin-id>:<plugin-web-resources-id>/images" )
#set( $image = "$images/<image-file-name>" )
<img src="$image" />
```
