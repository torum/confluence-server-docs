---
aliases:
- /server/confluence/wrappers-39368896.html
- /server/confluence/wrappers-39368896.md
category: devguide
confluence_id: 39368896
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368896
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368896
date: '2017-12-08'
legacy_title: Wrappers
platform: server
product: confluence
subcategory: other
title: Wrappers
---
# Wrappers

-   [Create a New Plugin](/server/confluence/create-a-new-plugin.snippet)
-   [Deploy Plugin](/server/confluence/deploy-plugin.snippet)
-   [New Plugin Preparation](/server/confluence/new-plugin-preparation.snippet)

