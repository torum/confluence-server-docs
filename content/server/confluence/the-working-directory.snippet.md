---
aliases:
- /server/confluence/the-working-directory-39368859.html
- /server/confluence/the-working-directory-39368859.md
category: devguide
confluence_id: 39368859
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368859
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368859
date: '2017-12-08'
legacy_title: The Working Directory
platform: server
product: confluence
subcategory: other
title: The Working Directory
---
# The Working Directory

1.  Open a terminal window and perform the following steps:
2.  Create the working directory for plugin development unless it already exists

    {{% note %}}

    While a working directory is not strictly necessary, it's a very good practice for reasons described later

    {{% /note %}}

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Windows</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>mkdir <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Linux/Mac</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ mkdir <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

    

3.  Change into the Directory

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Windows</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>cd <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Linux/Mac </td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ cd <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

