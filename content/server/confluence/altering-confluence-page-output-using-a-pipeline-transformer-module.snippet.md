---
aliases:
- /server/confluence/altering-confluence-page-output-using-a-pipeline-transformer-module-13631575.html
- /server/confluence/altering-confluence-page-output-using-a-pipeline-transformer-module-13631575.md
category: devguide
confluence_id: 13631575
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13631575
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13631575
date: '2017-12-08'
legacy_title: Altering Confluence page output using a Pipeline Transformer module
platform: server
product: confluence
subcategory: other
title: Altering Confluence page output using a Pipeline Transformer module
---
# Altering Confluence page output using a Pipeline Transformer module

{{% note %}}

DRAFT under construction

This tutorial is a DRAFT and is under construction.

{{% /note %}}

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial was tested against <strong>Confluence</strong> <strong>4.3</strong>, and should work on <strong>Confluence 4.0</strong> and higher.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>This is an advanced tutorial. You should have completed at least one intermediate tutorial before working through this tutorial. See the <a href="https://developer.atlassian.com/display/DOCS/Tutorials">list of tutorials in DAC</a>.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you approximately 1 hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

### Overview of the Tutorial

This tutorial shows you how to build a plugin to dynamically alter page output in Confluence. At the completion of the tutorial, the plugin will allow Confluence administrators to design custom page banners and assign these banners to be displayed on any pages that have a particular label applied. Your completed plugin will consist of the following components:

-   Java classes encapsulating the plugin logic.
-   Resources for display of the plugin UI.
-   A plugin descriptor (XML file) to enable the plugin module in the Atlassian application.

When you are done, all these components will be packaged in a single JAR file.

<img src="/server/confluence/images/configure-content-banner-plugin---confluence-4.jpg" width="500" height="256" />

*The tutorial plugin's completed administration page.*

<img src="/server/confluence/images/upcoming-features-for-new-product-launch---documentation---confluence.jpg" width="500" />

*An example of the banner applied to a page by the completed tutorial plugin.*

### Prerequisite Knowledge

To complete this tutorial, you must already understand the basics of Java development: classes, interfaces, methods, how to use the compiler, and so on. You should also understand:

-   How to create an Atlassian plugin project using the [Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).
-   Knowledge of XML and how to add elements to an XML document.

This tutorial will also cover some aspects of Confluence plugin development that are covered in greater detail by other plugin tutorials. You may also like to read:

-   [Adding a Custom Action to Confluence](/server/confluence/adding-a-custom-action-to-confluence)
-   [Adding Menu Items to Confluence](/server/confluence/adding-menu-items-to-confluence)
-   [Internationalising your plugin](https://developer.atlassian.com/display/DOCS/Internationalising+your+plugin)
-   [Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources)

### Plugin Source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you are done, you can find the complete plugin source code on Atlassian Bitbucket. Bitbucket serves a public Git repository containing the tutorial's code. To clone the repository, issue the following command:

    git clone https://bitbucket.org/atlassian_tutorial/confluence-pipeline-transformer-demo.git

Alternatively, you can download the source using the **get source** option here: <a href="https://bitbucket.org/atlassian_tutorial/confluence-pipeline-transformer-demo" class="uri external-link">https://bitbucket.org/atlassian_tutorial/confluence-pipeline-transformer-demo</a>.

{{% note %}}

About these Instructions

You can use any supported combination of OS and IDE to construct this plugin. These instructions were written using Eclipse Classic Version 3.7.1 on a MacBook Air running Mac OS X. If you are using another combination, you should use the equivalent operations for your specific environment.

{{% /note %}}

## Step 1. Create the Plugin Project

In this step, you'll use the two `atlas-` commands to generate stub code for your plugin and setup the stub code as an Eclipse project. The `atlas-` commands are part of the Atlassian Plugin SDK, and automate much of the work of plugin development for you.

1.  Open a terminal and navigate to your Eclipse workspace directory.
2.  Enter the following command to create a Confluence plugin skeleton:

        atlas-create-confluence-plugin

    When prompted, enter the following information to identify your plugin:

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.confluence.transformer</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>tutorial-confluence-transformer-demo</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.confluence.transformer</code></p></td>
    </tr>
    </tbody>
    </table>

3.  Confirm your entries when prompted.
4.  Change to the `tutorial-confluence-transformer-demo` directory created by the previous step.
5.  Run the command:

        atlas-mvn eclipse:eclipse

6.  Start Eclipse.
7.  Select **File-&gt;Import**.   
    Eclipse starts the **Import** wizard.
8.  Filter for **Existing Projects into Workspace** (or expand the **General** folder tree).
9.  Press **Next** and enter the root directory of your workspace.   
    Your Atlassian plugin folder should appear under **Projects**.
10. Select your plugin and click **Finish**.   
    Eclipse imports your project.

## Step 2. Review and Tweak (Optional) the Generated Stub Code

It is a good idea to familiarize yourself with the stub plugin code. In this section, we'll check a version value and tweak a generated stub class. Open your plugin project in Eclipse and follow along in the next sessions to tweak some

### Add Plugin Metadata to the POM

Add some metadata about your plugin and your company or organization.

1.  Edit the `pom.xml` file in the root folder of your plugin.
2.  Add your company or organization name and your website to the `<organization>`element:

    ``` javascript
    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>
    ```

3.  Update the `<description>`element:

    ``` javascript
    <description>A plugin that allows administrators to add custom banners to pages with a particular label</description>
    ```

4.  Update the <name> element:

    ``` javascript
    <name>Confluence Pipeline Transformer Demo Plugin</name>
    ```

5.  Save the file.

### Verify Your Confluence Version

When you generated the stub files, the Confluence version you chose was added to your `pom.xml` file (Project Object Model definition file). This file is located at the root of your project and declares the project dependencies. Take a moment and examine the Confluence dependency:

1.  Open the `pom.xml` file.
2.  Scroll to the bottom of the file.
3.  Find for the `<properties>` element.  
    This section lists the version of the Confluence version you selected in **Step 1** and also the version of the `atlas-` commands you are running.
4.  Verify that the Confluence version is correct.
5.  Save the `pom.xml` file

### Review the Generated Plugin Descriptor

Your stub code contains a plugin descriptor file `atlassian-plugin.xml`. This is an XML file that identifies the plugin to the host application (HOSTAPP) and defines the required plugin functionality. In your IDE, open the descriptor file which is located in your project under `src/main/resources` and you should see something like this:

``` javascript
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.artifactId}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>
</atlassian-plugin>
```

In the next step, you'll use the plugin module generator (another `atlas-` command) to generate the stub code for modules needed by the plugin.

## Step 3. Implement Storage Functionality

The first step in building the tutorial plugin is to implement a way for the plugin to store information about the configured page banners in the Confluence database. To do this, you will need to create two plugin modules. The first is a [Component Import Module](/server/confluence/component-import-module) to access the [PluginSettingsFactory](https://developer.atlassian.com/static/javadoc/sal/2.1/reference/com/atlassian/sal/api/pluginsettings/PluginSettingsFactory.html) component provided by the [Shared Access Layer](https://developer.atlassian.com/display/DOCS/Shared+Access+Layer) plugin. The second module is a [Component Module](/server/confluence/component-module) in order to write your own code that will use the `PluginSettingsFactory` to save information to the database.

### Add PluginSettingsFactory

To begin with, your plugin must first add a reference to the Shared Access Layer plugin. This allows the Plugin SDK and Eclipse to access the classes provided by this plugin and compile your plugin successfully.

1.  Edit the `pom.xml` file in the root folder of your plugin.
2.  Locate the part of the file that lists the `<dependencies>``</dependencies>` of your plugin
3.  Add the following XML snippet within the dependencies element:

    ``` xml
    <dependency>
      <groupId>com.atlassian.sal</groupId>
      <artifactId>sal-api</artifactId>
      <version>2.7.0</version>
      <scope>provided</scope>
    </dependency>
    ```

4.  Save the `pom.xml` file.

Let's add the Component Import module. You'll add this using the `atlas-create-confluence-plugin-module` command:

1.  Open a command prompt or terminal window and go to the plugin root folder (where the `pom.xml` file is located)
2.  Run `atlas-create-confluence-plugin-module`
3.  Choose the option labeled `Component Import`
4.  Supply the following information as prompted:

    |                           |                                                              |
    |---------------------------|--------------------------------------------------------------|
    | Fully Qualified Interface | `com.atlassian.sal.api.pluginsettings.PluginSettingsFactory` |
    | Module Key                | `pluginSettingsFactory`                                      |
    | Filter                    | (leave blank)                                                |

5.  Confirm your choices

### Add New Component

Next, add the Component module for your own code:

1.  Open the `atlassian-plugin.xml` file in the `src/main/resources` directory of your plugin folder
2.  Add the following XML snippet to the file, located anywhere within the `<atlassian-plugin></atlassian-plugin>` element:

    ``` xml
    <component key="bannerStorage" name="Configured Banner Storage"
               class="com.example.confluence.transformer.storage.BannerStorageImpl">
      <description>Stores configured banner information</description>
      <interface>com.example.confluence.transformer.storage.BannerStorage</interface>
    </component>
    ```

3.  Save the file and close it.

### Implement Component Classes

The `bannerStorage` component will be converted into a singleton Spring bean in Confluence when the plugin is run. The purpose of this component will be to load and save configured information to/from the database and expose this functionality as a service to other parts of the plugin.  This will require a set of four classes to be created:

-   An interface (BannerStorage) defining the operations available on this component
-   A concrete implementation class for the BannerStorage interface (BannerStorageImpl)
-   A data object class (Banner) that represents the information that will be saved to the database
-   An enumeration (BannerType) that provides a strongly-typed way to set the type of banner being saved.

To create these classes, you can copy the files from the Bitbucket source repository listed above, or copy them from the sections below. Note that all these classes should be created in the `/src/main/resources/java/com/example/confluence/transformer/storage` folder of your plugin (create the folder if it does not exist).

**BannerType.java**

``` java
package com.example.confluence.transformer.storage;
/**
 * This enumeration is used to persist the 'kind' of banner being saved to the database.
 * See {@link BannerStorage}
 */
public enum BannerType
{
    /**
     * Indicates that the banner should be created using the {info} macro.
     */
    INFO,
    /**
     * Indicates that the banner should be created using the {warning} macro.
     */
    WARNING,
    /**
     * Indicates that the banner should be created using the {tip} macro.
     */
    TIP,
    /**
     * Indicates that the banner should be created using the {note} macro.
     */
    NOTE
}
```

**Banner.java**

``` java
package com.example.confluence.transformer.storage;
/**
 * Represents an individual Banner that has been configured and saved to the
 * database.
 * See {@link BannerStorage}
 */
public class Banner
{
    private long labelId;
    private BannerType bannerType;
    private String bannerTitle;
    private String bannerText;

    /**
     * A no-args constructor. This is necessary in order for the serialisation
     * of this class to and from the database to function correctly.
     */
    public Banner()
    {
        // No-Op.
    }
    /**
     * A convenience constructor so that the {@Link BannerStorageImpl} class
     * can easily create new, pre-configured instances of {@link Banner}
     * @param labelId The unique ID of the label that this banner applies to.
     * @param type The kind of banner (eg. info/note/tip)
     * @param title The optional title of the banner
     * @param text The body text contained within the banner
     */
    Banner(long labelId, BannerType type, String title, String text)
    {
        this.labelId = labelId;
        this.bannerText= text;
        this.bannerTitle = title;
        this.bannerType = type;
    }
    public void setLabelId(long labelId)
    {
        this.labelId = labelId;
    }
    public long getLabelId()
    {
        return labelId;
    }
    public void setBannerText(String bannerText)
    {
        this.bannerText = bannerText;
    }

    public String getBannerText()
    {
        return bannerText;
    }
    public void setBannerType(BannerType bannerType)
    {
        this.bannerType = bannerType;
    }

    public BannerType getBannerType()
    {
        return bannerType;
    }
    public void setBannerTitle(String bannerTitle)
    {
        this.bannerTitle = bannerTitle;
    }

    public String getBannerTitle()
    {
        return bannerTitle;
    }
}
```

**BannerStorage.java**

``` java
package com.example.confluence.transformer.storage;
import com.atlassian.confluence.labels.Label;
import java.util.Collection;
/**
 * Defines the operations available on the bannerStorage component (see the
 * atlassian-plugin.xml). Responsible for saving and loading instances of
 * {@link Banner} to and from the Confluence database.
 */
public interface BannerStorage
{
    /**
     * Creates a new instance of {@link Banner} to the database, using the
     * provided information. The newly-created {@link Banner} can be
     * subsequently retrieved using the
     * {@link #getBannerForLabel(com.atlassian.confluence.labels.Label)} method.
     * <p/>
     * If a {@link Banner} already exists with the same {@link Label}, the
     * existing {@link Banner} will be overwritten and replaced by this one.
     *
     * @param label The {@link Label} to which this banner should apply.
     * @param type  The {@link BannerType} of the new banner.
     * @param title The optional title for the banner. Specify null or ""
     *              to create a banner with no title.
     * @param text  The body text of the new banner.
     */
    public void addBanner(Label label, BannerType type, String title, String text);
    /**
     * Returns all currently configured instances of {@link Banner}. If there
     * are currently no banners configured, an empty collection will be
     * returned.
     *
     * @return An un-ordered collection of all configured Banner instances.
     */
    public Collection<Banner> getConfiguredBanners();
    /**
     * Deletes the {@link Banner} associated with the specified {@link Label},
     * if one exists. If none exists, this method takes no action.
     *
     * @param label The corresponding {@link Label} object.
     */
    public void removeBanner(Label label);
    /**
     * Retrieves the {@link Banner} associated with the specified {@link Label},
     * if one exists. If none exists, this method will return {@code null}.
     *
     * @param label The corresponding {@link Label} object.
     * @return The {@link Banner} associared with the specified {@link Label},
     *         or {@code null} if no banner exists.
     */
    public Banner getBannerForLabel(Label label);
}
```

**BannerStorageImpl.java**

``` java
 package com.example.confluence.transformer.storage;
import com.atlassian.confluence.labels.Label;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
/**
 * Provides a concrete implementation of the {@link BannerStorage} component
 * interface.
 */
public class BannerStorageImpl implements BannerStorage
{
    private static final Logger log = LoggerFactory.getLogger(BannerStorageImpl.class);
    private static final String BANNER_STORAGE_KEY = "banners";
    private final PluginSettings pluginSettings;
    /**
     * Constructs a new instance of {@link BannerStorageImpl}. This constructor
     * is called by Confluence when the plugin is initialised.
     *
     * @param pluginSettingsFactory The {@link PluginSettingsFactory} component
     *                              provided by the Shared Access Layer ("SAL")
     *                              plugin. Confluence will automatically find
     *                              this component and set it as the
     *                              constructor parameter for your component.
     */
    public BannerStorageImpl(PluginSettingsFactory pluginSettingsFactory)
    {
        // Use the SAL pluginSettingsFactory to create a new instance of
        // the PluginSettings object. We can use this object to save simple
        // Key/Value pairs in the Confluence database. The value objects must
        // be serializable Java objects or primitive types.
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void addBanner(Label label, BannerType bannerType, String bannerTitle, String bannerText)
    {
        log.debug(String.format("Adding new Banner (%s) for Label (%s)", bannerText, label.getDisplayTitle()));
        Map<Long, Banner> banners = getBanners();
        banners.put(label.getId(), new Banner(label.getId(), bannerType, bannerTitle, bannerText));
        saveBanners(banners);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Banner> getConfiguredBanners()
    {
        // Return a collection that cannot be modified - this prevents other code
        // from sneakily doing things that bypass the public Add and Remove
        // methods on the BannerStorage interface.
        return Collections.unmodifiableCollection(getBanners().values());
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void removeBanner(Label label)
    {
        log.debug(String.format("Removing the banner associated with Label %s", label.getDisplayTitle()));
        Map<Long, Banner> banners = getBanners();
        banners.remove(label.getId());
        saveBanners(banners);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Banner getBannerForLabel(Label label)
    {
        return getBanners().get(label.getId());
    }
    /**
     * A private method to load the banners from the database. Because we are
     * storing the banners in a {@link Map} as a single Key/Value pair in the
     * database, we must load the entire map into memory from the database in
     * order to add or remove a single entry, and then save the entire map
     * back to the database again.
     *
     * Note that this approach to database persistence works fine for small
     * sets of data, but would not scale well to thousands or millions of
     * entries.
     */
    @SuppressWarnings("unchecked") // This should never happen.
    private Map<Long, Banner> getBanners()
    {
        Object value = pluginSettings.get(BANNER_STORAGE_KEY);
        if (value == null)
            // Initialise the Map if it doesn't yet exist in the database.
            return Maps.newHashMap();
        return (Map<Long, Banner>)value;
    }
    /**
     * A private method to save the configured banners back to the database.
     */
    private void saveBanners(Map<Long, Banner> banners)
    {
        pluginSettings.put(BANNER_STORAGE_KEY, banners);
    }
}
```

## Step 4: Implement Configuration Screen

Now we have a component for saving and loading banners, but no way to interact with it. The next step is to build a configuration screen so that Confluence administrators can define their own banners. To do this, we will need to add several new modules to the plugin:

-   A [Component Import Module](/server/confluence/component-import-module) for the Confluence labelManager component, so that the plugin can load and validate Confluence labels.
-   An i18n Resource for providing internationalisable text in the User Interface (for more information, see [Internationalising your plugin](https://developer.atlassian.com/display/DOCS/Internationalising+your+plugin))
-   A [Velocity Template](/server/confluence/velocity-template-overview) for generating the HTML for the User Interface
-   An [XWork-WebWork Module](/server/confluence/xwork-webwork-module) to register and configure the User Interface that the plugin will provide.
-   A [Web Item Plugin Module](/server/confluence/web-item-plugin-module) to display a link to the configuration screen in the Confluence Admin Console.

### Add LabelManager

Let's add the Component Import module for the labelManager. You'll add this using the `atlas-create-confluence-plugin-module` command:

1.  Open a command prompt or terminal window and go to the plugin root folder (where the `pom.xml` file is located)
2.  Run `atlas-create-confluence-plugin-module`
3.  Choose the option labeled `Component Import`
4.  Supply the following information as prompted:

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Fully Qualified Interface</td>
    <td><pre><code>com.atlassian.confluence.labels.LabelManager</code></pre></td>
    </tr>
    <tr class="even">
    <td>Module Key</td>
    <td><code>labelManager</code></td>
    </tr>
    <tr class="odd">
    <td>Filter</td>
    <td>(leave blank)</td>
    </tr>
    </tbody>
    </table>

5.  Confirm your choices

### Create Internationalisation Resource

Using an Internationalisation ("i18n") resource allows you to define the text of the user interface in a single location, and provides a framework for replacing the text with different languages when required.  It is a good practice to always use an i18n approach for text that appears on the screen, visible to the user. To create this resource:

1.  Run `atlas-create-confluence-plugin-module`
2.  Choose the option labeled {{Downloadable Plugin Resource}}

3.  Supply the following information as prompted:

    | Resource Name           | tutorial-confluence-transformer-demo-i18n |
    |-------------------------|-------------------------------------------|
    | Resource Type           | i18n                                      |
    | Location                | i18n/plugin                               |
    | Add Resource Parameter? | N                                         |

4.  Choose `N` for **Add Another Plugin Module**.

Now, create a file called `plugin.properties` and save it in the `/src/main/resources/i18n` folder of your plugin (create this folder if it does not exist). This file will contain every textual string that is displayed to users by this plugin. Here is an example containing all the text that will be included in the configuration screen for the plugin, which you should copy:

**plugin.properties**

``` javascript
## Text for the web-item Module
confluence-transformer-plugin.admin.link=Configure Content Banners

## Text used by the Velocity template
confluence-transformer-plugin.admin.title=Configure Content Banner Plugin
confluence-transformer-plugin.admin.description=Use this plugin to add panel macros to the content of a page without actually editing the content of a page. This relies on the Confluence Pipeline Transformer plugin module, which was added in Confluence 4.0.
confluence-transformer-plugin.admin.banners.title=Configured Banners
confluence-transformer-plugin.admin.banners.label=Label
confluence-transformer-plugin.admin.banners.banner=Banner
confluence-transformer-plugin.admin.banners.actions=Actions
confluence-transformer-plugin.admin.banners.actions.delete=Delete
```

### Create Velocity Template

Create a file called `view-config.vm` and save it in the `/src/main/resources/templates/content-banner` folder of your plugin (create this folder if it doesn't exist). Set the content of the file as follows:

**view-config.vm**

``` xml
<html>
<head>
    ## using '$i18n.getText()' looks up the text from your plugin.properties
    ## file created in the previous step. The supplied parameter is the 'key'
    ## of the text string to be included
    <title>$i18n.getText("confluence-transformer-plugin.admin.title")</title>
    ## The 'decorate' meta tag is an instruction to Confluence to render this
    ## velocity template using a specific 'Decorator'. We are using the "admin"
    ## decorator to add in the full Confluence Admin Console header, footer,
    ## sidebar and general styling to this page - automatically.
    <meta name="decorator" content="admin"/>
</head>
<body>
<p>
    $i18n.getText("confluence-transformer-plugin.admin.description")
</p>
<h2>$i18n.getText("confluence-transformer-plugin.admin.banners.title")</h2>
## Setting the class of this table to "aui" (short for 'Atlassian User Interface')
## applies the standard Atlassian cross-product table style to this table. For
## more information, see https://developer.atlassian.com/display/AUI/Tables
<table class="aui">
    <thead>
    <tr>
        <th>$i18n.getText("confluence-transformer-plugin.admin.banners.label")</th>
        <th>$i18n.getText("confluence-transformer-plugin.admin.banners.banner")</th>
    </tr>
    </thead>
    <tbody>
        ## The $action variable is automatically set to refer to the 
        ## XWork/WebWork Action object that rendered this template. We can use 
        ## this reference to the Action to access data that was loaded from the
        ## database by the action - in this case we are doing a loop over all 
        ## the banners stored in the database by this plugin, and listing them 
        ## in the HTML Table.
        #foreach($bannerView in $action.banners)
        <tr>
            ## The $req variable is automatically set to refer to the incoming 
            ## HttpServletRequest for this action. It provides a convenient way 
            ## to access the 'Context Path' being used by the Confluence server. 
            ## (For example, when logging in to Confuence via "http://localhost:1990/confluence"), 
            ## the 'context path' part of the URL is "/confluence". The 
            ## context path is configurable, and can even be omitted completely
            ## by the system administrator. Thus, when writing plugins, it is 
            ## important to ensure that any URLs you generate will work in 
            ## situations where there is a context path, and situations where 
            ## there is no context path.  '$req.contextPath' will automatically
            ## resolve to an empty string when there is no context path, so it 
            ## is safe (and recommended) to use in both circumstances.
            <td><a href="$req.contextPath$bannerView.label.urlPath">$bannerView.label.displayTitle</a></td>
            <td>$bannerView.banner.bannerText</td>
        </tr>
        #end
    </tbody>
</table>
</body>
</html>
```

### Create XWork-WebWork Module

The next step is to declare a new [XWork-WebWork Module](/server/confluence/xwork-webwork-module) in your plugin, which registers your user interface components with Confluence and provides a way for you to configure a mapping between your HTML templates (the velocity file) and the Java code that controls the behaviour of the User Interface (an Action class, which we will create in the next step).

Locate your `atlassian-plugin.xml` file in the `/src/main/resources` directory of your plugin. Paste the following XML snippet into the file, within the `<atlassian-plugin></atlassian-plugin>` element.

``` xml
    <!--
    Defines a set of user interface actions that get mapped to URLs in Confluence.
    The URL for each action is defined by the 'namespace' on the package, and the
    action name. So in this example, where the namespace is "/admin/content-banner"
    and the action name is "view", and the full Confluence URL is
    "http://localhost:1990/confluence", then the full URL to this action would be
    "http://localhost:1990/confluence/admin/content-banner/view.action"
    -->
    <xwork key="plugin-actions" name="Plugin XWork/WebWork Actions">
        <package name="configuration" extends="default" namespace="/admin/content-banner">
            <!--
             Do not forget the default-interceptor-ref - things will start
            breaking in weird ways if you leave this out!
            -->
            <default-interceptor-ref name="defaultStack"/>
            <!--
             A single action is controlled by a single Action class, which we
             will build in the next step.
            -->
            <action name="view" class="com.example.confluence.transformer.actions.ViewConfigurationAction">
                <!--
                 You can map multiple results for a single action. When the
                Action class runs, it is able to examine the incoming request
                parameters, and various other aspects of the application, to
                determine the 'outcome' of the action (for example, did it
                succeed or fail?). However, the action class itself does not
                directly control what the user sees as a result of that outcome.
                Instead, that is controlled by this XML snippet - where we can
                map the outcomes from the action to different responses.
                In this example, we are setting an outcome of "Render a velocity
                template" when the result of the action is "success".
                -->
                <result name="success" type="velocity">/templates/content-banner/view-config.vm</result>
            </action>
        </package>
    </xwork>
```

### Create Action Class

The final piece in the XWork-WebWork puzzle is to create a new Action class to control the action and implement its logic. Create a new file called `ViewConfigurationAction.java` in the `/src/main/java/com/example/confluence/transformer/actions` folder of your plugin (create this folder if it doesn't exist)

 

 

 

For this tutorial, you will need a "Custom Field" plugin module. You'll add this the `atlas-create-HOSTAPP-plugin-module` command.

1.  Open a command window and go to the plugin root folder (where the `pom.xml` is located).
2.  Run `atlas-create-HOSTAPP-plugin-module`.
3.  Choose the option labeled `Custom Field`.
4.  Supply the following information as prompted:

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>Enter New Classname</p></td>
    <td><p><code>XXXX</code> (This is the <code>XXXXX</code> class.)</p></td>
    </tr>
    <tr class="even">
    <td><p>Package Name</p></td>
    <td><p><code>com.example.plugins.tutorial.HOSTAPP.XXXX</code></p></td>
    </tr>
    </tbody>
    </table>

5.  Choose `N` for **Show Advanced Setup**.
6.  Choose `N` for **Add Another Plugin Module**.
7.  Confirm your choices.
8.  Return to Eclipse and **Refresh** your project.
9.  Review the components added by the plugin module generator.

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p><code>atlassian-plugin.xml</code></p></td>
    <td><p>The <code>xxxx-xxx</code> module was added.</p></td>
    </tr>
    <tr class="even">
    <td><p><code>xxxxxxx</code></p></td>
    <td><p>....</p></td>
    </tr>
    </tbody>
    </table>

## Step 4. Update Your Project and Refresh Your IDE

If you change your Atlassian project, Eclipse is not automatically aware of the changes. Moreover, sometimes your project dependencies require an update. We need to fix that.

1.  Switch to a terminal window.
2.  Change directory to the project root.   
    This is the directory with the `pom.xml` file.
3.  Update the on-disk project metadata with the new POM information.

    ``` javascript
    atlas-mvn eclipse:eclipse
    ```

4.  Back in Eclipse, refresh the plugin project to pickup the changes.

Remember to do this update and refresh step each time you edit your `pom.xml` or otherwise modify your plugin source with an Atlassian command.

## Step 5. Write the Plugin Code

You have already generated the stubs for your plugin modules. Now , you will write some code that will make your plugin do something. Recall that this plugin PLUGIN PURPOSE. To do this, you will implement the `XXXXX` interface, and OTHER TASK.

### Subtask

Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

``` javascript
class Bicycle {

    int cadence = 0;
    int speed = 0;
    int gear = 1;

    void changeCadence(int newValue) {
         cadence = newValue;
    }

    void changeGear(int newValue) {
         gear = newValue;
    }

    void speedUp(int increment) {
         speed = speed + increment;   
    }

    void applyBrakes(int decrement) {
         speed = speed - decrement;
    }

    void printStates() {
         System.out.println("cadence:" +
             cadence + " speed:" + 
             speed + " gear:" + gear);
    }
}
```

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.

### Subtask

Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.

``` javascript
class Bicycle {

    int cadence = 0;
    int speed = 0;
    int gear = 1;

    void changeCadence(int newValue) {
         cadence = newValue;
    }

    void changeGear(int newValue) {
         gear = newValue;
    }

    void speedUp(int increment) {
         speed = speed + increment;   
    }

    void applyBrakes(int decrement) {
         speed = speed - decrement;
    }

    void printStates() {
         System.out.println("cadence:" +
             cadence + " speed:" + 
             speed + " gear:" + gear);
    }
}
```

Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.

## Step 6. Build, Install and Run the Plugin

Follow these steps to build and install your plugin, so that you can test your code. \# Make sure you have saved all your code changes to this point.

1.  Open a terminal window and navigate to the plugin root folder (where the `pom.xml` file is).
2.  Run the following command:

        atlas-run

    This command builds your plugin code, starts a HOSTAPP instance, and installs your plugin in it. This may take several seconds or so, when the process completes you see many status lines on your screen concluding with something like the following lines:

        [INFO] HOSTAPP started successfully in 71s at http://localhost:XXXX/HOSTAPP
        [INFO] Type CTRL-D to shutdown gracefully
        [INFO] Type CTRL-C to exit

3.  Open your browser and navigate to the local HOSTAPP instance started by `atlas-run`.  
    If you followed the instructions, enter <a href="http://localhost:2990/jira" class="uri external-link">http://localhost:2990/jira</a> in your browser.
4.  At the HOSTAPP login, enter a username of `admin` and a password of `admin`.
5.  Click the Administration link in the dashboards's right hand corner.  
    The browser opens the **Administration** page.
6.  As needed add MORE STEPS to allow the user to verify the tutorial was completed successfully.

## Step 9. Iterate and retest your code

**Extra Credit**

Keep whichever Fastdev section is appropriate for your tutorial.

From this point onwards, you can use FastDev to reinstall your plugin behind the scenes as you work.

To trigger the reinstallation of your servlet plugin:

1.  Make the changes to your servlet module.
2.  Go to your browser and navigate to your servlet:  
    `http://localhost:XXXX/HOSTAPP/plugins/servlet/MY-SERVLET-NAME`.
3.  Do a hard refresh of the page:
    -   Shift+Reload in most browsers.
    -   Ctrl+Reload on Windows or in Internet Explorer.
    -   In Safari 5, you will need to hold down the Shift key while clicking the Reload icon in the Location bar.
4.  Go back to step 1.

Use the FastDev servlet to trigger the reload:

1.  Make the changes to your plugin module.
2.  Go to your browser and navigate to the FastDev servlet:  
    `http://localhost:XXXX/HOSTAPP/plugins/servlet/fastdev`.
3.  Do a hard refresh of the page:
    -   Shift+Reload in most browsers.
    -   Ctrl+Reload on Windows or in Internet Explorer.
    -   In Safari 5, you will need to hold down the Shift key while clicking the Reload icon in the Location bar.
4.  Go back to step 1.

The full instructions are in the [SDK guide](https://developer.atlassian.com/display/DOCS/Reloading+a+Plugin+after+Changes).

## Step 10. Expand the Plugin by Adding XXXXX

Now you have a basic plugin that can XXXX. You can expand its functionality so that it can XXXXXXX

**Doing more**

If the plugin needs additional modules, repeat steps 5 & 6 as often as you need them. You can structure this as you like. There is no need to repeat all the detail about logging in or testing each time. You have already provided detail on how to do that at this point.

This is an optional step. Remove it if you don't need it.

## Step 11. Write Unit and Integration Tests

**Testing**

It is a good idea to provide code samples and narrative for unit and integration tests. This is an optional step. Remove it if you don't need it.

{{% tip %}}

Congratulations, that's it

Have a chocolate!

{{% /tip %}}
