---
aliases:
- /server/confluence/2031794.html
- /server/confluence/2031794.md
category: devguide
confluence_id: 2031794
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031794
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031794
date: '2017-12-08'
legacy_title: What class should my XWork action plugin extend?
platform: server
product: confluence
subcategory: faq
title: What class should my XWork action plugin extend?
---
# What class should my XWork action plugin extend?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> What class should my XWork action plugin extend?

WebWork actions must implement `com.opensymphony.xwork.Action`. However, we recommend you make your action extend <a href="http://www.atlassian.com/software/confluence/docs/api/latest/com/atlassian/confluence/core/ConfluenceActionSupport.html" class="external-link">ConfluenceActionSupport</a>, which provides a number of helper methods and components that are useful when writing an Action that works within Confluence.

Other action base-classes can be found within Confluence, but we recommend you don't use them - the hierarchy of action classes in Confluence is over-complicated, and likely to be simplified in the future in a way that will break your plugins.
