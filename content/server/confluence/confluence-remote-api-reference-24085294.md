---
title: Confluence Remote API Reference 24085294
aliases:
    - /server/confluence/confluence-remote-api-reference-24085294.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24085294
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24085294
confluence_id: 24085294
platform:
product:
category:
subcategory:
---
# Confluence Server Development : Confluence Remote API Reference

{{% note %}}

Redirection Notice

This page will redirect to <https://developer.atlassian.com/x/UwAf> in about 2 seconds.

{{% /note %}}
