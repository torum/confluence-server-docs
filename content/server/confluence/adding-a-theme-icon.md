---
aliases:
- /server/confluence/adding-a-theme-icon-2031802.html
- /server/confluence/adding-a-theme-icon-2031802.md
category: reference
confluence_id: 2031802
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031802
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031802
date: '2017-12-08'
legacy_title: Adding a Theme Icon
platform: server
product: confluence
subcategory: modules
title: Adding a Theme icon
---
# Adding a Theme icon

You can package a theme icon with a theme to give the user a preview of how the theme will change the layout of Confluence. If you do not specify a custom icon for your theme, a default icon will be shown in the preview.

### Defining the theme icon in the atlassian-plugin.xml

To include an icon in the theme, you will need to reference it as a [Downloadable Plugin Resource](/server/confluence/adding-plugin-and-module-resources) from within the <a href="#theme-module" class="unresolved">theme module</a>.

Here is an example where an icon called my-theme-icon.gif is used in the Dinosaur Theme:

``` xml
<theme key="dinosaurs" name="Dinosaur Theme" class="com.atlassian.confluence.themes.BasicTheme">
    <description>A nice theme for the kids</description>
    <colour-scheme key="com.example.themes.dinosaur:earth-colours"/>
    <layout key="com.example.themes.dinosaur:main"/>
    <layout key="com.example.themes.dinosaur:mail-template"/>
    <resource name="themeicon.gif" type="download" location="com/example/themes/dinosaur/my-theme-icon.gif">
        <property key="content-type" value="image/gif"/>
    </resource>
</theme>
```

The resource parameter takes three arguments:

-   **Name**: The name of the icon 

    {{% note %}}

(has to be **themeicon.gif**)

    {{% /note %}}

-   **Type**: The type of resource-in this instance, 'download'.
-   **Location**: The location of the file represented in the jar archive you will use to bundle your theme.

The icon will automatically appear on the themes screen in the space and global administration and will be displayed next to the text and description of the theme.

![](/server/confluence/images/theme-preview.png)

### Creating your own theme icon

In order to keep the look and feel of the icons consistent, we recommend that you base the icon style on icons shipped with the Confluence themes. A good starting point when creating new icons is to use the default theme icon:  
![](/server/confluence/images/main-theme.gif)
