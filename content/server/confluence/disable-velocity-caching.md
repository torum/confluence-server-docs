---
aliases:
- /server/confluence/disable-velocity-caching-2031808.html
- /server/confluence/disable-velocity-caching-2031808.md
category: devguide
confluence_id: 2031808
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031808
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031808
date: '2018-04-19'
legacy_title: Disable Velocity Caching
platform: server
product: confluence
subcategory: faq
title: Disable Velocity caching
---
# Disable Velocity caching

When you develop plugins for Confluence, it is often useful to disable the caching of the Velocity templates
so that you don't have to restart the server to see Velocity changes.

Use the following steps to disable Velocity template caching in Confluence:

1.  Shut down your Confluence server.
1.  In your Confluence installation, open the `velocity.properties` file from `confluence/WEB-INF/classes/` for editing.
1.  Make the following changes to the `velocity.properties` file:
    1.   On all the lines that end with `...resource.loader.cache`, set the values to `false`.
    1.   Set the `class.resource.loader.cache` to `false`. (If this entry does not exist, you can skip this step.)
    1.   Set `velocimacro.library.autoreload` to `true`. (Uncomment the line if necessary.)
1.  Save the updated `velocity.properties` in `confluence/WEB-INF/classes/`. This file takes precedence over the
one found in the Confluence JAR file.
1.  Start your Confluence server again.

Note that the Velocity macro libraries (`macros.vm`, `menu\_macros.vm`) are only loaded once, when Velocity starts.
Therefore, regardless of the caching settings mentioned, any changes to these files in Confluence require restarting
the application to take effect.
