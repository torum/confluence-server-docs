---
aliases:
- /server/confluence/2031789.html
- /server/confluence/2031789.md
category: devguide
confluence_id: 2031789
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031789
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031789
date: '2017-12-08'
legacy_title: How do I get hold of the HttpServletRequest?
platform: server
product: confluence
subcategory: faq
title: How do I get hold of the HttpServletRequest?
---
# How do I get hold of the HttpServletRequest?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I get hold of the HttpServletRequest?

``` java
HttpServletRequest request = ServletActionContext.getRequest();
if (request != null)
{
     // do something here
}
```

You should *always* assume that ServletActionContext.getRequest() will return null. ServletActionContext is only populated if the request comes in through WebWork. There are a number of circumstances in which it will not be populated, either because a web request has come in through some other path, or because there was no web request in the first place:

-   AJAX requests that come in via the DWR servlet
-   SOAP/XML-RPC requests
-   Scheduled tasks, including the sending of email notifications

Treat `ServletActionContext` as a bonus. If it's populated you can do neat things with it, but don't rely on it.
