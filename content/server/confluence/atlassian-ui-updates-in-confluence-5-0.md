---
aliases:
- /server/confluence/atlassian-ui-updates-in-confluence-5.0-15335887.html
- /server/confluence/atlassian-ui-updates-in-confluence-5.0-15335887.md
category: devguide
confluence_id: 15335887
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=15335887
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=15335887
date: '2017-12-08'
legacy_title: Atlassian UI Updates in Confluence 5.0
platform: server
product: confluence
subcategory: updates
title: Atlassian UI updates in Confluence 5.0
---
# Atlassian UI updates in Confluence 5.0

Confluence 5.0 will include a major upgrade of [AUI](https://developer.atlassian.com/display/AUI) (the Atlassian User Interface) to AUI 5.0 to bring Confluence in line with the [new Atlassian Design Guidelines](https://developer.atlassian.com/design/). This will also update the version of jQuery that Confluence supports.

There will be visual changes to the appearance of Atlassian UI. Your plugin will look different if it uses AUI. If it does not yet use AUI, your plugin will look out of place in Confluence 5.0. We recommend that you follow the new [Atlassian Design Guidelines](https://developer.atlassian.com/design/).

We will be incrementally upgrading AUI in each of our 5.0 <a href="http://www.atlassian.com/software/confluence/download-eap" class="external-link">EAP releases</a>, starting with Confluence 5.0-m3 available in early November 2012.

### Sections updated

The table below shows which screens have been updated thus far. Our goal is to minimise markup changes within each screen. Any markup changes are highlighted in the table.

<table>
<colgroup>
<col style="width: 10%" />
<col style="width: 15%" />
<col style="width: 40%" />
<col style="width: 35%" />
</colgroup>
<thead>
<tr class="header">
<th>Screen Updated</th>
<th><div class="tablesorter-header-inner">
Updated In
</div></th>
<th>Markup Changes</th>
<th>Impact</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Login</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m3+EAP+Release+Notes" class="external-link">M3</a></td>
<td>Login and signup split into two separate views.</td>
<td>Low. May have some impact on theme developers looking to style the login page.</td>
</tr>
<tr class="even">
<td>Login</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m3+EAP+Release+Notes" class="external-link">M3</a></td>
<td>Buttons for login and sign up updated to use AUI.</td>
<td>Medium. Theme developers will need to override AUI button styles.</td>
</tr>
<tr class="odd">
<td>Login</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m3+EAP+Release+Notes" class="external-link">M3</a></td>
<td>New sitemesh decorator for login page.</td>
<td>Low. May have some impact on theme developers looking to style the login page.</td>
</tr>
<tr class="even">
<td>Login</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m3+EAP+Release+Notes" class="external-link">M3</a></td>
<td>New markup added for login / signup switch.</td>
<td>Low. New markup may need to be updated to use a different style.</td>
</tr>
<tr class="odd">
<td>Dashboard</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m3+EAP+Release+Notes" class="external-link">M3</a></td>
<td>None in m3.</td>
<td> </td>
</tr>
<tr class="even">
<td>View page and blog</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m3+EAP+Release+Notes" class="external-link">M3</a></td>
<td><p>No markup changes in m3, some elements of the page have been moved with CSS rather than the more conventional route of changing markup. This may change in future milestones.</p></td>
<td> </td>
</tr>
<tr class="odd">
<td>Editor</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m3+EAP+Release+Notes" class="external-link">M3</a></td>
<td>None in m3.</td>
<td> </td>
</tr>
<tr class="even">
<td>Space Admin</td>
<td><a href="https://confluence.atlassian.com/display/DOC/Confluence+5.0-m3+EAP+Release+Notes" class="external-link">M3</a></td>
<td><p>Added &quot;aui&quot; class to forms/tables. Buttons and input elements updated to use AUI. Some wrapper divs or and extra classes added.</p></td>
<td> </td>
</tr>
</tbody>
</table>

### Related pages

[Preparing for Confluence 5.0](/server/confluence/preparing-for-confluence-5-0)
