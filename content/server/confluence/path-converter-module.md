---
aliases:
- /server/confluence/path-converter-module-2031821.html
- /server/confluence/path-converter-module-2031821.md
category: reference
confluence_id: 2031821
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031821
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031821
date: '2017-12-08'
legacy_title: Path Converter Module
platform: server
product: confluence
subcategory: modules
title: Path Converter module
---
# Path Converter module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.8 and later</p></td>
</tr>
</tbody>
</table>

Path Converter plugin modules are useful for developers who are writing Confluence plugins. The Path Converter modules allow you to install custom path mapping as a part of your plugin.

-   For more information about plugins in general, read the [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins and macros, read <a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins) .

## The Path Converter Plugin Module

Each path converter can be used to map a friendly URL to an action either within Confluence or provided by your plugin. Here is an example `atlassian-plugin.xml` file containing a single path converter:

``` xml
<atlassian-plugin name="Hello World Converter" key="confluence.extra.simpleconverter">
<plugin-info>
<description>Example Path Converter</description>
<vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
<version>1.0</version>
</plugin-info>

<path-converter weight="10" key="example-converter" class="plugin.ExamplePathConverter"/>

</atlassian-plugin>
```

-   The **class** attribute specifies a class that implements `com.atlassian.confluence.servlet.simpledisplay.PathConverter`.
-   The **weight** element defines when the path converter is executed in relation to the other converters. See [DOC:Notes below](#notes).
-   The **key** is the normal plugin module identifier.

## Example

This converter is used by Confluence to convert the friendly `/display/SpaceKey/PageTitle/` URL format into a request against the correct WebWork action.

``` java
public class PagePathConverter implements PathConverter
{
    private static final String DISPLAY_PAGE_PATH = "/pages/viewpage.action?spaceKey=${spaceKey}&title=${title}";

    public boolean handles(String simplePath)
    {
        return new StringTokenizer(simplePath, "/").countTokens() == 2;
    }

    public ConvertedPath getPath(String simplePath)
    {
        StringTokenizer st = new StringTokenizer(simplePath, "/");
        String spaceKey = st.nextToken();
        String pageTitle = st.nextToken();

        ConvertedPath path = new ConvertedPath(DISPLAY_PAGE_PATH);
        path.addParameter("spaceKey",spaceKey);
        path.addParameter("title",pageTitle);
        return path;
    }
}
```

The **handles** method is called (in order of weight) on each converter and the first to return `true` will have its **getPath** method called.  
The **getPath** method will convert the friendly path into a **ConvertedPath** object.

The **ConvertedPath** object expects a URL template and a series of parameters. It uses Velocity internally to merge the template and the parameters, producing a final URL.

## Notes

The `com.atlassian.confluence.servlet.simpledisplay.SimpleDisplayServlet` will pass each incoming request that starts with '/display' to each of its configured converters. The converters will be checked starting with the converter with the lowest weight.

The PathConverters are autowired at registration time, so add a setter method on your converter to get access to Confluence's components.

``` java
public class MyPathConverter implements PathConverter
    private PageManager pageManager;

    // other methods    

    public void setPageManager(PageManager pageManager) 
    {
        this.pageManager = pageManager;
    }
}
```

## Core Converters

Confluence includes a series of core path converters that are used to provide friendly URLs for standard Confluence resources.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Weight</p></th>
<th><p>Core PathConverter</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>10</p></td>
<td><p>com.atlassian.confluence.servlet.simpledisplay.PagePathConverter</p></td>
</tr>
<tr class="even">
<td><p>20</p></td>
<td><p>com.atlassian.confluence.servlet.simpledisplay.MailPathConverter</p></td>
</tr>
<tr class="odd">
<td><p>30</p></td>
<td><p>com.atlassian.confluence.servlet.simpledisplay.BlogPathConverter</p></td>
</tr>
<tr class="even">
<td><p>40</p></td>
<td><p>com.atlassian.confluence.servlet.simpledisplay.UserPathConverter</p></td>
</tr>
<tr class="odd">
<td><p>50</p></td>
<td><p>com.atlassian.confluence.servlet.simpledisplay.SpacePathConverter</p></td>
</tr>
</tbody>
</table>

You can use any weight for your converter. If the same weight is used by multiple converters, they are executed in order of registration.
