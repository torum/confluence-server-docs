---
aliases:
- /server/confluence/39368882.html
- /server/confluence/39368882.md
category: devguide
confluence_id: 39368882
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368882
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368882
date: '2017-12-08'
legacy_title: Launch the Command Line Interface (CLI)
platform: server
product: confluence
subcategory: other
title: Launch the Command Line Interface (CLI)
---
# Launch the Command Line Interface (CLI)

1.  Open a terminal window  
    {{% note %}}
    The CLI needs to be a different terminal window from the one running Confluence, but they can both launch from the same directory.

    {{% /note %}}
2.  Launch the cli

    ``` javascript
    $ atlas-cli
    ```

    As with starting confluence, the cli can take a while to load; however, it will not take as long as starting Confluence.  The CLI is ready when the terminal shows:

    ``` javascript
    [INFO] Waiting for commands...
    maven> 
    ```

    {{% note %}}

    The pom.xml file will be read and used when the CLI launches. Any changes to the pom.xml (such as updating the plugin version number) will not take effect until the CLI is restarted.

    {{% /note %}}
