---
aliases:
- /server/confluence/create-and-prune-the-plugin-skeleton-33723236.html
- /server/confluence/create-and-prune-the-plugin-skeleton-33723236.md
category: devguide
confluence_id: 33723236
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33723236
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33723236
date: '2017-12-08'
legacy_title: Create and prune the plugin skeleton
platform: server
product: confluence
subcategory: other
title: Create and prune the plugin skeleton
---
# Create and prune the plugin skeleton

The Atlassian Software Development Kit (SDK) provides some commands that start wtih "atlas".  In this step, you will use the "atlas-create-confluence-plugin" to create a set of directories and files which form the skeleton of a Confluence plugin. You will be removing some of the automatically generated files as they are not needed for this tutorial

1.  Open a terminal and navigate to a directory where you would like to put the plugin files.  If you plan to use Eclipse, it is helpful to navigate to your Eclipse workspace directory.  
      
2.  Enter the following command to create a Confluence plugin skeleton:

        $ atlas-create-confluence-plugin

3.  When prompted, you will need to enter the following information to identify your plugin:  
    **group-id: ** This is a dot separated string which identifies the namespace the plugin belongs to.  
    **artifact-id: **  This is a string to represent the plugin as a whole which may contain multiple modules such as macros, event listeners, and web resources.  
    **version: **A string to represent the version of the plugin.  It's normally fine to accept the default.  
    **package:** A namespace for the java classes used by the plugin.  It's normally fine to accept the default.  
      
    An example of these values is given below for the "placebo" plugin in the "com.exampleco.plugins.confluence" namespace.

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p> com.exampleco.plugins.confluence</p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p>placebo</p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.exampleco.plugins.confluence</code></p></td>
    </tr>
    </tbody>
    </table>

4.  Confirm your entries when prompted with `Y` or `y`.

    Your terminal notifies you of a successful build:

        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESSFUL
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time: 1 minute 11 seconds
        [INFO] Finished at: Thu Jul 18 11:30:23 PDT 2013
        [INFO] Final Memory: 82M/217M
        [INFO] ------------------------------------------------------------------------

5.  Change to the project directory created by the previous step.

        $ cd theme-tutorial/

6.  Delete the test directories.

    Setting up testing for your plugin isn't part of this tutorial. Use the following commands to delete the generated test skeleton:

        $ rm -rf ./src/test/java
        $ rm -rf ./src/test/resources/

7.  Delete the unneeded Java class files.

        $ rm ./src/main/java/com/example/plugins/tutorial/confluence/*.java

8.  Edit the `src/main/resources/atlassian-plugin.xml` file.  
      
9.  Remove the generated `myPluginComponent` component declaration.

    ``` xml
    <!-- publish our component -->
    <component key="myPluginComponent" class="com.example.plugins.tutorial.confluence.simplebp.MyPluginComponentImpl" public="true">
       <interface>com.example.plugins.tutorial.confluence.simplebp.MyPluginComponent</interface>
    </component>
    ```

10. Save and close `atlassian-plugin.xml`.

### Import your project into your IDE

 

1.   Make your project available to Eclipse.

        atlas-mvn eclipse:eclipse

    You'll see a successful build message: 

        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESSFUL
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time: 54 seconds
        [INFO] Finished at: Tue Jul 16 11:03:59 PDT 2013
        [INFO] Final Memory: 82M/224M
        [INFO] ------------------------------------------------------------------------

2.  Start Eclipse.  
    You can open a new terminal window to perform this action.

        $ cd ~/eclipse
        $ ./eclipse

3.  Click **File &gt; Import**.   
    Eclipse starts the **Import** wizard.  
      
4.  Expand the **General** folder tree to choose **Existing Projects into Workspace**.  
      
5.  Click **Next. **  
    **  
    **
6.  Click **Browse **and enter the root directory of your workspace.  
    Your Atlassian plugin folder should now appear under **Projects.**  
    **  
    **
7.  Ensure your plugin checkbox is ticked and click **Finish**.   
    Eclipse imports your project, and it's now visible in your Package Explorer view.
