---
aliases:
- /server/confluence/xwork-webwork-module-2031815.html
- /server/confluence/xwork-webwork-module-2031815.md
category: reference
confluence_id: 2031815
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031815
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031815
date: '2017-12-08'
legacy_title: XWork-WebWork Module
platform: server
product: confluence
subcategory: modules
title: XWork-WebWork module
---
# XWork-WebWork module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.4 and later</p></td>
</tr>
</tbody>
</table>

XWork plugin modules enable you to deploy XWork / WebWork (now part of <a href="http://struts.apache.org/" class="external-link">Apache Struts</a>) actions and views as a part of your plugins.

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read <a href="https://confluence.atlassian.com/x/AwNTE" class="external-link">Installing Add-ons</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins).

## The XWork Plugin Module

Each XWork module is deployed as a plugin module of type **xwork** and contains one or more XWork `package` elements.

Here is an example `atlassian-plugin.xml` file containing a single XWork module:

``` xml
<atlassian-plugin name='List Search Macros' key='confluence.extra.livesearch'>
    ...

    <xwork name="livesearchaction" key="livesearchaction">
        <package name="livesearch" extends="default" namespace="/plugins/livesearch">
            <default-interceptor-ref name="defaultStack" />

            <action name="livesearch"
                class="com.atlassian.confluence.extra.livesearch.LiveSearchAction">
                <result name="success" type="velocity">/templates/extra/livesearch/livesearchaction.vm</result>
            </action>
        </package>
    </xwork>
</atlassian-plugin>
```

-   The **xwork** element has **no** class attribute.
-   You can specify multiple **package** elements within the **xwork** element. These are standard XWork package elements, just as you would specify in xwork.xml.
-   Ensure the `<result>` entry appears on a single line with no spaces.

## Writing an Action

For information on how to write a WebWork action, please consult the <a href="http://struts.apache.org/docs/home.html" class="external-link">WebWork documentation</a>.

WebWork actions must implement `com.opensymphony.xwork.Action`. However, we recommend you make your action extend <a href="http://docs.atlassian.com/com/atlassian/confluence/atlassian-confluence/latest/com/atlassian/confluence/core/ConfluenceActionSupport.html" class="external-link">ConfluenceActionSupport</a>, which provides a number of helper methods and components that are useful when writing an Action that works within Confluence.

Other action base-classes can be found within Confluence, but we recommend you don't use them - the hierarchy of action classes in Confluence is over-complicated, and likely to be simplified in the future in a way that will break your plugins.

## Accessing Your Actions

Actions are added to the XWork core configuration within Confluence, which means they are accessed like any other action!

For example, given the above `atlassian-plugin.xml`, the `livesearch` action would be accessed at <a href="http://yourserver/confluence/plugins/livesearch/livesearch.action" class="uri external-link">yourserver/confluence/plugins/livesearch/livesearch.action</a>.

## Creating a Velocity Template for Output

Your Velocity template must be specified with a leading slash (/) in the plugin XML configuration, and the path must correspond to the path inside the plugin JAR file.

In the above example, the Velocity template must be found in `/templates/extra/livesearch/livesearchaction.vm` inside the plugin JAR file. In the example plugin's source code, this would mean the Velocity template should be created in `src/main/resources/template/extra/livesearch/`.

Inside your Velocity template, you can use `$action` to refer to your action, and many other variables to access Confluence functionality. See [Confluence Objects Accessible From Velocity](/server/confluence/confluence-objects-accessible-from-velocity) for more information.

## Notes

Some issues to be aware of when developing or configuring an XWork plugin:

-   Your packages should *always* extend the `default` Confluence package. It is useful to be aware of what this provides to you in the way of interceptors and result types. Extending any other package will modify that package's configuration across the entire application, which is not supported or desirable.
-   You can give your packages any namespace you like, but we recommend using `/plugins/unique/value` - that is prefixing plugin packages with `/plugins` and then adding a string globally unique to your plugin. The only name you *can't* use is `servlet` as the `/plugins/servlet` URL pattern is reserved for [Servlet Module](/server/confluence/servlet-module).
-   Views must be bundled in the JAR file in order to be used by your actions. This almost always means using Velocity views.
-   It is useful to be aware of the actions and features already bundled with Confluence, for example your actions will all be auto-wired by Spring (see [Accessing Confluence Components from Plugin Modules](/server/confluence/accessing-confluence-components-from-plugin-modules)) and your actions can use useful interfaces like PageAware and SpaceAware to reduce the amount of work they need to do.
-   Currently only WebWork Modules are protected by the temporary secure administrator sessions. Other plugin types, such as [REST services](https://developer.atlassian.com/display/CONFDEV/REST+Module) or [servlets](https://developer.atlassian.com/display/CONFDEV/Servlet+Module) are not checked for an administrator session.

    All [webwork modules](https://developer.atlassian.com/display/CONFDEV/XWork-WebWork+Module) mounted under `/admin` will automatically be protected by secure administrator sessions. To opt out of this protection you can mark your class or webwork action method with the <a href="http://docs.atlassian.com/confluence/latest/com/atlassian/confluence/security/websudo/WebSudoNotRequired.html" class="external-link">WebSudoNotRequired</a> annotation. Conversely, all webwork actions mounted outside the `/admin` namespace are not protected and can be opted in by adding the <a href="http://docs.atlassian.com/confluence/latest/com/atlassian/confluence/security/websudo/WebSudoRequired.html" class="external-link">WebSudoRequired</a> annotation.

    Both of these annotations work on the class or the action method. If you mark a method with the annotation, only action invocations invoking that method will be affected by the annotations. If you annotate the class, any invocation to that class will be affected. Sub-classes inherit these annotations.

## Important Security Note

If you are writing an XWork plugin, it is *very important that you read* this security information: [XWork Plugin Complex Parameters and Security](/server/confluence/xwork-plugin-complex-parameters-and-security).

## See also

-   [Accessing Confluence Components from Plugin Modules](/server/confluence/accessing-confluence-components-from-plugin-modules)
-   [Confluence Objects Accessible From Velocity](/server/confluence/confluence-objects-accessible-from-velocity)
-   [Velocity Context Module](/server/confluence/velocity-context-module)
-   [XWork Plugin Complex Parameters and Security](/server/confluence/xwork-plugin-complex-parameters-and-security)
-   [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

