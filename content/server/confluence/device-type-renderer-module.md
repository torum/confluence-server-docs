---
aliases:
- /server/confluence/device-type-renderer-module-13633181.html
- /server/confluence/device-type-renderer-module-13633181.md
category: reference
confluence_id: 13633181
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13633181
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13633181
date: '2017-12-08'
legacy_title: Device Type Renderer Module
platform: server
product: confluence
subcategory: modules
title: Device Type Renderer module
---
# Device Type Renderer module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 4.3.3 and later</p></td>
</tr>
</tbody>
</table>

This plugin module enable you to register a new device-type specific renderer to be used when rendering Confluence content for a specific device type (where device type is known at render time by <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/content/render/xhtml/ConversionContext.html#getOutputDeviceType()" class="external-link">ConversionContext.html#getOutputDeviceType()</a>. The Renderer you are registering must already exist as a named bean in your plugins Spring configuration.

## Configuration

The root element for the Component plugin module is `device-type-renderer`. It allows the following attributes and child elements for configuration:

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner" style="margin-left: 0.0px;">
<p>Name*</p>
</div></th>
<th><div class="tablesorter-header-inner" style="margin-left: 0.0px;">
<p>Description</p>
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>The identifier of the plugin module. This key must be unique within the plugin where it is defined. The key must match the id of a Spring configured bean present in your plugins Spring context. The bean must implement the <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/content/render/xhtml/Renderer.html" class="external-link">Renderer</a> interface.</p>
<p><strong>Default:</strong> N/A</p></td>
</tr>
<tr class="even">
<td><p>i18n-name-key</p></td>
<td>The localisation key for the human-readable name of the plugin module.</td>
</tr>
<tr class="odd">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module. I.e. the human-readable name of the component.</p>
<p><strong>Default:</strong> The plugin key.</p></td>
</tr>
</tbody>
</table>

**\*key is required**

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner" style="margin-left: 0.0px;">
<p>Name*</p>
</div></th>
<th><div class="tablesorter-header-inner" style="margin-left: 0.0px;">
<p>Description</p>
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td>The description of the plugin module. The 'key' attribute can be specified to declare a localisation key for the value instead of text in the element body.</td>
</tr>
<tr class="even">
<td><p>device-type</p></td>
<td><p>The device type that this renderer will be used for (e.g. at render time <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/content/render/xhtml/ConversionContext.html#getOutputDeviceType()" class="external-link">ConversionContext.html#getOutputDeviceType()</a> will be queried and the <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/content/render/xhtml/Renderer.html" class="external-link">Renderer</a> with a configured 'device-type' that matches will be used.</p>
<p>This element can be repeated for each device type this renderer applies to.</p></td>
</tr>
</tbody>
</table>

**\*device-type is required**

## Example

Here is an example `atlassian-plugin.xml` file containing a single public component:

``` xml
<atlassian-plugin name="Hello World" key="example.plugin.helloworld" plugins-version="2">
    <plugin-info>
        <description>A basic component module test</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>
 <device-type-renderer name="Mobile Renderer" key="mobileViewRenderer">
 <description>The renderer used for rendering content in Confluence Mobile.</description>
 <device-type>mobile</device-type>
 </device-type-renderer>
</atlassian-plugin>
```

The matching [Spring configuration](https://developer.atlassian.com/display/DOCS/Advanced+Configuration+with+Spring+XML+Files) for the plugin would need to have a bean configured with the id 'mobileViewRenderer' to match the key on the &lt;device-type-renderer&gt; element, e.g.

**pluginContext.xml Snippet**

``` xml
<beans:bean id="mobileViewRenderer" class="com.atlassian.confluence.content.render.xhtml.DefaultRenderer">
    <beans:constructor-arg index="0">
        <beans:bean class="com.atlassian.confluence.content.render.xhtml.TransformerChain">
        etc...
```

##  Notes

Some information to be aware of when developing or configuring a Device Type Renderer plugin module:

-   Your plugin must have a Spring configured bean with an id matching the key of the device-type-renderer element. See [Advanced Configuration with Spring XML Files](https://developer.atlassian.com/display/DOCS/Advanced+Configuration+with+Spring+XML+Files) for details on creating Spring components.
-   The renderer component referred to must implement the <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/content/render/xhtml/Renderer.html" class="external-link">Renderer</a> interface. For example, it could be a <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/content/render/xhtml/DefaultRenderer.html" class="external-link">DefaultRenderer</a>. 

-   If multiple renderers register against the same device type only the last one registered will be used.

RELATED TOPICS

[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)   
<a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>

Information sourced from [Plugin Framework documentation](https://developer.atlassian.com/display/DOCS/_Content+for+Component+Plugin+Module)
