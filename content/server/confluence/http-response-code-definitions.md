---
aliases:
- /server/confluence/http-response-code-definitions-2031685.html
- /server/confluence/http-response-code-definitions-2031685.md
category: devguide
confluence_id: 2031685
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031685
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031685
date: '2017-12-08'
legacy_title: HTTP Response Code Definitions
platform: server
product: confluence
subcategory: faq
title: HTTP response code definitions
---
# HTTP response code definitions

### HTTP Response Codes

Below is a list of HTTP Response codes and their meaning.

This information was obtained from:  
<a href="http://www.searchenginepromotionhelp.com/m/http-server-response/http-response-codes.php" class="external-link">HTTP Response Code Definitions</a>

<table>
<colgroup>
<col style="width: 15%" />
<col style="width: 85%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Code</p></th>
<th><p>Meaning</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>100</p></td>
<td><p>Continue</p></td>
</tr>
<tr class="even">
<td><p>101</p></td>
<td><p>Switching Protocols</p></td>
</tr>
<tr class="odd">
<td><p>200</p></td>
<td><p>OK</p></td>
</tr>
<tr class="even">
<td><p>201</p></td>
<td><p>Created</p></td>
</tr>
<tr class="odd">
<td><p>202</p></td>
<td><p>Accepted</p></td>
</tr>
<tr class="even">
<td><p>203</p></td>
<td><p>Non-Authoritative Information</p></td>
</tr>
<tr class="odd">
<td><p>204</p></td>
<td><p>No Content</p></td>
</tr>
<tr class="even">
<td><p>205</p></td>
<td><p>Reset Content</p></td>
</tr>
<tr class="odd">
<td><p>206</p></td>
<td><p>Partial Content</p></td>
</tr>
<tr class="even">
<td><p>300</p></td>
<td><p>Multiple Choices</p></td>
</tr>
<tr class="odd">
<td><p>301</p></td>
<td><p>Moved Permanently</p></td>
</tr>
<tr class="even">
<td><p>302</p></td>
<td><p>Found</p></td>
</tr>
<tr class="odd">
<td><p>303</p></td>
<td><p>See Other</p></td>
</tr>
<tr class="even">
<td><p>304</p></td>
<td><p>Not Modified</p></td>
</tr>
<tr class="odd">
<td><p>305</p></td>
<td><p>Use Proxy</p></td>
</tr>
<tr class="even">
<td><p>307</p></td>
<td><p>Temporary Redirect</p></td>
</tr>
<tr class="odd">
<td><p>400</p></td>
<td><p>Bad Request</p></td>
</tr>
<tr class="even">
<td><p>401</p></td>
<td><p>Unauthorized</p></td>
</tr>
<tr class="odd">
<td><p>402</p></td>
<td><p>Payment Required</p></td>
</tr>
<tr class="even">
<td><p>403</p></td>
<td><p>Forbidden</p></td>
</tr>
<tr class="odd">
<td><p>404</p></td>
<td><p>Not Found</p></td>
</tr>
<tr class="even">
<td><p>405</p></td>
<td><p>Method Not Allowed</p></td>
</tr>
<tr class="odd">
<td><p>406</p></td>
<td><p>Not Acceptable</p></td>
</tr>
<tr class="even">
<td><p>407</p></td>
<td><p>Proxy Authentication Required</p></td>
</tr>
<tr class="odd">
<td><p>408</p></td>
<td><p>Request Time-out</p></td>
</tr>
<tr class="even">
<td><p>409</p></td>
<td><p>Conflict</p></td>
</tr>
<tr class="odd">
<td><p>410</p></td>
<td><p>Gone</p></td>
</tr>
<tr class="even">
<td><p>411</p></td>
<td><p>Length Required</p></td>
</tr>
<tr class="odd">
<td><p>412</p></td>
<td><p>Precondition Failed</p></td>
</tr>
<tr class="even">
<td><p>413</p></td>
<td><p>Request Entity Too Large</p></td>
</tr>
<tr class="odd">
<td><p>414</p></td>
<td><p>Request-URI Too Large</p></td>
</tr>
<tr class="even">
<td><p>415</p></td>
<td><p>Unsupported Media Type</p></td>
</tr>
<tr class="odd">
<td><p>416</p></td>
<td><p>Requested range not satisfiable</p></td>
</tr>
<tr class="even">
<td><p>417</p></td>
<td><p>Expectation Failed</p></td>
</tr>
<tr class="odd">
<td><p>500</p></td>
<td><p>Internal Server Error</p></td>
</tr>
<tr class="even">
<td><p>501</p></td>
<td><p>Not Implemented</p></td>
</tr>
<tr class="odd">
<td><p>502</p></td>
<td><p>Bad Gateway</p></td>
</tr>
<tr class="even">
<td><p>503</p></td>
<td><p>Service Unavailable</p></td>
</tr>
<tr class="odd">
<td><p>504</p></td>
<td><p>Gateway Time-out</p></td>
</tr>
<tr class="even">
<td><p>505</p></td>
<td><p>HTTP Version not supported</p></td>
</tr>
</tbody>
</table>

### HTTP Headers

It would be useful to obtain information on HTTP response headers. If you are using Mozilla Firefox, you can download an 'add-ons' (extension) called <a href="http://livehttpheaders.mozdev.org/" class="external-link">LiveHTTPHeaders</a> which will allow you to capture this information. If you are using Internet Explorer, you can use <a href="http://www.debugbar.com/" class="external-link">DebugBar</a> instead.

#### Live HTTP Headers Installation Instructions

For Live HTTP Headers, please do the following:

1.  <a href="http://livehttpheaders.mozdev.org/installation.html" class="external-link">Download</a> and install the Plugin
2.  Restart Firefox
3.  Go to **Tools** in the menu bar and click on **Live HTTP Headers**. This will trigger the functionality.

Now try accessing the Confluence main page and all HTTP request headers, cookies descriptions (such as the seraph authentication 'seraph.os.cookie') will be logged in the pop-up window. Please save this information in a text file, use the 'Save All' option.

#### DebugBar Installation Instructions

Run the downloaded installation file. After installing the DebugBar, the toolbar should automatically display on the next IE startup. If not, you might need to show the toolbar in IE by clicking on *View* &gt; *Explorer Bar* then select *DebugBar*

<a href="http://livehttpheaders.mozdev.org/installation.html" class="external-link">Download Live HTTP Headers add-on</a>  
<a href="http://www.debugbar.com/download.php" class="external-link">Download DebugBar</a>
