---
aliases:
- /server/confluence/decorator-module-2031731.html
- /server/confluence/decorator-module-2031731.md
category: reference
confluence_id: 2031731
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031731
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031731
date: '2017-12-08'
legacy_title: Decorator Module
platform: server
product: confluence
subcategory: modules
title: Decorator module
---
# Decorator module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.5 and later</p></td>
</tr>
</tbody>
</table>

Decorator plugin modules allow you to add decorators without using a [Theme Module](/server/confluence/theme-module).

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read <a href="https://confluence.atlassian.com/display/UPM/Installing+Add-ons" class="external-link">Installing Add-ons</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

## Decorator Plugin Module

The following is an example `atlassian-plugin.xml` file containing a single decorator:

``` xml
<atlassian-plugin key="com.atlassian.confluence.extra.sample" name="Sample Plugin">
...
    <decorator name="myDecorator" page="myDecorator.vmd" key="myDecorator">
        <description>My sample decorator.</description>
        <pattern>/plugins/sampleplugin/*</pattern>
    </decorator>
...
</atlassian-plugin>
```

-   the **page** attribute of `decorator` defines the name of the decorator resource file
-   the **pattern** element defines the url pattern for which the decorator will be applied to (you can only have one pattern per decorator)

## Decorator resource file

Decorator files are written in the Velocity templating language and have the VMD extension. The following is a sample decorator file:

``` xml
<html>
<head>
    <title>$title</title>

    #standardHeader()
</head>

<div id="PageContent">
    <table class="pagecontent" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td valign="top" class="pagebody">
            <div class="pageheader">
                 <span class="pagetitle">$title</span>
            </div>
            $body
         </td>
    </tr>
    </table>
</div>

</body>
</html>
```

You can familiarise yourself with Velocity at the <a href="http://confluence.atlassian.com/display/CONF25/Velocity+Template+Overview" class="external-link">Velocity Template Overview</a> and decorators in general at the <a href="http://wiki.sitemesh.org/" class="external-link">Sitemesh homepage</a>.
