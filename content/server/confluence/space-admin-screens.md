---
aliases:
- /server/confluence/space-admin-screens-18252061.html
- /server/confluence/space-admin-screens-18252061.md
category: devguide
confluence_id: 18252061
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=18252061
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=18252061
date: '2017-12-08'
legacy_title: Space Admin Screens
platform: server
product: confluence
subcategory: learning
title: Space Admin screens
---
# Space Admin screens

This page exists to since Confluence 5.0 to clarify the overlap between legacy screens and new ones.

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="header">
<th> </th>
<th>Doc Theme in 5.0</th>
<th>Default 5.0 Theme</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Availability</td>
<td>- Until Confluence 5.0</td>
<td>- Since Confluence 5.0</td>
</tr>
<tr class="even">
<td><p>Where to display Add-on settings for a space?</p>
<p><a href="/server/confluence/writing-a-space-admin-screen">Tutorial: Writing a Space Admin Screen</a></p></td>
<td><p>- In Space Operations</p>
<p>- In Space Admin</p></td>
<td>- In Space Tools</td>
</tr>
<tr class="odd">
<td><p>Where to display a new content type, such as Pages, Blogs or new types?</p>
<p>Imaginary example: An add-on which manages Surveys.</p>
<p><a href="/server/confluence/writing-an-space-index-page-for-a-content-type">Tutorial: Writing an space index page for a content type</a></p></td>
<td>- In a top-level Space Tab</td>
<td>- As an Index Page</td>
</tr>
<tr class="even">
<td><p>Mixed pattern</p>
<p><a href="/server/confluence/writing-a-mixed-space-screen">Tutorial: Writing a mixed Space screen</a></p></td>
<td>- In a top-level Space Tab</td>
<td>- In Space Tools</td>
</tr>
</tbody>
</table>

\* See last section of this page.

## Index Pages

Custom content types are displayed as a top-level tab in Doc Theme and as a Main Link / Index Page in the 5.0 Theme.

-   **Doc Theme**  
    **![](/server/confluence/images/image2013-3-12-9:59:22.png)**
-   **5.0 Theme**  
    **![](/server/confluence/images/image2013-3-12-10:17:59.png)**

See tutorial: [Writing an space index page for a content type](/server/confluence/writing-an-space-index-page-for-a-content-type)

## Space Admin

Plugin options are displayed as a Space Admin screen (or Space Operations if options should be available to all users) and as a Space Tools in the 5.0 Theme.

-   **Doc Theme**  
    ![](/server/confluence/images/image2013-3-12-9:57:29.png)  
    ![](/server/confluence/images/image2013-3-12-10:23:29.png)
-   **5.0 Theme**  
    **![](/server/confluence/images/image2013-3-12-10:20:9.png)**

See tutorial: [Writing a Space Admin Screen](/server/confluence/writing-a-space-admin-screen)

#### Why two different menus?

The Space Tools display items in a different order and categories than the Space Admin and Space Operations. We didn't want to disturb users of the Doc Theme with new categories and we hope they will have time to get used to the new Space Tools ones.

## Mixed Pattern

There's a natural tendency to put plugins in:

\- A top-level tab for Doc Theme,

\- The Space Tools for the 5.0 Theme.

This is what happens for the Mail plugin:

-   **Doc Theme**  
    ![](/server/confluence/images/image2013-3-12-10:41:53.png)
-   **5.0 Theme**  
    **![](/server/confluence/images/image2013-3-12-10:42:19.png)**

#### Can you avoid the mixed pattern?

-   If your intent is to display a new content type, it is better to follow the Index Page pattern,
-   If your intent is to display settings for a plugin, then your screen may make more sense in the Space Admin;
-   As a last resort, you can use the mixed pattern. The tutorial for it is being written at the time you read those lines <img src="/server/confluence/images/icons/emoticons/wink.png" alt="(wink)" class="emoticon-wink" />.

If you can't avoid this situation, [this tutorial will help you setup a mixed screen](/server/confluence/writing-a-mixed-space-screen).

