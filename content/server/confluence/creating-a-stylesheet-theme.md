---
aliases:
- /server/confluence/creating-a-stylesheet-theme-2031799.html
- /server/confluence/creating-a-stylesheet-theme-2031799.md
category: reference
confluence_id: 2031799
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031799
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031799
date: '2017-12-08'
legacy_title: Creating a Stylesheet Theme
platform: server
product: confluence
subcategory: modules
title: Creating a Stylesheet Theme
---
# Creating a Stylesheet Theme

To create a stylesheet theme, you need to first create your custom stylesheet for Confluence. You can do this using many CSS editing tools. See <a href="http://confluence.atlassian.com/display/DOC/Styling+Confluence+with+CSS" class="external-link">Styling Confluence with CSS</a> for more information.

Once you have a stylesheet (and optionally images) ready, this guide will show you how to package up your stylesheet for use in Confluence as a theme.

### Quick demonstration

The quick demonstration is the <a href="http://confluence.atlassian.com/display/CONFEXT/Easy+Blue+Stylesheet" class="external-link">Easy Blue theme</a>, which you can download here:

-   [easy-blue-theme-1.1.jar](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031799/2228272.jar)

You can quickly customise this theme by using a ZIP extractor program (like WinZip, 7-Zip, etc.) to extract the files, change them, then zip it back up into a JAR file and install it in Confluence.

NOTE: Since this theme was developed as a quick stylesheet demonstration for Confluence, it only has limited browser support. See <a href="http://confluence.atlassian.com/display/CONFEXT/Easy+Blue+Stylesheet" class="external-link">Easy Blue Stylesheet</a> for information about which browsers are supported.

The remainder of this document is a walk-through which describes in detail how to create a theme like this from scratch.

### Creating the descriptor file

Each theme plugin needs a plugin descriptor file, called `atlassian-plugin.xml`. For themes with a single stylesheet, the file is very simple. Below is an example with one stylesheet.

**Theme atlassian-plugin.xml with one stylesheet**

``` xml
<atlassian-plugin name="Simple Demo Theme" key="com.example.acme.simple">
  <plugin-info>
    <description>A Confluence stylesheet theme.</description>
    <vendor name="Acme Software Pty Ltd" url="http://acme.example.com"/>
    <version>1.0</version>
  </plugin-info>

  <theme key="simple-theme" name="Simple Demo Theme" class="com.atlassian.confluence.themes.BasicTheme">
    <!-- CSS -->
    <resource type="download" name="demo-theme.css" location="demo-theme.css"/>

    <param name="includeClassicStyles" value="false"/>
  </theme>
</atlassian-plugin>
```

To create your new theme from scratch:

-   Copy the above XML into a new text file
-   Customise the key, name and vendor of your theme throughout the file
-   Don't change the class in the `<theme>` tag
-   In the `<resource>` tag, put the name of your stylesheet in both the `name` and the `location` attributes
-   Save the customised XML file as `atlassian-plugin.xml` in a new directory with your stylesheet.

### Packaging the theme

The theme files need to be put into a JAR file. A JAR file is essentially a ZIP file with a `.jar` extension, so you can create it with whatever tool you like.

To use the command-line tool, `jar`, which ships with the Java Development Kit, you can run the following command in the directory with your files:

``` bash
jar cf my-awesome-theme-1.0.jar *.xml *.css *.gif *.png
```

This will wrap up the `atlassian-plugin.xml` file with whatever images and CSS files you have in your directory. Now you can <a href="http://confluence.atlassian.com/display/DOC/Installing+a+Plugin" class="external-link">upload the plugin into Confluence</a>.

**Now you're done!** If your theme is working great now, then you're finished. There might be a few more things you need to know, however. The later sections cover these details about further customisation of your stylesheet theme.

### Including the default stylesheet

Most themes that you write for Confluence will actually rely on the default theme stylesheets in Confluence. This includes the standard Confluence fonts, colours, and many other things. To include the Confluence styles in your theme, the `<theme>` tag in your plugin needs to include Confluence's default stylesheet as a resource:

**Theme atlassian-plugin.xml which has one stylesheet, and extends Confluence's default theme**

``` xml
<atlassian-plugin>
...
  <theme key="simple-theme" name="Simple Demo Theme" class="com.atlassian.confluence.themes.BasicTheme">
    <!-- CSS -->
    <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
        <param name="source" value="webContext"/>
    </resource>
    <resource type="download" name="demo-theme.css" location="demo-theme.css"/>

    <param name="includeClassicStyles" value="false"/>
  </theme>
</atlassian-plugin>
```

### Including images

For many themes, you will want to pull in custom background images, icons, and so on. This is very easy to do:

-   Put the images in the same directory as your CSS and `atlassian-plugin.xml` files.
-   Add a resource to your theme descriptor XML file for the image:

``` xml
<atlassian-plugin>
...
  <theme key="simple-theme" name="Simple Demo Theme" class="com.atlassian.confluence.themes.BasicTheme">
    <!-- CSS -->
    <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
        <param name="source" value="webContext"/>
    </resource>
    <resource type="download" name="image-theme.css" location="image-theme.css"/>
  
    <!-- Images -->
    <resource type="download" name="home-16.png" location="home-16.png"/>

    <param name="includeClassicStyles" value="false"/>
  </theme>
</atlassian-plugin>
```

#### Images in subdirectories (optional)

You can put your images into a subdirectory within your theme if you want to:

``` xml
...
    <!-- Images -->
    <resource type="download" name="filename.gif" location="images/filename.gif"/>
...
```

Note that when you reference that file in your CSS, you simply use the name and not the path in the location. For this example:

``` css
.selector {
    background-image: url("filename.gif");
}
```

#### Theme icon image (optional)

To polish off your new theme, you can create a theme icon for the "Choose Theme" menu (displayed next to your theme's name and description). If you don't set your own icon, a default image will be shown. Your theme will work either way.

To create a theme icon:

-   make a 110px × 73px .gif which represents your theme
-   add the image to your theme
-   set the `location` attribute (but don't change the `name`) with the following `<resource>`:

    ``` xml
      <theme key="simple-theme" name="Simple Demo Theme" class="com.atlassian.confluence.themes.BasicTheme">
        ...
        <resource key="icon" name="themeicon.gif" type="download" location="your-theme-icon.gif"/>
        ...
      </theme>
    ```

### Sample theme

Here's a listing of the files in the [source of the Easy Blue theme](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031799/2228272.jar) (demonstrated above):

-   atlassian-plugin.xml
-   divider.png
-   easy-blue-theme.css
-   gradient-comment-side-light.png
-   gradient-comment-side.png
-   gradient-comments-light.png
-   gradient-comments.png
-   gradient-dark-invert.png
-   gradient-dark.png
-   gradient-light.png
-   home-16.png
-   theme-icon.gif.

These are all zipped up into the [easy-blue-theme-1.1.jar](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031799/2228272.jar) file which can be installed into Confluence. In fact, the JAR file is almost exactly the same as the ZIP file. The only difference is a manifest file generated automatically by the `jar` command line tool, which is completely unnecessary for your theme to work in Confluence.

Here's the plugin descriptor file:

``` xml
<atlassian-plugin name="Easy Blue Theme" key="com.atlassian.confluence.ext.theme.easyblue">
  <plugin-info>
    <description>A Confluence theme with soft gradients and easy blue colours.</description>
    <vendor name="Atlassian Software Systems Pty Ltd" url="http://www.atlassian.com"/>
    <version>1.1</version>
  </plugin-info>

  <theme key="easyblue" name="Easy Blue Theme" class="com.atlassian.confluence.themes.BasicTheme">
    <!-- CSS -->
    <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
        <param name="source" value="webContext"/>
    </resource>
    <resource type="download" name="easy-blue-theme.css" location="easy-blue-theme.css"/>
    
    <!-- Images -->
    <resource type="download" name="divider.png" location="divider.png"/>
    <resource type="download" name="gradient-comment-side-light.png" location="gradient-comment-side-light.png"/>
    <resource type="download" name="gradient-comment-side.png" location="gradient-comment-side.png"/>
    <resource type="download" name="gradient-comments-light.png" location="gradient-comments-light.png"/>
    <resource type="download" name="gradient-comments.png" location="gradient-comments.png"/>
    <resource type="download" name="gradient-dark-invert.png" location="gradient-dark-invert.png"/>
    <resource type="download" name="gradient-dark.png" location="gradient-dark.png"/>
    <resource type="download" name="gradient-light.png" location="gradient-light.png"/>
    <resource type="download" name="home-16.png" location="home-16.png"/>

    <param name="includeClassicStyles" value="false"/>
    <resource key="icon" name="themeicon.gif" type="download" location="theme-icon.gif"/>
  </theme>
</atlassian-plugin>
```

You should ensure you update the plugin details if you copy this example.
