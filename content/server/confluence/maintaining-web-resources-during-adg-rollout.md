---
aliases:
- /server/confluence/maintaining-web-resources-during-adg-rollout-16974255.html
- /server/confluence/maintaining-web-resources-during-adg-rollout-16974255.md
category: devguide
confluence_id: 16974255
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16974255
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16974255
date: '2017-12-08'
legacy_title: Maintaining web resources during ADG rollout
platform: server
product: confluence
subcategory: updates
title: Maintaining web resources during ADG rollout
---
# Maintaining web resources during ADG rollout

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This document applies to <strong>Confluence 5.0</strong></p></td>
</tr>
</tbody>
</table>

## Source code

The full source code for the configuration explained on this page is available <a href="https://bitbucket.org/atlassian_tutorial/confluence-adg-rollout-demo" class="external-link">on Bitbucket</a>.

## Overview

We will be rolling out the [Atlassian Design Guidelines (ADG)](https://developer.atlassian.com/design/) as part of Confluence 5.0. If you are following [Atlassian User Interface (AUI)](https://developer.atlassian.com/display/AUI/Atlassian+User+Interface+(AUI)+Developer+Documentation) recommendations in the way you generate your DOM, you should be fine in most cases. To make full use of the design guidelines, you'll most likely have to adapt your DOM a bit to match the necessary selectors. However, this change might affect the appearance of your plugin in older versions of Confluence.

This page explains how to manage two versions of the same web resource in order to achieve consistent appearance, without the burden of having to manage two branches of the source code and the resulting build and provisioning complexity.

## How to manage two versions of a web resource

Let's assume we want to maintain the resources for a plugin which is compatible down to Confluence 3.5. For this example, the styled DOM is generated via a `web-panel` as part of the initial response for a user profile. The declaration in the `atlassian-plugin.xml` is as follows:

**web-panel**

``` xml
<web-panel key="hello-world" location="atl.userprofile">
    <resource name="view" type="velocity" location="templates/hello-world.vm"/>
</web-panel>
```

and the content of the template file is:

**template**

``` xml
<h1 class="hello-world">Hello World!</h1>
```

Now assume this DOM is an application of an AUI pattern. We have a CSS targeting this structure and modifying its appearance. In our example, this style sheet simply colours the text red, but in the real world this could be for example an override (more specific selector) for an AUI style.

**web-resource**

``` xml
<web-resource key="hello-world-stylesheets">
    <resource type="download" name="hello-world.css" location="css/hello-world.css"/>
    <context>atl.userprofile</context>
</web-resource>
```

and the content for the CSS file is

**CSS file**

``` css
h1.hello-world
{
    color: red;
}
```

Now assume this *override* is not necessary if the ADGs are enabled (Confluence 5.0). But for older versions of Confluence, we would need these overrides. Also assume that including these overrides in the page will conflict with ADG, thus those resources shall not be requested if ADGs are enabled.

One way of achieving this is to use a switch, as we have been going with some Atlassian-developed plugins.We moved all of our CSS resources into sub-folders called *old* and replaced them at their original locations with a new set of CSS resources. When the ADGs are enabled, the new resources are delivered, and if they are disabled, the *old* resources are delivered.

The switch is implemented via a new extension point in the plugin system, replacing the *${pdl.dir}* variable (PDL stands for Product Design Language, the old name of ADG) with either an empty string (in case ADGs are enabled) or *old/* (in case the ADGs are disabled). This approach alone does not work for plugins which still need to be deployed to older versions of Confluence, since the plugin system would not replace this variable. Thus one way of utilising this switch is to duplicate the resource declarations and deliver them mutually exclusively via the *BuildNumberCondition* (available since Confluence 3.0).

**CSS file declarations with ADG switch**

``` xml
<web-resource key="hello-world-stylesheets">
    <resource type="download" name="hello-world.css" location="css/${pdl.dir}hello-world.css"/>
    <context>atl.userprofile</context>
    <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.BuildNumberCondition">
        <param name="minBuildNumber">4035</param> <!-- 5.0-m9 -->
    </condition>
</web-resource>
<web-resource key="legacy-hello-world-stylesheets">
    <resource type="download" name="hello-world.css" location="css/old/hello-world.css"/>
    <context>atl.userprofile</context>
    <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.BuildNumberCondition" invert="true">
        <param name="minBuildNumber">4035</param> <!-- 5.0-m9 -->
    </condition>
</web-resource>
```

where the content of the `css/old/hello-world.css` is as above (`color: red`) and the content of the new `css/hello-world.css` is

**new CSS file**

``` css
h1.hello-world
{
    color: green;
}
```

The build number used here is an early 5.0 build number, ensuring that when the declaration with the `${pdl.dir}` variable is used, it is substituted correctly. For older versions of Confluence, the `legacy-hello-world-stylesheets` would be delivered.

Now if you go to a user profile page in Confluence 3.5.17 or 4.3.3, for example, you will see Hello World! in red (`legacy-hello-world-stylesheets` being delivered) but if you go to the page in Confluence 5.0-m9 you will see Hello World! in green (`hello-world-stylesheets` being delivered with` ${pdl.dir}` substituted by an empty string). In order to disable the ADGs in 5.0-m9, go to `/admin/darkfeatures.action` and remove `pdl` under *Site Dark Features*. Now if you go back to a user profile page, you should see Hello World! in red (`hello-world-stylesheets` being delivered with `${pdl.dir} `substituted by `old/`).
