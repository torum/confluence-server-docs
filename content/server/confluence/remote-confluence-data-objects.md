---
aliases:
- /server/confluence/remote-confluence-data-objects-16974149.html
- /server/confluence/remote-confluence-data-objects-16974149.md
category: devguide
confluence_id: 16974149
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16974149
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16974149
date: '2017-12-08'
legacy_title: Remote Confluence Data Objects
platform: server
product: confluence
subcategory: updates
title: Remote Confluence data objects
---
# Remote Confluence data objects

{{% note %}}

The XML-RPC and SOAP APIs are **deprecated since** **Confluence 5.5**.  
Confluence has a new [REST API](/server/confluence/confluence-server-rest-api) that is progressively replacing our existing APIs. We recommend plugin developers use the new REST APIs where possible.

{{% /note %}}

The [Confluence remote APIs](/server/confluence/confluence-xml-rpc-and-soap-apis) return structures that have a summary and a detailed form.  The summary form is a primary key (ie space key, page id) and a representative form (for example, space name, page title). The detailed form has all of the entity details client might need.  Unless otherwise specified, all returned structs are in detailed form.  For information about the remote methods that act on these data objects, see [Remote Confluence Methods](/server/confluence/remote-confluence-methods)

This page documents the following objects:

## ServerInfo

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>majorVersion</p></td>
<td><p>the major version number of the Confluence instance</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="even">
<td><p>minorVersion</p></td>
<td><p>the minor version number of the Confluence instance</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="odd">
<td><p>patchLevel</p></td>
<td><p>the patch-level of the Confluence instance</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="even">
<td><p>buildId</p></td>
<td><p>the build ID of the Confluence instance (usually a number)</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>developmentBuild</p></td>
<td><p>Whether the build is a developer-only release or not</p>
<p><strong>Type:</strong> Boolean</p></td>
</tr>
<tr class="even">
<td><p>baseUrl</p></td>
<td><p>The base URL for the confluence instance</p>
<p><strong>Type:</strong> int</p></td>
</tr>
</tbody>
</table>

Note: Version 1.0.3 of Confluence would be major-version: 1, minor-version: 0, patch-level: 3. Version 2.0 would have a patch-level of 0, even if it's not visible in the version number.

## SpaceSummary

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>the space key</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>the name of the space</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>type</p></td>
<td><p>type of the space</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>url</p></td>
<td><p>the url to view this space online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
</tbody>
</table>

## Space

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>key</p></td>
<td><p>the space key</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>the name of the space</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>url</p></td>
<td><p>the url to view this space online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>homePage</p></td>
<td><p>the id of the space homepage</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="odd">
<td><p>description</p></td>
<td><p>the HTML rendered space description</p>
<p><strong>Type:</strong> String</p></td>
</tr>
</tbody>
</table>

## PageSummary

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>the id of the page</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>space</p></td>
<td><p>the key of the space that this page belongs to</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>parentId</p></td>
<td><p>the id of the parent page</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>title</p></td>
<td><p>the title of the page</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>url</p></td>
<td><p>the url to view this page online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>permissions</p></td>
<td><p>the number of permissions on this page (<strong>deprecated</strong>: may be removed in a future version)</p>
<p><strong>Type:</strong> int</p></td>
</tr>
</tbody>
</table>

## Page

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>the id of the page</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>space</p></td>
<td><p>the key of the space that this page belongs to</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>parentId</p></td>
<td><p>the id of the parent page</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>title</p></td>
<td><p>the title of the page</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>url</p></td>
<td><p>the url to view this page online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>version</p></td>
<td><p>the version number of this page</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="odd">
<td><p>content</p></td>
<td><p>the page content</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>created</p></td>
<td><p>timestamp page was created</p>
<p><strong>Type:</strong> Date</p></td>
</tr>
<tr class="odd">
<td><p>creator</p></td>
<td><p>username of the creator</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>modified</p></td>
<td><p>timestamp page was modified</p>
<p><strong>Type:</strong> Date</p></td>
</tr>
<tr class="odd">
<td><p>modifier</p></td>
<td><p>username of the page's last modifier</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>homePage</p></td>
<td><p>whether or not this page is the space's homepage</p>
<p><strong>Type:</strong> Boolean</p></td>
</tr>
<tr class="odd">
<td><p>permissions</p></td>
<td><p>the number of permissions on this page (<strong>deprecated</strong>: may be removed in a future version)</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="even">
<td><p>contentStatus</p></td>
<td><p>status of the page (eg. current or deleted)</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>current</p></td>
<td><p>whether the page is current and not deleted</p>
<p><strong>Type:</strong> Boolean</p></td>
</tr>
</tbody>
</table>

## PageUpdateOptions

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>versionComment</p></td>
<td><p>Edit comment for the updated page</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>minorEdit</p></td>
<td><p>Is this update a 'minor edit'? (default value: false)</p>
<p><strong>Type:</strong> Boolean</p></td>
</tr>
</tbody>
</table>

## PageHistorySummary

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>the id of the historical page</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>version</p></td>
<td><p>the version of this historical page</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="odd">
<td><p>modifier</p></td>
<td><p>the user who made this change</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>modified</p></td>
<td><p>timestamp change was made</p>
<p><strong>Type:</strong> Date</p></td>
</tr>
<tr class="odd">
<td><p>versionComment</p></td>
<td><p>the comment made when the version was changed</p>
<p><strong>Type:</strong> String</p></td>
</tr>
</tbody>
</table>

## BlogEntrySummary

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>the id of the blog entry</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>space</p></td>
<td><p>the key of the space that this blog entry belongs to</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>title</p></td>
<td><p>the title of the blog entry</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>url</p></td>
<td><p>the url to view this blog entry online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>permissions</p></td>
<td><p>the number of permissions on this page (<strong>deprecated</strong>: may be removed in a future version)</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="even">
<td><p>publishDate</p></td>
<td><p>the date the blog post was published</p>
<p><strong>Type:</strong> Date</p></td>
</tr>
</tbody>
</table>

## BlogEntry

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>the id of the blog entry</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>space</p></td>
<td><p>the key of the space that this blog entry belongs to</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>title</p></td>
<td><p>the title of the page</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>url</p></td>
<td><p>the url to view this blog entry online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>version</p></td>
<td><p>the version number of this blog entry</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="even">
<td><p>content</p></td>
<td><p>the blog entry content</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>permissions</p></td>
<td><p>the number of permissions on this page (<strong>deprecated</strong>: may be removed in a future version)</p>
<p><strong>Type:</strong> int</p></td>
</tr>
</tbody>
</table>

## SearchResult

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>title</p></td>
<td><p>the feed's title</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>url</p></td>
<td><p>the remote URL needed to view this search result online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>excerpt</p></td>
<td><p>a short excerpt of this result if it makes sense</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>type</p></td>
<td><p>the <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/core/ContentEntityObject.html#getType%28%29" class="external-link">type</a> of this result - page, comment, spacedesc, attachment, userinfo, blogpost, status</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>id</p></td>
<td><p>the long ID of this result (if the type has one)</p>
<p><strong>Type:</strong> long</p></td>
</tr>
</tbody>
</table>

## Attachment

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>numeric id of the attachment</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>pageId</p></td>
<td><p>page ID of the attachment</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>title</p></td>
<td><p>title of the attachment</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>fileName</p></td>
<td><p>file name of the attachment (Required)</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>fileSize</p></td>
<td><p>numeric file size of the attachment in bytes</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>contentType</p></td>
<td><p>mime content type of the attachment (Required)</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>created</p></td>
<td><p>creation date of the attachment</p>
<p><strong>Type:</strong> Date</p></td>
</tr>
<tr class="even">
<td><p>creator</p></td>
<td><p>creator of the attachment</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>url</p></td>
<td><p>url to download the attachment online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>comment</p></td>
<td><p>comment for the attachment (Required) </p>
<p><strong>Type:</strong> String</p></td>
</tr>
</tbody>
</table>

## Comment

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>numeric id of the comment</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>pageId</p></td>
<td><p>page ID of the comment</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>title</p></td>
<td><p>title of the comment</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>content</p></td>
<td><p>notated content of the comment (use renderContent to render)</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>url</p></td>
<td><p>url to view the comment online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>created</p></td>
<td><p>creation date of the attachment</p>
<p><strong>Type:</strong> Date</p></td>
</tr>
<tr class="odd">
<td><p>creator</p></td>
<td><p>creator of the attachment</p>
<p><strong>Type:</strong> String</p></td>
</tr>
</tbody>
</table>

## User

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>name</p></td>
<td><p>the username of this user</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>fullname</p></td>
<td><p>the full name of this user</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>email</p></td>
<td><p>the email address of this user</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>url</p></td>
<td><p>the url to view this user online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
</tbody>
</table>

## ContentPermission

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>type</p></td>
<td><p>The type of permission. One of 'View' or 'Edit'</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>userName</p></td>
<td><p>The username of the user who is permitted to see or edit the content. Null if this is a group permission.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>groupName</p></td>
<td><p>The name of the group who is permitted to see or edit the content. Null if this is a user permission.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
</tbody>
</table>

## ContentPermissionSet

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>type</p></td>
<td><p>The type of permission. One of 'View' or 'Edit'</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>contentPermissions</p></td>
<td><p>The permissions. Each item is a <a href="#contentpermission">ContentPermission</a>.</p>
<p><strong>Type:</strong> List</p></td>
</tr>
</tbody>
</table>

## SpacePermissionSet

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>type</p></td>
<td><p>The type of permission. One of the available types listed in <a href="https://developer.atlassian.com/display/CONFDEV/Remote+Confluence+Methods#RemoteConfluenceMethods-spacepermissions">Remote Confluence Methods</a></p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>contentPermissions</p></td>
<td><p>The permissions. Each item is a <a href="#contentpermission">ContentPermission</a>. The ContentPermission type was reused for brevity. In this case, the type field in each ContentPermission element will be of the available types linked above.</p>
<p><strong>Type:</strong> List</p></td>
</tr>
</tbody>
</table>

## Label

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>name</p></td>
<td><p>the name of the label</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>owner</p></td>
<td><p>the username of the owner</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>namespace</p></td>
<td><p>the namespace of the label</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>id</p></td>
<td><p>the ID of the label</p>
<p><strong>Type:</strong> long</p></td>
</tr>
</tbody>
</table>

## UserInformation

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>username</p></td>
<td><p>the username of this user</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>content</p></td>
<td><p>the user description</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>creatorName</p></td>
<td><p>the creator of the user</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>lastModifierName</p></td>
<td><p>the url to view this user online</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>version</p></td>
<td><p>the version</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="even">
<td><p>id</p></td>
<td><p>the ID of the user</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="odd">
<td><p>creationDate</p></td>
<td><p>the date the user was created </p>
<p><strong>Type:</strong> Date</p></td>
</tr>
<tr class="even">
<td><p>lastModificationDate</p></td>
<td><p>the date the user was last modified </p>
<p><strong>Type:</strong> Date</p></td>
</tr>
</tbody>
</table>

## ClusterInformation

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>isRunning</p></td>
<td><p>true if this node is part of a cluster.</p>
<p><strong>Type:</strong> Boolean</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>the name of the cluster.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>memberCount</p></td>
<td><p>the number of nodes in the cluster, including this node (this will be zero if this node is not clustered.)</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="even">
<td><p>description</p></td>
<td><p>a description of the cluster.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>multicastAddress</p></td>
<td><p>the address that this cluster uses for multicasr communication.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>multicastPort</p></td>
<td><p>the port that this cluster uses for multicast communication.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
</tbody>
</table>

## NodeStatus

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>nodeId</p></td>
<td><p>an integer uniquely identifying the node within the cluster.</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="even">
<td><p>jvmStats</p></td>
<td><p>a Map containing attributes about the JVM memory usage of node.  Keys are &quot;total.memory&quot;, &quot;free.memory&quot;, &quot;used.memory&quot;.</p>
<p><strong>Type:</strong> Map</p></td>
</tr>
<tr class="odd">
<td><p>props</p></td>
<td><p>a Map containing attributes of the node. Keys are &quot;system.date&quot;, &quot;system.time&quot;, &quot;system.favourite.colour&quot;, &quot;java.version&quot;, &quot;java.vendor&quot;,<br />
&quot;jvm.version&quot;, &quot;jvm.vendor&quot;, &quot;jvm.implemtation.version&quot;, &quot;java.runtime&quot;, &quot;java.vm&quot;, &quot;user.name.word&quot;, &quot;user.timezone&quot;,<br />
&quot;operating.system&quot;, &quot;os.architecture&quot;,  &quot;fs.encoding&quot;.</p>
<p><strong>Type:</strong> Map</p></td>
</tr>
<tr class="even">
<td><p>buildStats</p></td>
<td><p>a Map containing attributes of the build of Confluence running on the node. Keys are &quot;confluence.home&quot;, &quot;system.uptime&quot;, &quot;system.version&quot;,<br />
&quot;build.number&quot;.</p>
<p><strong>Type:</strong> Map</p></td>
</tr>
</tbody>
</table>

## ContentSummaries

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>totalAvailable</p></td>
<td><p>The total number of content available to be retrieved.</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="even">
<td><p>offset</p></td>
<td><p>The index of the first content retrieved.</p>
<p><strong>Type:</strong> int</p></td>
</tr>
<tr class="odd">
<td><p>content</p></td>
<td><p>list of the retrieved content.</p>
<p><strong>Type:</strong> Vector of ContentSummary</p></td>
</tr>
</tbody>
</table>

## ContentSummary

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>id</p></td>
<td><p>The ID of the content.</p>
<p><strong>Type:</strong> long</p></td>
</tr>
<tr class="even">
<td><p>type</p></td>
<td><p>The type of content (e.g. &quot;page&quot;, &quot;comment&quot;, &quot;blog&quot;).</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>space</p></td>
<td><p>The key of the space to which the content belongs.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>status</p></td>
<td><p>The current status of the content (e.g. &quot;current&quot;, &quot;deleted&quot;).</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="odd">
<td><p>title</p></td>
<td><p>The title of the content.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>created</p></td>
<td><p>Timestamp page was created.</p>
<p><strong>Type:</strong> Date</p></td>
</tr>
<tr class="odd">
<td><p>creator</p></td>
<td><p>Username of the creator.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
<tr class="even">
<td><p>modified</p></td>
<td><p>Timestamp content was modified.</p>
<p><strong>Type:</strong> Date</p></td>
</tr>
<tr class="odd">
<td><p>modifier</p></td>
<td><p>Username of content's last modifier.</p>
<p><strong>Type:</strong> String</p></td>
</tr>
</tbody>
</table>







































































































































































































































