---
aliases:
- /server/confluence/introduction-to-plugin-development-39368844.html
- /server/confluence/introduction-to-plugin-development-39368844.md
category: devguide
confluence_id: 39368844
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368844
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368844
date: '2017-12-08'
legacy_title: Introduction to Plugin Development
platform: server
product: confluence
subcategory: other
title: Introduction to Plugin Development
---
# Introduction to Plugin Development

The Atlassian framework uses plugins to customize and extended all of it's products.  A plugin is a Java OSGi (<a href="https://en.wikipedia.org/wiki/OSGi" class="external-link">Open Services Gateway Initiative</a>) bundle which wraps some special configuration files, code and various resources such as images and javascript.  Plugins can be loaded and unloaded dynamically.

Every plugin is made up of one or more modules. Each module represents a single function of the plugin.  For example, a theme plugin may consist of a colour-scheme module, a number of layout modules and a theme module to link the pieces together.  Some plugins, such as the macro packs that come with Confluence, are just a collection of unrelated modules that just happen to be packaged together.

# Technologies at Play in the Atlassian Ecosystem

Below is a diagram showing the major technologies being employed by the Atlassian Framework:

![](/server/confluence/images/technology-overview.png)

Figure 1: Technologies in the Atlassian Ecosystem

 

### Server Side Technologies

The core code of Atlassian products is developed in Java and hosted with a Apache Tomcat.  (Apache Tomcat is used to receives the request, passes it to the Java code, and then forwards the results back.)  

### Template Languages

To assist in getting from java to html, there is a template language employed called "velocity".  Velocity templates look very much like HTML with some special syntax to retrieve information from Java objects.  To make it easier to insert blocks of html on the fly using javascript there is a template language employed called "soy".  Soy templates again look very much like HTML with some variables.  They are converted to javascript functions that can be called on the client side to insert blocks of HTML into the page.

### Client Side Technologies

In the client's web browser, the standard technologies for rich content are at play: html, javascript, cascading style sheets and regular files such as images.  The Atlassian ecosystem uses an enhanced version of jquery and provides a set of standard user interface elements (<a href="https://docs.atlassian.com/aui/latest/" class="external-link">The Atlassian User Interface or AUI</a>).  

## Prerequisites

The prerequisite to developing a plugin is the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).  The [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) can deploy stand alone instances of the Atlassian products for development and along with tools for creating and deploying plugins. The second thing that is needed is a code editor such as <a href="https://notepad-plus-plus.org/" class="external-link">Notepad++</a> or <a href="http://www.vim.org/" class="external-link">gvim</a>.  Many developers like using <a href="https://eclipse.org/" class="external-link">Eclipse</a> and the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) provides methods to transform a plugin into an Eclipse project.

{{% note %}}

Be cautioned that the Atlassian documentation for the Atlassian SDK and setting up Eclipse are out of date. At present, Java 1.8 is not only supported but required. If you install the variant of Eclipse called "Eclipse IDE for Java Developers", the maven dependencies are installed by default.

{{% /note %}}

## Plugin Development Deployment Approaches

Confluence Plugins are deployed as OSGi packages. This is a Java structure that allows code to be deployed into a running application.  It also bundles the code so that each plugin to either share a common library or have it's own copy to avoid collisions.  The general process is to build the java code in the plugin, package everything as an OSGi bundle and install into Confluence.  For production, a user can manually upload a plugin or download it from the Atlassian Marketplace.  This process is cumbersome for development.  Over time, Confluence has adopted several more efficient patterns for stitching together the process from building to deploying the plugin.

### QuickReload

The general pattern for this process is to have Confluence watch directories for changes using QuickReload and use the shell command **atlas-package** to compile the java code and bundle everything together.  

{{% note %}}

Transformerless & Transformation Required Plugins

 The traditional design is the "Transformation Required" pattern.  A plugin that requires transformation is inspected when uploaded for dependencies and repackaged with an OSGi manifest when pushed to Confluence.

The term transformerless refers to the fact that plugins do not need to be "transformed" into OSGi bundles when uploaded into Confluence.  The transformerless plugin uses the SpringScanner to build the manifests needed to make the packaged plugin a viable OSGi bundle.

{{% /note %}}

### FastDev (deprecated by QuickReload)

The FastDev would also watch a directory for changes.  It had some implementation issues and has be deprecated by QuickReload.  It is unclear if the FastDev approach supports the transformerless design.

### atlas-cli + pi

{{% warning %}}

Transformerless plugin development not supported.

{{% /warning %}}

The shell command **atlas-cli** brings up a maven command line interface.  Within the atlas-cli shell, the command **pi** ( package & install ) will build the plugin and push it to the runnning SDK instance of Confluence.  This seems to be the tried and true approach; however, it does not support the transformerless plugin architecture as the **pi** command does not call the spring scanner that is necessary for making the dependency manifest need by the OSGi bundle.

## Conventions 

### Working Directory

For all of the tutorials, the working directory is called *atlas-development *and is located in the users home directory.

### Development Deployment Paradigm

The tutorials use QuickReload and expect to produce transformerless plugins.

### POM.xml

In the tutorials, there is a parent POM.xml file in the working directory.  The POM.xml file in the plugin directory has been configured to inherit from the POM.xml and all redundant entries have been removed.  Typically, only the artifact version, name, description and build instructions are included. 

### Development Environment

In order to keep things simple, the development has been done in a plain text editor, gvim.  Many find the [Eclipse IDE](/server/confluence/conventions.snippet) a more productive environment. 

  

## Plugin Development By Example

 The general approach of this documentation is to provide a number of sequential tutorials that introduce the major facets of plugin development.  Once those development foundations are understood, it's up to the developer to refer to the Atlassian documentation for instructions on how to accomplish a particular goal.

**Plugin Development Tutorials**

-   [Setting up the Working Environment](/server/confluence/setting-up-the-working-environment.snippet)
-   [Hello World](/server/confluence/hello-world.snippet)
-   [Giants Color Theme](/server/confluence/giants-color-theme.snippet)
-   [HTML5 Multimedia](/server/confluence/html5-multimedia.snippet)
