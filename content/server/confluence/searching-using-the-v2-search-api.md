---
aliases:
- /server/confluence/searching-using-the-v2-search-api-2031828.html
- /server/confluence/searching-using-the-v2-search-api-2031828.md
category: devguide
confluence_id: 2031828
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031828
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031828
date: '2018-03-29'
guides: guides
legacy_title: Searching Using the V2 Search API
platform: server
product: confluence
subcategory: learning
title: Searching using the v2 search API
---
# Searching using the v2 search API

The v2 search API provides a fast way of searching content within Confluence. We highly recommend that all plugin authors switch to this API where possible.

To illustrate how to use this API, we have included a simple code snippet for a basic search that:

*   searches for all content labeled with *administration* in the space with key *DOC*.
*   sorts these results with the latest modified content displayed first.
*   limits the number of results to 10.

``` java
    @Named
    public class V2SearchExample {

        @ConfluenceImport
        private SearchManager searchManager;

        @Inject
        public V2SearchExample(SearchManager searchManager) {
            this.searchManager = searchManager;
        }

        public void search() {    
            SearchQuery query = BooleanQuery.andQuery(new LabelQuery("administration"), new InSpaceQuery("DOC"));
            SearchSort sort = new ModifiedSort(SearchSort.Order.DESCENDING); // latest modified content first
            SearchFilter searchFilter = SiteSearchPermissionsSearchFilter.getInstance();
            ContentSearch search = new ContentSearch(query, sort, searchFilter, 0, 10);
            SearchResults searchResults;
            try {
                searchResults = searchManager.search(search);
            } catch (InvalidSearchException e) {
                // discard search and assign empty results
                searchResults = new DefaultSearchResults(Collections.emptyList(), 0);
            }

            for (SearchResult searchResult : searchResults.getAll()) {
                System.out.println("Title: " + searchResult.getDisplayTitle());
                System.out.println("Content: " + searchResult.getContent());
                System.out.println("SpaceKey: " + searchResult.getSpaceKey());
            }
            System.out.println("Total number of results: " + searchResults.getUnfilteredResultsCount());
        }
    }
```

Further comments:

*   Please ensure you include `com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter` in your search. This is a bundled filter that will handle permission checking and content filtering automatically for you.
*   The number of results returned is limited to 10 and starting offset is set to 0 using `ContentSearch` parameters.
*   The search runs using `searchManager.search(search)`. This invocation returns search results populated with data from your index.
    *   To iterate over the search results returned, you can get a reference to the list of search results with `searchResults.getAll()` or an iterator to this list using `searchResults.iterator()`.
    *   You can extract common information about a search result like title, body, and space key from the search result using `getDisplayTitle()`, `getContent()`, and `getSpaceKey()` respectively. For more accessors, see the [API documentation](/server/confluence/java-api-reference) for `com.atlassian.confluence.search.v2.SearchResult`.
    *   This invocation does not go to the database to construct any search results. If you want the search to return `com.atlassian.bonnie.Searchable` objects from the database, call `searchManager.searchEntities(search)` instead.
*   An exception `com.atlassian.confluence.search.v2.InvalidSearchException` is thrown in one of the following variants:
    *   There is an error mapping a v2 search object to the corresponding Lucene search object.
    *   No mapper could be found to map one of the search objects. (The mapper plugin responsible for mapping this search may have been uninstalled.)
*   If an exception is thrown as described above, you should simply discard the search.

### Related topics

*   See the [Java API Reference](/server/confluence/java-api-reference) page for more information.
