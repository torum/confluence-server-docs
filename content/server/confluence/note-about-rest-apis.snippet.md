---
aliases:
- /server/confluence/-note-about-rest-apis-2031622.html
- /server/confluence/-note-about-rest-apis-2031622.md
category: devguide
confluence_id: 2031622
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031622
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031622
date: '2017-12-08'
legacy_title: _Note about REST APIs
platform: server
product: confluence
subcategory: other
title: _Note about REST APIs
---
# \_Note about REST APIs

{{% note %}}

The Confluence REST APIs are a prototype only

Confluence's REST APIs are evolving. Their functionality is currently limited to a subset of the existing Confluence API. We plan to improve the REST APIs in future releases. Please expect some API changes. If you decide to experiment with these APIs, we would welcome feedback. You can create an improvement request in the <a href="http://jira.atlassian.com/browse/CONF/component/13060" class="external-link">REST API component</a> of our <a href="http://jira.atlassian.com/secure/CreateIssue.jspa?pid=10470&amp;issuetype=4" class="external-link">JIRA project</a>.

For a production-ready API, consider using the [XML-RPC and SOAP APIs](/server/confluence/confluence-xml-rpc-and-soap-apis) or [JSON-RPC APIs](/server/confluence/confluence-json-rpc-apis). These APIs support development using Javascript/AJAX or simple HTTP requests.

{{% /note %}}
