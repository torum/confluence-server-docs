---
aliases:
- /server/confluence/javadoc-standards-2031644.html
- /server/confluence/javadoc-standards-2031644.md
category: devguide
confluence_id: 2031644
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031644
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031644
date: '2017-12-08'
guides: guides
legacy_title: Javadoc Standards
platform: server
product: confluence
subcategory: learning
title: Javadoc standards
---
# Javadoc standards

{{% note %}}

These are guidelines related to the development of Confluence. The guidelines mainly apply to Atlassian employees, but reading them should provide insight for third-party plugin developers as well, so we decided to make them public.

{{% /note %}}{{% note %}}

New Standard

Much of the Confluence codebase does not yet conform to this standard. Confluence developers should help the Confluence codebase out by bringing any code they touch into line with this standard

{{% /note %}}

### Read Me First

You really, *really* should read the Sun guide to writing Doc comments: http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html, especially the advice about avoiding phrases like "This class.." or "This method..."

### The Rules

-   All classes must have a doc comment
-   All public constants must have a doc comment
-   All public methods must have a doc comment except:
    -   Methods that implement or override a method in an interface or superclass without adding any interesting behaviour beyond what is already documented for the overridden method
    -   Trivial getters or setters

A trivial corollary to the above rule: all methods declared in an interface must have doc comments.

### Things You Should Document

-   Any side-effects of the method should be clear from the doc comment
-   `@param` tags for each parameter, even if the content is trivial
-   `@returns` tag for the return value, even if trivial
-   What type of object will be contained in any returned collection
-   What happens if any of the arguments supplied to the method are null (saying "Should never be null" in a {{@param}} is sufficient if the behaviour is undefined, but probably bad)
-   Whether the method ever returns null, or if not, what it returns if there is no value
-   `@throws` tags for all declared exceptions, describing the circumstances they are thrown
-   `@throws` tags for any likely undeclared exceptions
-   a `@since` tag for any interface, or interface method
-   `@see` tags for other classes or methods that interact with the thing being documented in interesting ways, or for other ways to get the same information

{{% note %}}

Tip

If you say something in the doc comment, you should have a test that proves it's true.

{{% /note %}}

### Things to avoid

-   Don't use the `@author` tag
-   Don't use the `@version` tag

### Package Level Comments

...would be nice, but every time I've attempted to add them, I've come up against how badly our packages are structured. I'd say not to bother right now.

### Deprecation

So we don't keep stale methods around forever, we should always document *when* a method was deprecated. For example:

``` java
/** @deprecated since 2.3 use {@link Space#isValidSpaceKey} */
boolean isValidSpaceKey(String key);
```

For more information on commenting deprecated methods, see the [Deprecation Guidelines](/server/confluence/deprecation-guidelines)
