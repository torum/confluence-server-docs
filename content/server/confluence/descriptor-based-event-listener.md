---
aliases:
- /server/confluence/eventlistener-example-2031800.html
- /server/confluence/eventlistener-example-2031800.md
category: reference
confluence_id: 2031800
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031800
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031800
date: '2018-04-27'
legacy_title: EventListener Example
platform: server
product: confluence
subcategory: modules
title: "Descriptor Based EventListener"
---

# Descriptor based Event Listener

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.4 and later</p></td>
</tr>
<tr class="even">
<td><p><strong>Legacy:</strong></p></td>
<td><p>In Confluence 3.3 and later, the preferred approach is to use annotation-based events with Atlassian Spring Scanner.  
More information and examples are on <a href="/server/confluence/event-listener-module">Event Listener module</a> page.</p></td>
</tr>
</tbody>
</table>

## The Listener Plugin XML

Each listener is a plugin module of type 'listener'. It is packaged with whatever Java classes and other resources the listener requires to run. Here is an example of `atlassian-plugin.xml` file containing a single listener:

``` xml
<atlassian-plugin name='Optional Listeners' key='confluence.extra.auditor'>
    <plugin-info>
        <description>Audit Logging</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <listener name='Audit Log Listener' class='com.example.listener.AuditListener' key='auditListener'>
        <description>Provides an audit log for each event within Confluence.</description>
    </listener>
</atlassian-plugin>
```

The listener module definition has no configuration requirements beyond any other module. You just need to give it a name,
a key, and provide the name of the class that implements the listener.

## Example class

Below is an example of an EventListener that listens for the LoginEvent and LogoutEvent.

``` java
package com.atlassian.confluence.extra.userlister;

import com.atlassian.confluence.event.events.security.LoginEvent;
import com.atlassian.confluence.event.events.security.LogoutEvent;
import com.atlassian.event.Event;
import com.atlassian.event.EventListener;
import org.apache.log4j.Logger;

public class UserListener implements EventListener {
    private static final Logger log = Logger.getLogger(UserListener.class);

    private Class[] handledClasses = new Class[]{ LoginEvent.class, LogoutEvent.class};


    public void handleEvent(Event event) {
        if (event instanceof LoginEvent) {
            LoginEvent loginEvent = (LoginEvent) event;
            log.info(loginEvent.getUsername() + " logged in (" +  loginEvent.getSessionId() + ")");
        } else if (event instanceof LogoutEvent) {
            LogoutEvent logoutEvent = (LogoutEvent) event;
            log.info(logoutEvent.getUsername() + " logged out (" +  logoutEvent.getSessionId() + ")");
        }
}

    public Class[] getHandledEventClasses() {
        return handledClasses;
    }

}
```

You can find a more detailed example with sample code in [Writing an Event Listener Plugin Module](/server/confluence/writing-an-event-listener-plugin-module).
