---
aliases:
- /server/confluence/enabling-developer-mode-2031767.html
- /server/confluence/enabling-developer-mode-2031767.md
category: devguide
confluence_id: 2031767
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031767
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031767
date: '2017-12-08'
legacy_title: Enabling Developer Mode
platform: server
product: confluence
subcategory: faq
title: Enabling developer mode
---
# Enabling developer mode

Confluence's **Developer Mode** is a system property setting that tells Confluence to enable various debugging features that are not otherwise exposed to users. To enable Developer Mode, you should start Confluence with the following system property set. See [AMPS build options](https://developer.atlassian.com/display/DOCS/About+AMPS+Build+Options) for instructions.

``` xml
 -Datlassian.dev.mode=true 
```

If you are writing a Confluence extension and want to check if Developer Mode is active, you can call `ConfluenceSystemProperties#isDevMode()`.

### Developer Mode Features

Currently, enabling Developer Mode will activate the following features:

**Prior to Confluence 2.0**

-   Developer Mode not available in these releases

**Confluence 2.0**

-   The System Information page and 500 error page will contain an entry noting that Developer Mode is enabled
-   The "view as HTML" button will be made available in the WYSIWYG rich-text editor

**Confluence 3.3**

-   [Secure administrator sessions](/server/confluence/how-do-i-develop-against-confluence-with-secure-administrator-sessions) will be disabled.
