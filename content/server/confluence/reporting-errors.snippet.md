---
aliases:
- /server/confluence/reporting-errors-39368926.html
- /server/confluence/reporting-errors-39368926.md
category: devguide
confluence_id: 39368926
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368926
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368926
date: '2017-12-08'
legacy_title: Reporting Errors
platform: server
product: confluence
subcategory: other
title: Reporting Errors
---
# Reporting Errors

To report an error in the macro...

``` javascript
import com.atlassian.renderer.v2.RenderUtils;
 
return RenderUtils.blockError( "Message in the yellow box", "Text after the yellow box" );
```
