---
aliases:
- /server/confluence/deploy-plugin-39368900.html
- /server/confluence/deploy-plugin-39368900.md
category: devguide
confluence_id: 39368900
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368900
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368900
date: '2017-12-08'
legacy_title: Deploy Plugin
platform: server
product: confluence
subcategory: other
title: Deploy Plugin
---
# Deploy Plugin

## Launch the Command Line Interface

1.  Open a terminal window  
    {{% note %}}
    The CLI needs to be a different terminal window from the one running Confluence, but they can both launch from the same directory.

    {{% /note %}}
2.  Launch the cli

    ``` javascript
    $ atlas-cli
    ```

    As with starting confluence, the cli can take a while to load; however, it will not take as long as starting Confluence.  The CLI is ready when the terminal shows:

    ``` javascript
    [INFO] Waiting for commands...
    maven> 
    ```

    {{% note %}}

    The pom.xml file will be read and used when the CLI launches. Any changes to the pom.xml (such as updating the plugin version number) will not take effect until the CLI is restarted.

    {{% /note %}}

## Package the Plugin

{{% warning %}}

The instructions below expect QuickReload to be used. QuickReload has replaced FastDEV and the atlas-cli methods for building and pushing changes to Confluence. When Confluence launches, it will scan for plugin directories to watch for changes.  Thus, to update the plugin all that is required is to re-package it. If the plugin directory does not exist when Confluence is launched, it will not be watched.

If the development plugin requires transformation, an equivalent approach is to use the **pi** command from withing the command line interface (**atlas-cli**)

{{% /warning %}}

The command below, **atlas-package**, will compile all of the java code write the appropriate manifest files and package the plugin into an OSGi bundle suitable for uploading to Confluence

``` javascript
$ altas-package
```

If the package is successfully built, a message similar to the one below will be displayed.

``` javascript
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 9.611 s
[INFO] Finished at: 2016-02-01T15:21:40-08:00
[INFO] Final Memory: 51M/389M
[INFO] ------------------------------------------------------------------------
```
