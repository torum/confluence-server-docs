---
aliases:
- /server/confluence/anti-xss-documentation-2031740.html
- /server/confluence/anti-xss-documentation-2031740.md
category: devguide
confluence_id: 2031740
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031740
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031740
date: '2017-12-08'
legacy_title: Anti-XSS documentation
platform: server
product: confluence
subcategory: security
title: Anti-XSS documentation
---
# Anti-XSS documentation

{{% note %}}

In Confluence 4.0, Anti-XSS encoding for plugins is enforced by default. This is a new setting for this release.

{{% /note %}}

This documentation is aimed at developers. It covers instructions to get Velocity template rendering to function correctly with the new Anti-XSS mechanism, as some Confluence 3.x plugins may have their content double encoded.  
  
  

## What is Anti XSS mode?

This mode will engage certain behaviours in Confluence intended to reduce the incidence of cross site scripting (XSS) vulnerabilities. At present this mode enables an automatic data encoding strategy designed to reduce XSS exploits arising from the incorrect encoding of data embedded in HTML templates. This mechanism does not encode HTML output that plugins generate outside of Velocity templates.

{{% note %}}

Developers interested in extending this XSS protection feature to their plugins should consult the [Enabling XSS Protection in Plugins](/server/confluence/enabling-xss-protection-in-plugins) document.

{{% /note %}}

### Automatic reference encoding in Velocity templates

Many of the past XSS vulnerabilities in Confluence have arisen simply because data from untrusted sources have not been encoded correctly when mixed with other HTML in Velocity templates. Such encoding failures lead to possible HTML injection attacks that can range from breaking page rendering, to hijacking user sessions. These security vulnerabilities will always be easily introduced when template authors have to make a conscious decision to specifically encode untrusted data when rendered. Other disadvantages of this opt-in security include a proliferation of noise in templates related directly to encoding operations (`$generalUtil.htmlEncode` et al) and a general obscuration of where data are being written unsafely to the client. Confluence uses a new rendering mode where all data is HTML encoded by default unless steps are taken explicitly to avoid this behaviour in templates.

### States of XSS Protection

The table below explains how to apply XSS protection and how your plugin will behave.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 40%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Protection state</p></th>
<th><p>Effect of protection</p></th>
<th><p>Activation mechanism</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Anti-XSS protection for plugins.</p></td>
<td><p>HTML encoding of Velocity templates is enforced for plugins.</p></td>
<td><p>Confluence Admin setting '<strong>Enforce Anti-XSS for plugins</strong>' in the '<strong>Security Configuration</strong>' screen (default is on).</p></td>
</tr>
<tr class="even">
<td><p><strong>Plugin opts in</strong>. Plugin chooses to enforce Anti-XSS</p></td>
<td><p>Keeps Anti-XSS encoding of plugin template's output even if Confluence administrator turns off Anti-XSS protection for plugins.</p></td>
<td><p>See <a href="/server/confluence/enabling-xss-protection-in-plugins">Enabling XSS Protection in Plugins</a>.</p></td>
</tr>
<tr class="odd">
<td><p><strong>Plugin opts out</strong>.</p></td>
<td><p>Useful when you encounter compatibility issues. Template output is not HTML encoded. If opting out, your plugin needs to be HTML encoding all the user-supplied data. It is recommended to update the plugin so that it is compatible with the new setting.</p></td>
<td><p>See <a href="/server/confluence/enabling-xss-protection-in-plugins">Enabling XSS Protection in Plugins</a>.</p></td>
</tr>
</tbody>
</table>

#### Understanding when HTML encoding is applied

The table below shows how and when HTML encoding of templates is applied for plugins.

<table>
<colgroup>
<col style="width: 60%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>'<strong>Enforce Anti-XSS for plugins</strong>' setting</p></th>
<th><p>On (default)</p></th>
<th><p>Off</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><strong>Plugin opts in</strong>. See <a href="/server/confluence/enabling-xss-protection-in-plugins">Enabling XSS Protection in Plugins</a>.</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
<tr class="even">
<td><p><strong>Plugin opts out</strong>. See <a href="/server/confluence/enabling-xss-protection-in-plugins">Enabling XSS Protection in Plugins</a>.</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
<tr class="odd">
<td><p><strong>Plugin takes no action</strong>.</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

**Key**: Yes = HTML encoding is applied. No = No HTML encoding.

NOTE: When taking no action, your plugin may no longer work correctly because it encounters double HTML encoding of output. See the following paragraphs for ways of addressing this. It is recommended to always opt in.

### Explicity opting in to automatic HTML encoding for plugins

See [Enabling XSS Protection in Plugins](/server/confluence/enabling-xss-protection-in-plugins).

### Opting out of automatic HTML encoding for plugins

See [Enabling XSS Protection in Plugins](/server/confluence/enabling-xss-protection-in-plugins).

#### Related topics

[Advanced HTML encoding](/server/confluence/advanced-html-encoding)  
[Enabling XSS Protection in Plugins](/server/confluence/enabling-xss-protection-in-plugins)  
[Preventing XSS issues with macros in Confluence 4.0](/server/confluence/preventing-xss-issues-with-macros-in-confluence-4-0)
