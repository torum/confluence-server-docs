---
aliases:
- /server/confluence/writing-macros-2031859.html
- /server/confluence/writing-macros-2031859.md
category: reference
confluence_id: 2031859
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031859
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031859
date: '2017-12-08'
legacy_title: Writing Macros
platform: server
product: confluence
subcategory: modules
title: Writing Macros
---
# Writing Macros

{{% note %}}

Writing a macro for Confluence 4.0 or later?

Please refer to [Creating a New Confluence Macro](/server/confluence/creating-a-new-confluence-macro) and [Macro Tutorials for Confluence](/server/confluence/macro-tutorials-for-confluence).

{{% /note %}}

Macros are written and deployed into Confluence as [macro plugin modules](/server/confluence/macro-module). This page describes how to write a macro (but not how to get the plugin working, refer to the other page for that).

### First steps

Make sure you have created your first macro plugin using our guide, [How to Build an Atlassian Plugin](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project). That will save you a lot of time.

### The Macro Class

All macros must implement the `com.atlassian.renderer.v2.macro.Macro` interface. The best place to start is with the Javadoc for the Confluence version you are developing for, such as:

<a href="https://docs.atlassian.com/atlassian-renderer/5.0/apidocs/index.html?com/atlassian/renderer/v2/macro/Macro.html" class="uri external-link">docs.atlassian.com/atlassian-renderer/5.0/apidocs/index.html?com/atlassian/renderer/v2/macro/Macro.html</a>

{{% note %}}

In Confluence version 3 and earlier, com.atlassian.renderer.v2.macro.Macro was the recommended interface; however, If your macro is to be used exclusively with Confluence 4.0 or later, you should use <a href="https://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/macro/Macro.html" class="uri external-link">docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/macro/Macro.html</a>. See [Creating a New Confluence Macro](/server/confluence/creating-a-new-confluence-macro) and [Macro Tutorials for Confluence](/server/confluence/macro-tutorials-for-confluence) for more information.

{{% /note %}}

*Note about the **BaseMacro class**:* While it's not a requirement, your macro should extend the `com.atlassian.renderer.v2.macro.BaseMacro` abstract class. This class does not contain any functionality, but if the `Macro` *interface* changes in the future, the `BaseMacro` *class* will be maintained in order to ensure backwards compatibility with existing macros.

### Writing Your Macro

When writing a macro, you will need to override the following methods:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Method</p></th>
<th><p>Should return...</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>hasBody()</p></td>
<td><p><code>true</code> if this macro expects to have a body, false otherwise</p></td>
</tr>
<tr class="even">
<td><p>getBodyRenderMode()</p></td>
<td><p>The <code>RenderMode</code> under which the body should be processed before being passed into the macro</p></td>
</tr>
<tr class="odd">
<td><p>isInline()</p></td>
<td><p><code>false</code> if the macro produces a block element (like a paragraph, table or div) true if it is inline and should be incorporated into surrounding paragraphs</p></td>
</tr>
<tr class="even">
<td><p>execute()</p></td>
<td><p>a fragment of HTML that is the rendered macro contents</p></td>
</tr>
</tbody>
</table>

### Understanding `RenderMode`

The `RenderMode` tells the Confluence wiki renderer which wiki-conversion rules should be applied to a piece of text. Once again, the best place to start is with the Javadoc for the Confluence version you are developing for, such as:

<a href="https://docs.atlassian.com/atlassian-renderer/5.0/apidocs/index.html?com/atlassian/renderer/v2/RenderMode.html" class="uri external-link">docs.atlassian.com/atlassian-renderer/5.0/apidocs/index.html?com/atlassian/renderer/v2/RenderMode.html</a>

There are a number of pre-defined render modes. The ones that would be useful to macro writers are probably:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Mode</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>RenderMode.ALL</code></p></td>
<td><p>Render everything</p></td>
</tr>
<tr class="even">
<td><p><code>RenderMode.NO_RENDER</code></p></td>
<td><p>Don't render anything: just return the raw wiki text</p></td>
</tr>
<tr class="odd">
<td><p><code>RenderMode.INLINE</code></p></td>
<td><p>Render things you'd normally find inside a paragraph, like links, text effects and so on</p></td>
</tr>
<tr class="even">
<td><p><code>RenderMode.SIMPLE_TEXT</code></p></td>
<td><p>Render text made up only of paragraphs, without images or links</p></td>
</tr>
</tbody>
</table>

If you want finer control, `RenderMode` is implemented as a bit-field. Each constant of `RenderMode` starting with `F_` is a feature of the renderer that can be turned on or off. You can construct a RenderMode by manipulating these bits through the following methods:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Method</p></th>
<th><p>Example</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>RenderMode.allow()</code></p></td>
<td><p><code>RenderMode.allow(RenderMode.F_LINKS || RenderMode.F_HTMLESCAPE)</code> will <em>only</em> render links, and escape HTML entities.</p>
<p><strong>Description:</strong> Allow only the renderings specified.</p></td>
</tr>
<tr class="even">
<td><p><code>RenderMode.suppress()</code></p></td>
<td><p><code>RenderMode.suppress(RenderMode.F_MACROS || RenderMode.F_TEMPLATE)</code> will render everything except macros and template variables.</p>
<p><strong>Description:</strong> Allow all renderings <em>except</em> those specified.</p></td>
</tr>
<tr class="odd">
<td><p><code>and()</code></p></td>
<td><p><code>RenderMode.SIMPLE_TEXT.and(RenderMode.suppress(RenderMode.F_PARAGRAPHS))</code> will render SIMPLE_TEXT without paragraphs.</p>
<p><strong>Description:</strong> Perform a logical AND on an existing render mode.</p></td>
</tr>
<tr class="even">
<td><p><code>or()</code></p></td>
<td><p><code>RenderMode.SIMPLE_TEXT.and(RenderMode.allow(RenderMode.F_MACROS))</code> will render SIMPLE_TEXT with macros.</p>
<p><strong>Description:</strong> Perform a logical OR on an existing render mode.</p></td>
</tr>
</tbody>
</table>

{{% note %}}

Many macros (like this note macro) produce a `div`. Often, if there's only one line of text within a div, you don't want it surrounded in paragraph tags. For this reason, the `RenderMode.F_FIRST_PARA` flag controls the first line of wiki text that is rendered. If `F_FIRST_PARA` is *not* set, and the first line of text is a paragraph, the paragraph tags will not be rendered.

{{% /note %}}

### How to determine the context your macro is being rendered in

One of the parameters to the execute() method, the one with type RenderContext, can be used to determine how the macro is being rendered. See the <a href="http://confluence.atlassian.com/pages/viewpage.action?pageId=194806430" class="external-link">relevant Confluence Developer FAQ entry</a> for the details.

### Accessing the Rest of the System

Like all Confluence plugin modules, Macros are autowired by the Spring framework. To obtain a manager object through which you can interact with Confluence itself, all you need to do is provide a Javabeans-style setter method for that component on your `Macro` class. See [Accessing Confluence Components from Plugin Modules](/server/confluence/accessing-confluence-components-from-plugin-modules)

### Advanced Macro Techniques

Macros are often most powerful when combined with other plugin modules. For example, the {livesearch} macro uses an [XWork plugin module](/server/confluence/xwork-webwork-module) to perform its server-side duties, and the {userlister} plugin uses a [listener plugin module](/server/confluence/event-listener-module) to listen for login events and determine who is online. You may also consider using a [component plugin module](/server/confluence/component-module) to share common code or state between macros.

### How Macros are Processed

If you want to know exactly what happens when a macro is processed, the following (slightly overly-detailed) description should help:

Consider the following code in a Wiki page:

``` xml
{mymacro:blah|width=10|height=20}This _is_ my macro body{mymacro}
```

1.  The `MacroRendererComponent` finds the first {`mymacro:blah|width=10|height=20`} tag, and asks the `MacroManager` if a macro is currently active with the name "mymacro". The `MacroManager` returns a singleton instance of your `Macro`.
2.  The `MacroRendererComponent` calls `hasBody()` on the `Macro`.
    -  If `hasBody()` returns false, the macro is processed with a 'null' body, and the next {mymacro} tag will be processed as a separate macro.
    -  If `hasBody()` returns true, the `MacroRendererComponent` looks for the closing {mymacro}. Anything between the two becomes the macro body.
        -  If there is a macro body, the `MacroRendererComponent` then calls `getRenderMode()` on the macro to determine how that body should be rendered
        -  The macro body is processed through the wiki renderer with the given RenderMode *before* being passed to the macro
3.  The `MacroRendererComponent` calls `execute` on the macro, passing in the macro parameters, the (processed) body, and the current RenderMode
    -   The `execute` method should return an HTML string. No further wiki processing is performed on macro output.
    -   The parameters are a `Map` of {{String}}s, keyed by parameter name.
        -   If any parameter is not named, it is keyed by the string representation of its position: so for the above example, `parameters.get("0")` would return `"blah"`.
        -   `parameters.get(Macro.RAW_PARAMS_KEY)` will return the raw parameter string, in this case: `"blah|width=10|height=20"`
4.  The `MacroRendererComponent` calls `isInline()` on the macro to determine if its results should be inserted into the surrounding page as an inline (i.e. part of a surrounding paragraph) or a block element.

##### RELATED TOPICS

-   [Plugin Tutorial - Writing Macros for Confluence](/server/confluence/writing-macros-for-pre-4-0-versions-of-confluence)
-   [Macro Module](/server/confluence/macro-module)
