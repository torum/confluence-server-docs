---
aliases:
- /server/confluence/writing-a-space-admin-screen-16973971.html
- /server/confluence/writing-a-space-admin-screen-16973971.md
category: devguide
confluence_id: 16973971
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16973971
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16973971
date: '2017-12-08'
legacy_title: Writing a Space Admin Screen
platform: server
product: confluence
subcategory: learning
title: Writing a Space Admin screen
---
# Writing a Space Admin screen

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to <strong>Confluence 5.0.</strong></p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>This is an <strong>intermediate</strong> tutorial. You should have completed at least one beginner tutorial before working through this tutorial. See the<a href="https://developer.atlassian.com/display/DOCS/Tutorials"> list of developer tutorials</a>.</p></td>
</tr>
</tbody>
</table>

If your plugin needs per-space configuration, you will need to create a screen in Space Tools.

Preparation

The example below is available on the following BitBucket repository:

<a href="https://bitbucket.org/atlassian_tutorial/space-plugin-example" class="uri external-link">bitbucket.org/atlassian_tutorial/space-plugin-example</a>

You need to create a Confluence plugin and update the version of Confluence to 5.0-beta5.

## Creating a Space Tools screen

You need two web-items, for Space Tools (5.0 themes) and Space Admin (pre-5.0 themes). If the screen should only be visible to space admins, you will need to use a Condition to check this.

**atlassian-plugin.xml**

``` xml
       <!-- Item in Space Tools -->
    <web-item key="space-admin-quick-link-manager" name="Quick Link Manager in Space Admin" section="system.space.tools/addons" weight="100">
        <label key="space.admin.quick.link.manager" />
        <link linkId="space-admin-quick-link-manager-id">/plugins/${project.artifactId}/add-link.action?key=$generalUtil.urlEncode($helper.spaceKey)</link>
        <conditions type="AND">
            <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.SpacePermissionCondition">
                <param name="permission">administer</param>
            </condition>
                    <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.SpaceSidebarCondition"/>
        </conditions>
    </web-item>
    <!-- Item in Space Admin (for Doc Theme) -->
    <web-item key="space-admin-quick-link-manager-2" name="Quick Link Manager in Space Admin" section="system.space.admin/addons" weight="100">
        <label key="space.admin.quick.link.manager" />
        <link linkId="space-admin-quick-link-manager-id">/plugins/${project.artifactId}/add-link.action?key=$generalUtil.urlEncode($helper.spaceKey)</link>
        <conditions type="AND">
            <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.SpacePermissionCondition">
                <param name="permission">administer</param>
            </condition>
                    <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.SpaceSidebarCondition" invert="true"/>
        </conditions>
    </web-item>
```

For this example, we will also declare an action:

**atlassian-plugin.xml**

``` xml
    <xwork name="Example Actions" key="example-actions">
        <description>Examples of actions</description>
        <package name="space-links-xwork-package" extends="default" namespace="/plugins/${project.artifactId}">
            <default-interceptor-ref name="validatingStack"/>
            <action name="add-link" class="com.atlassian.examples.MyAdminAction">
                <result name="input" type="velocity">/templates/add-link-action.vm</result>
                <result name="success" type="velocity">/templates/add-link-action.vm</result>
            </action>
        </package>
    </xwork>
```

The action must extend SpaceAdminAction, this will check whether the user has permissions for the space:

``` java
public class MyAdminAction extends SpaceAdminAction
{
    @Override
    public String doDefault()
    {
        return INPUT;
    }
}
```

The template needs two decorators:

**add-link-action.vm**

``` xml
<html>
    <head>
        <title>$action.getText("add.link.action")</title>
        <meta name="decorator" content="main"/>
    </head>
    #applyDecorator("root")
        #decoratorParam("helper" $action.helper)
        ## Name of the tab to highlight: space-operations is also valid.
        #decoratorParam("context" "space-administration") 

        #applyDecorator ("root")
            ## The .vmd to use - This one displays both in Space Admin and Space Tools.
            #decoratorParam ("context" "spaceadminpanel") 
            ## Key of the web-item to highlight in Space Admin
            #decoratorParam ("selection" "quick-link-manager-2") 
            ## Key of the web-item to highlight in Space Tools
            #decoratorParam ("selectedSpaceToolsWebItem" "quick-link-manager") 
            #decoratorParam ("helper" $action.helper)
            <body>
                <div class="pagecontent">
                    <p>${message}</p>
                </div>
            </body>
        #end
    #end
</html>
```

Context:

-   spaceadminpanel: Valid for Space Tools (for the new 5.0 themes) and Space Admin (in pre-5.0 themes).
-   spacecontentpanel: Valid for Space Tools (new 5.0 themes) and Space Operations (pre-5.0 themes).
-   spacetoolspanels: Valid for Space Tools only.

Here is the result:

<img src="/server/confluence/images/image2013-2-7-17:44:10.png" width="500" />

The example above is also compatible with the Doc Theme and other pre-5.0 themes:

<img src="/server/confluence/images/image2013-2-7-17:45:46.png" width="500" />

### Related Content

[Web UI Modules](/server/confluence/web-ui-modules)

