---
aliases:
- /server/confluence/servlet-module-2031809.html
- /server/confluence/servlet-module-2031809.md
category: reference
confluence_id: 2031809
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031809
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031809
date: '2018-04-27'
legacy_title: Servlet Module
platform: server
product: confluence
subcategory: modules
title: Servlet module
---
# Servlet module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.4 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this module type

Servlet plugin modules let you deploy Java Servlets as part of your plugins.

## Configuration

The root element for the Servlet plugin module is `servlet`. To configure it, use following attributes and child elements.

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class*</p></td>
<td><p>The Servlet Java class. Must be a subclass of <code>javax.servlet.http.HttpServlet</code>.</p>
<p>See the plugin framework guide to <a href="/server/framework/atlassian-sdk/creating-plugin-module-instances/">creating plugin module instances</a>.</p></td>
</tr>
<tr class="even">
<td><p>state</p>
<p> </p></td>
<td><p>Indicate whether the plugin module should be disabled by default (value='disabled') or enabled by default (value='enabled').</p>
<p><em>Default:</em> enabled.</p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td>The localization key for the human-readable name of the plugin module.</td>
</tr>
<tr class="even">
<td><p>key*</p></td>
<td>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.
<pre><code>&lt;servlet key=&quot;myServlet&quot;&gt;
...
&lt;/servlet&gt;</code></pre>
I.e. the identifier of the servlet.</td>
</tr>
<tr class="odd">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module. </p>
<p>i.e. the human-readable name of the Servlet.</p>
<p><em>Default:</em> the plugin key.</p></td>
</tr>
<tr class="even">
<td><p>system</p></td>
<td><p>Indicates whether this plugin module is a system plugin module (value='true') or not (value='false'). Only available for non-OSGi plugins.</p>
<p><em>Default:</em> false.</p></td>
</tr>
</tbody>
</table>

*Attributes marked with \*  are required.*

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>The description of the plugin module. The 'key' attribute can be specified to declare a localization key for the value instead of text in the element body. </p>
<p>i.e. the description of the Servlet module.</p></td>
</tr>
<tr class="even">
<td><p>init-param</p>
<p> </p></td>
<td><p>Initialization parameters for the servlet, specified using <code>param-name</code> and <code>param-value</code> sub-elements,</p>
<p>just as in <code>web.xml</code>. This element and its child elements may be repeated.</p></td>
</tr>
<tr class="odd">
<td><p>resource</p></td>
<td>A resource for this plugin module. This element may be repeated. A 'resource' is a non-Java file that a plugin may need to operate. Refer to <a href="/server/framework/atlassian-sdk/adding-resources-to-your-project/">Adding resources to your project</a> for details on defining a resource.</td>
</tr>
<tr class="even">
<td><p>url-pattern*</p></td>
<td><p>The pattern of the URL to match. This element may be repeated.</p>
<p></p>
<p>The URL pattern format is used in Atlassian plugin types to map them to URLs. On the whole, the pattern rules are consistent with those defined in the Servlet 2.3 API. The following wildcards are supported:</p>
<ul>
<li>* matches zero or many characters, including directory slashes.</li>
<li>? matches zero or one character.</li>
</ul>
<h6 id="ServletModule-Examples">Examples</h6>
<ul>
<li><code>/mydir/*</code> matches <code>/mydir/myfile.xml</code></li>
<li><code>/*/admin/*.??ml</code> matches <code>/mydir/otherdir/admin/myfile.html</code></li>
</ul></td>
</tr>
<tr class="odd">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module.</p>
<p>i.e. the human-readable name of the Servlet.</p>
<p><em>Default:</em> the plugin key.</p></td>
</tr>
<tr class="even">
<td><p>system</p></td>
<td><p>Indicates whether this plugin module is a system plugin module (value='true') or not (value='false'). Only available for non-OSGi plugins.</p>
<p><em>Default:</em> false.</p></td>
</tr>
</tbody>
</table>

*Elements marked with \*  are required.*

## Example of Servlet plugin module

Here is an sample `atlassian-plugin.xml` file containing a single Servlet module:

``` xml
<atlassian-plugin name="Hello World Servlet" key="example.plugin.helloworld" plugins-version="2">
    <plugin-info>
        <description>A basic Servlet module test - says "Hello World!</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <servlet name="Hello World Servlet" key="helloWorld" class="com.example.myplugins.helloworld.HelloWorldServlet">
        <description>Says Hello World, Australia or your name.</description>
        <url-pattern>/helloworld</url-pattern>
        <init-param>
            <param-name>defaultName</param-name>
            <param-value>Australia</param-value>
        </init-param>
    </servlet>
</atlassian-plugin>
```

## Accessing your Servlet

You can access your Servlet in the Atlassian web application via each `url-pattern` you specify, beneath the `/plugins/servlet` parent path.

For example, if you specify a `url-pattern` of `/helloworld` as above, and your Atlassian application is deployed at
<a href="http://yourserver/jira" class="uri external-link">yourserver/jira</a>, then you can access your servlet at
 <a href="http://yourserver/jira/plugins/servlet/helloworld" class="uri external-link">yourserver/jira/plugins/servlet/helloworld</a>.

## Notes

Here is some information you need to know when developing or configuring a Servlet plugin module:

*   Your servlet `init()` method is not called on web application start as for a normal servlet. Instead,
this method is called the first time your servlet is accessed after each time plugin module is enabled. This means that if you
disable a plugin containing a servlet or a single Servlet module, and re-enable it again, the servlet is
re-created and its `init()` method is called again.
*   Because Servlet modules are deployed beneath a common `/plugins/servlet` root, be careful when choosing each `url-pattern` under which your
servlet is deployed. It is recommended to use a value that will always be unique to the world.

##### Related topics

* [Writing Confluence plugins](/server/confluence/writing-confluence-plugins).
