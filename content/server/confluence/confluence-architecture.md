---
aliases:
- /server/confluence/confluence-architecture-2031647.html
- /server/confluence/confluence-architecture-2031647.md
category: devguide
confluence_id: 2031647
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031647
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031647
date: '2017-12-08'
guides: guides
legacy_title: Confluence Architecture
platform: server
product: confluence
subcategory: learning
title: Confluence architecture
---
# Confluence architecture

### Introduction

These pages are **internal developer documentation** for Confluence. The main audience for these documents is Atlassian developers, but hopefully plugin and extension developers might benefit from knowing more about how the application works. There are, however, a few caveats:

1.  This documentation is incomplete. All system documentation is a work in progress, and more documents will come online as they are written. (This is, after all, a wiki.)
2.  Confluence has been in development since 2003, much longer than these documents have existed. There are many parts of the application that do not follow these guidelines, and some of the architecture documents represent how *things should be from now on* rather than how they were in the past

### Understanding Confluence

These documents should give you some understanding of how the Confluence code-base is structured, where to find things, and where to put new things.

-   [High Level Architecture Overview](/server/confluence/high-level-architecture-overview)
-   [Confluence Permissions Architecture](/server/confluence/confluence-permissions-architecture)

<img src="/server/confluence/images/42732834.png" class="gliffy-macro-image" />

### Developer Guidelines

These documents are more general descriptions of How We Do Things Around Here. It's a good idea to be familiar with these documents, but keep in mind that no rule is set in stone, and the existence of a guideline *does not absolve you from your responsibility to think*.

-   [Spring Usage Guidelines](/server/confluence/spring-usage-guidelines)
-   [Exception Handling Guidelines](/server/confluence/exception-handling-guidelines)
-   [Logging Guidelines](/server/confluence/logging-guidelines)
-   [Deprecation Guidelines](/server/confluence/deprecation-guidelines)
-   [Hibernate Sessions and Transaction Management Guidelines](/server/confluence/hibernate-sessions-and-transaction-management-guidelines)
-   [Javadoc Standards](/server/confluence/javadoc-standards)
