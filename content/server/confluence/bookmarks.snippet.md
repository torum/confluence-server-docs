---
aliases:
- /server/confluence/.bookmarks-2031763.html
- /server/confluence/.bookmarks-2031763.md
category: devguide
confluence_id: 2031763
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031763
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031763
date: '2017-12-08'
legacy_title: .bookmarks
platform: server
product: confluence
subcategory: other
title: .bookmarks
---
# .bookmarks

{{% note %}}

Recent bookmarks in Confluence Development

This page is a container for all the bookmarks in this space. Do not delete or move it or you will lose all your bookmarks.

Unknown macro: {spacebookmarkslink}

Bookmarks in Confluence Development

\|

Unknown macro: {spacebookmarkslink}

Links for Confluence Development

{{% /note %}}

Unknown macro: {bookmarks}
