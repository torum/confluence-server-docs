---
aliases:
- /server/confluence/-cqlsupportedopequality-29952008.html
- /server/confluence/-cqlsupportedopequality-29952008.md
category: devguide
confluence_id: 29952008
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29952008
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29952008
date: '2017-12-08'
legacy_title: _CQLSupportedOpEquality
platform: server
product: confluence
subcategory: other
title: _CQLSupportedOpEquality
---
# \_CQLSupportedOpEquality

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>
