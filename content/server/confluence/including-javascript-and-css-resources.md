---
aliases:
- /server/confluence/including-javascript-and-css-resources-2031769.html
- /server/confluence/including-javascript-and-css-resources-2031769.md
category: devguide
confluence_id: 2031769
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031769
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031769
date: '2018-04-26'
guides: guides
legacy_title: Including Javascript and CSS resources
platform: server
product: confluence
subcategory: learning
title: Including Javascript and CSS resources
---
# Including Javascript and CSS resources

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.10 and later</p></td>
</tr>
<tr class="even">
<td><p>Deprecated:</p></td>
<td><p>DWR was deprecated in Confluence 3.3</p></td>
</tr>
</tbody>
</table>

Good style for web applications requires that JavaScript and CSS for web pages are kept separate from the HTML they enhance.
Confluence itself is moving towards this model, and the tools that Confluence uses to do this are also available to plugin developers.

{{% note %}}

If you develop a theme plugin and would like to include CSS resources, see [Theme stylesheets](/server/confluence/creating-a-theme#using-stylesheets) instead.

{{% /note %}}

## Including a custom JavaScript or CSS file from a plugin

In your `atlassian-plugin.xml`, you should add a Web Resource module. See [Web Resource module](/server/confluence/web-resource-module).

For each resource, the location of the resource should match the path to the resource in your plugin JAR file.
Resource paths are namespaced to your plugin, so they can't conflict with resources in other plugins with the same location (unlike, for instance, i18n or Velocity resources). However, to be consistent with these other types, you may find it convenient to use a path name that is specific to your plugin.

To include your custom Web Resource in a page where your plugin is used, use the `#requireResource` Velocity macro like this:

``` java
#requireResource("com.acme.example.plugin:web-resource-key")
```

Where `"com.acme.example.plugin:web-resource-key"` is your plugin key, a colon, and the key of the [Web resource module](/server/confluence/web-resource-module) in your plugin.

Only one instance of each script or stylesheet will be included, and they will appear in the order they are requested in the Velocity rendering process.

{{% note %}}

The rich text editor does not currently use dynamic stylesheets provided by a macro rendered in this way.

{{% /note %}}

## Web Resource configuration

Within your Web Resource plugin module, you will define one or more resource definitions. See [Adding plugin and module resources](/server/confluence/adding-plugin-and-module-resources).

Note that you can declare the media type (for CSS resources) and whether the resource should be wrapped in an <a href="http://www.quirksmode.org/css/condcom.html" class="external-link">Internet Explorer conditional comment</a>. This feature is also described in [Adding plugin and module resources](/server/confluence/adding-plugin-and-module-resources).

Here is a short example:

``` xml
<web-resource key="my-macro-resources">
    <resource type="download" name="macro.js" location="path/inside/jar/to/js/macro.js"/>
    <resource type="download" name="more-macro-stuff.js" location="path/inside/jar/to/js/more-macro-stuff.js"/>
    <resource type="download" name="macro.css" location="path/inside/jar/to/css/macro.css"/>
    <resource type="download" name="macro-ie.css" location="path/inside/jar/to/css/macro-ie.css">
        <param name="ieonly" value="true"/>
        <param name="title" value="IE styles for My Awesome Macro"/>
    </resource>
    <resource type="download" name="macro-print.css" location="path/inside/jar/to/css/macro-print.css">
        <param name="media" value="print"/>
    </resource>
    <dependency>confluence.web.resources:ajs</dependency> <!-- depends on jQuery/AJS -->
</web-resource>
```

See below for the libraries provided by Confluence that you can include as a dependency.

{{% note %}}

Resource dependencies are not supported in 2.10. You will need to define the depending resources explicitly.

{{% /note %}}

## Including a JavaScript library provided by Confluence

Confluence currently includes several JavaScript libraries that plugins can use. The versions of these libraries are subject to change,
but only across major versions of Confluence.

In the Confluence source code, these libraries are included in a plugin XML file called `web-resources.xml`.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
<col style="width: 20%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Library</p></th>
<th><p>Web resource key</p></th>
<th><p>Confluence 3.3+</p></th>
<th><p>Confluence 6.7</p></th>
<th><p>Details</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>jQuery + AJS</p></td>
<td><pre><code>confluence
  .web
  .resources:ajs</code></pre></td>
<td><p>1.4.2</p></td>
<td><p>1.7.2</p></td>
<td><p>Atlassian's JS abstraction on top of jQuery provides a few additional pieces of functionality.</p></td>
</tr>
<tr class="even">
<td><p>jQuery</p></td>
<td><pre><code>confluence
  .web
  .resources:jquery</code></pre></td>
<td><p>1.4.2</p></td>
<td><p>1.7.2</p></td>
<td><p>For compatibility with prototype, you must use 'jQuery()' not '$' to access jQuery.</p></td>
</tr>
</tbody>
</table>

To include one of these libraries in all pages where your Velocity template appears, simply use the `#requireResource` macro as above.
For example, if your macro requires jQuery, add the following to its Velocity template:

``` java
#requireResource("confluence.web.resources:jquery")
```

## Running scripts when the page loads

The recommended way to load scripts when the page is ready, known as 'on-DOM-ready', is to use the Atlassian JavaScript (AJS) abstraction.
This avoids depending on a particular JavaScript library that may not remain in Confluence.

``` javascript
AJS.toInit(function () {
    // ... your initialisation code here
});
```

This has the additional benefit of ensuring any functions or variables you declare here are not in the global scope,
which is important for best interoperability with other plugins in Confluence.

## Achieving progressive enhancement

 According to the design principles of progressive enhancement, we recommend you separate your markup, styles, and JavaScript when
 developing a Confluence plugin. To assist with this, there are a few hooks in AJS and in Confluence in general to make this easier.

##### Dynamic Content in JavaScript

If you need to pass information from Velocity to JavaScript, such as for localised text, you can use `AJS.params`.
This automatically looks up values inside `fieldsets` marked with a class of "parameters" inside your markup.
For example, given the following markup:

``` xml
<fieldset class="parameters hidden">
    <input type="hidden" id="deleteCommentConfirmMessage" value="$action.getText('remove.comment.confirmation.message')">
</fieldset>
```

You can have your JavaScript access the localised text without embedding it by using `AJS.params`:

``` javascript
if (confirm(AJS.params.deleteCommentConfirmMessage)) {
    // ...
}
```

##### Getting the Context Path

Usually, to avoid the need to know the context path, you can use relative paths in stylesheets and JavaScript.
However, Confluence makes this available through a meta tag in the header that looks like this:

``` xml
<meta name="ajs-context-path" content="/confluence">
```

Starting from Confluence 3.4, the best way of accessing this path is via the `AJS.Data` JavaScript method:

``` javascript
var relativeUrl = AJS.Data.get("context-path") + "/path/to/content";
```

## More Information

##### Couldn't you do this already? What's changed in Confluence 2.8?

Starting from Confluence 2.6, you've been able to use `#includeJavascript` that puts the script tag inline,
exactly where that Velocity macro appears. You've also always been able to include inline scripts or styles in your macros.
However, there are a couple of problems with this that we've solved in 2.8:

*  The JavaScript might override other script already present in the page, including scripts used by Confluence.
*  Inline JavaScript or styles might appear multiple times in the page, wasting bandwidth and potentially causing conflicts.

Many plugin authors found that including JavaScript in their plugins meant the plugin broke in some places, such as in the preview window,
if two copies of the macro were on the same page.

By using the new `#requireResource`, you're guaranteed to get only one instance of the script appearing on a page,
and it will be cached by browsers until your plugin is upgraded.

##### Do I have to use Velocity to request these resources? What about in Java?

You can achieve the same result in Java via the `WebResourceManager`. Use the same method as described previously for Velocity: `webResourceManager.requireResource(String)`  
The `WebResourceManager` is a bean you can get [injected by Spring](/server/confluence/accessing-confluence-components-from-plugin-modules).
Do this within the scope of the request.

In most cases, using Velocity makes more sense, because the declaration of the JS and CSS should be close to the code which uses it.

### Related topics

* [Web Resource module](/server/confluence/web-resource-module).  
* [Adding plugin and module resources](/server/confluence/adding-plugin-and-module-resources).  
* [Writing Confluence .lugins](/server/confluence/writing-confluence-plugins).  
