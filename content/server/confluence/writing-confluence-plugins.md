---
aliases:
- /server/confluence/writing-confluence-plugins-2031672.html
- /server/confluence/writing-confluence-plugins-2031672.md
category: devguide
confluence_id: 2031672
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031672
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031672
date: '2017-12-08'
guides: guides
legacy_title: Writing Confluence Plugins
platform: server
product: confluence
subcategory: learning
title: Writing Confluence plugins
---
# Writing Confluence plugins

Confluence plugins provide a standard mechanism for extending Confluence. By adding plugins to Confluence you will be able to customise the site's look and feel, add new macros, event listeners and periodic tasks, and even introduce whole new features.

You can read the [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide) for an overview of what plugins are. This document introduces Confluence plugins to developers who may want to write their own.

## Anatomy of a Plugin

A plugin is a single jar file that can be uploaded into Confluence. It consists of

-   A plugin descriptor
-   (Optional) Java classes
-   (Optional) Resources

Plugins are composed of a series of *modules*, each of which defines a point at which the plugin interfaces with Confluence.

## Creating your Plugin Descriptor

The plugin descriptor is a single XML file named `atlassian-plugin.xml` that tells the application all about the plugin and the modules contained within it. See [Creating your Plugin Descriptor](/server/confluence/creating-your-plugin-descriptor).

## Creating a Basic Macro Plugin Skeleton

While even the most basic plugin involves quite a few directories and config files, creating a plugin skeleton is pretty easy and straightforward. We have prepared a Maven 2 template which does almost all the work for you. Please refer to the documentation in the [Atlassian Developer Network](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) for instructions on setting up your development environment and creating the most basic Confluence macro plugin. You can use its basic code skeleton to evolve your plugin into one of the categories described below.

## Confluence Plugin Module Types

There are plenty of plugin types in Confluence. If you are new to plugin development in Confluence, we strongly suggest you start by writing a simple [Macro Plugin](/server/confluence/macro-module). Macros are easy to write and give you visual feedback at once. Since the default plugin created by the Maven 2 template is a macro too, you can get started in almost no time at all.

Once you know your way around the Confluence API, you can evolve your plugin into something else, or of course create a new plugin and start from scratch. Each of the following plugin type descriptions assumes you have been able to create the basic plugin skeleton mentioned in the above paragraph.

See [Confluence Plugin Module Types](/server/confluence/confluence-plugin-module-types).

## Java Classes and Accessing Confluence Components

When you upload a plugin JAR file into Confluence, all the Java classes contained within the JAR are available for your plugin to access. You can include as many classes as you like, and have them interact with each other. Because Confluence and plugins can [export components](/server/confluence/component-module) for your plugin to use, it's important that you follow the Java package naming conventions to ensure your plugin's classes do not conflict with Confluence classes or with other plugins.

If you are writing a Java implementation of a plugin module, you will be interested in [Accessing Confluence Components from Plugin Modules](/server/confluence/accessing-confluence-components-from-plugin-modules).

## Adding Plugin and Module Resources

A 'resource' is a non-Java file that a plugin may need in order to operate. See [Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources).

The simplest kind of resource, supported with all plugin module types, is of type `download`, which makes a resource available for download from the Confluence server at a particular URL. See [Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources).

## Adding a Configuration UI for your Plugin

A plugin for an Atlassian application can specify internal links within the application, to allow the user to configure options for the plugin. This is useful where your plugin requires configuration or user-specific settings to work. See [Adding a Configuration UI for your Plugin](/server/confluence/adding-a-configuration-ui-for-your-plugin).

## Ensuring Standard Page Decoration

If you're writing a plugin that is intended for *more than one* Atlassian application, you can use the standard page decorators supported by Confluence. This allows your plugin to generate new web pages with consistent decoration by the host application across the Atlassian products. See [Using Standard Page Decorators](/server/confluence/using-standard-page-decorators).

## Tutorials on Developing Confluence Plugins

If you would like a walkthrough on how to develop specific Confluence plugins, please check out our useful tutorials here: [Tutorials](/server/confluence/tutorials)

##### RELATED TOPICS

-   [Creating your Plugin Descriptor](/server/confluence/creating-your-plugin-descriptor)
-   [Accessing Confluence Components from Plugin Modules](/server/confluence/accessing-confluence-components-from-plugin-modules)
-   [Including Javascript and CSS resources](/server/confluence/including-javascript-and-css-resources)
-   [Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources)
-   [Adding a Configuration UI for your Plugin](/server/confluence/adding-a-configuration-ui-for-your-plugin)
-   [Using Standard Page Decorators](/server/confluence/using-standard-page-decorators)
-   [Making your Plugin Modules State Aware](/server/confluence/making-your-plugin-modules-state-aware)
-   [Form Token Handling](/server/confluence/form-token-handling)
-   [Converting a Plugin to Plugin Framework 2](/server/confluence/converting-a-plugin-to-plugin-framework-2)
-   [Enabling TinyMCE Plugins](/server/confluence/enabling-tinymce-plugins)
-   [Confluence Developer FAQ](/server/confluence/confluence-developer-faq)

