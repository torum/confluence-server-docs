---
aliases:
- /server/confluence/performing-text-searches-using-cql-29951918.html
- /server/confluence/performing-text-searches-using-cql-29951918.md
category: reference
confluence_id: 29951918
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29951918
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29951918
date: '2017-12-08'
legacy_title: Performing text searches using CQL
platform: server
product: confluence
subcategory: api
title: Performing text searches using CQL
---
# Performing text searches using CQL

This page provides information on how to perform text searches. It applies to [advanced searches](/server/confluence/performing-text-searches-using-cql) when used with the [CONTAINS](#contains) operator.

{{% tip %}}

Acknowledgements:

Confluence uses Apache Lucene for text indexing, which provides a rich query language. Much of the information on this page is derived from the <a href="http://lucene.apache.org/core/4_4_0/queryparser/org/apache/lucene/queryparser/classic/package-summary.html#Term_Modifiers" class="external-link">Query Parser Syntax</a> page of the Lucene documentation.

{{% /tip %}}

## Query terms

A query is broken up into **terms** and **operators.** There are two types of terms: **Single Terms** and **Phrases.**

A **Single Term** is a single word such as "`test`" or "`hello`".

A **Phrase** is a group of words surrounded by double quotes such as "`hello dolly`".

*Note: All query terms in Confluence are case insensitive.*

## Term modifiers

Confluence supports modifying query terms to provide a wide range of searching options.

{{% note %}}

**Related topics:**

-   [Advanced Searching using CQL](/server/confluence/advanced-searching-using-cql)

{{% /note %}}

### Wildcard searches: ? and \*

Confluence supports single and multiple character wildcard searches.

To perform a single character wildcard search use the "`?`" symbol.

To perform a multiple character wildcard search use the "`*`" symbol.

{{% note %}}

Wildcard characters need to be enclosed in quote-marks, as they are reserved characters in [advanced search](/server/confluence/performing-text-searches-using-cql). Use quotations, e.g. `summary ~ "cha?k and che*"`

{{% /note %}}

The single character wildcard search looks for terms that match that with the single character replaced. For example, to search for "`text`" or "`test`" you can use the search:

``` bash
te?t
```

Multiple character wildcard searches looks for 0 or more characters. For example, to search for `Windows`, `Win95` or `WindowsNT` you can use the search:

``` bash
win*
```

You can also use the wildcard searches in the middle of a term. For example, to search for `Win95` or `Windows95` you can use the search

``` bash
wi*95
```

{{% note %}}

You cannot use a \* or ? symbol as the first character of a search. The feature request for this is <a href="https://jira.atlassian.com/browse/JRA-6218" class="external-link">JRA-6218</a>

{{% /note %}}

### Fuzzy searches: ~

Confluence supports fuzzy searches. To do a fuzzy search use the tilde, "~", symbol at the end of a single word term. For example to search for a term similar in spelling to "`roam`" use the fuzzy search:

``` bash
roam~
```

This search will find terms like foam and roams.

Note: terms found by the fuzzy search will automatically get a boost factor of 0.2
