---
aliases:
- /server/confluence/preparing-for-confluence-6.3-49016220.html
- /server/confluence/preparing-for-confluence-6.3-49016220.md
category: devguide
confluence_id: 49016220
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=49016220
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=49016220
date: '2017-12-08'
legacy_title: Preparing for Confluence 6.3
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 6.3
---
# Preparing for Confluence 6.3

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 6.3.

{{% note %}}

**Latest milestone**

Looking for information about the latest Confluence EAP milestones? 

Head to [Confluence Development Releases](https://confluence.atlassian.com/display/DOC/Confluence+Development+Releases)

{{% /note %}}

Did you know we've got a new developer community? Head to <a href="https://community.developer.atlassian.com/" class="uri external-link">community.developer.atlassian.com/</a> to check it out!  We'll be posting in the **announcements** category if when new EAP releases are available. 

## Release Candidate - 3 July

-   Added three new languages - Estonian, Icelandic and Slovenian. 
-   Added support for MySQL 5.7

## Beta 3 - 26 June

-   Improved translations for several languages
-   Bug fixes and other improvements

## Beta 2 - 19 June

-   Bug fixes and other improvements. 
-   For the full release notes see <a href="https://confluence.atlassian.com/display/DOC/Confluence+6.3.0+beta+Release+Notes" class="external-link">Confluence 6.3.0 beta Release Notes</a>

## Beta 1 (internal)

This was an internal release. 

## Milestone 8 - 13 June

-   **Recently worked on** is now listed in the profile menu. This is to provide easy access to recently worked on pages for sites that use a page as the space homepage, rather than the dashboard. 
-   Upgraded Applinks to 5.3.2

## Milestone 5 - 5 June 

-   The number of users who can edit a page simultaneously has been limited to 12. This is a proactive change to help customers avoid performance issues if a large number of people edit at the same time. The limit can be modified using the `confluence.collab.edit.user.limit` <a href="https://confluence.atlassian.com/display/CONFEAP/Configuring+System+Properties" class="external-link">system property</a>.

## Milestone 4 - 29 May

-   Added support for PostgreSQL 9.6
-   Removed support for Solaris operating systems - see <a href="https://confluence.atlassian.com/display/DOC/End+of+Support+Announcements+for+Confluence" class="external-link">see announcement</a>

## Milestone 3 - 22 May

This is our first public milestone for Confluence 6.3. Not much has changed yet, but keep an eye on this page, as we'll be releasing a milestone every week in the lead up to Confluence 6.3.  

-   Updated Tomcat to 8.0.43

