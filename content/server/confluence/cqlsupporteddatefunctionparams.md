---
aliases:
- /server/confluence/-cqlsupporteddatefunctionparams-36472659.html
- /server/confluence/-cqlsupporteddatefunctionparams-36472659.md
category: reference
confluence_id: 36472659
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=36472659
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=36472659
platform: server
product: confluence
subcategory: api
title: _CQLSupportedDateFunctionParams
---
# \_CQLSupportedDateFunctionParams

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.
