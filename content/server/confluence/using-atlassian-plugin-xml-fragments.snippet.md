---
aliases:
- /server/confluence/using-atlassian-plugin.xml-fragments-39368928.html
- /server/confluence/using-atlassian-plugin.xml-fragments-39368928.md
category: devguide
confluence_id: 39368928
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368928
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368928
date: '2017-12-08'
legacy_title: Using atlassian-plugin.xml fragments
platform: server
product: confluence
subcategory: other
title: Using atlassian-plugin.xml fragments
---
# Using atlassian-plugin.xml fragments

The atlassian-plugin.xml file can become very large and complex.  It can therefore be advantageous to break it up into more manageable pieces.  The basic idea is to create a set of xml files that contain the various modules and specify in the plugin which folders contain the atlassian-plugin.xml fragments. The syntax for this last part is different between transformerless plugins and traditional plugins.

{{% warning %}}

Note, the expansions such as ${atlassian.plugin.key} do not propogate down to the xml fragments.

{{% /warning %}}

## The traditional atlassian-plugin.xml file

A traditional atlassian-plugin.xml file with a single macro may look like the following:

**atlassian-plugin.xml**

``` javascript
<?xml version="1.0" encoding="UTF-8"?>
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>

    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="hello-world"/>


    <!-- add our web resources -->
    <web-resource key="hello-world-resources" name="hello-world Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        
        <resource type="download" name="hello-world.css" location="/css/hello-world.css"/>
        <resource type="download" name="hello-world.js" location="/js/hello-world.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>hello-world</context>
    </web-resource>   
 
    <!-- define our macro -->
    <xhtml-macro name="hello" key="hello" class="com.example.Hello" >
        <parameters />
    </xhtml-macro

</atlassian-plugin>
```

## Create the xml fragments

For this example, the web-resource and xhtml-macro will be moved into separate fragments.  The web-resource module will be in **web-resources.xml**.  The xhtml-macro module will be in **hello.xml**.  One fragment will be in the directory **.../src/main/resources/META-INF/atlassian-plugin-web-resources**, the other will be in **.../src/main/resources/META-INF/atlassian-plugin-macros**.  The name of these containing folders are not special; they are specified either in the pom.xml (for a transformerless plugin) or the atlassian-plugin.xml file.

**atlassian-plugin.xml**

``` javascript
<?xml version="1.0" encoding="UTF-8"?>
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>

    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="hello-world"/>
 
</atlassian-plugin>
```

**.../src/main/resources/META-INF/atlassian-plugin-web-resources/web-resources.xml**

``` javascript
<?xml version="1.0" encoding="UTF-8"?>
<module-container description="this element is used as an xml container.  The container is required but can be called anything">
    <web-resource key="hello-world-resources" name="hello-world Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        
        <resource type="download" name="hello-world.css" location="/css/hello-world.css"/>
        <resource type="download" name="hello-world.js" location="/js/hello-world.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>hello-world</context>
    </web-resource>  
</module-container>
```

**.../src/main/resources/META-INF/atlassian-plugin-macros/hello.xml**

``` javascript
<?xml version="1.0" encoding="UTF-8"?>
<module-container description="this element is used as an xml container.  The container is required but can be called anything">
    <xhtml-macro name="hello" key="hello" class="com.example.Hello" >
        <parameters />
    </xhtml-macro
</module-container>
```

## Linking the fragments

### For Transformerless Plugins

{{% note %}}

This functionality was introduced with Confluence 5.9.4

{{% /note %}}

In the plugins pom.xml file, add the &lt;Atlassian-Scan-Folders&gt; element.  It's a comma & newline separated list of folders relative to the **resources** directory

``` javascript
<plugin>    
    <groupId>com.atlassian.maven.plugins</groupId>
    <artifactId>maven-jira-plugin</artifactId> <!-- or confluence, or ... -->
    <configuration>
        ...
        <instructions>
            ...
            <Atlassian-Scan-Folders>
               META-INF/atlassian-plugin-web-resources,
               META-INF/atlassian-plugin-macros,
            </Atlassian-Scan-Folders>
        </instructions>
    </configuration>
</plugin>
```

### For Plugins that are Transformed

{{% note %}}

This functionality was introduced into the atlassian-plugins 3.2.16 on 10 Nov 2014. The next release of Confluence after that date was 5.6.5.

{{% /note %}}

Add the &lt;scan-modules&gt; element anywhere inside the &lt;plugin-info&gt; element in the atlassian-plugin.xml file.  There can be multiple &lt;folder&gt; elements in the &lt;scan-modules&gt; parent element.

``` javascript
<?xml version="1.0" encoding="UTF-8"?>
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
 
        <scan-modules>
           <folder>META-INF/atlassian-plugin-web-resources</folder>
           <folder>META-INF/atlassian-plugin-macros</folder>
        </scan-modules>
    </plugin-info>

    <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="hello-world"/>
 
</atlassian-plugin>
```
