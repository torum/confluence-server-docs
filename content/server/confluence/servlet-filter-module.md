---
aliases:
- /server/confluence/servlet-filter-module-2031754.html
- /server/confluence/servlet-filter-module-2031754.md
category: reference
confluence_id: 2031754
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031754
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031754
date: '2018-04-27'
legacy_title: Servlet Filter Module
platform: server
product: confluence
subcategory: modules
title: Servlet Filter module
---
# Servlet Filter module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.10 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this module type

Using Servlet Filter plugin modules you can deploy Java Servlet filters as a part of your plugin, specifying the location
and ordering of your filter. In this way you can build filters that can tackle tasks like profiling, monitoring, and content generation.

## Configuration

The root element for the Servlet Filter plugin module is `servlet-filter`. To configure it, use the following attributes and child elements.

#### Attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>class*</p></td>
<td><p>The class which implements this plugin module. The class you need to provide depends on the module type. For example,
Confluence theme, layout and colour-scheme modules can use classes already provided in Confluence. So you can write a
theme-plugin without any Java code. But for macro and listener modules you need to write your own implementing class and
include it in your plugin. See the plugin framework guide to
<a href="/server/framework/atlassian-sdk/creating-plugin-module-instances/">creating plugin module instances</a>.</p>
<p>The servlet filter Java class must implement <code>javax.servlet.Filter</code>.</p></td>
</tr>
<tr class="even">
<td><p>state</p>
<p> </p></td>
<td><p>Indicate whether the plugin module should be disabled (value='disabled') or enabled by default (value='enabled').</p>
<p><em>Default:</em> enabled.</p></td>
</tr>
<tr class="odd">
<td><p>i18n-name-key</p></td>
<td>The localization key for the human-readable name of the plugin module.</td>
</tr>
<tr class="even">
<td><p>key*</p></td>
<td>The unique identifier of the plugin module. You refer to this key to use the resource from other contexts in your plugin, such as from the plugin Java code or JavaScript resources.
<pre><code>&lt;servlet-filter key=&quot;helloWorld&quot;&gt;
...
&lt;/servlet-filter&gt;</code></pre>
I.e. the identifier of the servlet filter.</td>
</tr>
<tr class="odd">
<td>location</td>
<td><p>The position of the filter in the application filter chain.</p>
<p>If two plugins provide filters at the same position, the 'weight' attribute (see later) is evaluated.</p>
<ul>
<li><code>after-encoding</code> – Near the very top of the filter chain in the application, but after any filters that ensure the integrity of the request.</li>
<li><code>before-login</code> – Before the filter that logs in the user with any authentication information included in the request.</li>
<li><code>before-decoration</code> – Before the filter that does decoration of the response, typically with Sitemesh.</li>
<li><code>before-dispatch</code> – At the end of the filter chain, before any servlet or the filter that handles the request by default.</li>
</ul>
<p><em>Default:</em> before-dispatch.</p></td>
</tr>
<tr class="even">
<td><p>name</p></td>
<td><p>The human-readable name of the plugin module. </p>
<p>i.e. the human-readable name of the filter.</p>
<p><em>Default:</em> the plugin key.</p></td>
</tr>
<tr class="odd">
<td><p>system</p></td>
<td><p>Indicates whether this plugin module is a system plugin module (value='true') or not (value='false'). Only available for non-OSGi plugins.</p>
<p><em>Default:</em> false.</p></td>
</tr>
<tr class="even">
<td>weight</td>
<td><p>The weight of the filter, used to decide in which order to place the filter in the chain of filters that have the same 'location' attribute specified (see previously).</p>
<p>The higher weight, the lower the filter's position.</p>
<em>Default:</em> 100.</td>
</tr>
</tbody>
</table>

*Attributes marked with \* are required.*

#### Elements

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>description</p></td>
<td><p>The description of the plugin module. The 'key' attribute can be specified to declare a localization key for the value instead of text in the element body. i.e. the description of the filter.</p></td>
</tr>
<tr class="even">
<td><p>init-param</p>
<p> </p></td>
<td><p>Initialization parameters for the filter, specified using <code>param-name</code> and <code>param-value</code> sub-elements, just as in <code>web.xml</code>.</p>
<p>This element and its child elements may be repeated.</p></td>
</tr>
<tr class="odd">
<td><p>resource</p></td>
<td>A resource for this plugin module. This element may be repeated. A 'resource' is a non-Java file that a plugin may need in order to operate. Refer to <a href="/server/framework/atlassian-sdk/adding-resources-to-your-project/">Adding Resources to your Project</a> for details on defining a resource.</td>
</tr>
<tr class="even">
<td><p>url-pattern*</p></td>
<td><p>The pattern of the URL to match. This element may be repeated.</p>
<p></p>
<p>The URL pattern format is used in Atlassian plugin types to map them to URLs. On the whole, the pattern rules are consistent with those defined in the Servlet 2.3 API. The following wildcards are supported:</p>
<ul>
<li>* matches zero or many characters, including directory slashes.</li>
<li>? matches zero or one character.</li>
</ul>
<h6 id="ServletFilterModule-Examples">Examples</h6>
<ul>
<li><code>/mydir/*</code> matches <code>/mydir/myfile.xml</code></li>
<li><code>/*/admin/*.??ml</code> matches <code>/mydir/otherdir/admin/myfile.html</code></li>
</ul></td>
</tr>
<tr class="odd">
<td>dispatcher</td>
<td><p>Determines when the filter is triggered. You can include multiple <code>dispatcher</code> elements.<br />
If this element is present, its content must be one of the following, corresponding to the filter dispatcher options defined in the Java Servlet 2.4 specification:</p>
<ul>
<li><code>REQUEST</code> – the filter applies to requests that came directly from the client.</li>
<li><code>INCLUDE</code> – the filter applies to server-side includes done via <code>RequestDispatcher.include()</code>.</li>
<li><code>FORWARD</code> – the filter applies to requests that are forwarded via <code>RequestDispatcher.forward()</code>.</li>
<li><code>ERROR</code> – the filter applies to requests that are handled by an error page.</li>
</ul>
<p>Note: This element is only available in Plugin Framework 2.5 and later.<br />
If this element is not present, the default is <code>REQUEST</code>. (This is also the behavior for Plugin Framework releases earlier than 2.5.)</p>
<p><em>Default:</em> filter will be triggered only for requests from the client.</p></td>
</tr>
</tbody>
</table>

*Elements marked with \* are required.*

## Example of Servlet Filter plugin module

Here is a sample `atlassian-plugin.xml` file containing a single Servlet Filter:

``` xml
<atlassian-plugin name="Hello World Filter" key="example.plugin.helloworld" plugins-version="2">
    <plugin-info>
        <description>A basic Servlet filter module test - says "Hello World!</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <servlet-filter name="Hello World Servlet" key="helloWorld" class="com.example.myplugins.helloworld.HelloWorldFilter" location="before-dispatch" weight="200">
        <description>Says Hello World, Australia or your name.</description>
        <url-pattern>/helloworld</url-pattern>
        <init-param>
            <param-name>defaultName</param-name>
            <param-value>Australia</param-value>
        </init-param>
        <dispatcher>REQUEST</dispatcher>
        <dispatcher>FORWARD</dispatcher>
    </servlet-filter>
</atlassian-plugin>
```

## Accessing your Servlet Filter

You can access your servlet in the Atlassian web application via each `url-pattern` you specify. But, unlike the [Servlet plugin module](/server/framework/atlassian-sdk/servlet-plugin-module/), the `url-pattern` is relative to the root of the web application.

For example, if you specify a `url-pattern` of `/helloworld` as previously, and your Atlassian application is deployed at <a href="http://yourserver/contextpath" class="uri external-link">yourserver/contextpath</a> -- then your servlet filter would be accessed at <a href="http://yourserver/contextpath/helloworld" class="uri external-link">yourserver/contextpath/helloworld</a>.

## Notes

Here is some information you need to know when developing or configuring a Servlet Filter plugin module:

*   Servlet filter `init()` method is not called on web application start as for a normal filter. Instead, this method is called the first time your filter is accessed after each time plugin module is enabled. This means that if you disable a plugin containing a filter or a single Servlet Filter module and re-enable it again, the filter is re-created and its `init()` method is called again.
*   Because servlet filters are deployed beneath root, be careful when choosing each `url-pattern` under which your filter is deployed. If you plan to handle the request in the filter, it is recommended to use a value that will always be unique to the world.
*   Some application servers, like WebSphere 6.1, don't call servlet filters if there is no underlying servlet to match the URL. On these systems, you will only be able to create a filter to handle normal application URLs.

##### Related topics

* [Writing Confluence plugins](/server/confluence/writing-confluence-plugins).
* Information sourced from [Plugin Framework documentation](/server/framework/atlassian-sdk/).
