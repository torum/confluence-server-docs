---
aliases:
- /server/confluence/job-module-2031706.html
- /server/confluence/job-module-2031706.md
category: reference
confluence_id: 2031706
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031706
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031706
date: '2017-12-08'
legacy_title: Job Module
platform: server
product: confluence
subcategory: modules
title: Job module
---
# Job module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>From Confluence 2.2 to Confluence 5.9</p></td>
</tr>
<tr class="even">
<td><p>Updated:</p></td>
<td><p>Job modules are auto-wired in Confluence 4.0.1 and later.</p></td>
</tr>
<tr class="odd">
<td>Status:</td>
<td><strong>Deprecated</strong> in Confluence 5.10 – use <a href="/server/confluence/job-config-module/">Job Config</a> instead</td>
</tr>
</tbody>
</table>

Using job plugin modules, you can add repeatable tasks to Confluence, which are in turn scheduled by [Trigger module](/server/confluence/trigger-module).

*   For more information about plugins in general, read [Confluence plugin guide](/server/confluence/confluence-plugin-guide).
*   To learn how to install and configure plugins (including macros), read <a href="#installing-a-plugin" class="unresolved">Installing a plugin</a>.
*   For an introduction to writing your own plugins, read [Writing Confluence plugins](/server/confluence/writing-confluence-plugins).

## Job plugin module

The Job plugin module adds a simple reusable component within a plugin. At a minimum, the module class must implement Quartz's <a href="http://quartz.sourceforge.net/javadoc/org/quartz/Job.html" class="external-link">Job interface</a>, but for access to
Confluence objects and database you should extend com.atlassian.quartz.jobs.AbstractJob. Jobs are scheduled
with [Trigger module](/server/confluence/trigger-module). 

### Configuration

In Confluence 4.0.1 and later, Jobs are autowired by Spring (see <a href="http://jira.atlassian.com/browse/CONF-20162" class="external-link">CONF-20162</a> for workarounds in earlier versions).

Here is a sample `atlassian-plugin.xml` fragment containing a single Job module:

``` xml
<atlassian-plugin name="Sample Component" key="confluence.extra.component">
    ...
    <job key="myJob"
          name="My Job"
          class="com.example.myplugin.jobs.MyJob" perClusterJob="false" />
    ...
</atlassian-plugin>
```

*   The `name` attribute represents how this component will be referred to in the Confluence interface.
*   The `key` attribute represents the internal system name for your Job. This is what the Trigger will refer to.
*   The `class` attribute represents the class of the Job to be created. The class *must* have a no-argument constructor,
or it will not be able to be instantiated by Confluence.
*   The `perClusterJob` attribute, if set to `true`, indicates that this job will run only once when triggered in a
cluster rather than once on every node.

For examples of how to schedule Jobs to be run, see [Trigger module](/server/confluence/trigger-module).

### Transaction Management

Note that transaction management *is not* automatically applied when you extend `com.atlassian.quartz.jobs.AbstractJob`.
The core Confluence services and managers are declaratively wrapped so individual calls to any of these services do not
require you to perform explicit transaction management. However, you may need to manually wrap your job execution in a transaction if:

*  You are making use of your own components which are not declaratively wrapped in a transaction via Spring configuration.
*  You are sharing data between a number of different calls to one or more services

To programmatically wrap in a transaction in your plugin you should first import the SAL transaction template.

1.  In your `atlassian-plugin.xml` include this:

    ``` xml
        <component-import name="SAL Transaction Template" key="transactionTemplate">
            <interface>com.atlassian.sal.api.transaction.TransactionTemplate</interface>
        </component-import> 
    ```

1.  Import the following classes:

    ``` java
    import com.atlassian.sal.api.transaction.TransactionCallback;
    import com.atlassian.sal.api.transaction.TransactionTemplate;
    ```

1.  Enable your job to be auto-wired via the constructor and wrap your execution in a transaction as shown:

    ``` java
    public class MyJob
    {
        private final TransactionTemplate transactionTemplate;


        public MyJob(final TransactionTemplate transactionTemplate)
        {
            this.transactionTemplate = transactionTemplate;
        }
     
        void performOperation()
        {   
            return transactionTemplate.execute(new TransactionCallback<T>()
            {
                @Override
                public T doInTransaction()
                {
                    // work goes here
                }
            });
        }

        ...
    ```

*Do not* implement a single transaction that spans the work of a long-lived job. This could impact the availability of the Confluence service.

(You can follow the discussion on automatic transaction management in jobs on <a href="https://jira.atlassian.com/browse/CONF-35465" class="external-link">CONF-35465</a>).
