---
category: devguide
date: '2018-04-25'
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence Releases
---
# Preparing for Confluence releases

If you're developing for Confluence, it's important to keep up to date with changes that may affect your add-ons (also known as apps). 

## About the Early Access Program (EAP)

EAP releases provide a snapshot of our work-in-progress and give you an opportunity to test and fix your add-ons before the final release. EAP releases are not production-ready, and should only be used for testing purposes.

We release a milestone version of Confluence each week, containing our work in progress, followed by a beta and release candidate. We release a new feature version of Confluence approximately every 7 weeks. 

[Download the latest EAP release](https://www.atlassian.com/software/confluence/download-eap)

## Finding out about a new EAP release

We'll post an announcement in the [Developer Community announcements category](https://community.developer.atlassian.com/tags/announcement) when the first EAP milestone for a new release is available to download.

The easiest way to be notified about new EAP releases is to watch the [Announcements](https://community.developer.atlassian.com/tags/announcement) channel. 

If you've subscribed to the Developers Updates list in [my.atlassian.com](my.atlassian.com) (under Email Preferences) we'll also send you an email a few days after a beta is released. 

## Preparing for the new release

To find out what changes are planned for an upcoming release, head to [Confluence Development Releases](https://confluence.atlassian.com/display/DOC/Confluence+Development+Releases). 

These pages are updated weekly, after we release a new milestone.

We'll provide as much prior notice as possible, and will let you know if it looks like a feature will slip to a later release. 

If you have questions or concerns about any upcoming changes, the best way to let us know is to use the **Feedback** button on the Confluence header. It's available in all EAP milestones. 





