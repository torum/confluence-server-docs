---
aliases:
- /server/confluence/plugin-development-upgrade-faq-for-4.0-2031722.html
- /server/confluence/plugin-development-upgrade-faq-for-4.0-2031722.md
category: devguide
confluence_id: 2031722
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031722
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031722
date: '2017-12-08'
legacy_title: Plugin Development Upgrade FAQ for 4.0
platform: server
product: confluence
subcategory: updates
title: Plugin development upgrade FAQ for 4.0
---
# Plugin development upgrade FAQ for 4.0

This page provides a resource for plugin developers who are migrating their plugins to Confluence 4.0. It explains what happens to incompatible plugins, how to identify how Confluence is handling a given plugin and links to resources on how to update your plugin code.

## Frequently Asked Questions

### What will Confluence 4.0 do on upgrade from 3.x?

Confluence 4.0 should publish most existing 3.5.x plugins with no changes required. When a plugin is not compatible with Confluence 4.0, it will gracefully attempt to provide a way for the plugin to work seamlessly. If that fails, the information from references listed on this page can be used to update the plugin code to make it 4.0 compatible.

### I'm not on Confluence 3.5.x What should I do?

All of the automated Confluence 4.0 migration and documentation around migrating plugins has been written for the case of moving from 3.5.x to 4.0 exclusively. You should roll forward through 3.x versions until you reach the 3.5 generation.

### How does Confluence determine which macros are compatible with Confluence 4.0?

On upgrading to Confluence 4.0, the Wiki Markup of all pages is parsed. This will detect the following occurences in the page content and wrap each one in a Wiki Markup block:

-   Invalid Wiki Markup. (i.e. any page content that displays an error when the page is viewed).
-   A macro that has been disabled. (i.e. not installed, or removed from the Macro Browser).
-   A bodied macro that does not have a 4.0 compatible version (A 3.x version of the macro is installed, but there is no 4.0 version installed).

NOTE: In order for a macro to be migrated, both a 3.x and 4.0 version have to exist in the system (noted in each plugin's [Atlassian plugin XML file](/server/confluence/creating-a-new-confluence-macro)).

### How does Confluence 4.0 handle plugins that are incompatible with Confluence 4.0?

If the first macro on a Confluence page isn't 4.0 compatible, it will wrap the entire 'scope' or 'level' of that macro in the Wiki Markup macro. This is the intended outcome, the reason for this is due to ambiguity around detecting whether the macro has an ending tag or not.

In this case, the only real solution is to update your plugin to be compatible with Confluence 4.0. See the following section for specific resources.

### What will happen if I don't update my plugin for Confluence 4.0?

The majority of plugins will continue to work with 4.0, but not updating your plugin has the following effects:

-   The page should continue to render correctly.
-   The macro will not show up in the macro browser.
-   The macro will render as a Wiki markup bodied placeholder in the editor (for migrated usages).

### How can I make my plugin Confluence 4.0 compatible?

See [Upgrading and Migrating an Existing Confluence Macro to 4.0](/server/confluence/upgrading-and-migrating-an-existing-confluence-macro-to-4-0).

### What API changes have occurred in Confluence 4.0?

The following Java methods have been removed from the Confluence 4.0 API:

-   `ContentEntityObject.setContent(String)`
-   `ContentEntityObject.getContent()`

If your macro (or plugin) makes use of these calls, it will fail. The failure case will only occur when a plugin built against 3.x is used in a 4.0 system - building the plugin against 4.0 will result in compile-time errors.

## Getting Help

If you need assistance with your plugin migration, try posting your questions on <a href="https://answers.atlassian.com/tags/confluence-development/" class="external-link">Atlassian Answers</a> with the tag "plugin-development". There, our vibrant developer community can see your questions and have the opportunity to answer them.
