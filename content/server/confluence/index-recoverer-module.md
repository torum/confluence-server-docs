---
aliases:
- /server/confluence/index-recoverer-module-33733844.html
- /server/confluence/index-recoverer-module-33733844.md
category: reference
confluence_id: 33733844
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33733844
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33733844
date: '2017-12-08'
legacy_title: Index Recoverer Module
platform: server
product: confluence
subcategory: modules
title: Index Recoverer module
---
# Index Recoverer module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Confluence 5.8 and later</p></td>
</tr>
</tbody>
</table>

The Index Recoverer module allows you to specify a plugin Lucene index for index recovery on start up of a node in a clustered environment if the index is found to be out of date or invalid.

## Index Recoverer

Here is an example `atlassian-plugin.xml` file containing an index recoverer.

``` xml
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="test-plugin" plugins-version="2">
    ...
    <index-recoverer key="edgeIndexRecoverer" class="com.atlassian.confluence.plugins.edgeindex.lucene.EdgeIndexRecoverer" public="true">
        <journal-id>edge_index</journal-id>
        <index-dir-name>edge</index-dir-name>
        <index-name>edge index</index-name>
    </index-recoverer>
    ...
</atlassian-plugin>
```

-   The element: `journal-id` is mandatory, this is name of the journal that your plugin index changes are written into.
-   The element: `index-dir-name `is mandatory, this is the name of the folder that is created to store your plugin lucene documents.
-    The element: `index-name `is optional, this is the name the index which will be used for logging purposes. If this name is not provided, then logs will default to using the `index-dir-name` supplied.

## The Index Recoverer Interface

All index recoverer classes need to implement the following interface:

``` java
package com.atlassian.confluence.api.model.index;
import java.io.File;
import java.io.IOException;
import javax.annotation.ParametersAreNonnullByDefault;
/**
 * Provides lucene index operations to be used when trying to recover an out of date or invalid index
 *
 * @since 5.8
 */
@ParametersAreNonnullByDefault
public interface IndexRecoverer
{
    /**
     * Allows taking a snapshot of the index.
     *
     * The snapshot should be taken in a safe way to prevent any write while it is copying index files over to the destination directory.
     * Suggestions include to use LuceneConnection#snapshot(Directory) method or alternatively to take the snapshot with SnapshotDeletionPolicy.
     *
     * @param destDir directory in which the snapshot should be saved
     */
    void snapshot(File destDir) throws IOException;
    /**
     * Closes the reader and the writer and create new ones.
     *
     * @param replaceIndex method that should be executed before the index has been closed and before the index has been reopened
     */
    void reset(Runnable replaceIndex);
    /**
     * Trigger a reindex
     */
    void reindex();
}
```

##  Example Index Recoverer

The following example comes from the edge index plugin bundled with Confluence, which indexes edges between a graph of users and content for the Popular stream. The index recoverer will attempt to recover this plugin edge index during start up if it is found to be out of date or invalid.

``` java
package com.atlassian.confluence.plugins.edgeindex.lucene;
import java.io.File;
import java.io.IOException;
import com.atlassian.bonnie.DirectoryUtil;
import com.atlassian.bonnie.ILuceneConnection;
import com.atlassian.confluence.api.model.index.IndexRecoverer;
import com.atlassian.confluence.plugins.edgeindex.EdgeIndexBuilder;
import static com.atlassian.confluence.plugins.edgeindex.EdgeIndexBuilder.EDGE_INDEX_REBUILD_DEFAULT_START_PERIOD;
import static com.atlassian.confluence.plugins.edgeindex.EdgeIndexBuilder.RebuildCondition.FORCE;
import org.springframework.beans.factory.annotation.Qualifier;
public class EdgeIndexRecoverer implements IndexRecoverer
{
    private final ILuceneConnection luceneConnection;
    private final EdgeIndexBuilder edgeIndexBuilder;
    public EdgeIndexRecoverer(@Qualifier ("edgeLuceneConnection") ILuceneConnection luceneConnection, @Qualifier ("edgeIndexBuilder") EdgeIndexBuilder edgeIndexBuilder)
    {
        this.luceneConnection = luceneConnection;
        this.edgeIndexBuilder = edgeIndexBuilder;
    }
    @Override
    public void snapshot(File destDir) throws IOException
    {
        luceneConnection.snapshot(DirectoryUtil.getDirectory(destDir));
    }
    @Override
    public void reset(Runnable replaceIndex)
    {
        luceneConnection.reset(replaceIndex);
    }
    @Override
    public void reindex()
    {
        edgeIndexBuilder.rebuild(EDGE_INDEX_REBUILD_DEFAULT_START_PERIOD, FORCE);
    }
}
```

**Note**: the edge index plugin uses `com.atlassian.bonnie.ILuceneConnection` to do index related operations. If your plugin does not use this method of managing your Lucene index, see suggestions provided in the Index Recoverer Interface section above.
