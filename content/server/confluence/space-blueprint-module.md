---
aliases:
- /server/confluence/space-blueprint-module-24084715.html
- /server/confluence/space-blueprint-module-24084715.md
category: reference
confluence_id: 24084715
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24084715
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24084715
date: '2017-12-08'
legacy_title: Space Blueprint Module
platform: server
product: confluence
subcategory: modules
title: Space Blueprint module
---
# Space Blueprint module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Confluence 5.3 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this module

As a plugin developer, you can use a Space Blueprint module to add to the Create Space dialog in Confluence to help users bootstrap the space creation process. Note that you need to define a `web-item` referring to a `space-blueprint` module for it to appear in the Create Space dialog. This page only documents the `space-blueprint` module. See [Space Blueprints](/server/confluence/space-blueprints) for concepts and tutorials.

## Configuration

The root element for the Space Blueprint module is `space-blueprint`. 

#### Element: `space-blueprint`

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>key</code></p></td>
<td><p>The identifier of the plugin module. This key must be unique within your plugin.</p>
<p><strong>Default:</strong> None</p></td>
</tr>
<tr class="even">
<td><p><code>i18n-name-key</code></p></td>
<td><p>A i18n key for the name of this Space Blueprint.</p>
<p><strong>Default:</strong> None</p></td>
</tr>
<tr class="odd">
<td><code>category</code></td>
<td><p>A space label to apply to the spaces created via the Space Blueprint.</p>
<p><strong>Default:</strong> None</p></td>
</tr>
</tbody>
</table>

**\*key and i18n-name-key attributes are required**

A` space-blueprint` can optionally contain the following elements:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Element</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="/server/confluence/content-template-module">content-template</a></p></td>
<td><p>The <code>content-template</code> element allows you to define the default page structure for spaces created via your space blueprint. The root content-template element becomes the homepage for the space and you can nest them to create a hierarchy. These elements require a ref attribute referring to a <code>content-template</code> definition in your plugin.</p></td>
</tr>
<tr class="even">
<td><p><a href="/server/confluence/promoted-blueprints-module">promoted-blueprints</a></p></td>
<td><p>The <code>promoted-blueprints</code> element allows you to define a list of Blueprints to be promoted by default in your space. This means that if a user presses &quot;Create&quot;, the &quot;promoted&quot; Blueprints will appear first for that space.</p></td>
</tr>
<tr class="odd">
<td><p><a href="/server/confluence/dialog-wizard-module">dialog-wizard</a></p></td>
<td>This is an optional element which allows you to easily create a dialog wizard for your space.</td>
</tr>
</tbody>
</table>

## Example

Example of a Space Blueprint with a single page wizard and default page hierarchy; including a homepage and two child pages.

**Example from Hello Blueprint**

``` xml
<content-template key="hierarchy-blueprint-content-template" i18n-name-key="confluence.hierarchy.blueprint.content.template.name">
    <description key="confluence.hierarchy.blueprint.content.template.description"/>
    <resource name="template" type="download" location="com/atlassian/confluence/plugins/hello_blueprint/xml/hierarchy-content-template.xml"/>
    <context-provider class="com.atlassian.confluence.plugins.hello_blueprint.HierarchyContextProvider"/>
</content-template>
<content-template key="hierarchy-child-a-content-template" i18n-name-key="confluence.hierarchy.child.a.blueprint.content.template.name">
        <description key="confluence.hierarchy.child.a.blueprint.content.template.description"/>
        <resource name="template" type="download" location="com/atlassian/confluence/plugins/hello_blueprint/xml/hierarchy-child-a-content-template.xml"/>
        <context-provider class="com.atlassian.confluence.plugins.hello_blueprint.HierarchyChildAContextProvider"/>
</content-template>
<content-template key="hierarchy-child-b-content-template" i18n-name-key="confluence.hierarchy.child.b.blueprint.content.template.name">
        <description key="confluence.hierarchy.child.b.blueprint.content.template.description"/>
        <resource name="template" type="download" location="com/atlassian/confluence/plugins/hello_blueprint/xml/hierarchy-child-b-content-template.xml"/>
</content-template>


<web-item key="hello-space-blueprint-item" i18n-name-key="confluence.hello.space.blueprint.name" section="system.create.space.dialog/content">
        <description key="confluence.hello.blueprint.description"/>
        <resource name="icon" type="download" location="com/atlassian/confluence/plugins/hello_blueprint/images/preview.png"/>
        <param name="blueprintKey" value="my-space-blueprint"/>
</web-item>

<space-blueprint key="my-space-blueprint" i18n-name-key="confluence.hello.space.blueprint.name">
        <content-template ref="hierarchy-blueprint-content-template">
            <content-template ref="hierarchy-child-a-content-template"/>
            <content-template ref="hierarchy-child-b-content-template"/>
        </content-template>
        <dialog-wizard key="my-space-blueprint-wizard">
            <dialog-page id="spaceBasicDetailsId"
                         template-key="Confluence.Templates.Blueprints.CreateSpace.createSpaceForm"
                         title-key="confluence.hello.blueprint.dialog.choose.title"
                         description-header-key="confluence.hello.blueprint.dialog.choose.heading"
                         description-content-key="confluence.hello.blueprint.dialog.choose.description"/>
        </dialog-wizard>
</space-blueprint>
```

