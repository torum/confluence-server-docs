---
aliases:
- /server/confluence/theme-configuration-2031842.html
- /server/confluence/theme-configuration-2031842.md
category: reference
confluence_id: 2031842
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031842
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031842
date: '2017-12-08'
legacy_title: Theme Configuration
platform: server
product: confluence
subcategory: modules
title: Theme Configuration
---
# Theme Configuration

The themes can be configured via the *Configuration* link on the Choose Theme page on both the space and global level.

<img src="/server/confluence/images/themeconfig.png" class="image-center" />

For example, the Left Navigation Theme allows for the specification of the title of the page which page should be used for navigation. The [XWork module](/server/confluence/xwork-webwork-module) allows for developing complex configurations for themes, which can be [saved in a config file](/server/confluence/saving-theme-configurations-with-bandana).

## Setting up the atlassian-plugin.xml

### Configuration path parameter.

Specify the path to the configuration action in the [atlassian-plugin.xml](/server/confluence/theme-module#the-theme-plugin-module):

``` xml
<theme key="dinosaurs" name="Dinosaur Theme"
       class="com.atlassian.confluence.themes.BasicTheme">
    <description>A nice theme for the kids</description>
    <colour-scheme key="com.example.themes.dinosaur:earth-colours"/>
    <layout key="com.example.themes.dinosaur:main"/>
    <layout key="com.example.themes.corporate:mail-template"/>
    <param name="space-config-path" value="/themes/dinosaurs/configuretheme.action"/>
    <param name="global-config-path" value="/admin/themes/dinosaurs/configuretheme.action"/>
</theme>
```

Note that two new parameters have been specified in the above xml.

-   **space-config-path** points to the action connected with the space theme configuration.
-   **global-config-path** points to the action connected with the global theme configuration.

As themes can be specified either on a global or space level, different configuration actions can be implemented for each level. If there is no need for configuration on a level, simply don't specify the config path.

### XWork Actions

Specify the configuration actions via the [Xwork plugin module](/server/confluence/xwork-webwork-module).  
Define two packages, one for the space-level and one for the global configuration.

``` xml
<xwork name="themeaction" key="themeaction">

    <package name="dinosaurs" extends="default" namespace="/themes/dinosaurs">
        <default-interceptor-ref name="defaultStack" />
        <action name="configuretheme" class="com.atlassian.confluence.extra.dinosaurs.ConfigureThemeAction" method="doDefault">
            <result name="input" type="velocity">/templates/dinosaurs/configuretheme.vm</result>
        </action>
        <action name="doconfiguretheme" class="com.atlassian.confluence.extra.dinosaurs.ConfigureThemeAction">
            <result name="success" type="redirect">/spaces/choosetheme.action?key=${key}</result>
        </action>
    </package>

    <package name="dinosaurs-admin" extends="default" namespace="/admin/themes/dinosaurs">
    <default-interceptor-ref name="defaultStack" />
        <action name="configuretheme" class="com.atlassian.confluence.extra.dinosaurs.ConfigureThemeAction" method="doDefault">
            <result name="input" type="velocity">/templates/dinosaurs/configurethemeadmin.vm</result>
        </action>
        <action name="doconfiguretheme" class="com.atlassian.confluence.extra.dinosaurs.ConfigureThemeAction">
            <result name="success" type="redirect">/admin/choosetheme.action</result>
        </action>
    </package>

</xwork>
```

-   **configuretheme** defines the velocity file used to display the input view.
-   **doconfiguretheme** defines the action to redirect to after the configuration was successful.

Note that the config-path parameters specified above matches the namespace plus the name of the action.

For example, given the above atlassian-plugin.xml, the configuretheme action would be accessed at

``` xml
http://yourserver/confluence/themes/dinosaurs/configuretheme.action
```

{{% note %}}

The namespace of the global action has to start with */admin*. Otherwise the action will not be decorated by the admin decorator, and the navigation of the admin area will not be visible.

{{% /note %}}

