---
aliases:
- /server/confluence/2031805.html
- /server/confluence/2031805.md
category: devguide
confluence_id: 2031805
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031805
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031805
date: '2017-12-08'
legacy_title: How do I find information about lost attachments?
platform: server
product: confluence
subcategory: faq
title: How do I find information about lost attachments?
---
# How do I find information about lost attachments?

You may like to use the [findattachments.jsp](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031805/2228583.jsp) which should detect missing attachments.

{{% note %}}

For Confluence 3.x, please download the corresponding script attached to <a href="#resolve-missing-attachments-in-confluence" class="unresolved">Resolve Missing Attachments in Confluence</a>

{{% /note %}}

Simply copy it to `confluence/admin/findattachments.jsp` and access it at `<confluence_base_url>/admin/findattachments.jsp`

Below is an example of the result generated by `http://<confluence\_base\_url>/admin/findattachments.jsp`

``` bash
Beginning search...
 Missing attachment: <path>/attachments/3477/279/1, filename: Final-OdysseyCodingConventions.doc, filetype: Word Document
```

As you can see in the above example, the script will report:

##### Location of the attachment missing

##### Full Name of the attachment

##### File type recognised :

-   PDF Document
-   Image
-   XML File
-   HTML Document
-   Text File
-   Word Document
-   Excel Spreadsheet
-   PowerPoint Presentation
-   Java Source File
-   Zip Archive
