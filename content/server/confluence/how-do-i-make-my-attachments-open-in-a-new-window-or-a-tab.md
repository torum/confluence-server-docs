---
aliases:
- /server/confluence/2031705.html
- /server/confluence/2031705.md
category: devguide
confluence_id: 2031705
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031705
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031705
date: '2017-12-08'
legacy_title: How do I make my attachments open in a new window or a tab?
platform: server
product: confluence
subcategory: faq
title: How do I make my attachments open in a new window or a tab?
---
# How do I make my attachments open in a new window or a tab?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I make my attachments open in a new window/tab?

You need to add a **TARGET = "\_blank"** to the &lt;a href&gt; HTML tag.

The **A** element used in HTML denotes an anchor which is a hypertext link.  
The **HREF** attribute specifies a hypertext link to another resource, such as an HTML document or a JPEG image.  
The **TARGET** attribute is used with frames to specify the frame in which the link should be rendered. If no frame with such a name exists, the link is rendered in a new window unless overridden by the user. Special frame names begin with  
an underscore. The frame used in this document is the **\_blank** which renders the link in a new, unnamed window.

*&lt;Source: http://www.w3.org/TR/html401/struct/links.html &gt;*

For example, by using the HTML code below, clicking on the link "a new window" will open the "newwindow.html" page in a new window:

``` xml
<A href="newwindow.html" _TARGET="_blank"_>a new window</A> 
```

## Open attachments listed for a Space

To open the attachments listed from the `Browse Space->Attachments` tab, in a new window, the `..\confluence\src\webapp\pages\listattachmentsforspace.vm` file under your &lt;Confluence-install&gt; directory has to be modified. Below are the listed steps:

1. Locate the following block of code in the **listattachmentsforspace.vm** file:

``` xml
foreach ($attachment in $pagedAttachments)
                    <tr #alternateRowColors() id="attachment_$attachment.id">
                        <td width="1%" nowrap valign="top"><a name="$generalUtil.urlEncode($attachment.content.realTitle)-attachment-$generalUtil.urlEncode($attachment.fileName)">#parse ("/pages/includes/attachment_icon.vm")</a> <a href="$req.contextPath$attachment.downloadPathWithoutVersion"     >$attachment.fileName</a></td>
                        <td width="1%" nowrap valign="top">$attachment.niceFileSize</td>
                        <td width="1%" nowrap valign="top">#usernameLink($attachment.creatorName) #if ($attachment.creatorName!=$attachment.lastModifierName) ($action.getText('last.modified.by') #usernameLink($attachment.lastModifierName)) #end</td>
                        <td width="1%" nowrap valign="top">$dateFormatter.format($attachment.lastModificationDate)</td>
                        <td>#contentLink2 ($attachment.getContent() true false)</td>
                    </tr>
                #end
```

2. In the line below:

``` xml
<td width="1%" nowrap valign="top"><a name="$generalUtil.urlEncode($attachment.content.realTitle)-attachment-$generalUtil.urlEncode($attachment.fileName)">
#parse ("/pages/includes/attachment_icon.vm")</a> <a href="$req.contextPath$attachment.downloadPathWithoutVersion"     >$attachment.fileName</a></td>
```

add the parameter **TARGET = "\_blank"** to the &lt;a href&gt; HTML tag, which will cause the URL specified in the href parameter to open in a new window or a new tag depending upon the option set in the browser. So the line above will be modified to:

``` xml
<td width="1%" nowrap valign="top"><a name="$generalUtil.urlEncode($attachment.content.realTitle)-attachment-$generalUtil.urlEncode($attachment.fileName)">
#parse ("/pages/includes/attachment_icon.vm")</a> <a href="$req.contextPath$attachment.downloadPathWithoutVersion"      TARGET = "_blank">$attachment.fileName</a></td>
```

## Open attachments listed for a Page

To open the page attachments listed from the Page's `Attachment(s)` tab, in a new window, the `..\confluence\src\webapp\pages\viewattachments.vm` file under your &lt;Confluence-install&gt; directory has to be modified. Below are the listed steps:

1. Locate the following block of code in the **viewattachments.vm** file:

``` xml
<td nowrap valign="top"><a name="$generalUtil.htmlEncode($generalUtil.urlEncode($page.title))-attachment-$generalUtil.htmlEncode($generalUtil.urlEncode($attachment.fileName))">#parse ("/pages/includes/attachment_icon.vm")</a> <a href="$generalUtil.htmlEncode("     ${req.contextPath}${attachment.downloadPathWithoutVersion}")"TARGET = "_blank">$generalUtil.htmlEncode($attachment.fileName)</a></td>
```

2. In the line below:

``` xml
<a name="$generalUtil.htmlEncode($generalUtil.urlEncode($page.title))-attachment-$generalUtil.htmlEncode($generalUtil.urlEncode($attachment.fileName))">
#parse ("/pages/includes/attachment_icon.vm")</a> <a href="$generalUtil.htmlEncode("     ${req.contextPath}${attachment.downloadPathWithoutVersion}")">$generalUtil.htmlEncode($attachment.fileName)</a>
```

add the parameter **TARGET = "\_blank"** to the &lt;a&gt; HTML tag, which will cause the URL specified in the href parameter to open in a new window or a new tag depending upon the option set in the browser. So the line above will be modified to:

``` xml
<a name="$generalUtil.htmlEncode($generalUtil.urlEncode($page.title))-attachment-$generalUtil.htmlEncode($generalUtil.urlEncode($attachment.fileName))">
#parse ("/pages/includes/attachment_icon.vm")</a> <a href="$generalUtil.htmlEncode("     ${req.contextPath}${attachment.downloadPathWithoutVersion}")" TARGET = "_blank">$generalUtil.htmlEncode($attachment.fileName)</a>
```
