---
aliases:
- /server/confluence/packaging-and-installing-a-theme-plugin-2031655.html
- /server/confluence/packaging-and-installing-a-theme-plugin-2031655.md
category: reference
confluence_id: 2031655
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031655
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031655
date: '2018-04-20'
legacy_title: Packaging and Installing a Theme Plugin
platform: server
product: confluence
subcategory: modules
title: Packaging and Installing a Theme plugin
---
# Packaging and installing a theme plugin

## What is the theme plugin module

The theme plugin module defines the theme itself. When administrators in Confluence select a theme either globally or within a space,
they select from the available theme modules.

``` xml
<theme key="dinosaurs" name="Dinosaur Theme"
       class="com.atlassian.confluence.themes.BasicTheme">
    <description>A nice theme for the kids</description>
    <colour-scheme key="com.example.themes.dinosaur:earth-colours"/>
    <layout key="com.example.themes.dinosaur:main"/>
    <layout key="com.example.themes.corporate:mail-template"/>
</theme>
```

The class of a theme must implement `com.atlassian.confluence.themes.Theme`. The `com.atlassian.confluence.themes.BasicTheme`
class that is provided with Confluence and represents a theme loaded from a Confluence plugin.

A theme can contain:

* An optional `colour-scheme` element that defines which colour-scheme module this theme will use.
* Any number of `layout` elements that define which layouts should be applied in this theme.

Refer to these modules by the *complete module key*.

A theme can use modules that aren't in the same plugin as the theme. Just keep in mind that if some plugin that the theme
depends on is removed your theme will be messed up.

## Installing the theme

Themes are installed as 'plugin modules'. The plugin module is a collection of files, usually zipped up in a JAR archive,
which tells Confluence how to find the decorators and colour-scheme of your theme.

You can use [plugins](/server/confluence/confluence-plugin-guide) in Confluence for many purposes, including themes.
The central configuration file that describes the plugin to Confluence is named `atlassian-plugin.xml`.

There are two steps to create the plugin module.

1.  Create a central configuration file for the theme – `atlassian-plugin.xml`.
1.  Create the JAR archive for your theme.

## Step 1. Write the `atlassian-plugin.xml` file

The structure of an `atlassian-plugin.xml` file is fairly self-explanatory. The next code segment is a full example of
an `atlassian-plugin.xml` file showing each of the decorators and colour-scheme in a way that Confluence can use to
override the default theme.
In other words, when a request is processed, this XML tells Confluence to look in certain locations for replacement decorators.

``` xml
<atlassian-plugin key="com.atlassian.confluence.themes.tabless" name="Plain Theme" plugins-version="2">
   <plugin-info>
       <description>
         This theme demonstrates a plain look and feel for Confluence.
         It is useful as a building block for your own themes.
       </description>
       <version>1.0</version>
       <vendor name="Atlassian Software Systems Pty Ltd" url="http://www.atlassian.com/"/>
   </plugin-info>

    <theme key="tabless" name="Tabless Theme" class="com.atlassian.confluence.themes.BasicTheme">
        <description>plain Confluence theme.</description>
        <layout key="com.atlassian.confluence.themes.tabless:main"/>
        <layout key="com.atlassian.confluence.themes.tabless:global"/>
        <layout key="com.atlassian.confluence.themes.tabless:space"/>
        <layout key="com.atlassian.confluence.themes.tabless:page"/>
        <layout key="com.atlassian.confluence.themes.tabless:blogpost"/>
        <layout key="com.atlassian.confluence.themes.tabless:mail"/>
        <colour-scheme key="com.atlassian.confluence.themes.tabless:earth-colours"/>
        // Optional: for themes which need configuration.
        <param name="space-config-path" value="/themes/tabless/configuretheme.action"/>
        <param name="global-config-path" value="/admin/themes/tabless/configuretheme.action"/>
    </theme>

    <layout key="main" name="Main Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/main.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/main.vmd"/>
    </layout>

    <layout key="global" name="Global Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/global.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/global.vmd"/>
    </layout>

    <layout key="space" name="Space Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/space.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/space.vmd"/>
    </layout>

    <layout key="page" name="Page Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/page.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/page.vmd"/>
    </layout>

    <layout key="blogpost" name="Blogpost Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/blogpost.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/blogpost.vmd"/>
    </layout>

    <layout key="mail" name="Mail Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/mail.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/mail.vmd"/>
    </layout>


    <colour-scheme key="earth-colours" name="Brown and Red Earth Colours"
                    class="com.atlassian.confluence.themes.BaseColourScheme">
         <colour key="topbar" value="#440000"/>
         <colour key="spacename" value="#999999"/>
         <colour key="headingtext" value="#663300"/>
         <colour key="link" value="#663300"/>
         <colour key="border" value="#440000"/>
         <colour key="navbg" value="#663300"/>
         <colour key="navtext" value="#ffffff"/>
         <colour key="navselectedbg" value="#440000"/>
         <colour key="navselectedtext" value="#ffffff"/>
    </colour-scheme>

</atlassian-plugin>
```

The class that each decorator or layout is mapped to must implement `com.atlassian.confluence.themes.VelocityDecorator`.

The layout entry must provide an `overrides` attribute, which defines which decorator the theme overrides within Confluence.

When telling Confluence to override a particular decorator with another one, the location of the custom decorator is specified. For example:

``` xml
<layout key="page" name="Page Decorator" class="com.atlassian.confluence.themes.VelocityDecorator"
            overrides="/decorators/page.vmd">
        <resource type="velocity" name="decorator"
                  location="com/atlassian/confluence/themes/tabless/page.vmd"/>
</layout>
```

The location attribute needs to be represented in the JAR archive you will use to bundle your theme.

## Step 2. Bundle the theme

1.  Place your decorators in a directory hierarchy in a way that makes sense to you.

    The `atlassian-plugin.xml` file should be placed in the resource directory, and then decorators should be placed in directories
    which make a meaningful division of what they do. The layout of the structure is up to you.
    You can even place each decorator alongside the `atlassian-plugin.xml` file.

    The essential thing is that location attribute of each decorator accurately tells Confluence how to load it.

    Thus, a recursive directory listing of the previous example theme gives:

    ``` bash
    atlassian-plugin.xml
    com/atlassian/confluence/themes/tabless/
    com/atlassian/confluence/themes/tabless/global.vmd
    com/atlassian/confluence/themes/tabless/space.vmd
    com/atlassian/confluence/themes/tabless/mail.vmd
    com/atlassian/confluence/themes/tabless/blogpost.vmd
    com/atlassian/confluence/themes/tabless/main.vmd
    com/atlassian/confluence/themes/tabless/page.vmd
    ```

1.  Now it is time to package your theme. As it was mentioned previously, a theme is a Confluence plugin,
so head to [Packaging and releasing your plugin](/server/framework/atlassian-sdk/packaging-and-releasing-your-plugin/)
page for more information.

### See also
* [Writing a Confluence theme](/server/confluence/writing-a-confluence-theme/).
