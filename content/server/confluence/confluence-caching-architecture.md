---
aliases:
- /server/confluence/confluence-caching-architecture-2031781.html
- /server/confluence/confluence-caching-architecture-2031781.md
category: devguide
confluence_id: 2031781
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031781
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031781
date: '2017-12-08'
guides: guides
legacy_title: Confluence Caching Architecture
platform: server
product: confluence
subcategory: learning
title: Confluence Caching architecture
---
# Confluence Caching architecture

The cache service provides centralised management of in-memory data caching within the Confluence application. Depending on which edition of Confluence you are running, the cache service may be backed by:

-   <a href="http://ehcache.sourceforge.net/" class="external-link">ehcache</a> (Cloud and Server editions),
-   <a href="http://www.hazelcast.org" class="external-link">Hazelcast</a> (Confluence Data Center 5.6 and later); OR
-   <a href="http://www.oracle.com/technology/products/coherence/index.html" class="external-link">Oracle Coherence</a> (clustered editions prior to Confluence 5.4).

Because of this, it is essential that you only code to the interfaces provided by Confluence, and do not rely on any of the concrete implementation classes.

## The CacheManager

The `cacheManager` bean in the Confluence Spring application context implements two interfaces: <a href="http://docs.atlassian.com/atlassian-cache-api/0.1/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html" class="external-link">CacheFactory</a> and <a href="http://docs.atlassian.com/atlassian-cache-api/0.1/atlassian-cache-api/apidocs/com/atlassian/cache/CacheManager.html" class="external-link">CacheManager</a>.

The only method on the manager you need to worry about (unless you are maintaining the caching system as a whole) is <a href="http://docs.atlassian.com/atlassian-cache-api/0.1/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html#getCache(java.lang.String)" class="external-link">DOC:CacheFactory#getCache(java.lang.String name)</a>. This method will return a <a href="http://docs.atlassian.com/atlassian-cache-api/0.1/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html" class="external-link">Cache</a>.

To prevent cache name clashes, we suggest using the same reverse domain name syntax for naming caches as you would for Java class names or plugin keys. You can provide a "friendly name" for the cache management UI by providing an I18N key: `cache.name.[DOC:cache name]`.

## Unflushable Caches

A small number of caches are configured to not be flushed by `CacheManager.flushCaches()`. These cache names are defined by the `nonFlushableCaches` bean in `cacheServiceContext.xml`. There is currently no plan to broaden this mechanism to allow plugin-specified caches to opt in to not being flushed.

## Differences Between Editions

The differences between the different editions of Confluence are:

-   Cloud and Server editions package the `confluence-cache-ehcache` module.
-   Data Center editions (5.6 and later) package the `confluence-cache-hazelcast`  module.
-   Clustered editions (5.4 and earlier) package the `confluence-cache-coherence` module.

These modules are contained in the `cache` subdirectory of the main Confluence source tree.

Exactly one of these modules must in exist in the Confluence classpath. Having multiple modules, or no module in the Confluence classpath will cause the system not to run.

## Implementation

There are a couple of different places the caching subsystem hooks into the rest of Confluence.

#### Bootstrapping

During bootstrapping, Confluence will try to load `/bootstrapCacheContext.xml` into the Spring context. This file will be found in one of the cache implementation jars. This context file is responsible for providing an implementation of the `ClusterManager`, `ClusterConfigurationHelper` and `HibernateCacheFactory`.

You can tell which edition of Confluence you are running by calling `ClusterManager#isClusterSupported`. This will return true in clustered editions, false otherwise.

#### Hibernate

Hibernate is configured to use the `ConfluenceCacheProvider` as its cache provider. This provider delegates to the `HibernateCacheFactory` (as defined in the bootstrap context above) to instantiate the correct cache implementation depending on the Confluence edition being run.

#### The Cache Manager

During main application startup, Confluence will try to load `/cacheProviderContext.xml` into the Spring context. This file will also be found in one of the cache implementation jars and is responsible for instantiating the correct implementation of the CacheManager.

#### The Cache Management UI

The user interface (and backing implementation) for viewing, flushing and adjusting the sizes of caches are implemented as plugins within each of the cache implementation jars.

## Gotchas

-   ehcache will log a warning if a cache is created for which it does not have an explicit configuration in `ehcache.xml (or cache-settings-overrides.properties `in Confluence 5.6 and later) We should ensure that there are no such warnings before releasing a new version of Confluence.
