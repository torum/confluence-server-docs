---
aliases:
- /server/confluence/user-macro-module-2031854.html
- /server/confluence/user-macro-module-2031854.md
category: reference
confluence_id: 2031854
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031854
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031854
date: '2018-04-13'
legacy_title: User Macro Module
platform: server
product: confluence
subcategory: modules
title: User Macro module
---
# User Macro module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.3 and later</p></td>
</tr>
</tbody>
</table>

{{% tip %}}

To create user macros without writing a plugin, define the user macro in the Confluence Administration Console.
For more information see <a href="https://confluence.atlassian.com/display/DOC/Writing+User+Macros" class="external-link">Writing user macros</a>.

{{% /tip %}}

## Adding a user macro plugin

User macro is a kind of Confluence plugin module.

*   For more information about plugins in general, read [Confluence plugin guide](/server/confluence/confluence-plugin-guide).
*   For an introduction to writing your own plugins, read [Writing confluence plugins](/server/confluence/writing-confluence-plugins).

## User macro plugin modules

With user macro plugin modules, developers can define simple macros directly in the `atlassian-plugin.xml` file
without writing any additional Java code. User macro plugin modules are functionally identical to user macros configured through
the administrative console, except that they can be packaged and distributed in the same way as normal plugins.

{{% note %}}

User macros installed as plugin modules *do not* appear in the user macro section of the administrative console, and
are not editable from within the user interface. They appear just as normal plugin modules in the plugin interface.

{{% /note %}}

### Configuring a macro plugin module

Configure entire macro plugin modules inside the `atlassian-plugin.xml` file as follows:

``` xml
<atlassian-plugin name='Hello World Macro' key='confluence.extra.helloworld' pluginsVersion='2'>
    <plugin-info>
        <description>Example user macro</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <user-macro name='helloworld' key='helloworld' hasBody='true' bodyType='raw' outputType='html'>
        <description>Hello, user macro</description>
        <template><![CDATA[Hello, $body and $paramexample !]]></template>
        <parameters>
            <parameter name="example" type="string"/>
        </parameters>      
    </user-macro>

    <!-- more macros... -->
</atlassian-plugin>
```

*   The `<template>` section is required. It defines the velocity template that renders the macro.
*   You can find all available velocity variables in <a href="https://confluence.atlassian.com/display/DOC/Writing+User+Macros" class="external-link">Writing user macros</a> tutorial.
*   The name and key of the macro must be specified the same way as in [Macro module](/server/confluence/macro-module).
*   No class attribute is required.
*   The attributes of the `user-macro` element match the configuration for user macros (see available attributes later on this page).
*   The `parameter` elements are the same as for [xhtml-macro](/server/confluence/including-information-in-your-macro-for-the-macro-browser).

{{% note %}}
If your macro doesn't define any `parameter`, you should leave `<parameters>` tag empty.
{{% /note %}}

### Available attributes

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute</p></th>
<th><p>Allowed Values</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>hasBody</p></td>
<td><ul>
<li><code>true</code> – the macro expects a body (i.e. {hello}World{hello}).</li>
<li><code>false</code> – the macro does not expect a body (i.e. Hello, {name})<br />
<em>Default:</em> false.</li>
</ul></td>
</tr>
<tr class="even">
<td><p>bodyType</p></td>
<td><ul>
<li><code>raw</code> – the body will not be processed before it is given to the template.</li>
<li><code>escapehtml</code> – HTML tags will be escaped before they are given to the template.</li>
<li><code>rendered</code> – the body will be rendered as wiki text before it is given to the template.<br />
<em>Default:</em> raw.</li>
</ul></td>
</tr>
<tr class="odd">
<td><p>outputType</p></td>
<td><ul>
<li><code>html</code> – the template produces HTML that should be inserted directly into the page.</li>
<li><code>wiki</code> – the template produces wiki text that should be rendered to HTML before it is inserted into the page.<br />
<em>Default:</em> html.</li>
</ul></td>
</tr>
</tbody>
</table>
