---
aliases:
- /server/confluence/rewrite-notes-33723303.html
- /server/confluence/rewrite-notes-33723303.md
category: devguide
confluence_id: 33723303
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33723303
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33723303
date: '2017-12-08'
legacy_title: Rewrite Notes
platform: server
product: confluence
subcategory: other
title: Rewrite Notes
---
# Rewrite Notes

-   Confluence Plugin Guide <img src="/server/confluence/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" />
    -   Internationalising Confluence Plugins
        -   This should not be the first item.  
        -   The content here is less than what I would expect.  It seems to be about creating the translations for existing plugins, and doesn't discuss the techniques for building in i18n support inside a plugin.  Perhaps the title should be "Extending supported languages for existing plugins".
    -   Writing Confluence Plugins
        -   The info box for "existing plugins and extensions" has a link to "Confluence Extensions" that goes nowhere.
        -   The document refers people to read the "Confluence Plugin Guide".  this is confusing as a first time reader won't realize that it's a pointer to the parent page which they probably just read.  I think that paragraph can just be deleted.  It's pretty obvious from the outline that you are not starting at the top, and a confused reader will jump to the parent using the outline.
        -   In the On this page section, item 2 is "Creating your Plugin descriptor".  In the outline, the next child page is "Creating your Plugin Descriptor".  It's really confusing to read a page and have sections jumping down to child pages.  It's much better to read a page, then go to the child page.  Or for confluence, use the include page macro (perhaps within an expand macro)
        -    
    -   The info "Confluence Extensions" to get to "existing plugins and extensions" links to "Writing Confluence Plugins". Very confusing.

 

Confluence Development

-   Internationalising Confluence Plugins should not be the first thing below 'Confluence Plugin Guide'
-   Perhaps move the tutorials for plugin development under Confluence Plugin Guide
