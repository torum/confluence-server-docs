---
aliases:
- /server/confluence/giants-color-theme-39368910.html
- /server/confluence/giants-color-theme-39368910.md
category: devguide
confluence_id: 39368910
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368910
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368910
date: '2017-12-08'
legacy_title: Giants Color Theme
platform: server
product: confluence
subcategory: other
title: Giants Color Theme
---
# Giants Color Theme

## Introduction

This tutorial will cover creating a plugin that provides a San Francisco Giants color theme for Confluence. The key learning is seeing how to deploy a simple module, in this case a theme module, through a plugin.

|                 |                                                                                                                                   |
|-----------------|-----------------------------------------------------------------------------------------------------------------------------------|
| Applicable To   | Confluence 4.x or higher                                                                                                          |
| Prerequisites   | This tutorial assumes that you completed the "[Giants Color Theme](/server/confluence/giants-color-theme.snippet)" tutorial |
| Time to Compete | 1.0 hour                                                                                                                          |

## Outline

## Create the plugin "giants-color-theme"

{{% warning %}}

In my instance, I used the include-page-with-replacment macro with the following substitution rule: /hello-world/giants-theme/

{{% /warning %}}

## Conventions

### Working Directory

For all of the tutorials, the working directory is called *atlas-development *and is located in the users home directory.

### Development Deployment Paradigm

The tutorials use QuickReload and expect to produce transformerless plugins.

### POM.xml

In the tutorials, there is a parent POM.xml file in the working directory.  The POM.xml file in the plugin directory has been configured to inherit from the POM.xml and all redundant entries have been removed.  Typically, only the artifact version, name, description and build instructions are included. 

### Development Environment

In order to keep things simple, the development has been done in a plain text editor, gvim.  Many find the [Eclipse IDE](/server/confluence/conventions.snippet) a more productive environment. 

## Open a Command Shell

The Atlassian SDK provides several tools that are designed to be launched from a command shell.  Using the table below which shows where in the application launcher for the various operating systems the command shell launcher is, launch the command shell

|         |                                                                 |
|---------|-----------------------------------------------------------------|
| Windows | Start -&gt; All Programs -&gt; Accessories -&gt; Command Prompt |
| OS X    | Applications -&gt; Utilities -&gt; Terminal                     |
| Linux   | Applications Menu -&gt; Accessories -&gt; Terminal              |

## Move to the Working Directory

 

1.  Open a terminal window and perform the following steps:
2.  Create the working directory for plugin development unless it already exists

    {{% note %}}

    While a working directory is not strictly necessary, it's a very good practice for reasons described later

    {{% /note %}}

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Windows</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>mkdir <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Linux/Mac</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ mkdir <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

    \* *

3.  Change into the Directory

    <table>
    <colgroup>
    <col style="width: 50%" />
    <col style="width: 50%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Windows</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>cd <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Linux/Mac </td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ cd <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

## Create a New Confluence Plugin

Enter the command **`atlas-create-confluence-plugin`**.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Windows</td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:</span>...<span class="op">&gt;</span> atlas<span class="op">-</span>create<span class="op">-</span>confluence<span class="op">-</span>plugin</div></code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Linux/Mac</td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>...<span class="at">$</span> <span class="at">atlas</span><span class="op">-</span>create<span class="op">-</span>confluence<span class="op">-</span>plugin</div></code></pre>
</div>
</div></td>
</tr>
</tbody>
</table>

The **`atlas-create-confluence-plugin`** tool will prompt you for various parameters:

| Parameter  | Description                                                                                                                                                | Example     |
|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|
| groupId    | A dot delimited string to create a namespace for plugin so that all of the modules can be uniquely identified even if another plugin users the same names. | com.example |
| artifactId | A string to uniquely identify the plugin with the **groupId** namespace                                                                                    | hello-world |
| version    | An arbitrary string to represent the version.                                                                                                              | 1.0.0       |
| package    | The package to use for the Java code. Note, dashes are not allowed in package names and the "." seperated strings will translate into a directory path.    | com.example |

 

The  tool will ask you to confirm your settings, press `y` to continue.

The tool then creates a **`helloworld`** folder with a basic plugin skeleton.

``` javascript
C:\Users\selberg\atlas-development>atlas-create-confluence-plugin
Executing: "C:\Applications\Atlassian\atlassian-plugin-sdk-6.2.2\apache-maven-3.2.1\bin\mvn.bat" com.atlassian.maven.plugins:maven-confluence-plugin:"6.2.1":create -gs C:\Applications\Atlassian\atlassian-plugin-sdk-6.2.2\apache-maven-3.2.1/conf/settings.xml
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=256M; support was removed in 8.0
[INFO] Scanning for projects...
[INFO]
[INFO] Using the builder org.apache.maven.lifecycle.internal.builder.singlethreaded.SingleThreadedBuilder with a thread count of 1
[INFO]
[INFO] ------------------------------------------------------------------------
[INFO] Building Maven Stub Project (No POM) 1
[INFO] ------------------------------------------------------------------------
[INFO]
[INFO] --- maven-confluence-plugin:6.2.1:create (default-cli) @ standalone-pom ---
[INFO] Google Analytics Tracking is enabled to collect AMPS usage statistics.
[INFO] Although no personal information is sent, you may disable tracking by adding <allowGoogleTracking>false</allowGoogleTracking> to the amps plugin configuration in your pom.xml
[INFO] Sending event to Google Analytics: AMPS:confluence - Create Plugin
[INFO] determining latest stable product version...
[INFO] using latest stable product version: 5.9.3
[INFO] determining latest stable data version...
[INFO] using latest stable data version: 5.6.6
Define value for groupId: : com.example
Define value for artifactId: : hello-world
Define value for version:  1.0.0-SNAPSHOT: : 1.0.0
Define value for package:  com.example: : com.example
Confirm properties configuration:
groupId: com.example
artifactId: hello-world
version: 1.0.0
package: com.example
 Y: :
[INFO] Setting property: classpath.resource.loader.class => 'org.codehaus.plexus.velocity.ContextClassLoaderResourceLoader'.
[INFO] Setting property: velocimacro.messages.on => 'false'.
[INFO] Setting property: resource.loader => 'classpath'.
[INFO] Setting property: resource.manager.logwhenfound => 'false'.
[INFO] Generating project in Batch mode
[INFO] Archetype repository missing. Using the one from [com.atlassian.maven.archetypes:confluence-plugin-archetype:RELEASE 
-> https://maven.atlassian.com/public] found in catalog internal
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 39,column 22] : ${atlassian.spring.scanner.version} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 46,column 22] : ${atlassian.spring.scanner.version} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 90,column 47] : ${atlassian.plugin.key} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 142,column 31] : ${project.groupId} is not a valid reference.
[WARNING] org.apache.velocity.runtime.exception.ReferenceException: reference : template = archetype-resources/pom.xml [line 142,column 50] : ${project.artifactId} is not a valid reference.
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 32.420 s
[INFO] Finished at: 2016-01-06T16:49:25-08:00
[INFO] Final Memory: 21M/328M
[INFO] ------------------------------------------------------------------------
```

{{% note %}}

The warning statements are a known issue and not a problem. See <a href="https://ecosystem.atlassian.net/browse/AMPS-57" class="uri external-link">https://ecosystem.atlassian.net/browse/AMPS-57</a> for more information.

{{% /note %}}

## (Optional) Remove the Java Code From the Plugin Scaffolding

The command, atlas-create-confluence-plugin, will provide hooks for testing and exporting a java api from the plugin. For simple plugins, such as one designed to deliver some javascript, these hooks are unnecessary complexity and can be removed.

### Removing Unnecessary Java Files

By default there will be a directory created for a plugin api at .../src/main/java/.../api and .../src/main/java/.../impl.  The api and impl directories can be deleted

{{% note %}}

If the default java files are removed, the test directories need to be removed as well as the tests will try and use the java code.

{{% /note %}}

### Removing the Test directories

Be default, there will be directories for test code under .../src/test/java and .../src/test/resources.  The java and resources directories can be deleted.

## (Optional) Link the plugin's POM.xml file to the Parent POM.xml

Unable to render {include} The included page could not be found.

## Launch the Development Instance of Confluence

{{% note %}}

Confluence may be launched from the plugin's folder or from a sibling plugin's folder if there is a parent POM.xml file. The recommended approach is to have a 'dummy' plugin named by the version of Confluece which gets instantiated.

{{% /note %}}

1.  Open a new command shell (so you can still execute commands in your existing shell)
2.  Change to the root directory of the plugin from which the development instance of Confluence will be launched.  
    *Note, the root directory of the plugin will contain the file "pom.xml" *

    |         |                                                          |
    |---------|----------------------------------------------------------|
    | Windows | c:&gt;cd "%HOMEDIR%%HOMEPATH%\\atlas-development\\5-9-4" |
    | Mac     | $ cd ~/atlas-development/5-9-4                           |

3.  Launch confluence

    ``` javascript
    $ atlas-run
    ```

    By default, the SDK instance of Confluence will be launched with the Developer Mode turned on. While this adds some extra debugging features, it slows Confluence down. If not needed, it can be turned off using the command: `atlas-run -Datlassian.dev.mode=false`

    [Read more about the Confluence Developer Mode](https://developer.atlassian.com/confdev/development-resources/confluence-developer-faq/enabling-developer-mode)

    {{% note %}}

    It can take quite some time for Confluence to get up and running. The very first time Confluence is launched through the SDK, it will download all of the necessary components. The second time, it will reuse what it downloaded early - but it still takes a while for Confluence to get up and running.

    When Confluence is ready, you will see the text:

    ``` javascript
    [INFO] confluence started successfully in XXXs at http://<myserver>:1990/confluence
    [INFO] Type Ctrl-D to shutdown gracefully
    [INFO] Type Ctrl-C to exit
    ```

    You may then open the url printed above (http://&lt;myserveer&gt;:1990/confluence) and login. The default administrator credentials are admin/admin.

    {{% /note %}}

## Log Into Confluence

To log into Confluence, visit <a href="http://localhost:1990/confluence" class="uri external-link">http://localhost:1990/confluence</a> in a web browser.  Login as the administrator (username = admin, password = admin)

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282494/image2015-9-28%2015%3A49%3A45.png?version=1&amp;modificationDate=1455032501047&amp;api=v2" class="confluence-external-resource" height="250" />

## Package the Plugin

{{% warning %}}

The instructions below expect QuickReload to be used. QuickReload has replaced FastDEV and the atlas-cli methods for building and pushing changes to Confluence. When Confluence launches, it will scan for plugin directories to watch for changes.  Thus, to update the plugin all that is required is to re-package it. If the plugin directory does not exist when Confluence is launched, it will not be watched.

If the development plugin requires transformation, an equivalent approach is to use the **pi** command from withing the command line interface (**atlas-cli**)

{{% /warning %}}

The command below, **atlas-package**, will compile all of the java code write the appropriate manifest files and package the plugin into an OSGi bundle suitable for uploading to Confluence

``` javascript
$ altas-package
```

If the package is successfully built, a message similar to the one below will be displayed.

``` javascript
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 9.611 s
[INFO] Finished at: 2016-02-01T15:21:40-08:00
[INFO] Final Memory: 51M/389M
[INFO] ------------------------------------------------------------------------
```

## (Optional) Validate the Install

In a web browser with Confluence displayed, select the "Add-ons" from the gear menu dropdown at the top of the browser window. 

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282685/image2015-9-28%2015%3A49%3A25.png?version=1&amp;modificationDate=1455047009147&amp;api=v2" class="confluence-external-resource" height="250" />

 

 From the "Manage add-ons" page, you should see your macro installed in the **User-installed add-ons** section.  If the plugin is in solid black text, it is installed and enabled.  If it is greyed out, it is installed by not enabled. 

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282685/image2015-9-28%2015%3A51%3A4.png?version=1&amp;modificationDate=1455046995053&amp;api=v2" class="confluence-external-resource" height="250" />

Add the color module to the **`atlassian-plugin.xml`** file

The file should be in the directory &lt;plugin-working-directoy&gt;\\giant-color-theme\\src\\main\\resources.  Open the file with a simple text editor and add the blocks below:

``` javascript
    <!-- Giants color theme -->
    <theme key="giants-color-theme" name="Giants Color Theme" class="com.atlassian.confluence.themes.BasicTheme">
        <description>A color scheme with the colors from the SF Giants baseball team.</description>
        
        <!-- Add in AND REFERENCE the default style sheet -->
        <body-class>lighter-editor theme-default aui-layout aui-theme-default giants-color-theme</body-class>
        <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
           <param name="source" value="webContext"/>
        </resource>

        <!-- Specify a custom default color scheme -->
        <colour-scheme key="${project.groupId}.${project.artifactId}:giants-color-scheme"/>

        <!-- Put a screenshot example 110x73 -->
        <!-- <resource key="icon" name="themeicon.gif" type="download" location="/images/screenshot.gif"/> -->
       
        <!-- recommended in the example -->
        <param name="includeClassicStyles" value="false"/>

        <!-- Enable the Atlassian sidebar -->
        <space-ia value="true"/>
    </theme>

    <!-- Giants color scheme -->
    <colour-scheme key="giants-color-scheme" name="Giants Color Scheme" class="com.atlassian.confluence.themes.BaseColourScheme">
        <colour key="property.style.topbarcolour" value="#000000"/>
        <colour key="property.style.topbarmenuitemtextcolour" value="#FFFDD0"/>
        <colour key="property.style.headerbuttonbasebgcolour" value="#FB5B1F"/>
        <colour key="property.style.headerbuttontextcolour" value="#000000"/>
        <colour key="property.style.topbarmenuselectedbgcolour" value="#333333"/>
        <colour key="property.style.topbarmenuselectedtextcolour" value="#FB5B1F"/>
        <colour key="property.style.menuitemselectedbgcolour" value="#000000"/>
        <colour key="property.style.menuitemselectedtextcolour" value="#FB5B1F"/>
        <colour key="property.style.menuselectedbgcolour" value="#000000"/>
        <colour key="property.style.menuitemtextcolour" value="#FB5B1F"/>
        <colour key="property.style.headingtextcolour" value="#000000"/>
        <colour key="property.style.spacenamecolour" value="#FB5B1F"/>
        <colour key="property.style.linkcolour" value="#FB5B1F"/>
        <colour key="property.style.bordercolour" value="#FB5B1F"/>
        <colour key="property.style.navbgcolour" value="#000000"/>
        <colour key="property.style.navtextcolour" value="#FFFDD0"/>
        <colour key="property.style.navselectedbgcolour" value="#000000"/>
        <colour key="property.style.navselectedtextcolour" value="#FB5B1F"/>
        <colour key="property.style.breadcrumbstextcolour" value="#FB5B1F"/>
        <colour key="property.style.searchfieldbgcolour" value="#FFFDD0"/>
        <colour key="property.style.searchfieldtextcolour" value="#000000"/>
    </colour-scheme>
```

## Update the plugin's POM.xml

The theme module depends on com.atlassian.confluence.themes.  For transformerless plugins, this needs to be explicitly added into the build instructions for the POM.

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  
    <artifactId>giants-color-theme</artifactId>
    <version>1.0.0</version>
    <name>giants-color-theme</name>
    <description>This is the com.example:giants-color-theme plugin for Atlassian Confluence.</description>
  
    <properties>        
        <!-- This key is used to keep the consistency between the key in atlassian-plugin.xml and the key to generate bundle. -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
    </properties>
    <modelVersion>4.0.0</modelVersion>
    <packaging>atlassian-plugin</packaging>
    <parent>
        <groupId>com.example</groupId>
        <artifactId>parent</artifactId>
        <version>1.0.0</version>
    </parent>
     <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-confluence-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <productVersion>${confluence.version}</productVersion>
                    <productDataVersion>${confluence.data.version}</productDataVersion>
                    <enableQuickReload>true</enableQuickReload>
                    <enableFastdev>false</enableFastdev>
                    <encoding>${encoding}</encoding>
                    <instructions>
                        <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>
 
                        <!-- Add package to export here -->
                        <Export-Package>
                        </Export-Package>
 
                        <!-- Add package import here -->
                        <Import-Package>
                            org.springframework.osgi.*;resolution:="optional",
                            org.eclipse.gemini.blueprint.*;resolution:="optional",
                            com.atlassian.confluence.themes,
                            *
                        </Import-Package>
 
                        <!-- Ensure plugin is spring powered - see https://extranet.atlassian.com/x/xBS9hQ  -->
                        <Spring-Context>*</Spring-Context>
                    </instructions>
                </configuration>
            </plugin>
        </plugins>
    </build>
 
</project>
```

## Re-package to Add in the New Changes

{{% warning %}}

The instructions below expect QuickReload to be used. QuickReload has replaced FastDEV and the atlas-cli methods for building and pushing changes to Confluence. When Confluence launches, it will scan for plugin directories to watch for changes.  Thus, to update the plugin all that is required is to re-package it. If the plugin directory does not exist when Confluence is launched, it will not be watched.

If the development plugin requires transformation, an equivalent approach is to use the **pi** command from withing the command line interface (**atlas-cli**)

{{% /warning %}}

The command below, **atlas-package**, will compile all of the java code write the appropriate manifest files and package the plugin into an OSGi bundle suitable for uploading to Confluence

``` javascript
$ altas-package
```

If the package is successfully built, a message similar to the one below will be displayed.

``` javascript
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 9.611 s
[INFO] Finished at: 2016-02-01T15:21:40-08:00
[INFO] Final Memory: 51M/389M
[INFO] ------------------------------------------------------------------------
```

## Set the Theme

Once the plugin is installed, visit Confluence's General Configuration page, then find the link for **Themes** on the left.  On that page, you can set the Confluence Theme to "Giants Color Theme" and it will pick up the new colors.  

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/17860831/image2016-2-10%201%3A46%3A56.png?version=1&amp;modificationDate=1455097616873&amp;api=v2" class="confluence-external-resource" height="250" />

The giants-color-theme loaded

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/17860831/image2016-2-10%201%3A44%3A2.png?version=1&amp;modificationDate=1455097443273&amp;api=v2" class="confluence-external-resource" height="250" />

The giants-color-theme applied

 

[&lt; Hello World](/server/confluence/hello-world.snippet)
