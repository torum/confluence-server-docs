---
aliases:
- /server/confluence/preventing-xss-issues-with-macros-in-confluence-4.0-2031864.html
- /server/confluence/preventing-xss-issues-with-macros-in-confluence-4.0-2031864.md
category: reference
confluence_id: 2031864
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031864
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031864
date: '2017-12-08'
legacy_title: Preventing XSS issues with macros in Confluence 4.0
platform: server
product: confluence
subcategory: modules
title: Preventing XSS issues with macros in Confluence 4.0
---
# Preventing XSS issues with macros in Confluence 4.0

|        |                                                                                    |
|--------|------------------------------------------------------------------------------------|
| Status | LEGACY This tutorial applies to Confluence versions that have reached end of life. |

In Confluence 4.0, the new XHTML renderer no longer has a 'render mode' for escaping the bodies passed to the macros with a plain text body, meaning that they will have to be escaped by the macro themselves.

You can use the `HtmlEscaper` static method `escapeAll` in order to escape the body of plain text, see the javadoc below for usage.

{{% note %}}

Replaces the HTML "special characters" &lt;, &gt;, ", ', and & with their equivalent entities in HTML 4 and returns the result. Also replaces the Microsoft "smart quotes" characters (extended ASCII 145-148) with their equivalent HTML entities.

Passing `true` for preserveExistingEntities will try to not break existing entities already found in the input string, by avoiding escaping ampersands which form part of an existing entity like &lt;. Passing `false` will do the normal behaviour of escaping everything.

`@param s` the String to escape  
`@param preserveExistingEntities` if `true`, will avoid escaping the ampersand in an existing entity like &lt;. If false, the method will do a normal escaping by replace all matched characters.  
@return the string with special characters replaced by entities.

{{% /note %}}

You will note in the above javadoc for `escapeAll` that quote is referred to as a special character in HTML. This is not strictly true (see the <a href="http://www.w3.org/TR/WD-html40-970708/sgml/entities.html" class="external-link">specification</a>) yet the quote does require special handling in order to prevent XSS attacks. Using something like `org.apache.commons.lang.StringEscapeUtils#escapeHtml(String)` instead of `escapeAll` will result in vulnerabilities as discussed in Apache Foundation issue <a href="https://issues.apache.org/jira/browse/LANG-572" class="external-link">LANG-572</a>.

