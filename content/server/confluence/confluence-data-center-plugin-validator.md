---
aliases:
- /server/confluence/confluence-data-center-plugin-validator-29466176.html
- /server/confluence/confluence-data-center-plugin-validator-29466176.md
category: devguide
confluence_id: 29466176
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29466176
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29466176
date: '2017-12-08'
legacy_title: Confluence Data Center Plugin Validator
platform: server
product: confluence
subcategory: faq
title: Confluence Data Center Plugin Validator
---
# Confluence Data Center Plugin Validator

Confluence Data Center is a clustered solution that provides High Availability (HA) compared to Confluence Server (which is a standalone installation). Running multiple instances of Confluence in a cluster places constraints on how plugins can be developed to work in a clustered environment.

The Confluence Data Center Plugin Validator tool will attempt to verify that the plugins installed into an existing Confluence installation do not contain any known issues which will prevent them from running under Confluence Data Center. This initial version of the tool finds where plugins are attempting to store non Serializable data into an Atlassian Cache. 

The tool has been developed using <a href="http://findbugs.sourceforge.net/" class="external-link">FindBugs</a> with a custom detector. The tool is a standalone JAR file that, on execution will:

1.  Extract the required files (a FindBugs distribution and Atlassian detector).
2.  Extract the plugin files from the specified Confluence database into the `plugins` directory.
3.  Run FindBugs on each of the plugins.
4.  Generate a summary report indicating whether each plugin passed or failed.

{{% note %}}

Please note: This tool is provided as-is without any form of warranty and is not supported by Atlassian Support.

{{% /note %}}

## Requirements

Running the tool requires:

-   A Java 6 runtime (or greater).
-   A working Confluence installation.
-   The credentials for the database used by Confluence (this is required to extract out the installed Plugins).

The tool currently supports the following databases systems:

-   PostgreSQL
-   Oracle
-   MySQL

## Download

The tool is available for download [cdc-plugin-validator-1.0.0.jar](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/29466176/29525066.jar).

The tool bundles a copy of <a href="http://findbugs.sourceforge.net/" class="external-link">FindBugs</a>, which is licensed under <a href="http://www.gnu.org/licenses/lgpl.html" class="external-link">Lesser GNU Public License</a>. More details are available at <a href="http://findbugs.sourceforge.net/manual/license.html" class="uri external-link">http://findbugs.sourceforge.net/manual/license.html</a>.

# Usage

The tool requires the following arguments when run:

| Argument        | Purpose                                         |
|-----------------|-------------------------------------------------|
| `-dbdriver`     | Specify the JDBC driver class                   |
| `-dbfile`       | Specify the JAR file containing the JDBC driver |
| `-dbpassword`   | Specify the database password                   |
| `-dburl`        | Specify the JDBC URL for the database           |
| `-dbuser`       | Specify the database userid                     |
| `-installation` | Specify the Confluence installation directory   |

An example invocation is:

``` bash
prompt% java -jar cdc-plugin-validator-1.0.0.jar \
     -installation /usr/local/atlassian-confluence-5.5.2 \
     -dbuser confuser -dbpassword confuser \
     -dburl jdbc:postgresql://localhost:5432/confluence \
     -dbdriver org.postgresql.Driver \
     -dbfile /usr/local/atlassian-confluence-5.5.2/confluence/WEB-INF/lib/postgresql-9.2-1002.jdbc4.jar
```

## Output

Running the tool generates a lot of output. 

``` bash
    [unzip] Expanding: /Users/oburn/work/cdc-tool-test/findbugs-2.0.3.zip into /Users/oburn/work/cdc-tool-test
   [delete] Deleting: /Users/oburn/work/cdc-tool-test/findbugs-2.0.3.zip
Exporting com.atlassian.analytics.analytics-client
    to ./plugins/com.atlassian.analytics.analytics-client--analytics-client-3.24.jar
fall back
Exporting com.atlassian.analytics.analytics-whitelist
    to ./plugins/com.atlassian.analytics.analytics-whitelist--analytics-whitelist-3.24.jar
fall back
Exporting com.atlassian.confluence.plugins.confluence-questions
    to ./plugins/com.atlassian.confluence.plugins.confluence-questions--confluence-questions-1.0.645.jar
fall back
Validating: ./plugins/com.atlassian.analytics.analytics-client--analytics-client-3.24.jar
 [findbugs] Executing findbugs FindBugsTask from ant task
 [findbugs] Running FindBugs...
 [findbugs] Missing classes: 24
 [findbugs] Calculating exit code...
 [findbugs] Setting 'missing class' flag (2)
 [findbugs] Exit code set to: 2
 [findbugs] Java Result: 2
 [findbugs] Classes needed for analysis were missing
 [findbugs] Output saved to /Users/oburn/work/cdc-tool-test/./plugins/com.atlassian.analytics.analytics-client--analytics-client-3.24.jar.report
Validating: ./plugins/com.atlassian.analytics.analytics-whitelist--analytics-whitelist-3.24.jar
 [findbugs] Executing findbugs FindBugsTask from ant task
 [findbugs] Running FindBugs...
 [findbugs] java.io.IOException: No files to analyze could be opened
 [findbugs]     at edu.umd.cs.findbugs.FindBugs2.execute(FindBugs2.java:275)
 [findbugs]     at edu.umd.cs.findbugs.FindBugs.runMain(FindBugs.java:393)
 [findbugs]     at edu.umd.cs.findbugs.FindBugs2.main(FindBugs2.java:1317)
 [findbugs] Java Result: 4
Validating: ./plugins/com.atlassian.confluence.plugins.confluence-jira-metadata--confluence-jira-metadata-1.7.3.jar
 [findbugs] Executing findbugs FindBugsTask from ant task
 [findbugs] Running FindBugs...
 [findbugs] Warnings generated: 2
 [findbugs] Missing classes: 6
 [findbugs] Calculating exit code...
 [findbugs] Setting 'missing class' flag (2)
 [findbugs] Setting 'bugs found' flag (1)
 [findbugs] Exit code set to: 3
 [findbugs] Java Result: 3
 [findbugs] Classes needed for analysis were missing
 [findbugs] Output saved to /Users/oburn/work/cdc-tool-test/./plugins/com.atlassian.confluence.plugins.confluence-jira-metadata--confluence-jira-metadata-1.7.3.jar.report
Validating: ./plugins/com.atlassian.confluence.plugins.confluence-questions--confluence-questions-1.0.645.jar
 [findbugs] Executing findbugs FindBugsTask from ant task
 [findbugs] Running FindBugs...
 [findbugs] Missing classes: 19
 [findbugs] Calculating exit code...
 [findbugs] Setting 'missing class' flag (2)
 [findbugs] Exit code set to: 2
 [findbugs] Java Result: 2
 [findbugs] Classes needed for analysis were missing
 [findbugs] Output saved to /Users/oburn/work/cdc-tool-test/./plugins/com.atlassian.confluence.plugins.confluence-questions--confluence-questions-1.0.645.jar.report

Summary:
========
PASS: ./plugins/com.atlassian.analytics.analytics-client--analytics-client-3.24.jar.report
PASS: ./plugins/com.atlassian.analytics.analytics-whitelist--analytics-whitelist-3.24.jar.report
FAIL: ./plugins/com.atlassian.confluence.plugins.confluence-jira-metadata--confluence-jira-metadata-1.7.3.jar.report
PASS: ./plugins/com.atlassian.confluence.plugins.confluence-questions--confluence-questions-1.0.645.jar.report
```

The important part of the output is the last part with the heading "Summary", where each extracted plugin is either flagged as a PASS or FAIL.

For example:

``` bash
Summary:
========
PASS: ./plugins/com.atlassian.analytics.analytics-client--analytics-client-3.24.jar.report
PASS: ./plugins/com.atlassian.analytics.analytics-whitelist--analytics-whitelist-3.24.jar.report
FAIL: ./plugins/com.atlassian.confluence.plugins.confluence-jira-metadata--confluence-jira-metadata-1.7.3.jar.report
PASS: ./plugins/com.atlassian.confluence.plugins.confluence-questions--confluence-questions-1.0.645.jar.report
```

In the example above the plugin "confluence-jira-metadata-1.7.3.jar" is flagged as failing. For more details, look at the contents of the file "./plugins/com.atlassian.confluence.plugins.confluence-jira-metadata--confluence-jira-metadata-1.7.3.jar.report".
