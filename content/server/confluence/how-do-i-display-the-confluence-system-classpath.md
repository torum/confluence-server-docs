---
aliases:
- /server/confluence/2031629.html
- /server/confluence/2031629.md
category: devguide
confluence_id: 2031629
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031629
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031629
date: '2017-12-08'
legacy_title: How do I display the Confluence System Classpath?
platform: server
product: confluence
subcategory: faq
title: How do I display the Confluence system classpath?
---
# How do I display the Confluence system classpath?

At times, you may see an error like this:

``` java
java.lang.NoSuchMethodError: org.apache.commons.fileupload.servlet.ServletFileUpload.setFileSizeMax
```

**Cause:** The Java classpath has another module (JAR) somewhere that overrides the one shipped with Confluence.

**Solution:**

1.  Check for duplicate JARs by going to the following URL:

    ``` xml
    http://path-to-confluence/admin/classpath.action
    ```

    The page lists all modules available to the class loader
      

2.  Scroll to the bottom of the page to make sure that there are no duplicate JARs, as indicated by the following message:  
      
    "No duplicate class files found in classpath JARs"

Resolve duplicate JARs if necessary.
