---
aliases:
- /server/confluence/14713072.html
- /server/confluence/14713072.md
category: devguide
confluence_id: 14713072
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=14713072
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=14713072
date: '2017-12-08'
legacy_title: How do I configure my Rich Text macro to receive unformatted input?
platform: server
product: confluence
subcategory: faq
title: How do I configure my Rich Text macro to receive unformatted input?
---
# How do I configure my Rich Text macro to receive unformatted input?

| **Applies To:** | Confluence 4.0 and later |
|-----------------|--------------------------|

When writing a bodied macro, developers can access the body content entered by the user in the `body` parameter of the Macro class' `execute` method.

``` java
public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException;
```

Developers may define their macro body as `RICH_TEXT` or `PLAIN_TEXT`. By default, the body of a Rich Text macro is automatically rendered by Confluence and converted from <a href="https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format" class="external-link">Confluence Storage Format</a> to rendered HTML *before* it is passed in to the macro's execute method. However, it is somtetimes necessary for a developer to access the un-rendered, raw Storage Format of the macro - and handle the rendering manually.

In earlier versions of Confluence, this was possible by setting the `getBodyRenderMode` method to return a value of `RenderMode.NO_RENDER`. However, this method is no longer available on the macro definition in Confluence 4.0 and later.

The new way to do this is by using the `RequiresFormat` annotation.  Add the `RequiresFormat` annotation to your `execute` method and set the value to `Storage`.  Here's a trivial example:

``` java
package com.example.test;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

import java.util.Map;

public class MyMacro implements Macro
{
    @Override
    public BodyType getBodyType() 
    {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    @Override
    @RequiresFormat(Format.Storage)
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException
    {
        return "Un-rendered macro body: " + body;
    }
}
```
