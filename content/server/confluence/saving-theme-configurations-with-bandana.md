---
aliases:
- /server/confluence/saving-theme-configurations-with-bandana-2031848.html
- /server/confluence/saving-theme-configurations-with-bandana-2031848.md
category: reference
confluence_id: 2031848
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031848
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031848
date: '2017-12-08'
legacy_title: Saving Theme Configurations with Bandana
platform: server
product: confluence
subcategory: modules
title: Saving Theme Configurations with Bandana
---
# Saving Theme Configurations with Bandana

To persist the configuration of a theme you can make use of the Bandana persistence framework.  
For example, the Left Navigation Theme uses the persister to store it's configuration values.

## Defining a Settings Bean

The recommended way of saving the settings, is to create a simple configuration bean which implements the Serializable interface.  
The bean for the Left Navigation Theme for example, simply consists of two String variables and their getter and setter methods.

``` java
package com.atlassian.confluence.extra.leftnavigation;
import java.io.Serializable;
public class LeftNavSettings implements Serializable
{
    private String space;
    private String page;


    public String getSpace()
    {
        return space;
    }

    public void setSpace(String space)
    {
        this.space = space;
    }

    public String getPage()
    {
        return page;
    }

    public void setPage(String page)
    {
        this.page = page;
    }
}
```

## Saving the Bean

Bandana can be used to save a configuration object with a given context, where the context refers to a space.  
The setValue function of the BandanaManager has three arguments:

-   @param **context** The context to store this value in
-   @param **key** The key of the object
-   @param **value** The value to be stored

``` java
// Create a setting bean.
LeftNavSettings settings = new LeftNavSettings();
settings.setSpace("example Space");
settings.setPage("example Page");

// Save the bean with the BandanaManager
bandanaManager.setValue(new ConfluenceBandanaContext(spaceKey), THEMEKEY, settings);
```

{{% note %}}

A Context can be defined on two levels:

-   Global: new ConfluenceBandanaContext()
-   Space level: new ConfluenceBandanaContext(spaceKey)

{{% /note %}}

## Retrieving the Bean

The configuration object can be retrieved by using bandanaManager.getValue. This method will get the configuration object, starting with the given context and looking up in the context hierarchy if no context is found.

-   @param context The context to start looking in
-   @param key The key of the BandanaConfigurationObject object
-   @return Object object for this key, or null if none exists.

``` java
LeftNavSettings settings = (LeftNavSettings) bandanaManager.getValue(new ConfluenceBandanaContext(spaceKey), THEMEKEY);
```
