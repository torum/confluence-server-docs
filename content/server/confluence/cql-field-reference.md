---
aliases:
- /server/confluence/cql-field-reference-29951939.html
- /server/confluence/cql-field-reference-29951939.md
category: reference
confluence_id: 29951939
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29951939
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29951939
date: '2018-04-26'
legacy_title: CQL Field Reference
platform: server
product: confluence
subcategory: api
title: CQL field reference
---
# CQL field reference

A field in CQL is a word that represents an indexed property of content in Confluence. In a clause, a field is followed by an [operator](/server/confluence/advanced-searching-using-cql#operator), which in turn is followed by one or more values (or [functions](/server/confluence/cql-function-reference#function)). The operator compares the value of the field
with one or more values or functions on the right, such that only true results are retrieved by the clause.

**List of Fields:**

*   [Ancestor](#ancestor)
*   [Container](#container)
*   [Content](#content)
*   [Created](#created)
*   [Creator](#creator)
*   [Contributor](#contributor)
*   [Favourite, favorite](#favourite-favorite)
*   [ID](#id)
*   [Label](#label)
*   [Last modified](#lastmodified)
*   [Macro](#macro)
*   [Mention](#mention)
*   [Parent](#parent)
*   [Space](#space)
	*   [Space category](#space-category)
	*   [Space key](#space-key)
	*   [Space title](#space-title)
	*   [Space type](#space-type)
*   [Text](#text)
*   [Title](#title)
*   [Type](#type)
*   [Watcher](#watcher)

#### Ancestor

Search for all pages that are descendants of a given ancestor page. This includes direct child pages and their descendants.
It is more general than the parent field.

###### Syntax

``` sql
ancestor
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find all descendant pages with a given ancestor page.

    ``` sql
    ancestor = 123
    ```

-   Find descendants of a group of ancestor pages.

    ``` sql
    ancestor in (123, 456, 789)
    ```

#### Container

Search for content that is contained in the content with the given ID.

###### Syntax

``` sql
container
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find attachments contained in a page with the given content ID.

    ``` sql
    container = 123 and type = attachment
    ```

-   Find content container in a set of pages with the given IDs.

    ``` sql
    container in (123, 223, 323)
    ```

#### Content

Search for content that have a given content ID.
This is an alias of the ID field.

###### Syntax

``` sql
content
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None  

###### Examples

-   Find content with a given content ID.

    ``` sql
    content = 123
    ```

-   Find content in a set of content IDs.

    ``` sql
    content in (123, 223, 323)
    ```

#### Created

Search for content that was created on, before, or after a particular date (or date range).

Note: search results will be relative to your configured time zone (which is by default the Confluence server time zone).

Use one of the following formats:

`"yyyy/MM/dd HH:mm"`  
`"yyyy-MM-dd HH:mm"`  
`"yyyy/MM/dd"`  
`"yyyy-MM-dd"`

###### Syntax

``` sql
created
```

###### Field Type

DATE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [endOfDay()](/server/confluence/cql-function-reference/#end-of-day)
-   [endOfMonth()](/server/confluence/cql-function-reference/#end-of-month)
-   [endOfWeek()](/server/confluence/cql-function-reference/#end-of-week)
-   [endOfYear()](/server/confluence/cql-function-reference/#end-of-year)
-   [startOfDay()](/server/confluence/cql-function-reference/#start-of-day)
-   [startOfMonth()](/server/confluence/cql-function-reference/#start-of-month)
-   [startOfWeek()](/server/confluence/cql-function-reference/#start-of-week)
-   [startOfYear()](/server/confluence/cql-function-reference/#start-of-year)

###### Examples

-   Find content created after the 1st September 2014.

    ``` sql
    created > 2014/09/01
    ```

-   Find content created in the last 4 weeks.

    ``` sql
    created >= now("-4w")
    ```

#### Creator

Search for content that was created by a particular user. You can search by the user's username.

###### Syntax

``` javascript
creator
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](/server/confluence/cql-function-reference/#current-user)

###### Examples

-   Find content created by jsmith.

    ``` sql
    created = jsmith
    ```

-   Find content created by john smith or bob nguyen.

    ``` sql
    created in (jsmith, bnguyen)
    ```

#### Contributor

Search for content that was created or edited by a particular user. You can search by the user's username.

###### Syntax

``` sql
contributor
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](/server/confluence/cql-function-reference/#current-user)

###### Examples

-   Find content created by jsmith.

    ``` sql
    contributor = jsmith
    ```

-   Find content created by john smith or bob nguyen.

    ``` sql
    contributor in (jsmith, bnguyen)
    ```

#### Favourite, favorite

Search for content that was favorited by a particular user. You can search by the user's username.

Due to security restrictions you are only allowed to filter on the logged in user's favourites.
This field is available in both British and American spellings.

###### Syntax

``` sql
favourite
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](/server/confluence/cql-function-reference/#current-user)

###### Examples

-   Find content that is favorited by the current user.

    ``` sql
    favourite = currentUser()
    ```

-   Find content favorited by jsmith, where jsmith is also the logged in user.

    ``` sql
    favourite = jsmith
    ```

#### ID

Search for content that have a given content ID.

###### Syntax

``` sql
id
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find content with the ID 123

    ``` sql
    id = 123
    ```

-   Find content in a set of content IDs.

    ``` sql
    id in (123, 223, 323)
    ```

#### Label

Search for content that has a particular label.

###### Syntax

``` sql
label
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find content that has the label 'finished'.

    ``` sql
    label = finished
    ```

-   Find content that doesn't have the label 'draft' or 'review'.

    ``` sql
    label not in (draft, review)
    ```

#### Last modified

Search for content that was last modified on, before, or after a particular date (or date range).

The search results will be relative to your configured time zone (which is by default the Confluence server time zone).

Use one of the following formats:

`"yyyy/MM/dd HH:mm"`  
`"yyyy-MM-dd HH:mm"`  
`"yyyy/MM/dd"`  
`"yyyy-MM-dd"`

###### Syntax

``` sql
lastmodified
```

###### Field Type

DATE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [endOfDay()](/server/confluence/cql-function-reference/#end-of-day)
-   [endOfMonth()](/server/confluence/cql-function-reference/#end-of-month)
-   [endOfWeek()](/server/confluence/cql-function-reference/#end-of-week)
-   [endOfYear()](/server/confluence/cql-function-reference/#end-of-year)
-   [startOfDay()](/server/confluence/cql-function-reference/#start-of-day)
-   [startOfMonth()](/server/confluence/cql-function-reference/#start-of-month)
-   [startOfWeek()](/server/confluence/cql-function-reference/#start-of-week)
-   [startOfYear()](/server/confluence/cql-function-reference/#start-of-year)

###### Examples

-   Find content that was last modified on 1st September 2014.

    ``` sql
    lastmodified = 2014-09-01
    ```

-   Find content that was last modified before the start of the year.

    ``` sql
    lastmodified < startOfYear()
    ```

-   Find content that was last modified on or after 1st September, but before 9am on 3rd September 2014.

    ``` sql
    lastmodified >= 2014-09-01 and lastmodified < "2014-09-03 09:00"
    ```

#### Macro

Search for content that has an instance of the macro with the given name in the body of the content.

###### Syntax

``` sql
macro
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content that has the Jira issue macro.

    ``` sql
    macro = jira
    ```

-   Find content that has Table of content macro or the widget macro.

    ``` sql
    macro in (toc, widget)
    ```

#### Mention

Search for content that mentions a particular user. You can search by the user's username.

###### Syntax

``` sql
mention
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](/server/confluence/cql-function-reference/#current-user)

###### Examples

-   Find content that mentions jsmith or kjones.

    ``` sql
    mention in (jsmith, kjones)
    ```

-   Find content that mentions jsmith.

    ``` sql
    mention = jsmith
    ```

#### Parent

Search for child content of a particular parent page.

###### Syntax

``` sql
parent
```

###### Field Type

CONTENT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

###### Examples

-   Find child pages of a parent page with ID 123.

    ``` sql
    parent = 123
    ```

#### Space

Search for content that is in a particular Space. By default, this searches by space key. You can also search by category, title, and type (see below). 

###### Syntax

``` sql
space
```

###### Field Type

SPACE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content in the development space or the QA space.

    ``` sql
    space in (DEV, QA)
    ```

-   Find content in the development space.

    ``` sql
    space = DEV
    ```

#### Space category

Search for spaces with a particular space category applied. Categories are used to organise spaces in the space directory. 

###### Syntax

``` sql
space.category
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find spaces that have the category 'development'

    ``` sql
    space.category = development
    ```

-   Find spaces that don't have the category 'marketing' or 'operations'

    ``` sql
    space.category not in (marketing, operations)
    ```
    
#### Space key

Search for spaces by space key. 

###### Syntax

``` sql
space.key
```

###### Field Type

STRING

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find the space that has the key 'DEV'

    ``` sql
    space.key = DEV
    ```

-   Find spaces that have either 'MKT' or 'OPS' or 'DEV'

    ``` sql
    space.key in (MKT, OPS, DEV)
    ```    
    
#### Space title

Search for spaces by title. 

###### Syntax

``` sql
space.title
```

###### Field Type

TEXT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find spaces with titles that match 'Development Team' (fuzzy match)

    ``` sql
    space.title ~ "Development Team"
    ```

-   Find spaces with titles that don't match "Project" (fuzzy match)

    ``` sql
    space.title !~ "Project"
    ```    

#### Space type

Search for spaces of a particular type. Supported content types are:

* personal
* global (also known as site spaces)
* favourite, favorite (also known as My Spaces) 

###### Syntax

``` sql
space.type
```

###### Field Type

TYPE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

None

###### Examples

-   Find only personal spaces

    ``` sql
    space.type = personal
    ```

-   Find only site / global spaces

    ``` sql
    space.type = global
    ```    
-   Find only site / favorite spaces

    ``` sql
    space.type = favorite
    ``` 

#### Text

This is a "master-field" that allows you to search for text across a number of other text fields.
These are the same fields used by Confluence's search user interface.

-   Title
-   Content body
-   Labels

Note: [Confluence text-search syntax](/server/confluence/performing-text-searches-using-cql) can be used with this field.

###### Syntax

``` sql
text
```

###### Field Type

TEXT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content that contains the word Confluence.

    ``` sql
    text ~ Confluence
    ```

-   Find content in the development space.

    ``` sql
    space = DEV
    ```

#### Title

Search for content by title, or with a title that contains particular text.

Note: [Confluence text-search syntax](/server/confluence/performing-text-searches-using-cql) can be used with this fields when used with the [CONTAINS](/server/confluence/advanced-searching-using-cql/#contains) operator ("~", "!~")

###### Syntax

``` sql
title
```

###### Field Type

TEXT

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find content with the title "Advanced Searching using CQL".

    ``` sql
    title = "Advanced Searching using CQL"
    ```

-   Find content that matches Searching CQL (i.e. a "fuzzy" match):

    ``` sql
    title ~ "Searching CQL"
    ```

#### Type

Search for content of a particular type. Supported content types are:

-   page
-   blogpost
-   comment
-   attachment

###### Syntax

``` sql
type
```

###### Field Type

TYPE

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

none

###### Examples

-   Find blogposts or pages.

    ``` sql
    type IN (blogpost, page)
    ```

-   Find attachments.

    ``` sql
    type = attachment
    ```

#### Watcher

Search for content that a particular user is watching. You can search by the user's username.

###### Syntax

``` sql
watcher
```

###### Field Type

USER

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Supported Functions

-   [currentUser()](/server/confluence/cql-function-reference/#current-user)

###### Examples

-   Search for content that you are watching.

    ``` sql
    watcher = currentUser()
    ```

-   Search for content that the user "jsmith" is watching.

    ``` sql
    watcher = "jsmith"
    ```
