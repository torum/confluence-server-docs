---
aliases:
- /server/confluence/lifecycle-module-2031667.html
- /server/confluence/lifecycle-module-2031667.md
category: reference
confluence_id: 2031667
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031667
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031667
date: '2017-12-08'
legacy_title: Lifecycle Module
platform: server
product: confluence
subcategory: modules
title: Lifecycle module
---
# Lifecycle module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.3 and later</p></td>
</tr>
</tbody>
</table>

Lifecycle plugins allow you to perform tasks on application startup and shutdown

## Application Lifecycle

**Startup** is performed after Confluence has brought up its Spring and Hibernate subsystems. If Confluence is being set up for the first time, the startup sequence is run *after* the completion of the setup wizard. This means that lifecycle plugins can assume access to a fully populated Spring container context, and a working database connection. (i.e. you don't need to check `isContainerSetup()` or `isSetupComplete()`)

**Shutdown** is performed when the application server is shutting down the web application, but before the Spring context is disposed of.

{{% note %}}

Plugin Activation and Deactivation

Activating or deactivating a lifecycle plugin will *not* cause any of its lifecycle methods to be run. If you want your plugin to respond to activation and deactivation, you should make sure it implements [Making your Plugin Modules State Aware](/server/confluence/making-your-plugin-modules-state-aware).

{{% /note %}}{{% warning %}}

Shutdown is not guaranteed

There are many situations in which the shutdown sequence will *not* be run, as it is dependent on the orderly shutdown of the application server. Plugins should *not* rely on shutdown being performed reliably, or even ever.

Shutdown lifecycle tasks are most useful for cleaning up resources or services that would otherwise leak in situations where the web application is being restarted, but the JVM is not exiting. (i.e. services that retain classloaders or threads that would otherwise prevent the application from being garbage-collected)

{{% /warning %}}

## Defining a Lifecycle Plugin

Lifecycle plugin definitions are quite simple. Here's a sample atlassian-plugin.xml fragment:

``` xml
<lifecycle key="frobozz" name="Frobozz Service" class="com.example.frobozz.Lifecycle" sequence="1200">
    <description>Start and stop the Frobozz service</description>
</lifecycle>
```

-   The **key** is the **required** plugin module key, which must be unique within the plugin.
-   The **name** is the **required** display name for the plugin.
-   The **class** is the **required** class name for the lifecycle service implementation.
-   The **sequence** number is **required**, and determines the order in which lifecycle plugins are run. On startup, they are run from lowest to highest sequence number, then in reverse order on shutdown.

## Defining a Lifecycle Service Implementation

If you are implementing a new lifecycle service, you should implement `com.atlassian.config.lifecycle.LifecycleItem`:

``` java
package com.atlassian.config.lifecycle;

public interface LifecycleItem
{
    /**
     * Called on application startup.
     *
     * @param context the application's lifecycle context
     * @throws Exception if something goes wrong during startup. No more startup items will be run, and the
     *         application will post a fatal error, shut down all LifecycleItems that have run previously, and
     *         die horribly.
     */
    void startup(LifecycleContext context) throws Exception;

    /**
     * Called on application shutdown
     *
     * @param context the application's lifecycle context
     * @throws Exception if something goes wrong during the shutdown process. The remaining shutdown items
     *         will still be run, but the lifecycle manager will log the error.
     */
    void shutdown(LifecycleContext context) throws Exception;
}
```

However, for convenience, and to make it easy to plug in third-party lifecycle events that are implemented as servlet context listeners, lifecycle service classes can instead implement `javax.servlet.ServletContextListener` - the `contextInitialized()` method will be called on startup, and `contextDestroyed()` on shutdown.

## Sequences

The sequence numbers of the lifecycle modules determine the order in which they are run. On startup, modules are run from lowest to highest sequence number, then on shutdown that order is reversed (first in, last out). As a general guideline:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Sequence number</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>0 - 500</p></td>
<td><p>Configuration tweaks and application sanity checks.</p></td>
</tr>
<tr class="even">
<td><p>800</p></td>
<td><p>Database and configuration upgrades.</p></td>
</tr>
<tr class="odd">
<td><p>1000</p></td>
<td><p>Zen Foundation configuration.</p></td>
</tr>
<tr class="even">
<td><p>5000</p></td>
<td><p>Start/stop the Quartz scheduler</p></td>
</tr>
</tbody>
</table>

-   If your startup lifecycle item has a sequence less than 800, you can't assume that the configuration or database schema are current
-   If you have a sequence number greater than 5000, you must keep in mind that scheduled jobs (including [Job Module](/server/confluence/job-module)) may run before you've started up, or after you've shut down.
