---
aliases:
- /server/confluence/language-module-2031778.html
- /server/confluence/language-module-2031778.md
category: reference
confluence_id: 2031778
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031778
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031778
date: '2017-12-08'
legacy_title: Language Module
platform: server
product: confluence
subcategory: modules
title: Language module
---
# Language module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.2 and later</p></td>
</tr>
</tbody>
</table>

To run Confluence in another language, you must install a language pack plugin for that translation. Guides and tools for collaboratively creating translations have been made available to the Confluence community.

{{% note %}}

This page provides a technical overview of plugins, for users interested in [creating](/server/confluence/creating-a-new-confluence-translation) or [updating](/server/confluence/updating-a-confluence-translation) a translation. To install a translation, please see <a href="#installing-a-language-pack" class="unresolved">Installing a Language Pack</a>.

{{% /note %}}{{% tip %}}

[Translations for the Rich Text Editor](/server/confluence/translations-for-the-rich-text-editor) can be part of a Confluence language pack plugin.

{{% /tip %}}

## Language Pack Overview

Language plugins are placed in the `<CONFLUENCE-INSTALL-DIRECTORY>/languages/<KEY>` directory, where `<KEY>` is the international language identifier. They consist of three files:

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 75%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Filename</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Language Plugin Descriptor</p></td>
<td><p><code>atlassian-plugin.xml</code></p>
<p><strong>Purpose:</strong> Defines language settings in <code>language</code> tag.</p>
<p><strong>Location:</strong> ./src/etc</p></td>
</tr>
<tr class="even">
<td><p>ConfluenceActionSupport Properties File</p></td>
<td><p><code>ConfluenceActionSupport&lt;KEY&gt;.properties</code></p>
<p><strong>Purpose:</strong> Contains text strings in <code>key:value</code> mapping.</p>
<p><strong>Location:</strong> ./src/etc/com/atlassian/confluence/core</p></td>
</tr>
<tr class="odd">
<td><p>Flag Image</p></td>
<td><p><code>&lt;KEY&gt;.png</code></p>
<p><strong>Purpose:</strong> Contains flag image for country.</p>
<p><strong>Location:</strong> ./src/etc/com/atlassian/confluence/core</p></td>
</tr>
</tbody>
</table>

### Directory Structure

The location of the three files that compose a Language Pack plugin is as follows:

``` bash
./src/etc/com/atlassian/confluence/<PATH_OF_PROPERTIES_FILE>
./src/etc/templates/languages/<LANGUAGE_KEY>/<LANGUAGE_KEY>.gif
./src/etc/atlassian-plugin.xml
```

As an example, this is the directory listing of the German translation ("de\_DE"):

``` bash
./confluence-2.2-std/plugins/de_DE/src
./confluence-2.2-std/plugins/de_DE/src/etc
./confluence-2.2-std/plugins/de_DE/src/etc/atlassian-plugin.xml
./confluence-2.2-std/plugins/de_DE/src/etc/com
./confluence-2.2-std/plugins/de_DE/src/etc/com/atlassian
./confluence-2.2-std/plugins/de_DE/src/etc/com/atlassian/confluence
./confluence-2.2-std/plugins/de_DE/src/etc/com/atlassian/confluence/core
./confluence-2.2-std/plugins/de_DE/src/etc/com/atlassian/confluence/core/ConfluenceActionSupport_de_DE.properties
./confluence-2.2-std/plugins/de_DE/src/etc/templates
./confluence-2.2-std/plugins/de_DE/src/etc/templates/languages
./confluence-2.2-std/plugins/de_DE/src/etc/templates/languages/de_DE
./confluence-2.2-std/plugins/de_DE/src/etc/templates/languages/de_DE/de_DE.gif
```

## Language Plugin Structure

The three components of a plugin must be updated for each translation. The following sections describe updating the language plugin descriptor, flag image and ConfluenceActionSupport properties file.

### Defining The Language Plugin Descriptor

This is an example `atlassian-plugin.xml` file for a Language Pack plugin for German:

``` xml
<atlassian-plugin name='German language pack' key='confluence.languages.de_DE'>
    <plugin-info>
        <description>This plugin contains translations for the German language</description>
        <vendor name="Atlassian Software Systems" url="http://www.atlassian.com"/>
        <version>1.0</version>
    </plugin-info>

    <language name="German" key="de_DE" language="de" country="DE">
        <!-- Define a flag that will be shown for this language -->
        <resource name="de_DE.gif" type="download" location="templates/languages/de_DE/de_DE.gif">
            <property key="content-type" value="image/gif"/>
        </resource>
    </language>
</atlassian-plugin>
```

#### Language Plugin Descriptor Attributes

The `atlassian-plugin.xml` file declares the language being bundled using the following attributes:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>language</p></td>
<td><p>The language being defined</p></td>
</tr>
<tr class="even">
<td><p>country</p></td>
<td><p>The country the language belongs to</p></td>
</tr>
<tr class="odd">
<td><p>variant</p></td>
<td><p>The variant of the language</p></td>
</tr>
</tbody>
</table>

**\*language attribure is required.**

These values are based off those defined in the <a href="http://java.sun.com/j2se/1.4.2/docs/api/java/util/Locale.html" class="external-link">java.util.Locale</a> class. For information on the valid values for the `language`, `country` and `variant` attributes, please see the `java.util.Locale` documentation.

The `key` attribute is an aggregation of the the three previous attributes, in the same format as that of <a href="http://java.sun.com/j2se/1.4.2/docs/api/java/util/Locale.html" class="external-link">java.util.Locale</a>: `language[DOC:_country][DOC:_variant]`

### Flag Images

Language packs define a flag that is to be used to represent the language. The `atlassian-plugin.xml` defines the language property:

``` xml
<resource name="en_AU.gif" type="download" location="templates/languages/en_AU/en_AU.gif">
    <property key="content-type" value="image/gif"/>
</resource>
```

When selecting a language, the flag defined above will be displayed. Additionally, the flag will appear during the setup process.

### ConfluenceActionSupport Properties File

This Java Properties file contains key-value pairs for each string in Confluence, and supports variables. For example:

``` java
remove.all.name=Remove All
view.mail.thread.desc.full=Entire Thread (Showing {0} of {1})
```
