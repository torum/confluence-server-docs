---
aliases:
- /server/confluence/2031825.html
- /server/confluence/2031825.md
category: devguide
confluence_id: 2031825
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031825
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031825
date: '2017-12-08'
legacy_title: How Do I find enabled and disabled plugins in the Database?
platform: server
product: confluence
subcategory: faq
title: How Do I find enabled and disabled plugins in the database?
---
# How Do I find enabled and disabled plugins in the database?

### Enabled Plugins

Plugins from the repository, once installed are stored in table PLUGINDATA. They are enabled after install.

### Disabled Plugins

All Plugins (bundled and from the repository) that have been disabled have an entry in table BANDANA where BANDANAKEY is plugin.manager.state.Map.  
For Example if the pagetree macro had been installed but is currently disabled would be reflected in BANDANAVALUE

``` xml
<map>
  <entry>
    <string>bnpparibas.confluence.pagetree</string>
    <boolean>false</boolean>
  </entry>
</map>
```

{{% note %}}

It turns out that the &lt;boolean&gt; expression is not really evaluated. When the plugin name is present in the map it is considered as **disabled**

{{% /note %}}
