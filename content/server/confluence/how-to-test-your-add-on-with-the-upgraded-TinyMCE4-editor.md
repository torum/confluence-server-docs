---
category: devguide
date: '2018-05-07'
platform: server
product: confluence
subcategory: learning
guides: development resources
title: "How to test your add-on with the upgraded TinyMCE 4 editor"
---
# How to test your add-on with the upgraded TinyMCE 4 editor

We're currently working on upgrading TinyMCE from version 3 to version 4.  As of Confluence 6.8, the upgrade is in a state where you can start testing it with your add-ons. If you have an add-on that interacts with the editor in any way, we strongly recommend you begin testing your add-on with the upgraded editor as soon as possible.  

## Timing

We don't have a set release date for the upgrade, but are tentatively targeting Confluence 6.11. We hope that there will be minimal, if any, changes required to add-ons, but there might be some things we don't expect. 

Until the version 4 upgrade ships in Confluence, we recommend testing your add-ons with both version 3 and version 4 to make sure it is still working fine in each upcoming Confluence version. Once we know exactly which version we plan to turn on the version 4 editor, we'll let you know, but we encourage you to start testing now.  

## Manual testing

To test your add-on, spin up a normal Confluence instance (6.8 or later), go to `<confluence-url>/admin/darkfeatures.action`, and add the dark feature `frontend.editor.v4`. Then, head to the editor and check that your add-on is working as expected.

## Automated tests

For any automated tests, you can spin up Confluence with the V4 editor enabled by adding the following to the JVM args used to start Confluence: `-Datlassian.darkfeature.frontend.editor.v4=true`. For example:

``` java
<configuration>
    ...
    <jvmArgs>-Datlassian.darkfeature.frontend.editor.v4=true</jvmArgs>
</configuration>
```

With automated tests, we recommend continuing to run without the dark feature enabled, and add a copy of the build with the feature enabled so that you can test both versions until v4 becomes the default.

## Does your add-on load the editor?

Does your add-on load the editor in its own views? If so, you will need to do the following once the v4 upgrade is rolled out.

### Editor context

Load additional web-resource context: editor-v4 along with the existing editor. 

For example, if you're using `requireResourcesForContext('editor')`, you should use `requireResourcesForContext('editor-v4,editor');`.

This only applies when `frontend.editor.v4` is enabled. Technically, in v3 you should 
use `requireResourcesForContext('editor-v3,editor');`, but this will generally not be necessary. You can use the `DarkFeaturesManager` in java or the `confluence/dark-features` AMD module in JS to load either v3 or v4 based on the `frontend.editor.v4` dark feature, to be ready for both. For example:

###### DarkFeaturesManager in Java

``` java
darkFeaturesManager.getDarkFeatures().isFeatureEnabled("frontend.editor.v4") // boolean
```

###### confluence/dark-features AMD module in JavaScript

``` javascript
require('confluence/dark-features').isEnabled("frontend.editor.v4") // boolean
```

### Editor tear down

If you also handle destroying the editor, make sure to use the `mceRemoveEditor` command:

``` javascript
// TinyMCE 4 API
tinymce.execCommand('mceRemoveEditor', true, 'wysiwygTextarea');
// TinyMCE 3 API
tinymce.execCommand('mceRemoveControl', true, 'wysiwygTextarea');
```

### Leverage TinyMCE 4.x API

Although the 3.x API is available using a polyfill TinyMCE plugin, we encourage you to use the newer API once v4 becomes the default. 
See [www.tinymce.com/docs](https://www.tinymce.com/docs/). 

### Reporting issues

If you encounter issues with your add-on in TinyMCE v4, please help us by doing some investigation into what is causing your add-on to break, then raise an [EAP feedback ticket](https://jira.atlassian.com/secure/CreateIssueDetails!init.jspa?pid=10470&issuetype=10000&components=43692&summary=Confluence+6.8++Editor+EAP+Feedback). You'll need to be logged in to [jira.atlassian.com](http://jira.atlassian.com) for this link to work. 

