---
aliases:
- /server/confluence/confluence-in-app-notifications-and-tasks-api-14712988.html
- /server/confluence/confluence-in-app-notifications-and-tasks-api-14712988.md
category: devguide
confluence_id: 14712988
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=14712988
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=14712988
date: '2017-12-08'
legacy_title: Confluence in-app notifications and tasks API
platform: server
product: confluence
subcategory: updates
title: Confluence in-app notifications and tasks API
---
# Confluence in-app notifications and tasks API

{{% note %}}

Confluence has a new [REST API](/server/confluence/confluence-server-rest-api) that is progressively replacing our existing APIs. We recommend plugin developers use the new REST APIs where possible.

{{% /note %}}

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 4.3 and later.</p></td>
</tr>
<tr class="even">
<td><p>Changed:</p></td>
<td><p>In Confluence 4.3.3 we announced full support for the in-app notifications and tasks APIs - the API is no longer experimental. This release also introduces better internationalisation support for plugins.</p></td>
</tr>
</tbody>
</table>

Confluence 4.3 introduced the workbox, for managing notifications in the app. This includes REST and Java APIs for interacting programmatically with the in-app notifications and tasks.

Here are some resources to help you get started.

## Reference documentation

Generated reference guides:

-   <a href="http://docs.atlassian.com/mywork-confluence-host-plugin/REST/" class="external-link">Confluence Notifications and Tasks REST API</a>
-   <a href="http://docs.atlassian.com/mywork-api/" class="external-link">Confluence Notifications and Tasks Java API</a>

## Sample code

Tutorials and examples:

-   [Posting Notifications in Confluence](https://developer.atlassian.com/display/CONFDEV/Posting+Notifications+in+Confluence)
-   <a href="https://bitbucket.org/cofarrell/mywork-example" class="external-link">A sample project using notifications and tasks</a>

## Giving us your feedback

You can provide feedback for the API by raising an issue in the <a href="https://jira.atlassian.com/browse/CONF" class="external-link">Confluence project on our issue tracker</a>.
