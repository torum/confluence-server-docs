---
aliases:
- /server/confluence/atlassian-cache-2-overview-27561089.html
- /server/confluence/atlassian-cache-2-overview-27561089.md
category: devguide
confluence_id: 27561089
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=27561089
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=27561089
date: '2018-04-27'
guides: guides
legacy_title: Atlassian Cache 2 Overview
platform: server
product: confluence
subcategory: learning
title: Atlassian Cache 2 overview
---
# Atlassian Cache 2 overview

## Introduction

Atlassian Cache 2 is a version of the Atlassian Cache API (atlassian-cache-api-2.x) that should be used within
Atlassian products (initially Jira 6.2 and Confluence 5.5). The API is designed to be backwardly compatible
with atlassian-cache-api-0.1.

The main interfaces in the API from an end-user perspective are:

*   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html"
class="external-link">Cache</a> – represents a cache.
*   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheLoader.html" class="external-link">CacheLoader</a> – used with cache to provide read-through semantics (see Cache Population later on this page).
*   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CachedReference.html"
class="external-link">CachedReference</a> – represents a resettable reference that is backed by a cache.
*   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Supplier.html"
class="external-link">Supplier</a> – used with a `CachedReference` to generate the referenced object.
*   <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html" class="external-link">CacheFactory</a> –
responsible for creating cache and `CachedReference`  
instances that may be configured using <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettings.html" class="external-link">CacheSettings</a>.

## Cache types

Cache API supports three distinct types of cache:

1.  Local – the data in a local cache is held on the JVM. The keys and values are stored by reference.
1.  Cluster – the data in a cluster cache is replicated across the cluster. The keys and values are stored by value
and must be serializable (this is only enforced at run time).
1.  Hybrid – the data in a hybrid cache is stored locally on each node in a cluster and is kept synchronized via
invalidation messages. The keys and values are stored by reference; however, the keys must be serializable
(this is only enforced at run time).

For more information on how to specify the type of cache, see cache creation later on this page.

## Cache population

For populating the data in cache, the Cache API supports two styles:

1.  Read-through cache semantics where values for cache misses are retrieved using a supplied 
`CacheLoader` when the cache is created. This is the preferred style for new development.
1.  Traditional put-style semantics where values are put into the cache using the 
<a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html#put(K,%20V)" class="external-link">put(K, V)</a> 
and <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html#putIfAbsent(K,%20V)" class="external-link">
putIfAbsent(K, V)</a> methods. This is supported for backwards compatibility.

{{% warning %}}

Any implementation of `CacheLoader` that is associated with a nonlocal cache must not make calls to any other nonlocal 
cache or `CachedReference`.

This limitation also applies to any implementation of `Supplier` that is associated with a non-local `CachedReference`.

{{% /warning %}}

## Cache eviction

The Cache API supports two rules for eviction of values from cache, which are specified in the `CacheSettings`:

1.  <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html#expireAfterAccess(long,%20java.util.concurrent.TimeUnit)" class="external-link">expireAfterAccess(long, java.util.concurrent.TimeUnit)</a> –
evicts entries that have not been accessed for a specified duration.
1.  <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html#expireAfterWrite(long,%20java.util.concurrent.TimeUnit)" class="external-link">expireAfterWrite(long, java.util.concurrent.TimeUnit)</a> –
evicts entries that have not been updated since a specified duration.

To be precise, the rules are actually hints to the underlying cache provider that may or may not be followed.
The user of the cache should be prepared to handle their values being evicted at any time.

## Cache creation

When creating cache, you can provide `CacheSettings` to control the configuration of cache, which are created
using the `CacheSettingsBuilder`. See <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html" class="external-link">CacheSettingsBuilder</a> for all the settings that can be specified.

When settings are not explicitly specified, the product specific default will be used. See each product's
documentation for detailed information on how defaults are determined.

The following table describes the how the different cache types are created based on the two properties <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html#remote()" class="external-link">remote()</a> and <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheSettingsBuilder.html#replicateViaCopy()" class="external-link">replicateViaCopy()</a>.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 40%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="header">
<th><strong>remote()</strong></th>
<th><strong>replicateViaCopy()</strong></th>
<th><strong>Cache Type Created</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>false</td>
<td>false</td>
<td>Local</td>
</tr>
<tr class="even">
<td>false</td>
<td>true</td>
<td>Local</td>
</tr>
<tr class="odd">
<td>true</td>
<td>false</td>
<td>Hybrid</td>
</tr>
<tr class="even">
<td>true</td>
<td>true</td>
<td>Cluster</td>
</tr>
</tbody>
</table>

{{% warning %}}

When creating a hybrid cache, pay extra attention to the following points:

{{% /warning %}}

*  Make sure to associate a `CacheLoader` with the cache using <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html#getCache(java.lang.String,%20com.atlassian.cache.CacheLoader)" class="external-link">getCache(String, CacheLoader)</a> or <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CacheFactory.html#getCache(java.lang.String,%20com.atlassian.cache.CacheLoader,%20com.atlassian.cache.CacheSettings)" class="external-link">getCache(String, CacheLoader, CacheSettings)</a>
methods when obtaining the cache. If `CacheLoader` is not associated, and the <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html#put(K,%20V)" class="external-link">put(K, V)</a> 
or <a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/Cache.html#putIfAbsent(K,%20V)" class="external-link">
putIfAbsent(K, V)</a> are used, then the cache will not operate correctly in a cluster, as each put operation
will invalidate local entries in all other nodes in the cluster.
* Whenever cache is created in a way described previously, the returned cache object should be retained for future use.
If the returned cache object is not retained for reuse, then every subsequent call to obtain a new cache object
will result in the current contents of the underlying cache to be evicted.

## Cache data sharing guidelines

This section provides guidelines for developers wanting to share data stored in cache. Sharing can be between:

*   Different plugins.
*   Different versions of a plugin (after an upgrade or downgrade).

### Local cache data

Local cache keys and values are stored by reference. Therefore, to ensure that the data can be accessed between
plugins and plugin restarts, use only the classes provided by one of the following:

*   Java JDK (e.g. `java.lang.String`).
*   The product core (such as Confluence core classes).

Note that a plugin can serialize custom classes to a portable format that is stored as a `java.lang.String` or `byte[]`.

### Cluster cache data

Cluster cache keys and values are stored by value, which means they are serialized on insertion to the cache,
and deserialized on extraction.

Therefore, to ensure that the data can be shared, both product and plugin developers must ensure that the objects
placed into a cluster cache implement serialization with support for versioning. See <a href="http://docs.oracle.com/javase/7/docs/platform/serialization/spec/version.html" class="external-link">Versioning of serializable objects</a>.

It's recommended that plugin developers use classes from the Java JDK and product core. If custom classes are
required to be shared between plugins, then a common shared library should be used for defining them.

### Hybrid cache data

Hybrid cache keys are stored by value, and values are stored by reference.

Therefore:

1.   Keys should follow the guidelines in *Cluster Cache data*.
1.   Values should follow the guidelines in *Local Cache data*.

### Cache code examples

The following example shows how to obtain a cache called `old-style` using defaults.

``` java
Cache<String, String> cache = cacheManager.getCache("old-style");

// Returns null
cache.get("money");
```

The following example shows how to obtain a cache called `with-loader` that uses a `CacheLoader`.

``` java
Cache<String, String> cache =
    cacheManager.getCache(
    "with-loader",
    new CacheLoader<String, String>() {
        public String load(String key) {
            return "value for " + key;
        }
    });

// Returns "value for money"
cache.get("money");
```

The following example shows how to obtain a hybrid cache called `adam` that expires values after 60 seconds of no access.

``` java
CacheSettings required =
    new CacheSettingsBuilder().
        remote().
        expireAfterAccess(60, TimeUnit.SECONDS).
        build();

Cache<String, String> c =
    cacheManager.getCache("adam", someCacheLoader, required);
```

## `CachedReference` code examples

The following example shows how to use a 
<a href="https://docs.atlassian.com/atlassian-cache-api/2.2.0/atlassian-cache-api/apidocs/com/atlassian/cache/CachedReference.html"
class="external-link">CachedReference</a>.

``` java
Supplier<Date> generateTheAnswer = new Supplier<>()
{
    public Date get()
    {
        return new Date();
    }
}

CachedReference<Date> cr =
    factory.getCachedReference(
        "mycache", generateTheAnswer);
cr.get(); // will call generateTheAnswer
cr.get(); // will get from the cache
cr.reset();
cr.get(); // will call generateTheAnswer
```
