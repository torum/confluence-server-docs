---
aliases:
- /server/confluence/content-template-module-24084713.html
- /server/confluence/content-template-module-24084713.md
category: reference
confluence_id: 24084713
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24084713
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24084713
date: '2017-12-08'
legacy_title: Content Template Module
platform: server
product: confluence
subcategory: modules
title: Content Template module
---
# Content Template module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Confluence 5.1 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this module

This module must be used with the [Blueprint](/server/confluence/confluence-blueprints) module or Space Blueprint module. 

Configuration

The root element for this module is `content-template`. 

#### Element: `content-template`

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Attribute*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>key</code></p></td>
<td><p>Identifies the plugin module. This key must be unique within your plugin.</p>
<p><strong>Default:</strong> None</p></td>
</tr>
<tr class="even">
<td><p><code>i18n-name-key</code></p></td>
<td><p>The localisation key for the human-readable name of the plugin module.</p>
<p><strong>Default:</strong> None</p></td>
</tr>
</tbody>
</table>

**\* key is required**

The `content-template` element can have the following child elements:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Element*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><code>resource</code></p></td>
<td><p>Defines the location of the <code>content-template</code>. See the <a href="/server/confluence/adding-plugin-and-module-resources">resource module</a> definition for information on this module. Define the template content in an XML file. The file can contain any valid <a href="https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format" class="external-link">Confluence storage format</a> markup.  The template file can live anywhere in your plugin project, by convention though, you should place it with the other project resources, for example in a <code>src/main/resources/template</code> folder.  </p>
<p><strong>Default:</strong> None</p></td>
</tr>
<tr class="even">
<td><p><code>context-provider</code></p></td>
<td>Specifies the class that implements the<code> com.atlassian.plugin.web.ContextProvider</code> interface. The context provider specifies what the content template is rendered with.</td>
</tr>
</tbody>
</table>

**\* resource is required**

#### Example:

For example, you might create a `src/main/resources/templates/basic.xml` file with the following content:

``` xml
 <p>hello world!</p>
```

Then, in your `atlassian-plugin.xml` file, you specify a `content-template` module:

``` xml
<content-template key="myplugin-template" i18n-name-key="myplugin.templates.content.name">
  <resource name="template" type="download" location="templates/basic.xml"/>
</content-template> 
```

For details on creating your own blueprint, see [Confluence Blueprints](/server/confluence/confluence-blueprints) in this documentation.
