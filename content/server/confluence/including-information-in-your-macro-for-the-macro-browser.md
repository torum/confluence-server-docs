---
aliases:
- /server/confluence/including-information-in-your-macro-for-the-macro-browser-2031776.html
- /server/confluence/including-information-in-your-macro-for-the-macro-browser-2031776.md
category: reference
confluence_id: 2031776
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031776
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031776
date: '2018-03-30'
legacy_title: Including Information in your Macro for the Macro Browser
platform: server
product: confluence
subcategory: modules
title: Including Information in your Macro for the Macro Browser
---
# Including information in your macro for the Macro Browser

The Macro Browser helps users to browse and insert macros while adding or editing content. If you are a plugin author,
this page provides details on how to make use of the new Macro Browser framework.

{{% note %}}

Starting from Confluence 4.0, all macros must contain metadata to function correctly in Confluence.

{{% /note %}}

## Including Macro Browser information in your macro

 To make sure your macro appears and behaves correctly in the Macro Browser, and displays the correct parameter input fields, you need to make simple additions to your macro definition in the `atlassian-plugin.xml` file.

### Macro descriptor attributes

The following macro attributes contain information specifically for the Macro Browser.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>documentation-url</p></td>
<td><p>The absolute URL pointing to the macro documentation.</p></td>
</tr>
<tr class="even">
<td><p>icon</p></td>
<td><p>The relative URL to the application for the macro icon. To display well in the macro browser, the image should be 80 pixels by 80 pixels, with no transparency.<br />
Note: if you have your icon defined as a <a href="/server/confluence/adding-plugin-and-module-resources">downloadable resource</a>, you can refer to this by specifying &quot;<code>/download/resources/PLUGIN_KEY/RESOURCE_NAME</code>&quot; as the icon attribute.</p></td>
</tr>
<tr class="odd">
<td><p>hide-body</p></td>
<td><p>This attribute is available for macros that falsely declare that they have body (most likely because they extend `BaseMacro`) when they don't.<br />
For example the Gallery macro. This attribute helps hide the body text field in the Macro Browser. </p>
<p><em>Default:</em> false.</p></td>
</tr>
<tr class="even">
<td><p>hidden</p></td>
<td><p>If set to true, the macro is not visible in the Macro Browser for selection. Plugin authors may want to hide macros that are for their plugin's internal use and shouldn't really be used by users.</p>
<p><em>Default:</em> false.</p></td>
</tr>
</tbody>
</table>

## Macro elements

The following macro elements contain information specifically for the Macro Browser.
They should be placed inside your `<xhtml-macro>` element.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Name*</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>category</p></td>
<td><p>The category the macro should appear in. Valid categories are listed below.</p></td>
</tr>
<tr class="even">
<td><p>alias</p></td>
<td><p>Defines an alias for the macro. This means that the Macro Browser will open for the defined aliases as if it were this macro.<br />
For example if <code>dynamictasklist</code> is an alias of <code>tasklist</code>, editing an existing <code>dynamictasklist</code> macro will open it as a <code>tasklist</code> macro.</p></td>
</tr>
<tr class="odd">
<td><p>parameters</p></td>
<td><p>Defines a group of parameter elements. See example below.</p></td>
</tr>
<tr class="even">
<td><p>parameter</p></td>
<td><p>This defines a single macro parameter. It must be an element of the parameters element. Its contents are described <a href="#parameter-options">below</a>.</p></td>
</tr>
</tbody>
</table>

*\*parameters is a required element.*

### Macro categories

The following categories for macros are defined
(see <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/macro/browser/beans/MacroCategory.html" class="external-link">MacroCategory</a>).
A macro with no category will show up in the default 'All' category.

*   `formatting`
*   `confluence-content`
*   `media`
*   `visuals`
*   `navigation`
*   `external-content`
*   `communication`
*   `reporting`
*   `admin`
*   `development`

### Parameter options

Each `<parameter>` element *must* have the following attributes:

*   `name` — A unique name of the parameter, or "" for the default (unnamed) parameter.
*   `type` — The type of parameter (see <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/macro/browser/beans/MacroParameterType.html" class="external-link">MacroParameterType</a>).
Currently the following parameter types are supported in the Macro Browser UI:
    *   `boolean` — displays a check box.
    *   `enum` — displays as a drop-down.
    *   `string` — displays an input field (this is the default if type is unknown).
    *   `spacekey` — displays an autocomplete field for search on space names.
    *   `attachment` — displays an autocomplete field for search on attachment filenames.
    *   `username` — displays an autocomplete field for search on username and full name.
    *   `confluence-content` — displays an autocomplete field for search on page and blog titles.

The following parameter attributes are optional:

*   `required` — use to mark parameter as required, defaults to 'false'.
*   `multiple` — use to mark that parameter may have several values, defaults to 'false'.
*   `default` — use to define the default value for the parameter.

It can also have the following optional child elements:

*   `<alias name="xxx"/>` — alias for the macro parameter.
*   `<value name="xxx"/>` — describes a single 'enum' value - only applicable for enum typed parameters.

### Hidden parameter

To send information along with your form, you can define a hidden parameter, which will be invisible for users.
Take a look at the following examples on how to define this.

1.  In the `atlassian-plugin.xml`, define a `string` parameter:

    ``` xml
    <xhtml-macro name="example-formatting" key="example-formatting" class="some.path.ExampleFormatting.java"
           icon="/download/resources/${atlassian.plugin.key}/images/example-formatting.png">
        <category name="formatting"/>
        <parameters>
            <parameter name="willbehidden" type="string"/>
        </parameters>
    </xhtml-macro>

    <resource type="download" name="images/" location="images">
        <param name="content-type" value="image/png"/>
    </resource>

    <web-resource key="hidden-field-parameter" name="Add a hidden field">
        <resource type="download" name="hidden-parameter-field.js" location="js/hidden-parameter-field.js" />
        <dependency>confluence.editor.actions:editor-macro-browser</dependency>
        <context>macro-browser</context>
    </web-resource>
    ```

1.  Inside the `js/hidden-parameter-field.js`, add the following:

    ``` javascript
    AJS.MacroBrowser.setMacroJsOverride("example-formatting", {
        fields: {
            string: {
                "willbehidden": function (param) {
                    var parameterField = AJS.MacroBrowser.ParameterFields["_hidden"](param, {});
                    if (!parameterField.getValue()) {
                        parameterField.setValue('hidden field value');
                    }
                    return parameterField;
                }
            }
        }
    });
    ```

{{% note %}}

Autocompletion on the parameter types may show up that `hidden` is also a supported type. Keep in mind that choosing this type will not make the field hidden.

{{% /note %}}

### Example of parameter use

The following code is an example of parameter use:

``` xml
 <xhtml-macro name="example-parameters" documentation-url="http://docs.example.com/some/path"
        class="some.path.ExampleFormatting.java" key="example-parameters">
        <category name="confluence-content"/>
        <parameters>
            <parameter name="users" type="username" multiple="true"/>
            <parameter name="spaces" type="spacekey" multiple="true">
                <alias name="space"/>
            </parameter>
            <parameter name="labels" type="string">
                <alias name="label"/>
            </parameter>
            <parameter name="width" type="percentage" default="100%"/>
            <parameter name="types" type="string">
                <alias name="type"/>
            </parameter>
            <parameter name="max" type="int" default="15">
                <alias name="maxResults"/>
            </parameter>
            <parameter name="theme" type="enum">
                <value name="concise"/>
                <value name="social"/>
                <value name="sidebar"/>
            </parameter>
            <parameter name="showProfilePic" type="boolean"/>
            <parameter name="hideHeading" type="boolean"/>
        </parameters>
    </xhtml-macro>
```

{{% warning %}}

Note that the previous example contains parameter types that aren't all supported in the macro browser UI, but may be in future releases. 

{{% /warning %}}

### Macro Icon Example

To provide an icon for your macro, perform the following steps:

1.  Create a resource for `icons/images` if you don't already have one. e.g.

    ``` xml
    <resource key="icons" name="icons/" type="download" location="myplugin/images/icons"/>
    ```

    This must be a top-level resource in your `atlassian-plugin.xml` and must be defined before the macro.

1.  Ensure your plugin contains the resource directory `myplugin/images/icons`.

1.  Set the icon attribute on the macro. For example:

    ``` xml
    icon="/download/resources/pluginkey/icons/iconfile.png"
    ```

### i18n Conventions

Instead of having to define i18n keys for each element in the macro definition, the following convention is used to look up i18n keys for the macro browser.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Key</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>{pluginKey}.{macroName}.label</p></td>
<td><p>Macro label/display name</p></td>
</tr>
<tr class="even">
<td><p>{pluginKey}.{macroName}.desc</p></td>
<td><p>Macro description</p></td>
</tr>
<tr class="odd">
<td><p>{pluginKey}.{macroName}.param.{paramName}.label</p></td>
<td><p>Macro parameter label</p></td>
</tr>
<tr class="even">
<td><p>{pluginKey}.{macroName}.param.{paramName}.desc</p></td>
<td><p>Macro parameter description</p></td>
</tr>
<tr class="odd">
<td><p>{pluginKey}.{macroName}.body.label</p></td>
<td><p>Macro body label (defaults to 'Body Text' if not provided)</p></td>
</tr>
<tr class="even">
<td><p>{pluginKey}.{macroName}.body.desc</p></td>
<td><p>Macro body description</p></td>
</tr>
</tbody>
</table>

You will need to place the keys in a .properties file with a [resource of type i18n](/server/confluence/adding-plugin-and-module-resources) in your plugin.
