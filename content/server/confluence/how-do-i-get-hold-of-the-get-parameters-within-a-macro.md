---
aliases:
- /server/confluence/2031757.html
- /server/confluence/2031757.md
category: devguide
confluence_id: 2031757
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031757
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031757
date: '2017-12-08'
legacy_title: How do I get hold of the GET-Parameters within a Macro?
platform: server
product: confluence
subcategory: faq
title: How do I get hold of the GET-Parameters within a macro?
---
# How do I get hold of the GET-Parameters within a macro?

If you want to get hold of the GET-Parameters within your Macro (Java-Code), you need to access the Java servlet.

First add a dependency for the servlet-api to your pom.xml:

``` xml
<dependency>
<groupId>javax.servlet</groupId>
<artifactId>servlet-api</artifactId>
<version>2.4</version>
<scope>provided</scope>
</dependency>
```

Now you can easily access every parameter you wish like this:

``` java
String showParam = ServletActionContext.getRequest().getParameter("show");
```
