---
aliases:
- /server/confluence/2031780.html
- /server/confluence/2031780.md
category: devguide
confluence_id: 2031780
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031780
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031780
date: '2017-12-08'
legacy_title: What is Bandana? One form of Confluence Persistence
platform: server
product: confluence
subcategory: faq
title: What is Bandana? One form of Confluence persistence
---
# What is Bandana? One form of Confluence persistence

Bandana is Atlassian's hierarchical data storage mechanism, it breaks objects into XML and stores them, to be retrieved later... uses xstream and a little hierarchical magic under the covers and has another strange Atlassian codename. It is one way to persist data inside your plugin. It is good for global config types of data.

It uses XStream to serialize Java strings (and objects?) to and from XML.

Examples:

The BandanaManager can be acquired via Confluence's (Spring's) dependency injection.

Data in this case is written to: confluence-data-dir/config/confluence-global.bandana.xml

Writing data:

``` java
bandanaManager.setValue(new ConfluenceBandanaContext(), GmapsManager.GOOGLE_MAPS_API_KEY, updateApiKey);
```

Retrieving data:

``` java
public String getGoogleApiKey()
    {
        return (String) bandanaManager.getValue(new ConfluenceBandanaContext(), GmapsManager.GOOGLE_MAPS_API_KEY);
    }
```

See also: [Persistence in Confluence](/server/confluence/persistence-in-confluence)
