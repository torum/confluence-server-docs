---
aliases:
- /server/confluence/confluence-remote-api-reference-24085294.html
- /server/confluence/confluence-remote-api-reference-24085294.md
category: reference
confluence_id: 24085294
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24085294
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24085294
platform: server
product: confluence
subcategory: api
title: Confluence Remote API reference
---
# Confluence Remote API reference

Redirection Notice

This page will redirect to https://developer.atlassian.com/x/UwAf in about 2 seconds.
