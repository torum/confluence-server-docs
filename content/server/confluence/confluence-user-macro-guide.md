---
aliases:
- /server/confluence/confluence-user-macro-guide-2031868.html
- /server/confluence/confluence-user-macro-guide-2031868.md
category: devguide
confluence_id: 2031868
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031868
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031868
date: '2017-12-08'
guides: guides
legacy_title: Confluence User Macro Guide
platform: server
product: confluence
subcategory: learning
title: Confluence User Macro guide
---
# Confluence User Macro guide

With a user macro, you can create simple formatting macros using the Confluence web interface.

To create a user macro:

-   Go to the **Confluence Administration Console**.
-   Select '**User Macros**'.
-   Enter the macro metadata and input data as prompted. See the <a href="http://confluence.atlassian.com/display/DOC/Writing+User+Macros" class="external-link">administrator's guide to writing user macros</a>.

{{% note %}}

User Macro Plugins and Macro Plugins

If you want to distribute your user macro as a plugin, please see the developer's guide to the [User Macro plugin module](/server/confluence/user-macro-module). If you want to create more complex, programmatic macros in Confluence, you may need to write a [Macro plugin module](/server/confluence/macro-module).

{{% /note %}}
