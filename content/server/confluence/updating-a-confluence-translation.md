---
aliases:
- /server/confluence/updating-a-confluence-translation-2031627.html
- /server/confluence/updating-a-confluence-translation-2031627.md
category: reference
confluence_id: 2031627
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031627
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031627
date: '2017-12-08'
legacy_title: Updating A Confluence Translation
platform: server
product: confluence
subcategory: modules
title: Updating a Confluence Translation
---
# Updating a Confluence Translation

This guide is for translating Confluence into non-English languages using a Spreadsheet, and covers:

1.  Improving or finishing a translation for an existing Language Plugin
2.  Updating an existing translation for a new version of Confluence

If you do not have a Language Plugin to deploy the updated `ConfluenceActionSupport_<KEY>.properties` file (where &lt;KEY&gt; is the international language identifier), you should instead go to the [Creating A New Confluence Translation](/server/confluence/creating-a-new-confluence-translation).

To make small updates, it is quicker to translate the file directly. If your changes are more substantial, you may prefer to translate using Excel.

## Translating Directly

This approach uses any file editor. If your translation uses English characters, you can skip to the next section.

#### Preparing Non-Unicode Files For Direct Translation

If you do not have the Sun Java JDK installed, please download it now. Version 5.0 can be downloaded <a href="http://java.sun.com/j2se/1.5.0/download.jsp" class="external-link">here</a>.

1.  Create a script or batch file that uses the native2ascii.exe program bundled in &lt;JAVA-JDK-DIRECTORY&gt;/bin to convert from the natively encoded file back to the Unicode file. For example, update the Russian properties file with a script or batch file that calls

    ``` bash
    native2ascii -encoding cp1251 JiraWebActionSupport_ru_RU-native.txt JiraWebActionSupport_ru_RU.properties
    ```

2.  Copy `ConfluenceActionSupport<KEY>.properties` to a new file `ConfluenceActionSupport<KEY>-native.txt`. Save the new file local non-Unicode character encoding.

#### Performing Direct Translation

These steps apply to both Unicode and non-Unicode translations:

1.  Open the properties file (or it's natively encoded equivalent) for editing, translate some or all of the properties file into your target language, and save the changes. If you are translating into a non-Unicode language, always edit `ConfluenceActionSupport<KEY>-native.txt`, otherwise modify `ConfluenceActionSupport<KEY>.properties`.
2.  Edit the file content in a text editor, referring to [Translating ConfluenceActionSupport Content](/server/confluence/translating-confluenceactionsupport-content) for more information on how to modify the string values. Users who are unsatisfied with simply opening two copies of the file in their favourite editor may want to try this freeware <a href="http://www.javasharing.com/javasharing/web/guest/properties_editor" class="external-link">properties editor</a>, that allows side-by-side comparisons.
3.  For non-Unicode translations only, run the `native2ascii` script to update `ConfluenceActionSupport<KEY>.properties`
4.  If you wish to test the update, copy the file back to its original location in the plugin. Then restart Confluence.

## Translating Using A Spreadsheet

The guide below uses the open-source <a href="https://excelbundle.dev.java.net/" class="external-link">ExcelBundle</a>, released under the <a href="http://www.apache.org/licenses/LICENSE-2.0" class="external-link">Apache License 2.0</a>. To translate from Excel or OpenOffice:

1.  Unzip [excelbundle0.9.zip](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/2031627/2228582.zip) to your local drive.
2.  Browse to your Confluence install and go to the `\confluence\WEB-INF\classes\com\atlassian\confluence\core` directory. Copy the `ConfluenceActionSupport.properties` file there into the `translation_tool` directory and rename it to `ConfluenceActionSupport_en.properties`.
3.  If you want to start a fresh translation, skip this step. To work from an existing translation, copy it into the `translation_tool` directory and remove any country variant from the filename, eg `ConfluenceActionSupport_ru_RU.properties` becomes `ConfluenceActionSupport_ru.properties`.
4.  Call the translation tool to create the spreadsheet file. For example, to create a Russian translation, open a terminal window in the `translation_tool` directory and call

    ``` bash
    java -jar excelbundle.jar -export translation_ru.xls -l en,ru -r "%cd%"
    ```

5.  Edit the file content, referring to [Translating ConfluenceActionSupport Content](/server/confluence/translating-confluenceactionsupport-content) for more information on how to modify the string values.
6.  Call the translation tool to export the updates back into the localised properties file. For the example Russian translation, open a terminal window, go to the `translation_tool` directory and call

    ``` bash
    java -jar excelbundle.jar -import translation_ru.xls -l ru -r "%cd%"
    ```

7.  Once you have completed editing, you must copy and rename the localised translation back to the language plugin directory. For frequent updates, you may wish to create a script to do this.
8.  To view the updates after copying across the new properties file, select the language plugin for your translation, then restart Confluence and refresh your browser.


