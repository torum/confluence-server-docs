---
aliases:
- /server/confluence/removing-unnecessary-complexity-39368870.html
- /server/confluence/removing-unnecessary-complexity-39368870.md
category: devguide
confluence_id: 39368870
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368870
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368870
date: '2017-12-08'
legacy_title: Removing Unnecessary Complexity
platform: server
product: confluence
subcategory: other
title: Removing Unnecessary Complexity
---
# Removing Unnecessary Complexity

The command, atlas-create-confluence-plugin, will provide hooks for testing and exporting a java api from the plugin. For simple plugins, such as one designed to deliver some javascript, these hooks are unnecessary complexity and can be removed.

### Removing Unnecessary Java Files

By default there will be a directory created for a plugin api at .../src/main/java/.../api and .../src/main/java/.../impl.  The api and impl directories can be deleted

{{% note %}}

If the default java files are removed, the test directories need to be removed as well as the tests will try and use the java code.

{{% /note %}}

### Removing the Test directories

Be default, there will be directories for test code under .../src/test/java and .../src/test/resources.  The java and resources directories can be deleted.

