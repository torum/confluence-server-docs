---
aliases:
- /server/confluence/validate-the-install-39368894.html
- /server/confluence/validate-the-install-39368894.md
category: devguide
confluence_id: 39368894
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368894
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368894
date: '2017-12-08'
legacy_title: Validate the Install
platform: server
product: confluence
subcategory: other
title: Validate the Install
---
# Validate the Install

In a web browser with Confluence displayed, select the "Add-ons" from the gear menu dropdown at the top of the browser window. 

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282685/image2015-9-28%2015%3A49%3A25.png?version=1&amp;modificationDate=1455047009147&amp;api=v2" class="confluence-external-resource" height="250" />

 

 From the "Manage add-ons" page, you should see your macro installed in the **User-installed add-ons** section.  If the plugin is in solid black text, it is installed and enabled.  If it is greyed out, it is installed by not enabled. 

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282685/image2015-9-28%2015%3A51%3A4.png?version=1&amp;modificationDate=1455046995053&amp;api=v2" class="confluence-external-resource" height="250" />

