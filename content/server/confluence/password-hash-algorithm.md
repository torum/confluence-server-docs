---
aliases:
- /server/confluence/password-hash-algorithm-2031624.html
- /server/confluence/password-hash-algorithm-2031624.md
category: devguide
confluence_id: 2031624
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031624
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031624
date: '2017-12-08'
guides: guides
legacy_title: Password Hash Algorithm
platform: server
product: confluence
subcategory: learning
title: Password hash algorithm
---
# Password hash algorithm

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Since:</p></td>
<td><p>Confluence 3.5 and later.</p></td>
</tr>
</tbody>
</table>

Confluence uses the salted PKCS5S2 implementation provided by Embedded Crowd.

Confluence versions before 3.5 used a password hash algorithm based on BouncyCastle's SHA1-512 implementation. You can see one version of the source code for it <a href="http://docs.atlassian.com/atlassian-user/1.7/xref/com/atlassian/user/impl/osuser/security/password/OSUPasswordEncryptor.html" class="external-link">here</a>. The entire Confluence source code is available <a href="http://confluence.atlassian.com/display/DOC/Building+Confluence+From+Source+Code" class="external-link">here</a>.

If you'd like to try to import users from a different user management system into a local instance of Confluence, you're likely to be better off using a different solution than re-hashing existing passwords. Some options would be:

1.  Use <a href="http://www.atlassian.com/software/crowd/" class="external-link">Crowd</a>, which is extendable and offers connectors to user repositories.
2.  Import users using their plain text passwords, leveraging the [Confluence XML-RPC and SOAP APIs](/server/confluence/confluence-xml-rpc-and-soap-apis). One good client is the <a href="#confluence-command-line-interface" class="unresolved">Confluence Command Line Interface</a>.
