---
aliases:
- /server/confluence/open-a-command-shell-39368854.html
- /server/confluence/open-a-command-shell-39368854.md
category: devguide
confluence_id: 39368854
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368854
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368854
date: '2017-12-08'
legacy_title: Open a Command Shell
platform: server
product: confluence
subcategory: other
title: Open a Command Shell
---
# Open a Command Shell

The Atlassian SDK provides several tools that are designed to be launched from a command shell.  Using the table below which shows where in the application launcher for the various operating systems the command shell launcher is, launch the command shell

|         |                                                                 |
|---------|-----------------------------------------------------------------|
| Windows | Start -&gt; All Programs -&gt; Accessories -&gt; Command Prompt |
| OS X    | Applications -&gt; Utilities -&gt; Terminal                     |
| Linux   | Applications Menu -&gt; Accessories -&gt; Terminal              |
