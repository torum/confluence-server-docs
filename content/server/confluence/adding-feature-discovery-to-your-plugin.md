---
aliases:
- /server/confluence/adding-feature-discovery-to-your-plugin-21463589.html
- /server/confluence/adding-feature-discovery-to-your-plugin-21463589.md
category: devguide
confluence_id: 21463589
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=21463589
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=21463589
date: '2017-12-08'
guides: guides
legacy_title: Adding Feature Discovery to your Plugin
platform: server
product: confluence
subcategory: learning
title: Adding Feature Discovery to your plugin
---
# Adding Feature Discovery to your plugin

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>This service is available in <strong>Confluence 5.1.5, Confluence 5.2, and later</strong>.</p></td>
</tr>
</tbody>
</table>

Confluence provides a feature discovery service that you can use in your plugin. Let's say you've added a new module or feature to your plugin, and you want to let users know about it. In other words, you need a way of letting users discover the new features. The feature discovery service is provided by the Confluence Feature Discovery plugin, which is bundled in Confluence 5.1.5, Confluence 5.2 and later.

## Summary: How to let users know about a new feature

In brief:

-   If necessary, register the new feature with the feature discovery service. This is not required if your feature is associated with a new plugin module.
-   Use the feature discovery service to find all modules or features that were enabled on the user's Confluence site within a specified period.
-   Let your users know. For example, you could add code to your plugin that highlights the new functionality in the Confluence user interface. (This functionality is **not** part of the feature discovery service.)

Details are on the rest of this page.

## Step 1. Register the new feature if necessary

The feature discovery service keeps track of features via a **context** (usually the plugin key) and a **key** (usually the module key).

The plugin automatically detects when plugin modules are enabled, using the plugin key as the context and the module key as the key. You do not have to register features that are backed by plugin modules.

### When do you need to register a feature?

Examples of situations where you need to register your new feature manually:

-   The feature is not associated with a plugin module. For example, the feature may be provided purely by JavaScript.
-   You have changed an existing plugin module. Note that you must unregister the module, and then register it again. Simply registering won't work as registration is an idempotent operation.

### How to register a feature

To register a feature, call one of the `register()` methods of the feature service, providing the context and key. See the [methods reference below](#reference-methods-in-the-feature-discovery-service). You could do this in a plugin upgrade task or any other place. Feature registration is an idempotent operation - if you try to register something that has already been registered, nothing happens.

### How to unregister a feature

To register a feature, call one of the `unregister()` methods. See the [methods reference below](#reference-methods-in-the-feature-discovery-service).

## Step 2. Use the feature discovery service to find new modules or features

### 2.1 Add a dependency on the Feature Discovery plugin

Add the following dependency in your plugin's `pom.xml`:

``` xml
<dependency>
  <groupId>com.atlassian.confluence.plugins</groupId>
  <artifactId>confluence-feature-discovery-plugin</artifactId>
  <version>1.7</version>
  <scope>provided</scope>
</dependency>
```

### 2.2 Import the component

Add the component-import in your plugin's `atlassian-plugin.xml`:

``` xml
<component-import key="featureDiscoveryService" interface="com.atlassian.confluence.plugins.featurediscovery.service.FeatureDiscoveryService"/>
```

### 2.3 Write Java code to find new features

Call `isNew()` to see if a given feature is newly installed/enabled on this Confluence site, or call `getNew()` to get a list of new features within a given set. See the [methods reference below](#reference-methods-in-the-feature-discovery-service).

## Step 3. Highlight the new features for your users

It is up to your plugin to let the Confluence users know about the new functionality. For example, you could add code to your plugin that highlights the new options in the Confluence user interface. This functionality is **not** part of the feature discovery service. See the [example below](#usage-example-new-blueprints-are-highlighted-in-the-confluence-create-dialog).

## Reference: Methods in the feature discovery service

<table>
<colgroup>
<col style="width: 40%" />
<col style="width: 60%" />
</colgroup>
<thead>
<tr class="header">
<th>Method signature - registering a feature or module</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>
<pre><code>void register(
  String context, 
  String key
)

void register(
  String context, 
  String key, 
  Date installationDate
)</code></pre></td>
<td><p>Call this method to register your plugin module or feature with the feature discovery service.</p>
<p>Parameters:</p>
<ul>
<li><code>context </code>- Usually the plugin key.</li>
<li><code>key </code>- Usually the module key, but can be a string of your choice that uniquely identifies the feature.</li>
<li><p><code>installationDate</code> - Optional. The date on which the plugin module or feature was installed. The default is the current time.</p></li>
</ul></td>
</tr>
<tr class="even">
<td>
<pre><code>void register(
  ModuleCompleteKey
    moduleCompleteKey
)

void register(
  ModuleCompleteKey
    moduleCompleteKey,
  Date installationDate
)</code></pre></td>
<td> 
<p>Call this method to register your plugin module with the feature discovery service.</p>
<p>Parameters:</p>
<ul>
<li><code>moduleCompleteKey </code>- The full key of the plugin module, as defined in the Atlassian plugin framework.</li>
<li><code>installationDate</code> - Optional. The date on which the plugin module or feature was installed. The default is the current time.</li>
</ul></td>
</tr>
<tr class="odd">
<td><pre><code>void register(
  FeatureCompleteKey
    featureCompleteKey
)

void register(
  FeatureCompleteKey
    featureCompleteKey,
  Date installationDate
)</code></pre></td>
<td><p>You can use this function to register features that are not backed by a plugin module.</p>
<p>Parameters:</p>
<ul>
<li><code>featureCompleteKey </code>- The context and key of the feature, combined to form the complete key of the feature.</li>
<li><code>installationDate</code> - Optional. The date on which the feature was installed. The default is the current time.</li>
</ul></td>
</tr>
<tr class="even">
<td>Method signature - unregistering a feature or module</td>
<td>Description</td>
</tr>
<tr class="odd">
<td><pre><code>void unregister(
  String context,
  String key
)</code></pre></td>
<td>Call this method to de-register a given module or feature.</td>
</tr>
<tr class="even">
<td><pre><code>void unregister(
  ModuleCompleteKey
    moduleCompleteKey
)</code></pre></td>
<td>Call this method to de-register a given module or feature.</td>
</tr>
<tr class="odd">
<td><pre><code>void unregister(
  FeatureCompleteKey
    featureCompleteKey
)</code></pre></td>
<td>Call this method to de-register a given feature.</td>
</tr>
<tr class="even">
<td>Method signature - checking if a feature or module is new</td>
<td>Description</td>
</tr>
<tr class="odd">
<td><pre><code>boolean isNew(
  ModuleCompleteKey 
    moduleCompleteKey
)

boolean isNew(
  ModuleCompleteKey
    moduleCompleteKey,
  TimePeriod
    timePeriod
)</code></pre></td>
<td><p>Call this method to query if a given plugin module is new. A module is considered new if it has been registered within the specified time period.</p>
<p>Parameters:</p>
<ul>
<li><code>moduleCompleteKey </code>- The full key of the plugin module, as defined in the Atlassian plugin framework.</li>
<li><p><code>timePeriod </code>- Optional time period. The default is 7 days.</p></li>
</ul>
<p>Returns: <code>true</code> if the module is found and is new, otherwise <code>false</code>.</p></td>
</tr>
<tr class="even">
<td><pre><code>boolean isNew(
  String context,
  String key
)

boolean isNew(
  String context,
  String key,
  TimePeriod timePeriod
)</code></pre></td>
<td><p>Call this method to query if a given feature or plugin module is new. A feature or module is considered new if it has been registered within the specified time period.</p>
<p>Parameters:</p>
<ul>
<li><code>context</code> - Usually the plugin key.</li>
<li><code>key </code> - Usually the module key, but can be a string of your choice that uniquely identifies the feature.</li>
<li><p><code>timePeriod</code> - Optional time period. The default is 7 days.</p></li>
</ul>
<p>Returns: <code>true</code> if the feature or module is found and is new, otherwise <code>false</code>.</p></td>
</tr>
<tr class="odd">
<td>Method signature - checking multiple features or modules for newness</td>
<td>Description</td>
</tr>
<tr class="even">
<td><pre><code>List&lt;
  ModuleCompleteKey
&gt; getNew(
  List&lt;ModuleCompleteKey&gt;
    moduleCompleteKeys
)

List&lt;
  ModuleCompleteKey
&gt; getNew(
  List&lt;ModuleCompleteKey&gt;
    moduleCompleteKeys,
  TimePeriod
    timePeriod
)</code></pre></td>
<td><p>Call this method to find the new plugin modules within a given set. A module is considered new if it has been registered within the specified time period.</p>
<p>Parameters:</p>
<ul>
<li><code>moduleCompleteKeys </code>- A collection of complete keys of plugin modules.</li>
<li><p><code>timePeriod </code>- Optional time period. The default is 7 days.</p></li>
</ul>
<p>Returns: A collection with just the new module complete keys found in the given input collection.</p></td>
</tr>
<tr class="odd">
<td><pre><code>List&lt;
  FeatureCompleteKey
&gt; getNewFeatures(
  List&lt;FeatureCompleteKey&gt;
    featureCompleteKeys
)

List&lt;
  FeatureCompleteKey
&gt; getNewFeatures(
  List&lt;FeatureCompleteKey&gt;
    featureCompleteKeys,
  TimePeriod
    timePeriod
)</code></pre></td>
<td><p>Call this method to find the new features or plugin modules within a given set. A feature or module is considered new if it has been registered within the specified time period.</p>
<p>Parameters:</p>
<ul>
<li><code>featureCompleteKeys </code>- A collection of complete keys of plugin modules or features.</li>
<li><p><code>timePeriod </code>- Optional time period. The default is 7 days.</p></li>
</ul>
<p>Returns: A collection of complete keys for just the new features or modules found in the given input collection.</p></td>
</tr>
</tbody>
</table>

## Usage example: New blueprints are highlighted in the Confluence 'Create' dialog

In the Confluence 'Create' dialog, you will notice a 'NEW' lozenge next to recenty-installed blueprints. This dialog is produced by the Confluence Create Content plugin, which uses the feature discovery service to detect new blueprints. In the Confluence Create Content plugin, the `DefaultBlueprintWebItemService `issues the following call to the feature discovery service:

``` java
featureDiscoveryService.getNew(blueprintsModuleCompleteKeys);
```

*Screenshot: The 'New' lozenge next to newly-installed blueprints*

<img src="/server/confluence/images/blueprints-featurediscovery.png" width="600" />

## REST API

The Feature Discovery plugin exposes a limited REST API.

### Action: Check if a given module is new

<table>
<colgroup>
<col style="width: 15%" />
<col style="width: 85%" />
</colgroup>
<tbody>
<tr class="odd">
<td>HTTP Method:</td>
<td>GET</td>
</tr>
<tr class="even">
<td>URL format:</td>
<td><pre><code>https://{my.confluence.com}
  /rest
  /feature-discovery
  /latest
  /{context}/{key}

https://{my.confluence.com}
  /rest
  /feature-discovery
  /latest
  /{context}/{key}
  ?newPeriod=&lt;number of seconds&gt;</code></pre>
<p>Path variables and URL parameters:</p>
<ul>
<li><p><code>my.confluence.com</code> - The base URL of your Confluence site.</p></li>
<li><code>context </code>-- The plugin key.</li>
<li><code>key</code> - The module key.</li>
<li><code>newPeriod=&lt;number of seconds&gt;</code> - Optional time period. A module is considered new if it has been registered within the specified time period. The default is 7 days.</li>
</ul></td>
</tr>
<tr class="odd">
<td>Sample response:</td>
<td><pre><code>{
  &quot;context&quot;:&quot;com.atlassian.confluence.plugins.myplugin&quot;,
  &quot;key&quot;:&quot;mymodule&quot;,
  &quot;isNew&quot;:false
}</code></pre></td>
</tr>
</tbody>
</table>

### Action: Send the server a list of modules and get back a filtered list of what's new

<table>
<colgroup>
<col style="width: 15%" />
<col style="width: 85%" />
</colgroup>
<tbody>
<tr class="odd">
<td>HTTP Method:</td>
<td>POST</td>
</tr>
<tr class="even">
<td>URL format:</td>
<td><pre><code>https://{my.confluence.com}
  /rest
  /feature-discovery
  /latest/new

https://{my.confluence.com}
  /rest
  /feature-discovery/
  latest/new
  ?newPeriod=&lt;number of seconds&gt;</code></pre>
<p>Path variables and URL parameters:</p>
<ul>
<li><p><code>my.confluence.com</code> - The base URL of your Confluence site.</p></li>
<li><p><code>newPeriod=&lt;number of seconds&gt;</code> - Optional time period. A module is considered new if it has been registered within the specified time period. The default is 7 days.</p></li>
</ul></td>
</tr>
<tr class="odd">
<td>Body:</td>
<td><p>The post body must contain a JSON array.</p>
<p>Sample body:</p>
<pre><code>[
  {
    &quot;context&quot;:
      &quot;com.atlassian.confluence.plugins.myplugin&quot;,
    &quot;key&quot;:&quot;mymodule&quot;},
  {
    &quot;context&quot;:
      &quot;com.atlassian.confluence.plugins.myotherplugin&quot;,
    &quot;key&quot;:&quot;myothermodule&quot;
  }
]</code></pre></td>
</tr>
</tbody>
</table>

## Notes

### How the service detects new and existing modules

The feature discovery service listens for plugin module 'enable' events. On enabling of a module, if an entry does not already exist, the service stores the plugin key, the module key and the current time in an Active Objects table.

The service also does a full scan of all enabled plugin modules when Confluence starts up and whenever the Feature Discovery plugin is enabled or reinstalled. Items discovered during scans are considered to be installed when the system was installed, because the plugin does not have a date for them.

### The effect of installation, uninstallation, enabling and disabling

-   If a plugin module is installed but is disabled by default, it will only be recognised as new when an administrator enables it.
-   The feature discovery service ignores 'uninstall' events. If a plugin is uninstalled and then re-installed, the original installation date is still stored in the system and is not updated.
-   The feature discovery service ignores 'disable' events. If an administrator disables a plugin module then re-enables it, the installation date is not updated.
-   You can override the installation date by unregistering and re-registering your plugin module inside a plugin upgrade task. This will have the effect of forcing an item to be new.

### Dark features

If your plugin module has a `DarkFeatureEnabledCondition` on it, and that is the only condition, the plugin module is ignored. This means it will not be recognised as new until the condition is removed.

If you wish to use the dark feature functionality, ensure that the only condition on your module is a dark feature condition. If there is more than one condition, the module will be registered and its installation date will be set to the date the dark feature was installed This is mostly due to limitations in the interfaces for compound conditions.

### Name clashes

Make sure your `context` does not clash with an existing plugin module key. If there is a clash, only the first key is registered.

## More notes

More things to know:

-   The feature discovery is at the level of a plugin **module** (not the whole plugin).
-   The Confluence system installation date can now be programmatically accessed using the `getInstallationDate()` method on the `SystemInformationService`'s` ConfluenceInfo` object. System content is used to estimate the date if it was not recorded when Confluence was installed.

    ``` java
    featureDiscoveryService.getNew(blueprintsModuleCompleteKeys); 
    ```
