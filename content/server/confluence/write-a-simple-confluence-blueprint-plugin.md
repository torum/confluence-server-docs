---
aliases:
- /server/confluence/write-a-simple-confluence-blueprint-plugin-21464027.html
- /server/confluence/write-a-simple-confluence-blueprint-plugin-21464027.md
category: devguide
confluence_id: 21464027
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=21464027
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=21464027
date: '2018-03-23'
guides: guides
legacy_title: Write a simple Confluence blueprint plugin
platform: server
product: confluence
subcategory: learning
title: Write a simple Confluence blueprint plugin
---
# Write a simple Confluence blueprint plugin

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to Confluence Server 5.9.1 and higher.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>Beginner.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you less than 1 hour to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

{{% tip %}}

In this tutorial, you will learn how to create a very simple blueprint plugin (without writing any Java classes) that can be installed only on Confluence Server. To create a blueprint for Confluence Cloud go to [Multi-page blueprints with Confluence Connect](/cloud/confluence/multi-page-blueprints-with-confluence-connect/).

{{% /tip %}}

## Before you begin

To complete this tutorial, you should:

- Work through the [Atlassian Plugin SDK](/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/) tutorial.

### Plugin source

If you want to skip ahead or check your work when you finish, you can find the plugin source code on Atlassian Bitbucket. Alternatively, you can <a href="https://bitbucket.org/atlassian_tutorial/confluence-simple-blueprint/get/master.zip" class="external-link">download the source as a ZIP archive</a>.  To clone the repository, issue the following command:

``` bash
git clone https://bitbucket.org/atlassian_tutorial/confluence-simple-blueprint.git
```

{{% note %}}

About these Instructions

You can use any supported combination of operating system and IDE to create this plugin. These instructions were written using
IntelliJ IDEA 2017.2 on macOS Sierra. If you are using another operating system or IDE combination, you should use
the equivalent operations for your specific environment.

This tutorial was last tested with Confluence 6.7.0 using the Atlassian SDK 6.3.10.

{{% /note %}}

## Step 1. Create the plugin project and prune the skeleton

In this step, you'll generate skeleton code for your plugin.  Since you won't need many of the skeleton files, you also delete those unused files in this step. 

1.  Open a Terminal on your machine and navigate to directory where you normally keep your plugin code.
1.  To create a plugin skeleton, run the following command:

    ``` bash
    atlas-create-confluence-plugin
    ```

    The `atlas-` commands are part of the Atlassian Plugin SDK, and automate some of the work of plugin development for you.  

1.  To identify your plugin enter the following information:

    <table>
    <colgroup>
    <col style="width: 25%" />
    <col style="width: 75%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence.simplebp</code></p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>simplebp</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence.simplebp</code></p>
    </td>
    </tr>
    </tbody>
    </table>

1.  Confirm your entries when prompted.  
    The SDK creates your project skeleton and puts it in a `simplebp` directory. 
1.  Navigate to the `simplebp` directory created in the previous step.

1.  Delete the test directories.

    Setting up blueprint testing is not part of this tutorial. Use the following command to delete the generated test skeleton:

    ``` bash
    rm -rf ./src/test/java
    rm -rf ./src/test/resources/
    ```

1.  Delete the unneeded Java class files.

    A basic blueprint doesn't require you to write any Java code. Use the following command to delete the generated Java class skeleton:

    ``` bash
    rm -rf ./src/main/java/com/example/plugins/tutorial/confluence/simplebp/*
    ```

1. Import the project into your favorite IDE   

## Step 2. Run your plugin 

At this point, you haven't actually done anything but create a skeleton plugin.  You can run that skeleton in Confluence anyway.  In this step, you do just that.  

1.  To start a local Confluence instance, run the following command:

    ``` bash
    atlas-run
    ```

    This command takes a minute or so to run. It builds your plugin code, starts a Confluence instance, and installs your plugin. When the process has finished, you see many status lines on your screen concluding with something like the following:

    ``` bash
    [INFO] [talledLocalContainer] INFO: Starting Coyote HTTP/1.1 on http-1990
    [INFO] [talledLocalContainer] Tomcat 8.x started on port [1990]
    [INFO] confluence started successfully in 132s at http://localhost:1990/confluence
    [INFO] Type Ctrl-D to shutdown gracefully
    [INFO] Type Ctrl-C to exit
    ```

    You'll see the output includes a URL for the Confluence instance.

1.  Log into the instance as user `admin` using a password of `admin`.  
    The Confluence Dashboard appears.
1.  Leave Confluence running in your browser.  
     

## Step 3. Create some page content for your template

A template is an XML file that describes a page using Confluence source format. The simplest blueprints need only an XHTML template to do something cool.  
In this step, use Confluence to design a simple template, and then use the Confluence Source Editor add-on to copy the source format.   
In your browser where Confluence is running do the following:

1. In the Confluence header click **Spaces**  > **Demonstration Space**.  
1. At the top of the page, click **Create**
    The system displays the **Create** dialog. It is this dialog you customize by adding your own blueprint to it.   
    <img src="/server/confluence/images/createdialog.png" width="500" /> 
1.  Click the **Blank page** item, and then click the **Create** button.  
    The system puts you in a new page. 
1.  Add a two column table to the page.
    <table>
    <thead>
    <tr>
    <th>Name</th>
    <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>Enter your name here.</td>
    <td>Enter today's date here.</td>
    </tr>
    </tbody>
    </table>

1.  Enter a title of your page **Template Content**, and then click **Save**.
1.  Go to<img src="/server/confluence/images/image2016-7-26-14:37:2.png" class="confluence-thumbnail" width="25" /> &gt; **View Storage Format**.  
    ![view-storage-format.png](/server/confluence/images/view-storage-format.png "view-storage-format.png")
1.  The storage format of the page opens in a new browser window. It should look like:

    ``` xml
      <p class="auto-cursor-target"><br/></p>
         <table>
             <colgroup>
                 <col/>
                 <col/>
             </colgroup>
             <tbody>
                 <tr>
                     <th>Name</th>
                     <th>Date</th>
                 </tr>
                 <tr>
                     <td>Enter your name here.</td>
                     <td>Enter today&rsquo;s date here.</td>
                 </tr>
             </tbody>
         </table>
         <p><br/></p>
    ```
    Leave it open for now. 

## Step 4. Create a template for your project

Your plugin must include the template file in its directory structure. By convention, a template is a plugin resource so a good place to store your template is in the `resource` directory.  At this point, you have Confluence with the source content displayed. So, open a second Terminal window on your local machine and do the following:

1.  Navigate to the root of your `simplebp` project directory.
1.  Create a `templates` subdirectory in `resources`.

    ``` bash
    mkdir src/main/resources/templates
    ```

1.  Create a `mytemplate.xml` file in your newly created `templates` directory. 
1.  Open the `mytemplate.xml` file for editing.
1.  Open the browser window with the source content.
1.  Copy the storage format into the `mytemplate.xml` file and close the browser window.

    ``` xml
    <table>
      <tbody>
        <tr>
          <th>Name</th>
          <th>Date</th>
        </tr>
        <tr>
          <td>Enter your name here</td>
          <td>Enter today's date here</td>
        </tr>
      </tbody>
    </table>
    ```

1.  Save and close the `mytemplate.xml` file.

## Step 5. Add additional resources to your project

In this step, you'll modify the generated simplebp.properties file that supports internationalization.  You'll also add an image to your project to represent your blueprint in the **Create** dialog.
In your Terminal, do the following:

1.  Edit your project's `src/main/resources/simplebp.properties` file
1.  Replace the auto-generated properties with following:

    ``` xml
    my.blueprint.name=Example Blueprint
    my.blueprint.title=Sample Template
    my.create-link.title=My Sample Template
    my.create-link.description=Create a new SimpleBP template
    ```

    You can change the title of the template or the description to anything you want.

1.  Save and close the `simplebp.properties` file.
1.  Save [myblueprint.png](/server/confluence/images/myblueprint.png) image to your project's `src/main/resources/images` directory.  
    ![myblueprint.png](/server/confluence/images/myblueprint.png "myblueprint.png")
1.  Check your work.  
    At this point, your project's src/main/resources directory should contain the following files:

    ``` text
    |____atlassian-plugin.xml
    |____css
    | |____simplebp.css
    |____images
    | |____myblueprint.png
    | |____pluginIcon.png
    | |____pluginLogo.png
    |____js
    | |____simplebp.js
    |____simplebp.properties
    |____templates
    | |____mytemplate.xml
    ```

## Step 6. Add the modules to the atlassian-plugin.xml file

Each blueprint relies on three component modules:

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr>
<th>Module</th>
<th>Purpose</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>content-template</code></td>
<td>Describes the templates available with your plugin. You must have one of these. You can have multiple if your plugin includes multiple templates.</td>
</tr>
<tr>
<td><code>blueprint</code></td>
<td>Identifies the blueprint.</td>
</tr>
<tr>
<td><code>web-item</code></td>
<td>Adds the option for your blueprint to the **Create** dialog.</td>
</tr>
</tbody>
</table>

To add the modules to the file, on your plugin project root, do the following:

1.  Edit the `src/main/resources/atlassian-plugin.xml` file.
1.  Add a `content-template` module for your template.

    ``` xml
     <!-- Template for Blueprint -->
        <content-template key="simplebp-template" i18n-name-key="my.blueprint.title">
            <resource name="template" type="download" location="/templates/mytemplate.xml" />
        </content-template>
    ```

1.  Add a `blueprint` module for your template.  
     

    ``` xml
        <!-- Blueprint -->
        <blueprint key="my-blueprint" content-template-key="simplebp-template" index-key="my-index" i18n-name-key="my.blueprint.name"/>
    ```

1.  Add a link for your template to the **Create** dialog.

    ``` xml
         <!-- Add to the Create Menu -->
        <web-item key="create-by-sample-template" i18n-name-key="my.create-link.title" section="system.create.dialog/content">
            <description key="my.create-link.description" />
            <resource name="icon" type="download" location="/images/myblueprint.png" />
            <param name="blueprintKey" value="my-blueprint" />
         </web-item>
    ```

1.  Check your work.

    ``` xml
    <atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
        <plugin-info>
            <description>${project.description}</description>
            <version>${project.version}</version>
            <vendor name="${project.organization.name}" url="${project.organization.url}" />
            <param name="plugin-icon">images/pluginIcon.png</param>
            <param name="plugin-logo">images/pluginLogo.png</param>
        </plugin-info>
        <!-- add our i18n resource -->
        <resource type="i18n" name="i18n" location="simplebp"/>

        <!-- add our web resources -->
        <web-resource key="simplebp-resources" name="simplebp Web Resources">
            <dependency>com.atlassian.auiplugin:ajs</dependency>

            <resource type="download" name="simplebp.css" location="/css/simplebp.css"/>
            <resource type="download" name="simplebp.js" location="/js/simplebp.js"/>
            <resource type="download" name="images/" location="/images"/>
            <context>simplebp</context>
        </web-resource>

        <!-- Template for Blueprint -->
        <content-template key="simplebp-template" i18n-name-key="my.blueprint.title">
            <resource name="template" type="download" location="/templates/mytemplate.xml" />
        </content-template>

        <!-- Add to the Create Menu -->
        <web-item key="create-by-sample-template" i18n-name-key="my.create-link.title" section="system.create.dialog/content">
            <description key="my.create-link.description" />
            <resource name="icon" type="download" location="/images/myblueprint.png" />
            <param name="blueprintKey" value="my-blueprint" />
         </web-item>

        <!-- Blueprint -->
        <blueprint key="my-blueprint" content-template-key="simplebp-template" index-key="my-index" i18n-name-key="my.blueprint.name"/>      

    </atlassian-plugin>
    ```

1.  Save and close your `atlassian-plugin.xml` file.

## Step 7. Build your changes and see your plugin in action

Now, you are ready to build your plugin and see it in action.

1.  Rebuild your plugin from the command line using the following command that triggers [QuickReload](/server/framework/atlassian-sdk/automatic-plugin-reinstallation-with-quickreload/).

    ``` bash
        atlas-package
    ```
1.  Make sure you are still in the Demonstration Space.
1.  Click **Create**.
    The **Create** dialog appears and now contains your blueprint entry.  
    <img src="/server/confluence/images/mysamplebp.png" width="500" /> 
1.  Click the **My Sample Template** option.  
    The system creates a new page using your template.

## Next steps
Now that you know how to create blueprint plugin you’re ready to take it to the next level. Check out the tutorial for building an [intermediate blueprint plugin](/server/confluence/write-an-intermediate-blueprint-plugin/) or try to write a [Confluence Space blueprint] (/server/confluence/write-a-simple-confluence-space-blueprint/).
