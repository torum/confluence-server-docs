---
aliases:
- /server/confluence/code-formatting-module-2031876.html
- /server/confluence/code-formatting-module-2031876.md
category: reference
confluence_id: 2031876
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031876
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031876
date: '2017-12-08'
legacy_title: Code Formatting Module
platform: server
product: confluence
subcategory: modules
title: Code Formatting module
---
# Code Formatting module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.2 to 3.4.</p></td>
</tr>
<tr class="even">
<td><p>Deprecated:</p></td>
<td><p>As from Confluence 3.5, the code macro does not support custom code highlighting modules.</p></td>
</tr>
</tbody>
</table>

Code formatting plugin modules allow you to add new languages to the {code} macro.Whenever the code macro is invoked, the macro checks the 'language' parameter against the languages supported by the available formatting plugins, and uses that plugin to format the source code.

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read <a href="#" class="unresolved">Installing a Plugin</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

## Code Formatting Plugins

Here is an example `atlassian-plugin.xml` file containing a single code formatter:

``` xml
<atlassian-plugin name="My Formatter" key="confluence.extra.formatters">
    ...
    <codeformatter name="ruby" key="ruby" class="com.example.confluence.formatters.RubyFormatter">
        <description>Code formatter for the Ruby programming language</description>
    </codeformatter>
    ...
</atlassian-plugin>
```

-   the **class** attribute defines the class that will be added to the available formatters. This class must implement `com.atlassian.renderer.v2.macro.code.SourceCodeFormatter`

## The SourceCodeFormatter Interface

All code formatters must implement the following simple interface:

``` java
package com.atlassian.renderer.v2.macro.code;

/**
 * Strategy for converting a block of source code into pretty-printed HTML. SourceCodeFormatters MUST be forgiving:
 * they will be dealing with user-supplied input, so they can't afford to blow up on bad data.
 */
public interface SourceCodeFormatter
{
    /**
     * Inform the CodeMacro which languages this formatter supports. So if someone writes {code:java}, then only
     * the formatter that returns "java" from this method will be used to format it.
     *
     * @return an array of languages that this formatter supports
     */
    String[] getSupportedLanguages();

    /**
     * Convert source code into HTML.
     *
     * @param code the source code as a string
     * @param language the programming language that it is believed this code is written in
     * @return the source code formatted as HTML
     */
    String format(String code, String language);
}
```

## Formatter Priority

There is no concept of priority for formatters. If two formatters are installed and both return the same value from `getSupportedLanguages()`, one will be selected pretty much at random. If you want to avoid this behaviour, deactivate formatters that you no longer want to use.
