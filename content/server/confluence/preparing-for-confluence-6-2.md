---
aliases:
- /server/confluence/preparing-for-confluence-6.2-48049618.html
- /server/confluence/preparing-for-confluence-6.2-48049618.md
category: devguide
confluence_id: 48049618
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=48049618
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=48049618
date: '2017-12-08'
legacy_title: Preparing for Confluence 6.2
platform: server
product: confluence
subcategory: development resources
title: Preparing for Confluence 6.2
---
# Preparing for Confluence 6.2

This documentation is intended for Confluence developers who want to ensure that their existing plugins and add-ons are compatible with Confluence 6.2.

{{% note %}}

**Latest milestone**

Looking for information about the latest Confluence EAP milestones? 

Head to [Confluence Development Releases](https://confluence.atlassian.com/display/DOC/Confluence+Development+Releases)

{{% /note %}}
 

## Release Candidate 1 - 9 May

-   The 'Link to this page' tiny link has moved to the Share button. 

## Beta 1 - 28 April

-   <a href="https://bitbucket.org/atlassian/aui-adg/wiki/versions/product-version-matrix" class="external-link">AUI</a> upgraded to 6.0.6
-   <a href="https://confluence.atlassian.com/display/APPLINKS/Application+Links+Version+Matrix" class="external-link">Applinks</a> upgraded to 5.2.6
-   [Atlassian Plugin Framework](https://developer.atlassian.com/display/DOCS/Plugin+Framework+Version+Matrix) upgraded to 4.4.7

For the full release notes see <a href="https://confluence.atlassian.com/display/DOC/Confluence+6.2.0+beta+Release+Notes" class="external-link">Confluence 6.2.0 beta Release Notes</a>

## Milestone 19 - 18 April

-   Support Tools and Health check plugins updated. 

## Milestone 18 - 10 April

-   You can now invite people to edit a page with you, right from the editor. 
-   We've updated the Share page experience to include a short link that you can use to share the page. 
-   People can now request access to a page that has Edit restrictions. Previously this was only available when a page had View restrictions.

## Milestone 15 - 3 April

-   There are no notable changes in this milestone.

## Milestone 9 - 27 March

-   This is our first public milestone for Confluence 6.2.

