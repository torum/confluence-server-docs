---
aliases:
- /server/confluence/setting-up-the-working-environment-39368906.html
- /server/confluence/setting-up-the-working-environment-39368906.md
category: devguide
confluence_id: 39368906
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368906
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368906
date: '2017-12-08'
legacy_title: Setting up the Working Environment
platform: server
product: confluence
subcategory: other
title: Setting up the Working Environment
---
# Setting up the Working Environment

## Introduction

This tutorial will cover creating a working directory for plugin development and development instance of Confluence.

|                 |                                                                                                                                                                                                      |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Applicable To   | Confluence 4.x or higher                                                                                                                                                                             |
| Prerequisites   | This tutorial assumes that you have installed Java and the [Atlassian SDK](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project) version 6.2.2 or higher |
| Time to Compete | 30 minutes                                                                                                                                                                                           |

## Example Outline

## Overview

The shell command **atlas-create-confluence-plugin** will create a directory with scaffolding for a new plugin.  Executing **atlas-run** from the plugin's root directory will then launch a process to download and instantiate a development version of Confluence.  The first launch is painfully slow; The second launch is only slow.  In order to minimize the overhead associated with instantiating the development instance of Confluence, the recommended workflow is to create a working directory under which all of the plugins being authored are located and one or more 'dummy' plugins to host various versions of Confluence.  When using QuickReload, as long as the working directory has a POM.xml file - even if the file is blank - when Confluence starts, it will watch all of the sibling folders for changes.  If using **atlas-cli** and transformer based pluging, the **atlas-cli** command may be launched from the plugin's root directory and the **pi** command will build and push the plugin to the running development instance of Confluence.

## Conventions

### Working Directory

For all of the tutorials, the working directory is called *atlas-development *and is located in the users home directory.

### Development Deployment Paradigm

The tutorials use QuickReload and expect to produce transformerless plugins.

### POM.xml

In the tutorials, there is a parent POM.xml file in the working directory.  The POM.xml file in the plugin directory has been configured to inherit from the POM.xml and all redundant entries have been removed.  Typically, only the artifact version, name, description and build instructions are included. 

### Development Environment

In order to keep things simple, the development has been done in a plain text editor, gvim.  Many find the [Eclipse IDE](/server/confluence/conventions.snippet) a more productive environment. 

## Open a Command Shell

The Atlassian SDK provides several tools that are designed to be launched from a command shell.  Using the table below which shows where in the application launcher for the various operating systems the command shell launcher is, launch the command shell

|         |                                                                 |
|---------|-----------------------------------------------------------------|
| Windows | Start -&gt; All Programs -&gt; Accessories -&gt; Command Prompt |
| OS X    | Applications -&gt; Utilities -&gt; Terminal                     |
| Linux   | Applications Menu -&gt; Accessories -&gt; Terminal              |

## Move to the Working Directory

1.  Open a terminal window and perform the following steps:
2.  Create the working directory for plugin development unless it already exists

{{% note %}}

While a working directory is not strictly necessary, it's a very good practice for reasons described later

{{% /note %}}

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Windows</td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>mkdir <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Linux/Mac</td>
<td><div class="pdl code panel" style="border-width: 1px;">
<div class="panelContent pdl codeContent">
<pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ mkdir <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
</div>
</div></td>
</tr>
</tbody>
</table>

3.  Change into the Directory

    <table>
    <colgroup>
    <col style="width: 20%" />
    <col style="width: 80%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td>Windows</td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">C<span class="op">:&gt;</span>cd <span class="st">&quot;%HOMEDRIVE%%HOMEPATH%</span><span class="sc">\a</span><span class="st">tlas-development&quot;</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    <tr class="even">
    <td>Linux/Mac </td>
    <td><div class="pdl code panel" style="border-width: 1px;">
    <div class="panelContent pdl codeContent">
    <pre class="sourceCode javascript" data-syntaxhighlighter-params="brush: jscript; gutter: false; theme: Confluence" data-theme="Confluence"><code class="sourceCode javascript"><div class="sourceLine" id="1" href="#1" data-line-number="1">darwin<span class="op">:</span>$ cd <span class="op">~</span><span class="ss">/atlas-development</span></div></code></pre>
    </div>
    </div></td>
    </tr>
    </tbody>
    </table>

## Create a Dummy Plugin to be the Host for Confluence

<img src="/server/confluence/plugins/servlet/confluence/placeholder/unknown-macro" class="wysiwyg-unknown-macro" />

## Create the Parent POM.xml file

There are two approaches to setting up the POM.xml files.  The first is to create an empty file called POM.xml in the working directory and maintain independant POM.xml files for every plugin.  The empty POM.xml file in the working directory is to trigger the QuickReload scanner to watch the sibling folders for changes.  The other approach, and the one used in these tutorials, is to collect common values into the working directories' POM.xml.  In this approach any value defined in the parent pom will be inherited by the child pom; however any property also defined in the child pom will override the parent's value.  If the child pom does overrides a property - the value set by the child pom will be used for expanding values in the parent pom.

1.  Copy the POM.xml file in the newly created plugin's directory into the working directory.
2.  Change the &lt;artifactId&gt; value to **parent**
3.  The value for &lt;packaging&gt;atlassian-plugin&lt;/packaging&gt; must be changed to &lt;packaging&gt;pom&lt;/packaging&gt;
4.  (optional) Remove the entries in the &lt;Export-Package&gt; element as nothing will be exported by this plugin.

The command **atlas-create-confluence-plugin** will create a POM.xml file at the top level.  This file is used by the Apache Maven system to figure out dependencies.  The POM provided by default works but has some warnings.  It's good to add some extra information about the plugin and take care of the warnings.

 When working with the POM.xml file, it's important to know changes will not be picked up till the maven is exited and restarted (**atlas-run**, **atlas-package** or **atlas-cli**)

The POM file supports property variables. The body of an xml child element of the properties elemnt with the name **X** and can be inserted anywhere else in the POM.xml file using the syntax **${X}**. See below for a quick example.

``` javascript
<useMyValue>${myValue}</useMyValue>
...
<properties>
   <myKey>myValue</myKey>
</properties>
```

### Setting the Plugin Version and Ownership

While the groupId, artifactId and version were set when the plugin was created with **atlas-create-confluence-plugin**, it's a good idea to edit the POM.xml file and provide real values for the name of the plugin and it's description.  This is also where the version number will be set.

``` javascript
    <groupId>com.atlassian.tutorial</groupId>
    <artifactId>newtest</artifactId>
    <version>1.0.0</version>

    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>

    <name>newtest</name>
    <description>This is the com.atlassian.tutorial:newtest plugin for Atlassian Confluence.</description>
```

{{% note %}}

In the atlassian-plugin.xml file, there will be references to the following variables. They are defined here in the POM.xml file

${project.groupId}  
${project.artifactId}  
${<a href="http://project.name" class="external-link">project.name</a>}  
${<a href="http://project.organization.name" class="external-link">project.organization.name</a>}  
${project.orgainzation.url} 

{{% /note %}}

### Plugin Version Specification

When starting the command line interface, maven may complain that some of the plugin versions have not been specified

{{% note %}}

    [INFO] Scanning for projects...
    [WARNING]
    [WARNING] Some problems were encountered while building the effective model for com.atlassian.tutorial:newtest:atlassian-plugin:1.0.0
    [WARNING] 'build.plugins.plugin.version' for org.apache.maven.plugins:maven-resources-plugin is missing. @ line 147, column 14
    [WARNING]
    [WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
    [WARNING]
    [WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
    [WARNING]

{{% /note %}}

It's unclear how critical this message is; however, it seems to be a pretty strong message.  Experimentally, the following plugins need to have the version explicitly called out to avoid the warning.

``` javascript
....
      <plugin>
         <artifactId>maven-resources-plugin</artifactId>
         <version>${maven.resources.plugin.version}</version>
      </plugin>

      <plugin>
         <artifactId>maven-compiler-plugin</artifactId>
         <version>${maven.compiler.plugin.version}</version>
      </plugin>
 
   </plugins>
...
   <properties>
...
      <maven.resources.plugin.version>2.7</maven.resources.plugin.version>
      <maven.compiler.plugin.version>3.3</maven.compiler.plugin.version>
   </properties>
```

It can be challenging to figure out what valid version numbers are.  When an invalid version number is specified and the project it built (**atlas-package**), maven will attempt to download the version, print out the url and then exit.  Browsing to the location with a regular web browser is an easy way to learn what versions for each of the plugins are available.

In general, the Atlassian maven plugins are hosted at the <a href="https://maven.atlassian.com/content/groups/public/org/apache/maven/plugins/" class="uri external-link">https://maven.atlassian.com/content/groups/public/org/apache/maven/plugins/</a>

### File Encoding Warnings

When building on Windows, the default files encoding defaults to Cp1252 which is not platform independant and maven will show the following to indicate this error.  UTF-8 is the recommended value.

{{% note %}}

\[WARNING\] Using platform encoding (Cp1252 actually) to copy filtered resources, i.e. build is platform dependent!

{{% /note %}}

To set the file encoding, it needs to be added to maven-confluence-plugin, maven-resources-plugin and maven-compiler-plugin.

``` javascript
....
   <plugins>
      <plugin>
         <groupId>com.atlassian.maven.plugins</groupId>
         ...
         <configuration>
            ...
            <encoding>${encoding}</encoding>
         </configuration>
      </plugin>
 
      ...


      <plugin>
         <artifactId>maven-resources-plugin</artifactId>
         ...
         <configuration>
            <encoding>${encoding}</encoding>
         </configuration>
      </plugin>

      <plugin>
         <artifactId>maven-compiler-plugin</artifactId>
         ...
         <configuration>
            <encoding>${encoding}</encoding>
         </configuration>
      </plugin>
   </plugins>
....
   <properties>
...
      <encoding>UTF-8</encoding>
   </properties></project>
```

### Manifest Warnings

When the plugin is first pushed to your development instance of Confluence, there may be a warning published such as:

``` javascript
[INFO] Manifest found, validating...
[WARNING] The manifest should contain versions for all imports to prevent 
ambiguity at install time due to multiple versions of a package.  Here are 
some suggestions for the maven-confluence-plugin configuration generated 
for this project to start from:
  <configuration>
    <instructions>
      <Import-Package>
        org.springframework.context.annotation;version="0.0.0",
        org.springframework.beans;version="3.2.10.RELEASE",
        org.w3c.dom;version="2.11.0-atlassian-01",
        com.atlassian.tutorials.helloworld.api;version="0.0.0",
        com.google.common.*;version="11.0.2",
        org.eclipse.gemini.blueprint.service.*;version="0.0.0",
        org.osgi.util.tracker;version="4.2.1",
        com.atlassian.plugin.osgi.factory;version="4.0.8",
        org.osgi.framework;version="4.2.1",
        org.apache.commons.logging;version="1.7.12",
        com.atlassian.sal.api;version="3.0.5",
        org.springframework.osgi.service.*;version="0.0.0",
        org.springframework.stereotype;version="2.0.8",
        org.springframework.beans.factory*;version="3.2.10.RELEASE",
        org.apache.commons.lang;version="2.6",
        org.springframework.util;version="3.2.8.RELEASE",
        javax.inject;version="1"
      </Import-Package>
    </instructions>
  </configuration>
You may notice many packages you weren't expecting.  This is usually because 
of a bundled jar that references packages that don't apply.  You can usually 
remove these or if necessary, mark them as optional by adding ';resolution:=optional' 
to the package import.  Packages that are detected as version '0.0.0' usually mean 
either they are JDK packages or ones that aren't referenced in your project, 
and therefore, likely candidates for removal entirely.
[INFO] Manifest validated
```

The culprit for the warning comes from the &lt;instructions&gt; element under the maven-confluence-plugin artifact in the POM.xml

``` javascript
<instructions>
    <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>
        
    <!-- Add package to export here -->
    <Export-Package>
        com.atlassian.tutorials.helloworld.api,
    </Export-Package>
    
    <!-- Add package import here -->
    <Import-Package>
        org.springframework.osgi.*;resolution:="optional",
        org.eclipse.gemini.blueprint.*;resolution:="optional",
        *
    </Import-Package>
 
    <!-- Ensure plugin is spring powered - see https://extranet.atlassian.com/x/xBS9hQ  -->
    <Spring-Context>*</Spring-Context>
</instructions>
```

The recommendation from Atlassian is to be explicit about the list to avoid version issues; however, getting the list right is often described as part science and part art.  For more detail, see the page [Constructing the Dependency List in the POM.xml file's &lt;Import-Package&gt; element](/server/confluence/clean-up-the-pom-xml-file.snippet)

**The Parent POM.xml**  Expand source

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>com.example</groupId>
    <artifactId>parent</artifactId>
    <version>1.0.0</version>

    <organization>
        <name>Example Company</name>
        <url>http://www.example.com/</url>
    </organization>
 
    <name>5-9-4</name>
    <description>This is the com.example:5-9-4 plugin for Atlassian Confluence.</description>
    <packaging>pom</packaging>

    <properties>
        <confluence.version>5.9.4</confluence.version>
        <confluence.data.version>5.6.6</confluence.data.version>
        <amps.version>6.2.1</amps.version>
        <plugin.testrunner.version>1.2.3</plugin.testrunner.version>
        <atlassian.spring.scanner.version>1.2.6</atlassian.spring.scanner.version>
        <maven.resources.plugin.version>2.7</maven.resources.plugin.version>
        <maven.compiler.plugin.version>3.3</maven.compiler.plugin.version>
        <encoding>UTF-8</encoding>
        <!-- This key is used to keep the consistency between the key in atlassian-plugin.xml and the key to generate bundle. -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
    </properties>

    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.10</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence</artifactId>
            <version>${confluence.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
            <version>${atlassian.spring.scanner.version}</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-runtime</artifactId>
            <version>${atlassian.spring.scanner.version}</version>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
            <version>1</version>
            <scope>provided</scope>
        </dependency>
 
        <!-- WIRED TEST RUNNER DEPENDENCIES -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-testrunner</artifactId>
            <version>${plugin.testrunner.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
            <version>1.1.1</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.2.2-atlassian-1</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-confluence-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <productVersion>${confluence.version}</productVersion>
                    <productDataVersion>${confluence.data.version}</productDataVersion>
                    <enableQuickReload>true</enableQuickReload>
                    <enableFastdev>false</enableFastdev>
                    <encoding>${encoding}</encoding>
                    <instructions>
                        <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>

                        <!-- Add package to export here -->
                        <Export-Package>
                        </Export-Package>

                        <!-- Add package import here -->
                        <Import-Package>
                            org.springframework.osgi.*;resolution:="optional",
                            org.eclipse.gemini.blueprint.*;resolution:="optional",
                            *
                        </Import-Package>

                        <!-- Ensure plugin is spring powered - see https://extranet.atlassian.com/x/xBS9hQ  -->
                        <Spring-Context>*</Spring-Context>
                    </instructions>
                </configuration>
            </plugin>

            <plugin>
                <groupId>com.atlassian.plugin</groupId>
                <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
                <version>${atlassian.spring.scanner.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>atlassian-spring-scanner</goal>
                        </goals>
                        <phase>process-classes</phase>
                    </execution>
                </executions>
                <configuration>
                    <scannedDependencies>
                        <dependency>
                            <groupId>com.atlassian.plugin</groupId>
                            <artifactId>atlassian-spring-scanner-external-jar</artifactId>
                        </dependency>
                    </scannedDependencies>
                    <verbose>false</verbose>
                </configuration>
            </plugin>

            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <version>${maven.resources.plugin.version}</version>
                <configuration>
                    <encoding>${encoding}</encoding>
                </configuration>
            </plugin>
       
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <configuration>
                    <encoding>${encoding}</encoding>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
 
```

## Edit the Plugin's POM.xml File (The Child POM.xml)

{{% note %}}

This step assumes that the parent directory is setup with a [configured parent POM](#create-the-parent-pom-xml-file).

{{% /note %}}

In the plugin's POM.xml file, a &lt;parent&gt; element must be added allowing redundant elements to be removed.  The &lt;parent&gt; element's group and artifact id need to match the group, artifact and version definitions in the parent POM.xml file ( for example, group = **com.example**, artifact = **parent** and version = **1.0.0**). For the plugins which will be used to host development instances of Confluence, it is important to set the property containing the version of Confluence to be instantiated in the plugin's POM.xml properties section rather than inheriting it from the parent POM.xml.

1.  Add the &lt;parent&gt; element
2.  (optional) Remove redundant elements (For plugins to host a development instance of Confluence, be sure to keep the confluence version property )
3.  (optional) Remove the &lt;Export-Package&gt; if not classes are to be exported.  
      

**Plugin POM.xml**  Expand source

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
 
    <artifactId>5-9-4</artifactId>
    <version>1.0.0</version>
    <name>5-9-4</name>
    <description>This is the com.example:5-9-4 plugin for Atlassian Confluence.</description>
 
    <properties>        
        <confluence.version>5.9.4</confluence.version>
        <!-- This key is used to keep the consistency between the key in atlassian-plugin.xml and the key to generate bundle. -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
    </properties>
    <modelVersion>4.0.0</modelVersion>
    <packaging>atlassian-plugin</packaging>
    <parent>
        <groupId>com.example</groupId>
        <artifactId>parent</artifactId>
        <version>1.0.0</version>
    </parent>
</project>
```

## Remove the Java Code From the Plugin Scaffolding

The command, atlas-create-confluence-plugin, will provide hooks for testing and exporting a java api from the plugin. For simple plugins, such as one designed to deliver some javascript, these hooks are unnecessary complexity and can be removed.

### Removing Unnecessary Java Files

By default there will be a directory created for a plugin api at .../src/main/java/.../api and .../src/main/java/.../impl.  The api and impl directories can be deleted

{{% note %}}

If the default java files are removed, the test directories need to be removed as well as the tests will try and use the java code.

{{% /note %}}

### Removing the Test directories

Be default, there will be directories for test code under .../src/test/java and .../src/test/resources.  The java and resources directories can be deleted.

## Launch the Development Instance of Confluence

{{% note %}}

Confluence may be launched from the plugin's folder or from a sibling plugin's folder if there is a parent POM.xml file. The recommended approach is to have a 'dummy' plugin named by the version of Confluece which gets instantiated.

{{% /note %}}

1.  Open a new command shell (so you can still execute commands in your existing shell)
2.  Change to the root directory of the plugin from which the development instance of Confluence will be launched.  
    *Note, the root directory of the plugin will contain the file "pom.xml"*

    |         |                                                          |
    |---------|----------------------------------------------------------|
    | Windows | c:&gt;cd "%HOMEDIR%%HOMEPATH%\\atlas-development\\5-9-4" |
    | Mac     | $ cd ~/atlas-development/5-9-4                           |

3.  Launch confluence

    ``` javascript
    $ atlas-run
    ```

    By default, the SDK instance of Confluence will be launched with the Developer Mode turned on. While this adds some extra debugging features, it slows Confluence down. If not needed, it can be turned off using the command: `atlas-run -Datlassian.dev.mode=false`

    [Read more about the Confluence Developer Mode](https://developer.atlassian.com/confdev/development-resources/confluence-developer-faq/enabling-developer-mode)

    {{% note %}}

    It can take quite some time for Confluence to get up and running. The very first time Confluence is launched through the SDK, it will download all of the necessary components. The second time, it will reuse what it downloaded early - but it still takes a while for Confluence to get up and running.

    When Confluence is ready, you will see the text:

    ``` javascript
    [INFO] confluence started successfully in XXXs at http://<myserver>:1990/confluence
    [INFO] Type Ctrl-D to shutdown gracefully
    [INFO] Type Ctrl-C to exit
    ```

    You may then open the url printed above (http://&lt;myserveer&gt;:1990/confluence) and login. The default administrator credentials are admin/admin.

    {{% /note %}}

## Log into Confluence

To log into Confluence, visit <a href="http://localhost:1990/confluence" class="uri external-link">http://localhost:1990/confluence</a> in a web browser.  Login as the administrator (username = admin, password = admin)

<img src="https://wiki2.collaboration.is.keysight.com/download/attachments/22282494/image2015-9-28%2015%3A49%3A45.png?version=1&amp;modificationDate=1455032501047&amp;api=v2" class="confluence-external-resource" height="250" />

<img src="/server/confluence/plugins/servlet/confluence/placeholder/unknown-macro" class="wysiwyg-unknown-macro" />

