---
aliases:
- /server/confluence/2031792.html
- /server/confluence/2031792.md
category: devguide
confluence_id: 2031792
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031792
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031792
date: '2018-04-06'
legacy_title: How do I associate my own properties with a ContentEntityObject?
platform: server
product: confluence
subcategory: faq
title: How do I associate my own properties with a ContentEntityObject?
---
# How do I associate my own properties with a ContentEntityObject?

To associate your own properties with a `ContentEntityObject`, you need the
<a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/core/ContentPropertyManager.html" class="external-link">
ContentPropertyManager</a> (see [how to retrieve it](/server/confluence/how-do-i-get-a-reference-to-a-component)).
 This manager allows you to store and retrieve arbitrary string values associated with a `ContentEntityObject`.

Properties are stored as simple key/value pairs. We recommend that anyone writing a third-party plugin uses the standard Java
"reverse domain name" syntax to ensure their keys are unique. Keys may be no longer than 200 characters.

``` java
// Set the property
contentPropertyManager.setText(page, "com.example.myProperty", "This is the value");
// Retrieve it
String myProperty = contentPropertyManager.getText(page, "com.example.myProperty");
```

`getText` and `setText` can store strings of arbitrary length (up to the size-limit for CLOBs in your database).
There are also a `getString` and `setString`, which are slightly more efficient, but limited to 255 characters per value.
