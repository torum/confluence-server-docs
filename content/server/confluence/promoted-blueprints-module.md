---
aliases:
- /server/confluence/promoted-blueprints-module-24084716.html
- /server/confluence/promoted-blueprints-module-24084716.md
category: reference
confluence_id: 24084716
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24084716
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24084716
date: '2017-12-08'
legacy_title: Promoted Blueprints Module
platform: server
product: confluence
subcategory: modules
title: Promoted Blueprints module
---
# Promoted Blueprints module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>Confluence 5.3 and later</p></td>
</tr>
</tbody>
</table>

## Purpose of this module

Currently you can only use this module with a [Space Blueprint](/server/confluence/space-blueprint-module) definition.

A Space Blueprint can "promote" page Blueprints within the space. This means that if a user presses "Create", the "promoted" Blueprints will appear first. If your space Blueprint comes with a set of recommend page Blueprints, we recommend you "promote" them to make them more discoverable and easier to create. For example, the bundled Knowledge Base Space Blueprint promotes two Blueprints by default; the How To and Troubleshooting Articles.

Configuration

The root element for the Promoted Blueprints module is `promoted-blueprints`. It should define one or more `blueprint` elements, referring to the blueprints to be promoted. 

#### Element: `promoted-blueprints`

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<thead>
<tr class="header">
<th>Element</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>blueprint</td>
<td><p>This element must have a <code>ref</code> attribute referring to the complete module key of the blueprint to be promoted. The complete module key is in the following format: &quot;PLUGIN_KEY:BLUEPRINT_MODULE_KEY&quot;.<br />
Note that you can refer to blueprints that are defined outside your plugin. </p>
<p><strong>Required.</strong></p></td>
</tr>
</tbody>
</table>

#### Example

As an example, the following code adds `blueprint-key`, `meeting-notes-blueprint`, `blank-page` and `blog-posts` as promoted content items in the create dialog.

``` xml
<space-blueprint key="example-space-blueprint" i18n-name-key="confluence.blueprints.space.example.name" category="examples">
    <promoted-blueprints>
        <blueprint ref="com.your.plugin.key:blueprint-key"/>
        <blueprint ref="com.atlassian.confluence.plugins.confluence-business-blueprints:meeting-notes-blueprint"/>
        <blueprint ref="com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blank-page"/>
        <blueprint ref="com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blog-post"/>
    </promoted-blueprints>  
...
</space-blueprint>
```
