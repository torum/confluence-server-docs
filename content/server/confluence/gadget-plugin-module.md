---
aliases:
- /server/confluence/gadget-plugin-module-2031852.html
- /server/confluence/gadget-plugin-module-2031852.md
category: reference
confluence_id: 2031852
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031852
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031852
date: '2017-12-08'
legacy_title: Gadget Plugin Module
platform: server
product: confluence
subcategory: modules
title: Gadget Plugin module
---
# Gadget Plugin module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>The Gadget plugin module is available only for OSGi-based plugins in Confluence 3.1 and later.</p></td>
</tr>
</tbody>
</table>

Atlassian gadgets provide a new way to include external content into a Confluence wiki page. Other Atlassian applications also support the gadget module type. Please refer to the [gadget developer documentation](https://developer.atlassian.com/display/GADGETS/Packaging+your+Gadget+as+an+Atlassian+Plugin).
