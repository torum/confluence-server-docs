---
aliases:
- /server/confluence/writing-a-confluence-theme-2818703.html
- /server/confluence/writing-a-confluence-theme-2818703.md
category: devguide
confluence_id: 2818703
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2818703
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2818703
date: '2018-03-26'
legacy_title: Writing a Confluence Theme
platform: server
product: confluence
subcategory: learning
title: Writing a Confluence theme
---
# Writing a Confluence theme

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to Confluence 5.9.1 and higher.</p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>Beginner.</p></td>
</tr>
<tr class="odd">
<td><p>Time estimate:</p></td>
<td><p>It should take you less than 30 minutes to complete this tutorial.</p></td>
</tr>
</tbody>
</table>

## Tutorial overview

This tutorial shows you how to create a Confluence custom theme as a plugin. You'll add `theme` and `colour-scheme` modules to your plugin to personalize look and feel. You can replace your organization's hex codes for the example colors used in this tutorial. 

Concepts covered in this tutorial:

-   Adding `theme` and `colour-scheme` modules to your plugin descriptor.
-   Using [QuickReload](/server/framework/atlassian-sdk/automatic-plugin-reinstallation-with-quickreload/) to reload Confluence with source code changes.

### Before you begin

You should have a basic understanding of HTML and CSS. You can complete this tutorial successfully even if you've never created a plugin before.

### Plugin source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you have finished, you can find the plugin source code on Atlassian Bitbucket. To clone the repository, run the following command:

``` bash
git clone git@bitbucket.org:atlassian_tutorial/creating-a-confluence-theme.git
```

{{% note %}}

About these instructions

You can use any supported combination of operating system and IDE to create this plugin. These instructions were written using Intellij IDEA 2017.2 on macOS Sierra. If you are using another operating system or IDE combination, you should use the equivalent operations for your specific environment.

This tutorial was last tested with **Confluence 6.7.1** using the Atlassian SDK 6.3.10. 

{{% /note %}}

## Step 1. Create and prune the plugin skeleton

In this step, you'll create a plugin skeleton using `atlas-` commands. Because you won't need some of the files created in the skeleton, you'll also delete them in this step.

1.  Open Terminal and navigate to your workspace directory.  

1.  To create a Confluence plugin skeleton, run the following command:

    ``` bash
    atlas-create-confluence-plugin
    ```

1.  To identify your plugin, enter the following information:

    <table>
    <colgroup>
    <col style="width: 30%" />
    <col style="width: 70%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence</code><br />
    </p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>theme-tutorial</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence</code></p></td>
    </tr>
    </tbody>
    </table>

1.  Confirm your entries when prompted with `Y` or `y`.

    Your terminal notifies you of a successful build:

    ``` bash
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESSFUL
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 1 minute 11 seconds
    [INFO] Finished at: Thu Jul 18 11:30:23 PDT 2013
    [INFO] Final Memory: 82M/217M
    [INFO] ------------------------------------------------------------------------
    ```

1.  Navigate to the project directory created in the previous step.

    ``` bash
    cd theme-tutorial/
    ```

1.  Delete the test directories.

    Setting up testing for your plugin isn't part of this tutorial. To delete the generated test skeleton, run the following command:

    ``` bash
    rm -rf ./src/test/java
    rm -rf ./src/test/resources/
    ```

1.  Delete the unneeded Java class files.

    ``` bash
    rm -rf ./src/main/java/com/example/plugins/tutorial/confluence/*
    ```

1.  Import the project into your favorite IDE.

## Step 2. Create a simple theme and update the `atlassian-plugin.xml` descriptor

When you generate the plugin skeleton, a CSS directory is created automatically under `src/main/resources/css`. In this step, you'll add some rudimentary CSS to the file. You'll also declare this CSS resource in the `atlassian-plugin.xml` descriptor file, which describes how your plugin should interact with Confluence. 

You'll add the `Theme` module to disable Confluence's default theme ( `<param name="includeClassicStyles" value="false"/>`), and include a resource to your custom CSS file instead. This module uses an existing Confluence class, `BasicTheme`, which instructs Confluence to load the theme from your plugin. This module also includes an optional `name` and `description` field, which are both visible from the Confluence Admin pages.

1.  Because there is only runtime dependency on BasicTheme, you must import the package `com.atlassian.confluence.themes` explicitly. In your `pom.xml` file, add it inside `<Import-Package>` tag.

    ``` xml
        <plugin>
            <groupId>com.atlassian.maven.plugins</groupId>
            <artifactId>maven-confluence-plugin</artifactId>
            <version>${amps.version}</version>
            <extensions>true</extensions>
            <configuration>
                <productVersion>${confluence.version}</productVersion>
                <productDataVersion>${confluence.data.version}</productDataVersion>
                <enableQuickReload>true</enableQuickReload>
                <enableFastdev>false</enableFastdev>
                <instructions>
                    <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>
                    <Export-Package>
                        com.atlassian.tutorial.confluence.api,
                    </Export-Package>
                    <Import-Package>
                        com.atlassian.confluence.themes,
                        org.springframework.osgi.*;resolution:="optional",
                        org.eclipse.gemini.blueprint.*;resolution:="optional",
                        *
                    </Import-Package>
                    <Spring-Context>*</Spring-Context>
                </instructions>
            </configuration>
        </plugin>
    ```

1.  In your IDE, navigate to `src/main/resources/css`.  

1.  Add the following CSS to the `theme-tutorial.css` file, save changes, and then close the file.


    ``` css
    #header .aui-header {
        background-color: black;
        border-color: black;
    }
    ```

    This transforms the default Atlassian-blue `#header` to black.  

1.  Open `atlassian-plugin.xml` from `src/main/resources`.  

1.  Add the following `<theme>` module before the closing tag of `<atlassian-plugin/>`:
    Configure `includeClassicStyles` to `false` so Confluence ignores backwards-compatible classic themes. `space-ia` is used to enable space sidebar.
    The paths to resources that you add here point to Confluence directories, not your plugin. You'll add your resource in the next step.
    ``` xml
    <theme key="simpletheme" name="Simple Theme" class="com.atlassian.confluence.themes.BasicTheme">
        <description>A simple custom theme</description>
        <param name="includeClassicStyles" value="false"/>
        <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
            <param name="source" value="webContext"/>
        </resource>
        <space-ia value="true"/>
    </theme>
    ```

1.  Add your CSS file as a resource inside the `</theme>` tag.
    ``` xml
    <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>
    ```

1.  Save the file. Here's what the descriptor file should look like up to this point:  

    ``` xml
    <atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
        <plugin-info>
            <description>${project.description}</description>
            <version>${project.version}</version>
            <vendor name="${project.organization.name}" url="${project.organization.url}" />
            <param name="plugin-icon">images/pluginIcon.png</param>
            <param name="plugin-logo">images/pluginLogo.png</param>
        </plugin-info>
        <!-- add our i18n resource -->
        <resource type="i18n" name="i18n" location="theme-tutorial"/>

        <!-- add our web resources -->
        <web-resource key="theme-tutorial-resources" name="theme-tutorial Web Resources">
            <dependency>com.atlassian.auiplugin:ajs</dependency>

            <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>
            <resource type="download" name="theme-tutorial.js" location="/js/theme-tutorial.js"/>
            <resource type="download" name="images/" location="/images"/>
            <context>theme-tutorial</context>
        </web-resource>

    <theme key="simpletheme" name="Simple Theme" class="com.atlassian.confluence.themes.BasicTheme">
        <description>A simple custom theme</description>
        <param name="includeClassicStyles" value="false"/>
        <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
            <param name="source" value="webContext"/>
        </resource>
        <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>
        <space-ia value="true"/>
    </theme>        
    </atlassian-plugin>
    ```

## Step 3. Build, install, and run your plugin

In this step you'll install your plugin and run Confluence. You'll activate your Simple Theme and verify your CSS changes.

1.  Open Terminal window and navigate to the plugin root folder. 

    ``` bash
    cd theme-tutorial/
    ```

1.  To start Confluence from your project root, run the following command.

    ``` bash
    atlas-run
    ```

    This command builds your plugin code, starts a Confluence instance, and installs your plugin. This may take a few minutes.

1.  Locate the URL for Confluence.  
    Your terminal outputs the location of your local Confluence instance, defaulted to <a href="http://localhost:1990/confluence" class="uri external-link">localhost:1990/confluence</a>.

    ``` bash
    [INFO] confluence started successfully in 71s at http://localhost:1990/confluence
    [INFO] Type CTRL-D to shutdown gracefully
    [INFO] Type CTRL-C to exit
    ```

1.   In your browser, navigate to your local Confluence instance, and log in with `admin` / `admin`.

1.  In the navigation bar, click the **Cog icon** > **General Configuration**.  
    The administration console loads.  

1.  In the left-hand sidebar, under **Look & Feel**, click **Themes**.
    The Site Theme page loads, with your custom-created "Simple Theme" listed as an option. Note the title and descriptive text match what you entered.  
    <img src="/server/confluence/images/themespageconfluence.png" title="The Themes page in Confluence" alt="The Themes page in Confluence" width="500" />

1.  Select the **Simple Theme** radio button, and then click **Confirm**.  
    The page reloads, with no changes apparent: the changes you made don't effect the admin page.   

1.  To return to the main dashboard, click the **Confluence** logo in the upper-left corner.  
    Your CSS is installed. All spaces, except for the admin console, will have this navigation color scheme now.   
    <img src="/server/confluence/images/confluencelookingsharpinblack.png" title="Confluence looking sharp in black" alt="Confluence looking sharp in black" width="500" />

## Step 4. Add a `colour-scheme` module to the descriptor file

Black is classic, but in this step you'll use a sophisticated `colour-scheme` module to your descriptor file. Right now, your plugin is pretty basic — it's a theme that just turns the header black. Here, you'll expand your theme to add multiple colors, and use [QuickReload](/server/framework/atlassian-sdk/automatic-plugin-reinstallation-with-quickreload/) to reload the changes in Confluence. 

1.  Open `atlassian-plugin.xml` file.   

1.  Between the `</theme>` and `</atlassian-plugin>` closing tags, add the following code.

    ``` xml
        <colour-scheme key="earth-colours" name="Brown and Red Earth Colours" class="com.atlassian.confluence.themes.BaseColourScheme">
            <colour key="property.style.topbarcolour" value="#440000"/>
            <colour key="property.style.spacenamecolour" value="#999999"/>
            <colour key="property.style.headingtextcolour" value="#663300"/>
            <colour key="property.style.linkcolour" value="#663300"/>
            <colour key="property.style.bordercolour" value="#440000"/>
            <colour key="property.style.navbgcolour" value="#663300"/>
            <colour key="property.style.navtextcolour" value="#ffffff"/>
            <colour key="property.style.navselectedbgcolour" value="#440000"/>
            <colour key="property.style.navselectedtextcolour" value="#ffffff"/>
        </colour-scheme>
    ```

    This creates an earthy theme with a white background, with different links, text colors, and other attributes.

1.  Tie your `colour-scheme` module to your `theme` module. 

    Before the closing `</theme>` tag, add the following code to include it in the resources.

    ``` xml
     <colour-scheme key="${project.groupId}.${project.artifactId}:earth-colours"/>
    ```

1.  Save and close the file.   

1.  To rebuild your plugin, run `atlas-package` command, and [QuickReload](/server/framework/atlassian-sdk/automatic-plugin-reinstallation-with-quickreload/) ensures you see the changes applied.

{{% note %}}

If you don't see the changes:

You may need to return to **Confluence Admin &gt; Themes** and toggle your theme on or off. Then, even from the admin page, you'll see the changes applied.

{{% /note %}}

<img src="/server/confluence/images/earthtonethemeapplied.png" title="Earth theme applied" alt="Earth theme applied" width="500" />

This is what your `atlassian-plugin.xml` descriptor file should resemble at this point:

``` xml
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
    </plugin-info>
      <!-- add our i18n resource -->
    <resource type="i18n" name="i18n" location="theme-tutorial"/>    
    <!-- add our web resources -->
    <web-resource key="theme-tutorial-resources" name="theme-tutorial Web Resources">
        <dependency>com.atlassian.auiplugin:ajs</dependency>        
        <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>
        <resource type="download" name="theme-tutorial.js" location="/js/theme-tutorial.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>theme-tutorial</context>
    </web-resource>  

    <theme key="simpletheme" name="Simple Theme" class="com.atlassian.confluence.themes.BasicTheme">
        <description>A simple custom theme</description>
        <param name="includeClassicStyles" value="false"/>
        <resource type="download" name="default-theme.css" location="/includes/css/default-theme.css">
            <param name="source" value="webContext"/>
        </resource>        
        <resource type="download" name="theme-tutorial.css" location="/css/theme-tutorial.css"/>        
        <colour-scheme key="${project.groupId}.${project.artifactId}:earth-colours"/>
        <space-ia value="true"/>
    </theme>
    <colour-scheme key="earth-colours" name="Brown and Red Earth Colours" class="com.atlassian.confluence.themes.BaseColourScheme">
        <colour key="property.style.topbarcolour" value="#440000"/>
        <colour key="property.style.spacenamecolour" value="#999999"/>
        <colour key="property.style.headingtextcolour" value="#663300"/>
        <colour key="property.style.linkcolour" value="#663300"/>
        <colour key="property.style.bordercolour" value="#440000"/>
        <colour key="property.style.navbgcolour" value="#663300"/>
        <colour key="property.style.navtextcolour" value="#ffffff"/>
        <colour key="property.style.navselectedbgcolour" value="#440000"/>
        <colour key="property.style.navselectedtextcolour" value="#ffffff"/>
    </colour-scheme>    
</atlassian-plugin>
```

## Next steps

You've changed the look and feel of your Confluence instance. For a more customized workspace in Confluence, you might consider <a href="https://confluence.atlassian.com/doc/customizing-site-and-space-layouts-149374.html" class="external-link">Customizing layouts of your Confluence spaces.</a>
