---
aliases:
- /server/confluence/confluence-test-data-for-plugin-developers-11927560.html
- /server/confluence/confluence-test-data-for-plugin-developers-11927560.md
category: devguide
confluence_id: 11927560
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=11927560
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=11927560
date: '2017-12-08'
guides: guides
legacy_title: Confluence Test data for Plugin Developers
platform: server
product: confluence
subcategory: learning
title: Confluence Test data for Plugin Developers
---
# Confluence Test data for Plugin Developers

When testing a Confluence plugin, it's a good idea to use test data that demonstrates a broad range of unexpected input that users could enter into production instances. Test data makes it significantly easier to find bugs and harden your code during development.

The data provided is the same set that Atlassian uses for testing the core Confluence product and Confluence plugins. The data has a number of features:

-   XSS attack strings in many fields
-   separate admin (full sys-admin) and cadmin (non-sys-admin) users
-   long strings in many fields
-   anonymously-accessible content
-   spaces using non-default themes
-   customised HTML header
-   demonstration space and Smoke test space content
-   examples of macro usages

{{% note %}}

Test Data Compatibility

To work with Confluence 4.3 use this file: [generated-test-resources-4.3.zip](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/11927560/21528917.zip)  
To work with Confluence 5.0 and above use this file: [generated-test-resources.zip](https://dac-lf.prod.atl-paas.net/server/confluence/attachments/11927560/21528918.zip)

{{% /note %}}

## How to load the test data

To automatically load the data when starting your plugin with AMPS, you download the data and reference it from your plugin's `pom.xml`.

1.  Download the test data linked above to your local machine.  
    Make sure you place the `ZIP` file in a location accessible to your plugin code.
2.  Change directory to your Confluence plugin project folder.
3.  Edit the project's `pom.xml` file.
4.  Locate the `<configuration>` element for the `<artifactId>maven-confluence-plugin</artifactId>` plugin.
5.  Add a `<productDataPath>` sub element whose value is the absolute path to the `QAData-Confluence.zip` file you downloaded.  
    The following example demonstrates a Mac OSX address:

    ``` xml
    <plugin>
        <groupId>com.atlassian.maven.plugins</groupId>
        <artifactId>maven-confluence-plugin</artifactId>
        <version>${amps.version}</version>
        <extensions>true</extensions>
        <configuration>
            <productVersion>${confluence.version}</productVersion>
            <productDataVersion>${confluence.data.version}</productDataVersion>
            <productDataPath>/Users/mhrynczak/confluence/testdata/generated-test-resources.zip</productDataPath>
        </configuration>
    </plugin>
    ```

6.  Save your changes to the `pom.xml` file.
7.  Use the `atlas-run` command to start Confluence and load your plugin.  
    After it starts successfully, the `atlas-run` output provides you with the base URL for the  
    Confluence instance:

    ``` bash
    ... [WARNING] [talledLocalContainer] INFO: Server startup in 124054 ms [INFO] [talledLocalContainer] Tomcat 6.x started on port [1990] [INFO] confluence started successfully in 186s at http://myhost.local:1990/confluence [INFO] Type Ctrl-D to shutdown gracefully [INFO] Type Ctrl-C to exit
    ```

    In this example, the base URL is http://myhost.local:1990/confluence. You can change this base URL from Confluence's general configuration page.

To check that this has worked successfully, load the base URL in a browser. You should see the Confluence Dashboard, with a yellow customised banner. There are two spaces visible to an anonymous user - `Demonstration Space` and `Smoke Test Space`. If you log in, you will be able to view additional test spaces.

Confluence requires that you install plugins, configure mail servers, and other restricted tasks as the **admin** user, with full sysadmin rights. Atlassian recommends that you test other admin tasks as the **cadmin** user, which does not have full sys-admin rights, and test all other features using the **test** user. The test user uses `test` for both its username and password. The system administrator user uses `admin` for both its username and password. The Confluence administrator uses `cadmin` for both its username and password.

If you have problems using the data or feedback on the data itself, please use the **Feedback** button on this page.

## Create your own test data

You can add your own data to this dataset, or start a new one from scratch. To extract your own data to use when starting Confluence with AMPS, see [atlas-create-home-zip](https://developer.atlassian.com/display/DOCS/atlas-create-home-zip).
