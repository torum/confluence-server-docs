---
aliases:
- /server/confluence/plugin-development-tutorials-39368904.html
- /server/confluence/plugin-development-tutorials-39368904.md
category: devguide
confluence_id: 39368904
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368904
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368904
date: '2017-12-08'
legacy_title: Plugin Development Tutorials
platform: server
product: confluence
subcategory: other
title: Plugin Development Tutorials
---
# Plugin Development Tutorials

-   [Setting up the Working Environment](/server/confluence/setting-up-the-working-environment.snippet)
-   [Hello World](/server/confluence/hello-world.snippet)
-   [Giants Color Theme](/server/confluence/giants-color-theme.snippet)
-   [HTML5 Multimedia](/server/confluence/html5-multimedia.snippet)
