---
aliases:
- /server/confluence/39368876.html
- /server/confluence/39368876.md
category: devguide
confluence_id: 39368876
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368876
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368876
date: '2017-12-08'
legacy_title: Constructing the Dependency List in the POM.xml file's <Import-Package>
  element
platform: server
product: confluence
subcategory: other
title: Constructing the Dependency List in the POM.xml file's <Import-Package> element
---
# Constructing the Dependency List in the POM.xml file's element

## Introduction

Atlassian Plugins are OSGi bundles. OSGI is a very involved topic, but the primary advantage is it wraps Java code in it's own shell so that bundles can be loaded dynamically into an environment without disrupting the other bundles or the environment.  This is done by giving every bundle it's own class loader so that different bundles can run different versions of the same class simultaneously without conflict.  To make this work correctly, imported libraries (and the required versions) should be explicitly called out in the bundle The Atlassian SDK hides much of the work for this process, but can not hide it all.

## The Default Confluence Plugin

If a plugin is constructed with** atlas-create-confluence-plugin**, a default &lt;instructions&gt; statement is added into the POM.xml file for the maven-confluence-plugin element.  If the instructions block is not present, the plugin will be transformed into an OSGi bundle when it is loaded into Confluence.  However, Atlassian recommends being explicity about the dependant packages and creating a transformerless plugin as it allows Confluence to load faster.

**POM.xml excerpt**  Expand source

``` javascript
...
    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-confluence-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <productVersion>${confluence.version}</productVersion>
                    <productDataVersion>${confluence.data.version}</productDataVersion>
                    <enableQuickReload>true</enableQuickReload>
                    <enableFastdev>false</enableFastdev>
                    <instructions>
                        <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>

                        <!-- Add package to export here -->
                        <Export-Package>
                            com.atlassian.tutorials.helloworld.api,
                        </Export-Package>

                        <!-- Add package import here -->
                        <Import-Package>
                            org.springframework.osgi.*;resolution:="optional",
                            org.eclipse.gemini.blueprint.*;resolution:="optional",
                            *
                        </Import-Package>

                        <!-- Ensure plugin is spring powered - see https://extranet.atlassian.com/x/xBS9hQ  -->
                        <Spring-Context>*</Spring-Context>
                    </instructions>
                    <encoding>${encoding}</encoding>
                </configuration>
            </plugin>
            ...
      </plugins>
   </build>
...
```

When the plugin is built through the Atlassian SDK, the default configuration will generate a warning statement

``` javascript
[INFO] Manifest found, validating...
[WARNING] The manifest should contain versions for all imports to prevent 
ambiguity at install time due to multiple versions of a package.  Here are 
some suggestions for the maven-confluence-plugin configuration generated 
for this project to start from:
  <configuration>
    <instructions>
      <Import-Package>
        org.springframework.context.annotation;version="0.0.0",
        org.springframework.beans;version="3.2.10.RELEASE",
        org.w3c.dom;version="2.11.0-atlassian-01",
        com.atlassian.tutorials.helloworld.api;version="0.0.0",
        com.google.common.*;version="11.0.2",
        org.eclipse.gemini.blueprint.service.*;version="0.0.0",
        org.osgi.util.tracker;version="4.2.1",
        com.atlassian.plugin.osgi.factory;version="4.0.8",
        org.osgi.framework;version="4.2.1",
        org.apache.commons.logging;version="1.7.12",
        com.atlassian.sal.api;version="3.0.5",
        org.springframework.osgi.service.*;version="0.0.0",
        org.springframework.stereotype;version="2.0.8",
        org.springframework.beans.factory*;version="3.2.10.RELEASE",
        org.apache.commons.lang;version="2.6",
        org.springframework.util;version="3.2.8.RELEASE",
        javax.inject;version="1"
      </Import-Package>
    </instructions>
  </configuration>
You may notice many packages you weren't expecting.  This is usually because 
of a bundled jar that references packages that don't apply.  You can usually 
remove these or if necessary, mark them as optional by adding ';resolution:=optional' 
to the package import.  Packages that are detected as version '0.0.0' usually mean 
either they are JDK packages or ones that aren't referenced in your project, 
and therefore, likely candidates for removal entirely.
[INFO] Manifest validated
```

Ideally, the &lt;Import-Package&gt; text from the warning statement could be cut and pasted into the POM.xml and the warning would disappear; however, this turns out to not be the case.  Some of the packages listed in the warning, for example the com.atlassian.tutorials.helloworld.api class, are provided by the module not by other bundles and will therefore not be found in other bundles.  Also, the warning may indicate versions of plugins that do not exist.  A better way to get the list is to use the OSGi browser from a SDK instance of Conflunce.

So, step by step

1.  Build and upload the plugin to and SDK Devlopment Instance of Confluence 
2.  Login as an administrator
3.  Visit the General Configuration page
4.  Open the OSGi page in the **Atlassian Marketplace** section

5.  Filter for your plugin

6.  Use the tool to see the list of Import-Package

7.  Edit the &lt;Import-Package&gt; block in the POM.xml to reflect the imported packages
    1.  note, a line is defined as **&lt;class&gt;;version="X.X.X";resolution:="optional",**
    2.  A simple version such as "0.0.0" means that version of greater is required.  The syntax "\[0.0,2.0)" means any version bewteen 0.0 and 2.0
    3.  The **resolution:="optional"** is itself an optional attribute 
8.  Rebuild and upload the plugin

## A working list for the default plugin

At the time of this writing (Confluence 5.9.4), the following &lt;Import-Package&gt; statement worked for the default plugin

``` javascript
                        <Import-Package>
                           org.springframework.osgi.service.exporter.support;version="0.0.0";resolution:="optional",
                           org.springframework.osgi.service.importer.support;version="0.0.0";resolution:="optional",
                           org.eclipse.gemini.blueprint.service.exporter;version="0.0.0";resolution:="optional",
                           org.eclipse.gemini.blueprint.service.exporter.support;version="0.0.0";resolution:="optional",
                           org.eclipse.gemini.blueprint.service.importer.support;version="0.0.0";resolution:="optional",
                           com.atlassian.plugin.osgi.factory;version="0.0.0",
                           com.atlassian.sal.api;version="[3.0,4)",
                           com.google.common.base;version="0.0.0",
                           com.google.common.collect;version="0.0.0",
                           javax.inject;version="0.0.0",
                           org.apache.commons.lang;version="[2.6,3)",
                           org.apache.commons.logging;version="0.0.0",
                           org.osgi.framework;version="[1.7,2)",
                           org.osgi.util.tracker;version="[1.5,2)",
                           org.springframework.beans;version="0.0.0",
                           org.springframework.beans.factory;version="0.0.0",
                           org.springframework.beans.factory.annotation;version="0.0.0",
                           org.springframework.beans.factory.config;version="0.0.0",
                           org.springframework.beans.factory.parsing;version="0.0.0",
                           org.springframework.beans.factory.support;version="0.0.0",
                           org.springframework.beans.factory.xml;version="0.0.0",
                           org.springframework.context.annotation;version="0.0.0",
                           org.springframework.stereotype;version="0.0.0",
                           org.springframework.util;version="0.0.0",
                           org.w3c.dom;version="0.0.0"
                        </Import-Package
```

## Some useful OSGi tools

### Exported Beans: To see what classes can be imported into a module using Spring

visit the following in a browser: http://&lt;confluence\_url&gt;/admin/pluginexports.action

### OSGi Viewer

http://&lt;confluence\_url&gt;/plugins/servlet/upm/osgi

### OSGI Browser (Felix)

http://&lt;confluence\_url&gt;/plugins/servlet/system/console/bundles
