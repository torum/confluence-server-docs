---
aliases:
- /server/confluence/renderer-component-module-2031807.html
- /server/confluence/renderer-component-module-2031807.md
category: reference
confluence_id: 2031807
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031807
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031807
date: '2017-12-08'
legacy_title: Renderer Component Module
platform: server
product: confluence
subcategory: modules
title: Renderer Component module
---
# Renderer Component module

{{% note %}}

Renderer Component plugin modules are available in Confluence 2.8 and later.

{{% /note %}}

Renderer Component plugins allow you to add additional processors when converting wiki markup to HTML

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read <a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

## Warning

This document concerns plugging renderer components into Confluence. The implementation of renderer components themselves is not documented.

Renderer component plugins were added to Confluence to make certain development tasks easier inside Atlassian. They are documented here for Atlassian developers, and for the sake of completeness, but we *do not* recommend customers add their own plugins to this area. The wiki markup rendering process is quite fragile, and simple changes can have wide-reaching effects.

In other words, you're on your own here. 

## Defining a Renderer Component Plugin

Here's a sample atlassian-plugin.xml fragment:

``` xml
<renderer-component key="foo" name="Foo Renderer" class="com.example.frobozz.FooRendererComponent" weight="1000"> 
    <description>Convert foo markup to HTML</description> 
</renderer-component> 
```

-   The **key** is the **required** plugin module key, which must be unique within the plugin.
-   The **name** is the **required** display name for the plugin.
-   The **class** is the **required** class name for the renderer component implementation.
-   The **weight** number is **required**, and determines the order in which renderer component plugins are run over the wiki markup. Components are run from lowest to highest weights

## Implementing a Renderer Component Plugin

The class referred to by the module descriptor must implement one of the following interfaces:

-   com.atlassian.renderer.v2.components.RendererComponent
-   com.atlassian.renderer.v2.plugin.RendererComponentFactory

This allows you to provide either a component directly, or a factory that can be used to instantiate more complex components. If you are using a factory, you can provide arbitrary parameters that will be passed to the factory when the component is instantiated:

``` xml
<renderer-component key="foo" name="Foo Renderer" class="com.example.frobozz.FooRendererComponentFactory" weight="1000"> 
     <param name="animal">monkey</param>
     <param name="vegetable">lettuce</param>
     <param name="mineral">quartz</param>
</renderer-component> 
```

These parameters will be passed into the factory's instantiate method as a Map&lt;String, String&gt;.

