---
aliases:
- /server/confluence/2031700.html
- /server/confluence/2031700.md
category: devguide
confluence_id: 2031700
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031700
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031700
date: '2017-12-08'
legacy_title: How do I develop against Confluence with Secure Administrator Sessions?
platform: server
product: confluence
subcategory: faq
title: How do I develop against Confluence with secure administrator sessions?
---
# How do I develop against Confluence with secure administrator sessions?

Secure administrator sessions is a security feature introduced in Confluence 3.3. This provides an additional layer of authentication for administration functions. If you are developing a plugin for Confluence 3.3 or later, you will need to take note of the information below.

{{% note %}}

The information on this page relates to plugin development using AMPS/Atlassian Plugin SDK. Atlassian Maven Plugin Suite (AMPS) is part of the <a href="#atlassian-plugin-sdk" class="unresolved">Atlassian Plugin SDK</a>. We strongly recommend that you use the Atlassian Plugin SDK to build plugins for our Confluence. It includes a number of features that simplify the plugin development process.

{{% /note %}}

You **must** run Confluence (3.3 and later) in [developer mode](/server/confluence/enabling-developer-mode) to develop against Confluence using AMPS or deploy a plugin using the Atlassian Plugin SDK. If you do not do this, you will receive an exception when deploying the plugin. This is because the plugin will be expecting the plugin upload screen when it is uploaded, but will get the secure administration session authentication screen instead.

Please note, if you use AMPS to develop against Confluence, it will start Confluence in [developer mode](/server/confluence/enabling-developer-mode). This will automatically disable the secure administrator session authentication checks, so you should not encounter any problems. You also will not run into this problem if you are developing against Confluence 3.2 and earlier, as these versions do not have the secure administrator sessions feature.

#### Plugin Development

Currently only WebWork Modules are protected by the temporary secure administrator sessions. Other plugin types, such as [REST services](/server/confluence/rest-module) or [servlets](/server/confluence/servlet-module) are not checked for an administrator session.

All [webwork modules](/server/confluence/xwork-webwork-module) mounted under `/admin` will automatically be protected by secure administrator sessions. To opt out of this protection you can mark your class or webwork action method with the <a href="http://docs.atlassian.com/confluence/latest/com/atlassian/confluence/security/websudo/WebSudoNotRequired.html" class="external-link">WebSudoNotRequired</a> annotation. Conversely, all webwork actions mounted outside the `/admin` namespace are not protected and can be opted in by adding the <a href="http://docs.atlassian.com/confluence/latest/com/atlassian/confluence/security/websudo/WebSudoRequired.html" class="external-link">WebSudoRequired</a> annotation.

Both of these annotations work on the class or the action method. If you mark a method with the annotation, only action invocations invoking that method will be affected by the annotations. If you annotate the class, any invocation to that class will be affected. Sub-classes inherit these annotations.
