---
aliases:
- /server/confluence/2031739.html
- /server/confluence/2031739.md
category: devguide
confluence_id: 2031739
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031739
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031739
date: '2017-12-08'
legacy_title: How do I find the logged in user?
platform: server
product: confluence
subcategory: faq
title: How do I find the logged in user?
---
# How do I find the logged in user?

### <img src="http://confluence.atlassian.com/images/icons/favicon.png" class="confluence-external-resource" /> How do I find the logged in user?

This can be retrieved easily from the `com.atlassian.confluence.user.AuthenticatedUserThreadLocal` class which will give you the current logged in user as a `com.atlassian.confluence.user.ConfluenceUser` object.

``` java
ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
```

Should the user not be logged in the `user` object will be null.
