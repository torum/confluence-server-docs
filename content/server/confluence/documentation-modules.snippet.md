---
aliases:
- /server/confluence/documentation-modules-39368848.html
- /server/confluence/documentation-modules-39368848.md
category: devguide
confluence_id: 39368848
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368848
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368848
date: '2017-12-08'
legacy_title: Documentation Modules
platform: server
product: confluence
subcategory: other
title: Documentation Modules
---
# Documentation Modules

The pages below are modules designed to be included in other pages.  This simplifies the maintenance work by having the content centralized.

-   [Clean Up the POM.xml File](/server/confluence/clean-up-the-pom-xml-file.snippet)
-   [Constructing the Dependency List in the POM.xml file's &lt;Import-Package&gt; element](/server/confluence/constructing-the-dependency-list-in-the-pom-xml-files-<import-package>-element.snippet)
-   [Conventions](/server/confluence/conventions.snippet)
-   [Create a Confluence Plugin](/server/confluence/create-a-confluence-plugin.snippet)
-   [Create the Parent POM.xml File](/server/confluence/create-the-parent-pom-xml-file.snippet)
-   [Launching Confluence](/server/confluence/launching-confluence.snippet)
-   [Launch the Command Line Interface (CLI)](/server/confluence/launch-the-command-line-interface-cli.snippet)
-   [Link to the Parent POM.xml](/server/confluence/link-to-the-parent-pom-xml.snippet)
-   [Log into Confluence](/server/confluence/log-into-confluence.snippet)
-   [Open a Command Shell](/server/confluence/open-a-command-shell.snippet)
-   [Package and Install a Plugin](/server/confluence/package-and-install-a-plugin.snippet)
-   [Package the Plugin](/server/confluence/package-the-plugin.snippet)
-   [Plugin Development Deployment Paradigm](/server/confluence/plugin-development-deployment-paradigm.snippet)
-   [Removing Unnecessary Complexity](/server/confluence/removing-unnecessary-complexity.snippet)
-   [The Working Directory](/server/confluence/the-working-directory.snippet)
-   [Validate the Install](/server/confluence/validate-the-install.snippet)
-   [Wrappers](/server/confluence/wrappers.snippet)
