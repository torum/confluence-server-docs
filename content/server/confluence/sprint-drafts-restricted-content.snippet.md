---
aliases:
- /server/confluence/sprint-drafts-restricted-content-13009024.html
- /server/confluence/sprint-drafts-restricted-content-13009024.md
category: devguide
confluence_id: 13009024
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13009024
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13009024
date: '2017-12-08'
legacy_title: SPRINT DRAFTS - restricted content
platform: server
product: confluence
subcategory: other
title: SPRINT DRAFTS - restricted content
---
# SPRINT DRAFTS - restricted content

This page is a container for the tutorials developed during the <a href="https://confluence.atlassian.com/display/DOCSPRINT/Doc+Sprint+-+August+2012" class="external-link">August 2012 doc sprint</a>. The page restrictions on this page ensure that it, and all its children, are visible to the following people only:

-   Atlassian staff (the group).
-   Community authors ( the group of [ACLA](https://developer.atlassian.com/display/ABOUT/Atlassian+Contributor+License+Agreement) signees).
-   Individual doc sprinters who have not signed the ACLA.

After review, we will move the pages out from under this page, making them children of [Tutorials](/server/confluence/tutorials) and thus visible to everyone.

## Child pages

-   [draft - Adding a Custom Action to Confluence](/server/confluence/draft-adding-a-custom-action-to-confluence-13631686.html)
-   [Writing Integration Tests for Your Plugin](/server/confluence/writing-integration-tests-for-your-plugin.snippet)
-   [Altering Confluence page output using a Pipeline Transformer module](/server/confluence/altering-confluence-page-output-using-a-pipeline-transformer-module.snippet)
-   [Copy of - Plugin Tutorial - Defining a Pluggable Service in a Confluence Plugin](/server/confluence/copy-of-plugin-tutorial-defining-a-pluggable-service-in-a-confluence-plugin-13631958.html)
-   [Rewrites for Review](/server/confluence/rewrites-for-review.snippet)
    -   [Confluence Themes](/server/confluence/confluence-themes.snippet)
        -   [Writing a Confluence Theme - Draft](/server/confluence/writing-a-confluence-theme-draft.snippet)
    -   [Rewrite Notes](/server/confluence/rewrite-notes.snippet)
    -   [Writing a Basic Plugin](/server/confluence/writing-a-basic-plugin.snippet)
        -   [Create and prune the plugin skeleton](/server/confluence/create-and-prune-the-plugin-skeleton.snippet)

