---
aliases:
- /server/confluence/cql-function-reference-29952571.html
- /server/confluence/cql-function-reference-29952571.md
category: reference
confluence_id: 29952571
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=29952571
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=29952571
date: '2018-04-26'
legacy_title: CQL Function Reference
platform: server
product: confluence
subcategory: api
title: CQL function reference
---
# CQL function reference

The instructions on this page describe how to use functions in CQL to define structured search
queries to [search for content in Confluence](/server/confluence/advanced-searching-using-cql).  

## Functions Reference

A function in CQL appears as a word followed by parentheses, which may contain one or more explicit values.
In a clause, a function is preceded by an [operator](/server/confluence/advanced-searching-using-cql#operator-reference) that
in turn is preceded by a [field](/server/confluence/advanced-searching-using-cql#field-reference).
A function performs a calculation on either specific Confluence data or the function content in parentheses,
such that only true results are retrieved by the function and then again by the clause in which the function is used.

## List of Functions

This list provides explanations and examples for the following functions:

* currentUser()
* endOfDay()
* endOfMonth()
* endOfWeek()
* endOfYear()
* startOfDay()
* startOfMonth()
* startOfWeek()
* startOfYear()
* favouriteSpaces()
* recentlyViewedContent()
* recentlyViewedSpaces()

#### Current user

Perform searches based on the currently logged-in user.

Note that this function can only be used by logged-in users. Anonymous users cannot use this function.

###### Syntax

``` sql
currentUser()
```

###### Supported Fields

-   [creator](/server/confluence/advanced-searching-using-cql/#creator)
-   [contributor](/server/confluence/advanced-searching-using-cql/#contributor)
-   [mention](/server/confluence/advanced-searching-using-cql/#mention)
-   [watcher](/server/confluence/advanced-searching-using-cql/#watcher)
-   [favourite](/server/confluence/advanced-searching-using-cql/#favourite-favorite)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content that was created by me.

    ``` sql
    creator = currentUser()
    ```

-   Find content that mentions me but wasn't created by me.

    ``` sql
    mention = currentUser() and creator != currentUser()
    ```

#### End of day

Perform searches based on the end of the current day.

###### Syntax

``` sql
endOfDay()
```

or

``` sql
endOfDay("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [created](/server/confluence/advanced-searching-using-cql/#created)
-   [lastmodified](/server/confluence/advanced-searching-using-cql/#lastmodified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the end of yesterday.

    ``` sql
    created > endOfDay("-1d")
    ```

#### End of month

Perform searches based on the end of the current month.

###### Syntax

``` sql
endOfMonth()
```

or

``` sql
endOfMonth("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [created](/server/confluence/advanced-searching-using-cql/#created)
-   [lastmodified](/server/confluence/advanced-searching-using-cql/#lastmodified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content modified before the end of the month.

    ``` sql
    lastmodified < endOfMonth()
    ```

#### End of week

Perform searches based on the end of the current week.

###### Syntax

``` sql
endOfWeek()
```

or

``` sql
endOfWeek("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [created](/server/confluence/advanced-searching-using-cql/#created)
-   [lastmodified](/server/confluence/advanced-searching-using-cql/#lastmodified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created after the end of last week.

    ``` sql
    created > endOfWeek("-1w")
    ```

#### End of year

Perform searches based on the end of the current year.

###### Syntax

``` sql
endOfYear()
```

or

``` sql
endOfYear("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [created](/server/confluence/advanced-searching-using-cql/#created)
-   [lastmodified](/server/confluence/advanced-searching-using-cql/#lastmodified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created by the end of this year.

    ``` sql
    created < endOfYear()
    ```

#### Start of day

Perform searches based on the start of the current day.

###### Syntax

``` sql
startOfDay()
```

or

``` sql
startOfDay("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [created](/server/confluence/advanced-searching-using-cql/#created)
-   [lastmodified](/server/confluence/advanced-searching-using-cql/#lastmodified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the start of today.

    ``` sql
    created > startOfDay()
    ```

-   Find content created in the last 7 days.

    ``` sql
    created > startOfDay("-7d")
    ```

#### Start of month

Perform searches based on the start of the current month.

###### Syntax

``` sql
startOfMonth()
```

or

``` sql
startOfMonth("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [created](/server/confluence/advanced-searching-using-cql/#created)
-   [lastmodified](/server/confluence/advanced-searching-using-cql/#lastmodified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the start of the month.

    ``` sql
    created >= startOfMonth()
    ```

#### Start of week

Perform searches based on the start of the current week.

###### Syntax

``` sql
startOfWeek()
```

or

``` sql
startOfWeek("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [created](/server/confluence/advanced-searching-using-cql/#created)
-   [lastmodified](/server/confluence/advanced-searching-using-cql/#lastmodified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created since the start of the week.

    ``` sql
    created >= startOfWeek()
    ```

#### Start of year

Perform searches based on the start of the current year.

``` sql
startOfYear()
```

or

``` sql
startOfYear("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

-   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
-   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [created](/server/confluence/advanced-searching-using-cql/#created)
-   [lastmodified](/server/confluence/advanced-searching-using-cql/#lastmodified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created this year.

    ``` sql
    created >= startOfYear()
    ```

#### Now

Perform searches based on current time.

###### Syntax

``` sql
now()
```
or

``` sql
now("inc")
```

where `inc` is an optional increment of `(+/-)nn(y|M|w|d|h|m)`

*   If the plus/minus `(+/-)` sign is omitted, plus is assumed.
*   nn: number; y: year, M: month; w: week; d: day; h: hour; m: minute.

###### Supported Fields

-   [created](/server/confluence/advanced-searching-using-cql/#created)
-   [lastmodified](/server/confluence/advanced-searching-using-cql/#lastmodified)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content created before current time.

    ``` sql
    created <= now()
    ```

###### Available from version

5.7

#### Favorite spaces

Returns a list of space keys, corresponding to the favorite spaces of the logged in user. 

###### Syntax

``` sql
favouriteSpaces()
```

###### Supported Fields

-   [space](/server/confluence/advanced-searching-using-cql/#space)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

###### Examples

-   Find content which exists in the favorite spaces of the logged in user.

``` sql
space IN favouriteSpaces()
```

-   Find content which exists in the favourite spaces of the logged in user as well as other listed spaces

``` sql
space IN (favouriteSpaces(), 'FS', 'TS')
```

###### Available from version

5.9

#### Current space

Returns a current space. Requires `cqlcontext` with `spaceKey`.

###### Syntax

``` sql
currentSpace()
```

###### Supported Fields

-   [space](/server/confluence/advanced-searching-using-cql/#space)

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Yes</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
</tr>
</tbody>
</table>

###### Examples

*   Find content that exists in the current spaces.

``` sql
space = currentSpace()
```

*   Curl example.

``` bash
curl -u user:pwd 'http://localhost:8080/confluence/rest/api/search?cql=space%20=%20currentSpace()&cqlcontext=%7B"spaceKey":"TEST"%7D'
```

###### Available from version

5.7

#### Recently viewed content

Returns a list of IDs of recently viewed content for the logged in user.

###### Syntax

``` sql
recentlyViewedContent(limit, offset)
```

###### Supported Fields

-   ancestor
-   content
-   id
-   parent

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

Examples

-   Find contents with limit to recent 10 history.

``` sql
id in recentlyViewedContent(10)
```

-   Find contents with limit to recent 10 history, but skip first 20.

``` sql
id in recentlyViewedContent(10, 20)
```

###### Available from version

5.9

#### Recently viewed spaces

Returns a list of key of spaces recently viewed by the logged in user.

###### Syntax

``` sql
recentlyViewedSpaces(limit)
```

###### Supported Fields

-   space

###### Supported Operators

<table style="width:100%;">
<colgroup>
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 10%" />
</colgroup>
<thead>
<tr class="header">
<th><p>=</p></th>
<th><p>!=</p></th>
<th><p>~</p></th>
<th><p>!~</p></th>
<th><p>&gt;</p></th>
<th><p>&gt;=</p></th>
<th><p>&lt;</p></th>
<th><p>&lt;=</p></th>
<th><p>IN</p></th>
<th><p>NOT IN</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>No</p></td>
<td><p>Yes</p></td>
<td><p>Yes</p></td>
</tr>
</tbody>
</table>

Examples

-   Find spaces with limit to recent 10 history.

``` sql
 space in recentlyViewedSpaces(10)
```

###### Available from version

5.9

## Reserved Characters

CQL has a list of reserved characters:

-   **space** (`" "`)
-   `"+"`
-   `"."`
-   `","`
-   `";"`
-   `"?"`
-   `"|"`
-   `"*"`
-   `"/"`
-   `"%"`
-   `"^"`
-   `"$"`
-   `"#"`
-   `"@"`
-   `"["`
-   `"]"`

If you wish to use these characters in queries, you need to:

-   surround them with quote-marks (you can use either single quote-marks (`'`) or double quote-marks (`"`));  
    *and*, if you are searching a text field and the character is on the list of <a href="https://confluence.atlassian.com/display/JIRA/Performing+Text+Searches#PerformingTextSearches-escaping" class="external-link">reserved characters for Text Searches</a>.
-   precede them with two backslashes.

## Reserved Words

CQL has a list of reserved words. These words need to be surrounded by quote-marks if you wish to use them in queries:

"after", "and", "as", "avg", "before", "begin", "by","commit", "contains", "count", "distinct", "else", "empty", "end", "explain", "from", "having", "if", "in", "inner", "insert", "into", "is", "isnull", "left", "like", "limit", "max", "min", "not", "null", "or", "order", "outer", "right", "select", "sum", "then", "was", "where", "update"

### Related topics:

*   [Advanced searching using CQL](/server/confluence/advanced-searching-using-cql).
*   [Performing text searches using CQL](/server/confluence/performing-text-searches-using-cql).
*   [CQL field reference](/server/confluence/cql-field-reference).
