---
aliases:
- /server/confluence/custom-actions-with-the-blueprint-api-22020367.html
- /server/confluence/custom-actions-with-the-blueprint-api-22020367.md
category: reference
confluence_id: 22020367
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=22020367
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=22020367
date: '2018-04-09'
legacy_title: Custom actions with the Blueprint API
platform: server
product: confluence
subcategory: api
title: Custom actions with the Blueprint API
---
# Custom actions with the blueprint API

The `confluence-create-content-plugin` provides an  `AbstractCreateBlueprintPageAction`  that you can extend to add custom behavior.
To use this module, make sure your `pom.xml` includes the following dependency:

``` xml
      <dependency>
            <groupId>com.atlassian.confluence.plugins</groupId>
            <artifactId>confluence-create-content-plugin</artifactId>
            <version>${create-content.version}</version>
            <scope>provided</scope>
        </dependency>
```

The `AbstractCreateBlueprintPageAction` provides a couple of useful methods for working with blueprints:

*   `populateBlueprintPage` loads the blueprint content template and generates a blueprint page from the template and the context.
*   `getOrCreateIndexPage` gets the index page for the blueprint or creates one if it doesn't exist, and pins it to the sidebar.

For more details on how to write and link an action in Confluence, see [XWork-WebWork module](/server/confluence/xwork-webwork-module/).

{{% note %}}

You cannot, at this stage, use custom names for your XWork actions that render the editor. You must use `createpage` and `docreatepage`.
Using anything else prevents the buttons on the bottom left of the Confluence editor from displaying.

{{% /note %}}{{% warning %}}

Deprecation Warning

 The `AbstractCreateBlueprintPageAction` is no longer used internally and is slated for removal in a future release.
Please let us know via the feedback form if your plugin currently extends this action, or if you plan to extend it.

{{% /warning %}}

## BlueprintManager interface

If you need to interact with the blueprints API at a low level, you can use `BlueprintManager` ([see how to retrieve it](/server/confluence/how-do-i-get-a-reference-to-a-component)).
It gives access to the following method.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Method</th>
<th> </th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code> createAndPinIndexPage </code></td>
<td><p>Creates an index page if required for this blueprint, and pins it to the Space Sidebar.</p></td>
</tr>
</tbody>
</table>

## UserBlueprintConfigManager

If you need to interact with the blueprint configuration, you can use `UserBlueprintConfigManager` ([see how to retrieve it](/server/confluence/how-do-i-get-a-reference-to-a-component)).
It gives an access to the following method.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Method</th>
<th> </th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code> setBlueprintCreatedByUser </code></td>
<td><p>Flags that a Blueprint of a certain type has been created by a given user</p></td>
</tr>
</tbody>
</table>

## BlueprintContentGenerator interface

If you need to interact with the blueprint API at a low level, you can also use `BlueprintContentGenerator` ([see how to retrieve it](/server/confluence/how-do-i-get-a-reference-to-a-component))
to get access to the render context. It gives an access to following methods.

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="header">
<th>Component Key</th>
<th>Methods</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code> createIndexPageObject </code></td>
<td><p>Creates an index page object given a plugin template and a render context.</p></td>
</tr>
<tr class="even">
<td><p><code> generateBlueprintPageObject </code></p></td>
<td><p>Creates an unsaved content page object for a create-Blueprint-page request.</p></td>
</tr>
</tbody>
</table>
