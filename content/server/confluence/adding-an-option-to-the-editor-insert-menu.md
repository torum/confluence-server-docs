---
aliases:
- /server/confluence/adding-an-option-to-the-editor-insert-menu-13631974.html
- /server/confluence/adding-an-option-to-the-editor-insert-menu-13631974.md
category: devguide
confluence_id: 13631974
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13631974
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13631974
date: '2017-12-08'
legacy_title: Adding an Option to the Editor Insert Menu
platform: server
product: confluence
subcategory: learning
title: Adding an option to the editor insert menu
---
# Adding an option to the editor insert menu

This tutorial shows you how to add an option to the Confluence editor's 'Insert' menu, allowing people to access your plugin feature from within the editor.

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Applicable:</p></td>
<td><p>This tutorial applies to <strong>Confluence 4.3.</strong></p></td>
</tr>
<tr class="even">
<td><p>Level of experience:</p></td>
<td><p>This is an <strong>intermediate</strong> tutorial. You should have completed at least one beginner tutorial before working through this tutorial. See the<a href="https://developer.atlassian.com/display/DOCS/Tutorials"> list of developer tutorials</a>.</p></td>
</tr>
<tr class="odd">
<td>Status</td>
<td><strong>LEGACY</strong> This tutorial applies to Confluence versions that have reached end of life.</td>
</tr>
</tbody>
</table>

## Example code

If you want to take a look at the complete source for this example, you can find it on Bitbucket here: https://bitbucket.org/appfusions/tutorial-editoricon

## Step 1. Create a plugin

If you do not already have a plugin that needs the option in the editor menu, create a new Confluence plugin. You can use the Atlassian Plugin SDK to do this. See [Set up the Atlassian Plugin SDK and Build a Project](https://developer.atlassian.com/display/DOCS/Set+up+the+Atlassian+Plugin+SDK+and+Build+a+Project).

## Step 2. Add your icon image to your resources

You need to set up the icon file that you will use, both in terms of putting the file in the right location in your plugin's resources directory and in terms of defining the image as a resource for use in your `atlassian-plugin.xml` file.

To refer to an image file that you will provide with your plugin, you must define an images directory as a plugin resource. (You can define just the image file as a resource, but it is neater to define a directory. Thisl will mean fewer changes to the plugin file later if you need more images.)

Reference: [Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources).

**Defining an images directory resource and choosing a location for your icon file:**

1.  Edit your `atlassian-plugin.xml` file and add an element to define your images directory like this:

    ``` xml
    <resource name="images/" type="download" location="${project.artifactId}/includes/images"/>
    ```

    Notice the variable `${project.artifactId}` used in the above definition. That is a nice shortcut if you are planning to build multiple plugins with a similar architecture. The Atlassian SDK has defined that variable for you in your `pom.xml`. Let's say, for example, that the artifactId in your `pom.xml` is 'my-macro'. The definition in your `pom.xml` looks like this:

    ``` xml
       <modelVersion>4.0.0</modelVersion>
       <groupId>com.appfusions.confluence.macros</groupId>
       <artifactId>my-macro</artifactId>
    ```

2.  Add your icon file and the necessary directories to your plugin project. Because you are using the Atlassian plugin SDK to build your plugin, we recommend that you use the default location for resources. All your non-Java files should be located under `src/main/resources`. The path to your images directory looks like this:

    ``` bash
    src/main/resources/my-macro/includes/images/
    ```

      
    In our example, the file will be called icon.png. 

## Step 3. Set up the icon CSS class that the editor will use

The icon file in the plugin and the resources definition is in place now. You still need to communicate to the editor to **use** the icon via a CSS class. 

We will follow a standard naming convention to define the CSS class. 

**To set up the CSS:**

1.  Edit the `atlassian-plugin.xml` for your plugin.
2.  Add a `web-resource` definition pointing to your editor CSS class, and set its usage context to be the editor:

    ``` xml
    <web-resource key="editorWebResource" name="Editor Web Resource">      
          <context>editor</context>
          <dependency>com.atlassian.confluence.tinymceplugin:editor-resources</dependency>
          <resource type="download" name="editor.css" location="${project.artifactId}/includes/editor/css/editor.css"/>
    </web-resource>
    ```

3.  Create the CSS class `editor.css` in the indicated location. As with the images resource defined previously, the location will be the `src/main/resources` directory.

    ``` bash
    src/main/resources/my-macro/includes/editor/css/editor.css
    ```

    The naming convention for the relevant class is 'macro-NAMEOFMACRO'. 

    While we have not defined the macro yet, in our example, the name attribute of the macro will be defined as 'mymacro'. So, the class will be called `macro-mymacro`. 

    In addition, we must scope the CSS class within the insert menu, and identify the element that will be styled. So, the full class description will be`#insert-menu .macro-mymacro .icon`. You must set the background of this class to use the icon image resource we defined earlier. This will look like this:  
      

    ``` css
    #insert-menu .macro-mymacro .icon {
       background: transparent url("images/icon.png") 0 0 no-repeat;
       margin-top: 0px;
    }
    ```

## Step 4. Create a macro module type

Your addition to the editor should point to some functionality. For this example, you will add a Confluence macro module.

If you are not sure how to write a Confluence macro, you may want to review the following documentation: 

-   [Macro Module](/server/confluence/macro-module) 
-   [Writing Macros](/server/confluence/writing-macros)
-   [Including Information in your Macro for the Macro Browser](/server/confluence/including-information-in-your-macro-for-the-macro-browser)

**To create a macro module:**

1.  Edit the `atlassian-plugin.xml` for your plugin.
2.  Remove the `<xhtml-macro>`  element containing the '`ExampleMacro`' definition added by the Atlassian Plugin SDK, if present.
3.  Add your macro definition, as shown below.
    -   Note the macro name attribute. We used it previously in the CSS definition, and will use the same name here.
    -   Adjust the class definition in the example below (`com.appfusions.confluence.macros`) to contain the groupId of your macro.

**Example definition**:

``` xml
<xhtml-macro key="mymacro" 
       name="mymacro" 
       class="com.appfusions.confluence.macros.ExampleMacro">

      <description>Description of My Macro goes here</description>
      <category name="external-content"/> <!-- see Including Information in your Macro for the Macro Browser for options -->
      <parameters> <!-- optional, if you have parameters -->
         <parameter name="param1" type="string" required="true">
         </parameter>
         <parameter name="param2" type="string">
         </parameter>
      </parameters>
</xhtml-macro>
```

## Step 5. Create a web-item to point to your macro in the editor

The final step is to define a `web-item` module in your `atlassian-plugin.xml`. This will tell the editor to display your icon as a link to your macro. 

You may wish to review the web-item module documentation:

-   [Web UI Modules](/server/confluence/web-ui-modules)
-   [Web Item Plugin Module](/server/confluence/web-item-plugin-module)

**To add the web-item:**

1.  Edit the `atlassian-plugin.xml` for your plugin.
2.  Add your `web-item` definition.   
    -   Set the section attribute to `system.editor.featured.macros.default`.
    -   Set the label key to the i18n property that you have defined for it. See [Adding Plugin and Module Resources](/server/confluence/adding-plugin-and-module-resources) for an example of definining your i18n properties file.
    -   Set the link's linkId attribute to the name attribute you defined for your macro

It will look like this:

``` xml
<web-item key="editor-macrolinkkey" name="Name of my Editor Feature" section="system.editor.featured.macros.default" weight="11">
    <label key="i18n.property.key.for.the.text.describing.your.icon"/>      
    <link linkId="mymacro"/>
</web-item>
```

## Step 6. Build, install and run the plugin

Follow these steps to build and install your plugin, so that you can test your code. If you have not already started the application, start it now:

-   Open a command window and go to the plugin root folder (where the `pom.xml` is located).
-   Run `atlas-run` (or `atlas-debug` if you might want to launch the debugger in your IDE).

The **atlas-run** command will compile the plugin project and then launch a local instance of Confluence.

Once Confluence has loaded, access your local instance with this URL:

``` bash
http://localhost:1990/confluence/
```

Log in to Confluence with username "admin" and password "admin, then edit a page in the demonstration ('ds') space.

#####  Example

{{% tip %}}

Congratulations, you've completed this tutorial.

{{% /tip %}}
