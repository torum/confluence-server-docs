---
aliases:
- /server/confluence/rendering-velocity-templates-in-a-macro-2031866.html
- /server/confluence/rendering-velocity-templates-in-a-macro-2031866.md
category: devguide
confluence_id: 2031866
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031866
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031866
date: '2018-03-28'
guides: guides
legacy_title: Rendering Velocity templates in a macro
platform: server
product: confluence
subcategory: learning
title: Rendering Velocity templates in a macro
---
# Rendering Velocity templates in a macro

When writing a [Macro plugin](/server/confluence/macro-module) , it's a common requirement to render a Velocity file
with some data provided by your plugin.

The easiest way to render Velocity templates from within a macro is to use `VelocityUtils.getRenderedTemplate`,
and then simply return the result as the macro output. You can use it like this:

``` java
public String execute(Map<String, String> map, String s, ConversionContext conversionContext) throws MacroExecutionException {
    // do something with params ...

    Map context = MacroUtils.defaultVelocityContext();
    context.put("page", page);
    context.put("labels", labels);
    return VelocityUtils.getRenderedTemplate("com/atlassian/confluence/example/sample-velocity.vm", context);
}
```

### RELATED TOPICS

Find more information on a [Macro Module](/server/confluence/macro-module) page.
