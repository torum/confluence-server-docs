---
aliases:
- /server/confluence/link-to-the-parent-pom.xml-39368887.html
- /server/confluence/link-to-the-parent-pom.xml-39368887.md
category: devguide
confluence_id: 39368887
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368887
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368887
date: '2017-12-08'
legacy_title: Link to the Parent POM.xml
platform: server
product: confluence
subcategory: other
title: Link to the Parent POM.xml
---
# Link to the Parent POM.xml

{{% note %}}

This step assumes that the parent directory is setup with a [configured parent POM](#configured-parent-pom).

{{% /note %}}

In the plugin's POM.xml file, a &lt;parent&gt; element must be added allowing redundant elements to be removed.  The &lt;parent&gt; element's group and artifact id need to match the group, artifact and version definitions in the parent POM.xml file ( for example, group = **com.example**, artifact = **parent** and version = **1.0.0**). For the plugins which will be used to host development instances of Confluence, it is important to set the property containing the version of Confluence to be instantiated in the plugin's POM.xml properties section rather than inheriting it from the parent POM.xml.

1.  Add the &lt;parent&gt; element
2.  (optional) Remove redundant elements (For plugins to host a development instance of Confluence, be sure to keep the confluence version property )
3.  (optional) Remove the &lt;Export-Package&gt; if not classes are to be exported.  
      

**Plugin POM.xml**  Expand source

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
 
    <artifactId>5-9-4</artifactId>
    <version>1.0.0</version>
    <name>5-9-4</name>
    <description>This is the com.example:5-9-4 plugin for Atlassian Confluence.</description>
 
    <properties>        
        <confluence.version>5.9.4</confluence.version>
        <!-- This key is used to keep the consistency between the key in atlassian-plugin.xml and the key to generate bundle. -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
    </properties>
    <modelVersion>4.0.0</modelVersion>
    <packaging>atlassian-plugin</packaging>
    <parent>
        <groupId>com.example</groupId>
        <artifactId>parent</artifactId>
        <version>1.0.0</version>
    </parent>
</project>
```
