---
aliases:
- /server/confluence/2031788.html
- /server/confluence/2031788.md
category: devguide
confluence_id: 2031788
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031788
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031788
date: '2018-03-28'
legacy_title: How do I get a reference to a component?
platform: server
product: confluence
subcategory: faq
title: How do I get a reference to a component?
---
# How do I get a reference to a component?

Confluence component system is powered by Spring, but we've done a lot of nice things to make it easier for developers
to get their hands on a component at any time.

## Autowired Objects

If your object is being autowired (for example, another plugin module or an XWork action), the easiest way to access a component
is to use constructor injection.

For example, if you need a <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/spaces/SpaceManager.html" class="external-link">SpaceManager</a>, you can inject it as follows:

1.  Mark class with `@javax.inject.Named` or `@org.springframework.stereotype.Component` annotation and place `@Inject` or `@Autowired` respectively;
1.  Mark your field with `@ConfluenceImport` annotation to let the scanner know that it requires to import it from Confluence;

``` java
@Named
public class ExampleClass {

    @ConfluenceImport
    private SpaceManager spaceManager;

    @Inject
    public void setSpaceManager(SpaceManager spaceManager){
        this.spaceManager = spaceManager;
    }
}
```

You can do above points in any order.

{{% note %}}
This approach uses an <a href="https://bitbucket.org/atlassian/atlassian-spring-scanner/src/master/" class="external-link">Atlassian Spring Scanner</a>.
{{% /note %}}

## Non-Autowired Objects

If your object is not being autowired, you may need to retrieve the component explicitly. This is done using the `ContainerManager`.
For example:

``` java
SpaceManager spaceManager = (SpaceManager) ContainerManager.getComponent("spaceManager");
```
