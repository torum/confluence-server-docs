---
aliases:
- /server/confluence/making-your-plugin-modules-state-aware-2031782.html
- /server/confluence/making-your-plugin-modules-state-aware-2031782.md
category: devguide
confluence_id: 2031782
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031782
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031782
date: '2017-12-08'
guides: guides
legacy_title: Making your Plugin Modules State Aware
platform: server
product: confluence
subcategory: learning
title: Making your Plugin Modules State Aware
---
# Making your Plugin Modules State Aware

## Description

As a plugin developer, it may be necessary to perform initialisation or shutdown actions when your plugin is enabled or disabled. The approach required in order to achieve this depends on the type of plugin being developed.

For **Version 1** plugins, the `StateAware` interface can be implemented by [plugin modules](/server/confluence/confluence-plugin-module-types) which need to know when they are enabled or disabled.

For **Version 2** plugins, Spring lifecycle interfaces can be implemented by [Component](/server/confluence/component-module) modules which need to know when they are enabled or disabled.

## Implementation (Version 1 Plugins)

To be notified of enablement/disablement, implement the following in your [Macro Module](/server/confluence/macro-module), [Event Listener Module](/server/confluence/event-listener-module) or [Component Module - Old Style](/server/confluence/component-module-old-style):

``` java
public class YourMacro extends BaseMacro implements com.atlassian.plugin.StateAware
```

This has two methods you must implement:

``` java
public void enabled()
{
    // Your enablement code goes here.
}

public void disabled()
{
    // Your disablement code goes here.
}
```

### Call Sequence

These methods are called in the following circumstances:

#### `enabled()`

1.  At server startup, if the plugin is already installed and enabled.
2.  If the plugin is installed via uploading
3.  If the plugin is enabled after having been disabled.
4.  If the specific module is enabled after having been disabled.

#### `disabled()`

1.  At server shutdown, if the plugin is installed and enabled.
2.  If the plugin is uninstalled.
3.  If the plugin is disabled.
4.  If the specific module is disabled.

### Notes

Each method is only called once at each logical enablement/disablement event. Please note that the module class's constructor is *not* a reliable place to put initialisation code either, as the classes are often constructed or destructed more often than they are disabled/enabled. However, once enabled, the same class will remain in memory until it is disabled.

### Supported Module Types

Not all module types have been tested, but the following have the following status:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Module Type</p></th>
<th><p>Confluence Version</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="/server/confluence/macro-module">Macro Module</a></p></td>
<td><p>2.3.3</p>
<p><strong>Working.</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="/server/confluence/component-module-old-style">Component Module - Old Style</a></p></td>
<td><p>2.3.3</p>
<p><strong>Working.</strong></p></td>
</tr>
<tr class="odd">
<td><p><a href="/server/confluence/event-listener-module">Event Listener Module</a></p></td>
<td><p>2.3.3</p>
<p><strong>Working.</strong></p></td>
</tr>
<tr class="even">
<td><p><a href="/server/confluence/lifecycle-module">Lifecycle Module</a></p></td>
<td><p>2.3.3</p>
<p><strong>Does not work.</strong></p></td>
</tr>
</tbody>
</table>

## Implementation (Version 2 Plugins)

The [Component Module](/server/confluence/component-module) type for OSGi (version 2) plugins doesn't support the `StateAware` interface. To achieve the same effect, you can use the two Spring lifecycle interfaces: <a href="http://static.springframework.org/spring/docs/2.5.x/api/org/springframework/beans/factory/InitializingBean.html" class="external-link">InitializingBean</a> and <a href="http://static.springframework.org/spring/docs/2.5.x/api/org/springframework/beans/factory/DisposableBean.html" class="external-link">DisposableBean</a>. The afterPropertiesSet() and destroy() methods on these interfaces will be called when the module is enabled or disabled, exactly like StateAware.

Making this change to a component in an existing plugin will be backwards compatible. That is, a component module in a legacy plugin which implements InitializingBean will have its init() method called when it is enabled, exactly the same as such a component in an OSGi plugin.
