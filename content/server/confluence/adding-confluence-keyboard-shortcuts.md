---
aliases:
- /server/confluence/adding-confluence-keyboard-shortcuts-13631990.html
- /server/confluence/adding-confluence-keyboard-shortcuts-13631990.md
category: devguide
confluence_id: 13631990
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13631990
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13631990
date: '2018-04-26'
legacy_title: Adding Confluence Keyboard Shortcuts
platform: server
product: confluence
subcategory: learning
title: Adding Confluence keyboard shortcuts
---
# Adding Confluence keyboard shortcuts

<table>
<colgroup>
<col style="width: 30%" />
<col style="width: 70%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Applicable:</td>
<td>This tutorial applies to <strong>Confluence 5.9.1 and higher</strong></td>
</tr>
<tr class="even">
<td>Level of experience:</td>
<td>Beginner</td>
</tr>
<tr class="odd">
<td>Time estimate</td>
<td>It should take you less than 1 hour to complete this tutorial.</td>
</tr>
</tbody>
</table>

{{% tip %}}

This tutorial is for 'beginner' level, so you can follow it even if you have
never developed a plugin before. Our tutorials are classified as 'beginner', 'intermediate' and 'advanced'.

{{% /tip %}}

## Overview

This tutorial shows you how to use a [Keyboard shortcut module](/server/confluence/keyboard-shortcut-module/) 
in your plugin to add keyboard shortcuts to different parts of the Confluence UI and navigation between pages.
To do this, you create a very simple Confluence plugin consisting of the following components:

*   Plugin descriptor – to enable the plugin module in Confluence.
*   Internationalization properties file – for multi-language support.

A single JAR file contains all the components. Each component is further discussed in the examples later on this page.

This plugin is designed to show two ways of adding new keyboard shortcuts to Confluence:

*  Navigate to a different page.
*  Scroll the window to an element, and then click that element.

## Before you begin

To complete this tutorial, you should work through the [Atlassian plugin SDK](/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/) tutorial first.

### Plugin source

We encourage you to work through this tutorial. If you want to skip ahead or check your work when you have finished,
you can find the plugin source code on Atlassian Bitbucket. Alternatively, you can 
<a href="https://bitbucket.org/atlassian_tutorial/adding-confluence-keyboard-shortcuts/get/master.zip" class="external-link">
download the source as a ZIP archive</a>. 

To clone the repository, run the following command:

``` bash
git clone git@bitbucket.org:atlassian_tutorial/adding-confluence-keyboard-shortcuts.git
```

{{% note %}}

About these instructions

You can use any supported combination of operating system and IDE to create this plugin. These instructions
were written using Intellij IDEA 2017.2 on macOS Sierra. If you are using another operating system or IDE
combination, you should use the equivalent operations for your specific environment.
This tutorial was last tested with Confluence 6.7.1 using the Atlassian SDK 6.3.10.

{{% /note %}}

## Step 1. Create a plugin project

In this step, you'll create a plugin skeleton using `atlas-` commands. Because you won't need some
of the files created in the skeleton, you'll also delete them in this step.

1.  Open a Terminal and navigate to your workspace directory.

1.  To create a Confluence plugin skeleton, run the following command:

    ``` bash
    atlas-create-confluence-plugin
    ```

1.  To identify your plugin, enter the following information.

    <table>
    <colgroup>
    <col style="width: 30%" />
    <col style="width: 70%" />
    </colgroup>
    <tbody>
    <tr class="odd">
    <td><p>group-id</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence</code><br />
    </p></td>
    </tr>
    <tr class="even">
    <td><p>artifact-id</p></td>
    <td><p><code>adding-confluence-keyboard-shortcuts</code></p></td>
    </tr>
    <tr class="odd">
    <td><p>version</p></td>
    <td><p><code>1.0-SNAPSHOT</code></p></td>
    </tr>
    <tr class="even">
    <td><p>package</p></td>
    <td><p><code>com.example.plugins.tutorial.confluence</code></p></td>
    </tr>
    </tbody>
    </table>

1.  Confirm your entries with `Y` or `y`.

1.  Navigate to the project directory created in the previous step.

    ``` bash
    cd adding-confluence-keyboard-shortcuts/
    ```

1.  Delete the test directories.

    Setting up testing for your plugin isn't part of this tutorial. Run the following commands to delete the generated test skeleton:

    ``` bash
    rm -rf ./src/test/java
    rm -rf ./src/test/resources/
    ```

1.  Delete the unneeded Java class files.

    ``` bash
    rm -rf ./src/main/java/com/example/plugins/tutorial/confluence/*
    ```

## Step 2. Edit the `atlassian-plugin.xml` file

In this step, you register the plugin module in your plugin descriptor, that is `atlassian-plugin.xml` file.

1. In `atlassian-plugin.xml` file, add the following code to your between the `<atlassian-plugin>` 
tags, but below the `<plugin-info>` tag group.

``` xml
   <keyboard-shortcut key="about-confluence-page" i18n-name-key="keyboard.shortcut.go.to.about.page">
        <order>200</order>
        <description key="keyboard.shortcut.go.to.about.page.desc"/>
        <shortcut>ga</shortcut>
        <operation type="goTo">/aboutconfluencepage.action</operation>
        <context>global</context>
   </keyboard-shortcut>
```

Let's break down that XML code. In this example we create a keyboard shortcut that
navigates to <a href="http://localhost:1990/confluence/aboutconfluencepage.action">Confluence About page</a>
when triggered.

To do this, we've done a number of things.

### The Keyboard Shortcut module

In the previous code sample, this line involves 2 attributes:

``` xml
<keyboard-shortcut key="about-confluence-page" i18n-name-key="keyboard.shortcut.go.to.about.page">
```

1.  `key="about-confluence-page"` sets an internal name for the new item.
1.  `i18n-name-key="keyboard.shortcut.go.to.about.page"` is the internationalization key
for the item. It is displayed in the admin section as the name of the module in the plugin.

### Controlling where the keyboard shortcut is displayed

Examine these lines of code:

``` xml
<order>200</order>
<description key="keyboard.shortcut.go.to.about.page.desc"/>
```

In Confluence, if you go to the **Help** menu, and then select **Keyboard Shortcuts**, you will see a dialog
box with list of keyboard shortcuts.

<img src="/server/confluence/images/keyboard-shortcuts-example.png" width="650" />

Each item has a value for `order` between 10 and 100. A smaller value means the label for your
keyboard shortcut appears higher in the list. A value of 200 means that the item is almost certainly
placed at the bottom of the list.

The description contains an attribute `key`. This is the internationalization key that you can find in a
properties file (explained later in the tutorial). The value of this element defines the label for
the keyboard shortcut in this dialog box.

### Controlling how the keyboard shortcut is triggered

This line defines how the keyboard shortcut is triggered:  `<shortcut>ga</shortcut>`

This example is triggered by pressing the keys in this order: "g", and then "a".

### Defining what the keyboard shortcut does

This line defines what the keyboard shortcut does: `<operation type="goTo">/aboutconfluencepage.action</operation>`

In this case, the `window.location` is changed to a path below the server base URL. So, for a Confluence
installation at http://locahost:1990/confluence, this will correspond to http://localhost:1990/confluence/aboutconfluencepage.action
Find more descriptions of operation types on [Keyboard Shortcut Module](/server/confluence/keyboard-shortcut-module/) page.

### Controlling when the keyboard shortcut can be triggered

In this line, we allow the keyboard shortcuts to be triggered globally within Confluence: `<context>global</context>`.

The context tag also accepts the page or content values for other situations.

### Adding new resource files and internationalization

We will want to specify a text label to display in our Confluence keyboard shortcuts. You could just
hard-code this information into your `atlassian-plugin.xml` file. However, by adding it in a new resource file,
we can make our plugin compatible with internationalization.

To do so, simply add a following lines into
`adding-confluence-keyboard-shortcuts.properties` file:

``` xml
keyboard.shortcut.go.to.about.page = About page keyboard shortcut
keyboard.shortcut.go.to.about.page.desc = Navigate to About page
```

The first line – `keyboard.shortcut.go.to.about.page` – describes the module name in **Cog wheel** > **Add-ons**.

<img src="/server/confluence/images/keyboard-shortcut-addon-admin-page.png" width="650" />

{{% note %}}
Displaying friendly names rather than module keys can be useful when there are many modules in a Confluence plugin.
{{% /note %}}

The second line – `keyboard.shortcut.go.to.about.page.desc` – is the label for the keyboard shortcut in
the Keyboard Shortcut dialog displayed earlier.

SDK automatically generates a reference to the resource file (`adding-confluence-keyboard-shortcuts.properties`)
in our `atlassian-plugin.xml` file:

``` xml
<resource type="i18n" name="i18n" location="adding-confluence-keyboard-shortcuts.properties"  />
```

To know more about internationalization, see our 
<a href="http://confluence.atlassian.com/display/DISC/i18n+in+Confluence+Plugin+Development" class="external-link">
Confluence documentation on the topic</a>.

## Step 3. Build, install and run the plugin

1.  To compile the plugin project, and then launch a local instance of Confluence, run the following command:

    ``` bash
      atlas-run
    ```

1.  After Confluence loads, go to the local instance with this URL:

    ``` text
    http://localhost:1990/confluence/
    ```

1.  Log in with username "admin" and password "admin".
1.  Type "g", and then "a". Your browser displays the following.

    <img src="/server/confluence/images/keyboard-shortcut-about-page.png" height="250" />

1.  Go back to **Dashboard**, and then select **Keyboard Shortcuts** from the **Help** menu (or just enter "**?**").
You'll see your new keyboard shortcut has been added.  

    <img src="/server/confluence/images/keyboard-shortcut-with-plugin-installed.png" width="650" />

## Step 4. For extra points

In this step, you add a new keyboard shortcut that triggers the **User Menu** to be toggled open or closed when you press *Alt+U*.

<img src="/server/confluence/images/keyboard-shortcut-user-menu-toogle.png" width="340" />

1.  In your `atlassian-plugin.xml` file, add a new `keyboard-shortcut` code block as follows:

    ``` xml
      <keyboard-shortcut key="scroll-and-toggle-user-menu" i18n-name-key="keyboard.shortcut.toggle.user.menu">
            <order>200</order>
            <description key="keyboard.shortcut.toggle.user.menu.desc"/>
            <shortcut>[Alt+U]</shortcut>
            <operation type="moveToAndClick">#user-menu-link</operation>
            <context>global</context>
        </keyboard-shortcut>
    ```

    This is very similar to code block in step 2, except for two key points:

    *   The shortcut now triggers when two keys are pressed simultaneously. 
    *   The operation type is now `moveToAndClick` that should be fairly self explanatory.  
        *   It moves the web browser window so that the User menu is visible.
        *   The value `#user-menu-link` is a CSS selector for the id attribute on the **User** menu anchor element.

1.  To rebuild your plugin, run `atlas-package` command, and
[QuickReload](/server/framework/atlassian-sdk/automatic-plugin-reinstallation-with-quickreload/)
automatically reloads your plugin for you.

1.  Go to your local Confluence installation and press *Alt+U* several times.
You should see the **User** drop-down opening and closing.

This example shows how you can quickly add
your own keyboard shortcuts to enhance productivity or accessibility.

{{% tip %}}

Congratulations!
You have completed this tutorial.

{{% /tip %}}
