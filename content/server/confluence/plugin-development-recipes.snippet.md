---
aliases:
- /server/confluence/plugin-development-recipes-39368920.html
- /server/confluence/plugin-development-recipes-39368920.md
category: devguide
confluence_id: 39368920
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39368920
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39368920
date: '2017-12-08'
legacy_title: Plugin Development Recipes
platform: server
product: confluence
subcategory: other
title: Plugin Development Recipes
---
# Plugin Development Recipes

-   [Documenting a Macro](/server/confluence/documenting-a-macro.snippet)
-   [Using atlassian-plugin.xml fragments](/server/confluence/using-atlassian-plugin-xml-fragments.snippet)
-   [Centralized Class for OSGi Component Imports](/server/confluence/centralized-class-for-osgi-component-imports.snippet)
-   [Reporting Errors](/server/confluence/reporting-errors.snippet)
