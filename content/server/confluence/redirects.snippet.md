---
aliases:
- /server/confluence/-redirects-24085288.html
- /server/confluence/-redirects-24085288.md
category: devguide
confluence_id: 24085288
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24085288
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24085288
date: '2017-12-08'
legacy_title: _Redirects
platform: server
product: confluence
subcategory: other
title: _Redirects
---
# \_Redirects

-   [Confluence Remote API Reference](/server/confluence/confluence-remote-api-reference-24085294.html)
-   [Confluence Tutorials](/server/confluence/confluence-tutorials.snippet)
-   [Creating a Cross-Platform Admin Configuration Form](/server/confluence/creating-a-cross-platform-admin-configuration-form.snippet)
-   [Writing a Multi-page Blueprint using Atlassian Connect](/server/confluence/writing-a-multi-page-blueprint-using-atlassian-connect.snippet)

