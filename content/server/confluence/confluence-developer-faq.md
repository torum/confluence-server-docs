---
aliases:
- /server/confluence/confluence-developer-faq-2031707.html
- /server/confluence/confluence-developer-faq-2031707.md
category: devguide
confluence_id: 2031707
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031707
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031707
date: '2017-12-08'
legacy_title: Confluence Developer FAQ
platform: server
product: confluence
subcategory: faq
title: Confluence developer FAQ
---
# Confluence developer FAQ

This is a constantly updated FAQ listing questions and answers asked by people developing Confluence plugins and working with the Confluence code base in general. For general questions, check <a href="#confluence-faq" class="unresolved">Confluence FAQ</a>.

{{% note %}}

If you have a question, please ask it as a comment and someone from Atlassian will reply. Comment threads will gradually be merged back into this FAQ as needed. Please try to be as specific as possible with your questions.

{{% /note %}}

## Questions

-   [How do I get the base URL and ContextPath of a Confluence installation?](/server/confluence/how-do-i-get-the-base-url-and-contextpath-of-a-confluence-installation)
-   [How do I get my macro output exported to HTML and PDF?](/server/confluence/how-do-i-get-my-macro-output-exported-to-html-and-pdf)
-   [How do I get hold of the HttpServletRequest?](/server/confluence/how-do-i-get-hold-of-the-httpservletrequest)
-   [How do I get a reference to a component?](/server/confluence/how-do-i-get-a-reference-to-a-component)
-   [How do I load a resource from a plugin?](/server/confluence/how-do-i-load-a-resource-from-a-plugin)
-   [What is the best way to load a class or resource from a plugin?](/server/confluence/what-is-the-best-way-to-load-a-class-or-resource-from-a-plugin)
-   [How do I autowire a component?](/server/confluence/how-do-i-autowire-a-component)
-   [How do I display the Confluence System Classpath?](/server/confluence/how-do-i-display-the-confluence-system-classpath)
-   [How Do I find enabled and disabled plugins in the Database?](/server/confluence/how-do-i-find-enabled-and-disabled-plugins-in-the-database)
-   [How does RENDERMODE work?](/server/confluence/how-does-rendermode-work)
-   [REV400 - How do I link to a comment?](/server/confluence/rev400-how-do-i-link-to-a-comment)
-   [Starting a Confluence cluster on a single machine](/server/confluence/starting-a-confluence-cluster-on-a-single-machine)
-   [Confluence Data Center Plugin Validator](/server/confluence/confluence-data-center-plugin-validator)
-   [Troubleshooting Macros in the Page Gadget](/server/confluence/troubleshooting-macros-in-the-page-gadget)
-   [How do I get hold of the GET-Parameters within a Macro?](/server/confluence/how-do-i-get-hold-of-the-get-parameters-within-a-macro)
-   [How to switch to non-minified Javascript for debugging](/server/confluence/how-to-switch-to-non-minified-javascript-for-debugging)
-   [How do I get the information about Confluence such as version number, build number, build date?](/server/confluence/how-do-i-get-the-information-about-confluence-such-as-version-number-build-number-build-date)
-   [How do I tell if a user has permission to...?](/server/confluence/how-do-i-tell-if-a-user-has-permission-to)
-   [How do I get the location of the confluence.home directory?](/server/confluence/how-do-i-get-the-location-of-the-confluence-home-directory)
-   [How do I associate my own properties with a ContentEntityObject?](/server/confluence/how-do-i-associate-my-own-properties-with-a-contententityobject)
-   [How do I find the logged in user?](/server/confluence/how-do-i-find-the-logged-in-user)
-   [How can I determine the context my macro is being rendered in?](/server/confluence/how-can-i-determine-the-context-my-macro-is-being-rendered-in)
-   [How do I develop against Confluence with Secure Administrator Sessions?](/server/confluence/how-do-i-develop-against-confluence-with-secure-administrator-sessions)
-   [How do I find information about lost attachments?](/server/confluence/how-do-i-find-information-about-lost-attachments)
-   [Enabling Developer Mode](/server/confluence/enabling-developer-mode)
-   [What's the easiest way to render a velocity template from Java code?](/server/confluence/whats-the-easiest-way-to-render-a-velocity-template-from-java-code)
-   [What is Bandana? One form of Confluence Persistence](/server/confluence/what-is-bandana-one-form-of-confluence-persistence)
-   [How do I check which Jar file a class file belong to?](/server/confluence/how-do-i-check-which-jar-file-a-class-file-belong-to)
-   [How do I make my attachments open in a new window or a tab?](/server/confluence/how-do-i-make-my-attachments-open-in-a-new-window-or-a-tab)
-   [How do I ensure my add-on works properly in a cluster?](/server/confluence/how-do-i-ensure-my-add-on-works-properly-in-a-cluster)
-   [How do I cache data in a plugin?](/server/confluence/how-do-i-cache-data-in-a-plugin)
-   [How do I prevent my rendered wiki text from being surrounded by paragraph tags?](/server/confluence/how-do-i-prevent-my-rendered-wiki-text-from-being-surrounded-by-paragraph-tags)
-   [I am trying to compile a plugin, but get an error about the target release](/server/confluence/i-am-trying-to-compile-a-plugin-but-get-an-error-about-the-target-release)
-   [How do I find Confluence Test Suite?](/server/confluence/how-do-i-find-confluence-test-suite)
-   [Encrypting error messages in Sybase](/server/confluence/encrypting-error-messages-in-sybase)
-   [What class should my XWork action plugin extend?](/server/confluence/what-class-should-my-xwork-action-plugin-extend)
-   [What class should my macro extend?](/server/confluence/what-class-should-my-macro-extend)
-   [How do I configure my Rich Text macro to receive unformatted input?](/server/confluence/how-do-i-configure-my-rich-text-macro-to-receive-unformatted-input)
-   [Within a Confluence macro, how do I retrieve the current ContentEntityObject?](/server/confluence/within-a-confluence-macro-how-do-i-retrieve-the-current-contententityobject)
-   [How do I convert wiki text to HTML?](/server/confluence/how-do-i-convert-wiki-text-to-html)
-   [HTTP Response Code Definitions](/server/confluence/http-response-code-definitions)
-   [Disable Velocity Caching](/server/confluence/disable-velocity-caching)

##### RELATED TOPICS

[Accessing Classes from Another Plugin](https://developer.atlassian.com/display/DOCS/Accessing+Classes+from+Another+Plugin)
