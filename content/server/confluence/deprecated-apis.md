---
aliases:
- /server/confluence/deprecated-apis-40000001.html
- /server/confluence/deprecated-apis-40000001.md
category: devguide
confluence_id: 40000001
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40000001
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40000001
date: '2017-12-08'
legacy_title: Deprecated APIs
platform: server
product: confluence
subcategory: updates
title: Deprecated APIs
---
# Deprecated APIs

REST is the recommended and supported remote API for Confluence Server, and it's where we'll focus our attention by adding functionality and fixing bugs. If you're an add-on developer, we recommend you use the REST API wherever possible. See [Confluence Server REST API](/server/confluence/confluence-server-rest-api). 

The following APIs were deprecated as of Confluence Server 5.5. We won't add new features or fix bugs for them.

-   [REST (Prototype)](/server/confluence/confluence-rest-apis-prototype-only)
-   [XML-RPC and SOAP](/server/confluence/confluence-xml-rpc-and-soap-apis)
-   [JSON-RPC](/server/confluence/confluence-json-rpc-apis)
