---
aliases:
- /server/confluence/2031681.html
- /server/confluence/2031681.md
category: devguide
confluence_id: 2031681
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031681
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031681
date: '2017-12-08'
legacy_title: How do I find Confluence Test Suite?
platform: server
product: confluence
subcategory: faq
title: How do I find Confluence test suite?
---
# How do I find Confluence test suite?

All our Tests are stored inside the 'source release' you can download if you have a commercial licence from atlassian main <a href="http://www.atlassian.com/software/confluence/ConfluenceSourceDownloads.jspa" class="external-link">site</a>

When you expand the 'source', you can locate the following:

-   unit and integration test  
    `...confluence-project/confluence/src`
-   acceptance test  
    `...confluence-project/src/test`
