---
aliases:
- /server/confluence/dynamically-migrate-macro-parameters-33739686.html
- /server/confluence/dynamically-migrate-macro-parameters-33739686.md
category: devguide
confluence_id: 33739686
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=33739686
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=33739686
date: '2017-12-08'
guides: guides
legacy_title: Dynamically migrate macro parameters
platform: server
product: confluence
subcategory: learning
title: Dynamically migrate macro parameters
---
# Dynamically migrate macro parameters

|           |                          |
|-----------|--------------------------|
| Available | Confluence 5.8 and later |

This guide describes how to use the new macro-schema-migrator plugin module to migrate parameters of a macro from one schema version to another.  For example, to migrate the parameters of a macro from individual parameters for label, space, author to a single CQL parameter.  

In this guide we'll be migrating a simplified Content by Label macro.

## Steps

Here's the original macro module descriptor. Note the multiple parameter types:

``` xml
    <macro name="contentbylabel" key="simplecontentbylabel"
           class="com.example.SimpleLabelledContentMacro">
        <parameters>
            <parameter name="label" type="string" required="true">
            <parameter name="spaces" type="spacekey">
        </parameters>
    </macro>
```

This is the new module descriptor with a single CQL parameter and schemaVersion: 

``` xml
<macro name="contentbylabel" key="simplecontentbylabel" schema-version="2"
       class="com.example.SimpleLabelledContentMacro">
    <parameters>
        <parameter name="cql" type="cql" required="true">
    </parameters>
</macro>
```

To migrate from the schema version the plugin should declare a new macro migration module in atlassian-plugin.xml, specifying the schema version that the macro migration migrates from:

``` xml
<macro-schema-migrator key="contentbylabel-migrator" macro="simplecontentbylabel"
                       version="1" 
                       class="com.example.SimpleLabelledContentMacroCqlSchemaMigrator"/>
```

And implement the MacroMigration interface:

``` java
public class LabelledContentMacroCqlSchemaMigrator implements MacroMigration
{
    public MacroDefinition migrate(MacroDefinition macro, ConversionContext context)
    {
        String cqlQuery = String.format("label in (%s) and space = %s", 
            convertLabelParameterToCQL(macro.getParameter("label")), 
            convertSpaceParameterToCQL(macro.getParameter("spacekey"));
        macro.setParameter("cql", cqlQuery);
        macro.setTypedParameter("cql", cqlQuery);
        macro.setSchemaVersion(2);  // set the schema version to be the target schema version
    }
}
```

## When does macro migration occur?

Migration is performed by the MacroSchemaMigrator dynamically when marshalling from the MacroDefinition to the content body representation, specifically it occurs in the: 

-   StorageMacroV2Marshaller
-   ViewMacroMarshaller
-   DelegatingEditorMarshaller

<img src="/server/confluence/images/image2015-6-2-16:29:6.png" width="848" height="402" />

It's possible to opt out of migration on view, for example if you would like to keep rendering the old parameters, and only use the new parameters when dealing with newly created macro to reduce the migration risk of new features.  This can be done by setting the excluded-schema-migration-points attribute on the macro module to view like this:

``` xml
<macro name="contentbylabel" key="simplecontentbylabel" schema-version="2"
       class="com.example.SimpleLabelledContentMacro"
       excluded-schema-migration-points="view">
```
