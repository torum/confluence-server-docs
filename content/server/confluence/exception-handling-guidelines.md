---
aliases:
- /server/confluence/exception-handling-guidelines-2031649.html
- /server/confluence/exception-handling-guidelines-2031649.md
category: devguide
confluence_id: 2031649
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031649
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031649
date: '2017-12-08'
guides: guides
legacy_title: Exception Handling Guidelines
platform: server
product: confluence
subcategory: learning
title: Exception handling guidelines
---
# Exception handling guidelines

{{% note %}}

These are guidelines related to the development of Confluence. The guidelines mainly apply to Atlassian employees, but reading them should provide insight for third-party plugin developers as well, so we decided to make them public.

{{% /note %}}

Randomly sorted guidelines.

1.  Don't catch `Exception` unless that's all you're having thrown to you.
2.  Don't declare that you throw `Exception` ever.
3.  Both rules \#1 and \#2 apply to `RuntimeException` as well.
4.  Don't catch `Throwable` if you want to continue breathing.
5.  Rule \#4 applies to `Error` and any subclasses of `Error` as well.
6.  Don't catch, log and rethrow.
7.  Familiarise yourself with the standard RuntimeException subclasses (IllegalStateException, IllegalArgumentException, UnsupportedOperationException, IndexOutOfBoundsException), and use them in preference to creating your own runtime exception class.
    -   For example, if the problem is that an object reference (or "pointer") which you didn't expect to be null is in fact null, why not throw a `NullPointerException`?
8.  If you explicity throw any RuntimeException in a method, document it in the method's `@throws` Javadoc like you would a checked exception.

### Meaningful exceptions

Where possible create, document and throw meaningful unchecked exceptions. For example, write this:

``` java
public class MyGroupManager
{
    /**
     * ...
     * @throws InvalidGroupException if the group cannot be handled
     */
    public void handleGroup(Group group) throws InvalidGroupException
    {
        if (!isValidGroup(group))
            throw new InvalidGroupException("Group is invalid: " + group.toString());
        // do something with the group
    }
}

public class InvalidGroupException extends RuntimeException
{
    // ...
}
```

In preference to this:

``` java
public class EvilGroupManager
{
    public void handleGroup(Group group)
    {
        if (!isValidGroup(group))
            throw new RuntimeException("Group is invalid: " + group.toString());
        // do something with the group
    }
}
```

The latter implementation is not as good because it gives the calling code very little discretion as to what kind of exceptions it wants to handle.
