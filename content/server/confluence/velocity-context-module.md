---
aliases:
- /server/confluence/velocity-context-module-2031874.html
- /server/confluence/velocity-context-module-2031874.md
category: reference
confluence_id: 2031874
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031874
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031874
date: '2017-12-08'
legacy_title: Velocity Context Module
platform: server
product: confluence
subcategory: modules
title: Velocity Context module
---
# Velocity Context module

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 1.4 and later</p></td>
</tr>
</tbody>
</table>

Velocity Context plugin modules enable you to add components to Confluence's velocity context, making those components available in templates rendered from decorators, themes, XWork actions or macros.

-   For more information about plugins in general, read [Confluence Plugin Guide](/server/confluence/confluence-plugin-guide).
-   To learn how to install and configure plugins (including macros), read <a href="http://confluence.atlassian.com/display/DOC/Installing+a+Plugin" class="external-link">Installing a Plugin</a>.
-   For an introduction to writing your own plugins, read [Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)

## Velocity Context Plugin Module

Each component module adds a single object to Confluence's default velocity context. This context is the collection of objects that are passed to each velocity template during rendering of macros, decorators, themes and XWork actions. This allows you to create helper objects that perform tasks too complex to represent in Velocity templates.

The objects are autowired by Spring before being added to the context.

Here is an example `atlassian-plugin.xml` file containing a single velocity context module:

``` xml
<atlassian-plugin name="Sample Component" key="confluence.extra.component">
    ...
    <velocity-context-item key="myVelocityHelper"
          name="My Plugin's Velocity Helper" context-key="myVelocityHelper"
          class="com.example.myplugin.helpers.MyVelocityHelper" />
    ...
</atlassian-plugin>
```

-   the **name** attribute represents how this component will be referred to in the Confluence interface.
-   the **key** attribute represents the internal, system name for your component.
-   the **context-key** attribute represents the variable that will be created in Velocity for this item. So if you set a context-key of `myVelocityHelper`, the object will be available as `$myVelocityHelper` in Velocity templates
-   the **class** attribute represents the class of the component to be created.

**Note:** Every velocity context module needs a unique key, or Confluence will not be able to render the module.


