---
aliases:
- /server/confluence/event-listener-module-2031834.html
- /server/confluence/event-listener-module-2031834.md
category: reference
confluence_id: 2031834
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031834
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031834
date: '2018-04-27'
legacy_title: Event Listener Module
platform: server
product: confluence
subcategory: modules
title: Event Listener module
---
# Event Listener module

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 3.3 and later.</p></td>
</tr>
</tbody>
</table>

Every time something important happens within Confluence (a page is added or modified, the configuration is changed, etc.), an 'event' is triggered. Listeners allow you to extend Confluence by installing code that responds to those events.

## Limitations of events

*   Events are notifications that something has occurred. The event system is not designed to allow a listener to veto the action that caused the event.
*   There is no loop detection. If you write a listener for the `SpaceModifiedEvent` that itself causes a `SpaceModifiedEvent` to be generated, you are responsible for preventing the start of an infinite loop.
*   You can listen for plugin install/uninstall/enable/disable events. However, this will be unreliable when trying to listen for events about your own plugin. To trigger actions for these events, your modules should implement Spring lifecycle interfaces: `InitializingBean` and `DisposableBean`.
The `afterPropertiesSet()` and `destroy()` methods on these interfaces will be called when the module is enabled or disabled,
*   Confluence events are currently processed synchronously. That is, Confluence waits for your event to finish processing before returning from the method that is the source of the event. This makes it **very important** that any event listener you write completes as quickly as possible.

## Adding a Listener plugin

Listeners are a kind of Confluence plugin modules.

*   For more information about plugins in general, read [Confluence plugin guide](/server/confluence/confluence-plugin-guide).
*   For an introduction to writing your own plugins, read [Writing Confluence plugins](/server/confluence/writing-confluence-plugins)

## Annotation-Based Event Listeners with <a href="https://bitbucket.org/atlassian/atlassian-spring-scanner" class="external-link">Atlassian Spring Scanner</a>

{{% note %}}

Events 2.0

Starting from **Confluence 3.3**, you can benefit from annotation-based event listeners.

{{% /note %}}

Annotation-based event listeners let you annotate methods to be called for specific event types. The annotated methods must take a single parameter specifying the type of event that should trigger the method.

To use the annotation-based event listeners, you must register your listener as a component and start listening to `EventPublisher`.

``` java
@Named
public class EventListener implements DisposableBean {

    @ConfluenceImport
    private EventPublisher eventPublisher;

    @Inject
    public EventListener(EventPublisher eventPublisher) {
        eventPublisher.register(this);  //just for example
    }

    // Unregister the listener if the plugin is uninstalled or disabled.
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }
}
```

You can check [Writing advanced Confluence blueprint](/server/confluence/write-an-advanced-blueprint-plugin/#step-4-add-an-event-listener)
for an example of a simple listener.

## Events and Event types

All events within Confluence extend from <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/event/events/ConfluenceEvent.html" class="external-link">com.atlassian.com.event.events.ConfluenceEvent</a>. In general, we use the following convention for naming each type of `ConfluenceEvent`:

> &lt;Object&gt;&lt;Operation&gt;Event

For example, we have the following event types relating to space events: `SpaceCreateEvent`, `SpaceUpdateEvent`, and `SpaceRemoveEvent`. In the previous description, *space* would correspond to &lt;Object&gt; and *create*, *update*, or *remove* would correspond to &lt;Operation&gt;.

Occasionally, an operation is so singular that its meaning will be obvious without use of this naming convention; for example, a `LoginEvent` or `ConfigurationEvent`.

## Legacy

There are a few legacy ways to register event listener:

*   [Descriptor based Event Listener](/server/confluence/descriptor-based-event-listener/)
*   [Annotation based Event Listener - legacy](/server/confluence/annotation-based-event-listener-legacy)
