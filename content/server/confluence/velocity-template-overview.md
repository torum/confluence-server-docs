---
aliases:
- /server/confluence/velocity-template-overview-2031773.html
- /server/confluence/velocity-template-overview-2031773.md
category: devguide
confluence_id: 2031773
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031773
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031773
date: '2017-12-08'
guides: guides
legacy_title: Velocity Template Overview
platform: server
product: confluence
subcategory: learning
title: Velocity template overview
---
# Velocity template overview

<a href="http://velocity.apache.org/" class="external-link">Velocity</a> is a server-side template language used by Confluence to render page content. Velocity allows Java objects to be called alongside standard HTML. Users who are writing <a href="#writing-user-macros" class="unresolved">Writing User Macros</a> or [plugins](/server/confluence/confluence-plugin-guide) (or <a href="#customised-pdf-exports" class="unresolved">customised PDF exports</a> in Confluence 2.10.x and earlier versions) may need to modify Velocity content. General information is available from the <a href="http://velocity.apache.org/engine/devel/user-guide.html" class="external-link">Velocity user guide</a>.

### Useful Resources

-  [What's the easiest way to render a velocity template from Java code?](/server/confluence/whats-the-easiest-way-to-render-a-velocity-template-from-java-code)
-  [Confluence UI architecture](/server/confluence/confluence-ui-architecture)
-  [Confluence Objects Accessible From Velocity](/server/confluence/confluence-objects-accessible-from-velocity)
-  [Disable Velocity Caching](/server/confluence/disable-velocity-caching)

