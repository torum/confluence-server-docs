---
aliases:
- /server/confluence/2031793.html
- /server/confluence/2031793.md
category: devguide
confluence_id: 2031793
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031793
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031793
date: '2018-03-29'
legacy_title: How do I convert wiki text to HTML?
platform: server
product: confluence
subcategory: faq
title: How do I convert wiki text to HTML?
---
# How do I convert wiki text to HTML?

To convert wiki text to HTML, you will need the <a href="https://docs.atlassian.com/atlassian-confluence/latest-server/com/atlassian/confluence/xhtml/api/XhtmlContent.html" class="external-link">XhtmlContent</a>
(see [how to retrieve it](/server/confluence/how-do-i-get-a-reference-to-a-component)).

The `XhtmlContent` has `convertWikiToView()` method. See the following examples on how to use it.  

### Convert wiki text in a macro

To convert wiki text to HTML in a macro, do the following:

``` java
    public String execute(Map<String, String> map, String s, ConversionContext conversionContext) throws MacroExecutionException {
        String wiki = "## Some wiki";
        String xhtml = "";
        try {
            xhtml = xhtmlContent.convertWikiToView(wiki, conversionContext, new ArrayList<>());
        } catch (XMLStreamException | XhtmlException e) {
            e.printStackTrace();
        }
        return xhtml;
    }
```

### Convert wiki text in a different component

If you don't have `ConversionContext`, you can pass `DefaultConversionContext`, which takes `RenderContext` as an argument.

If you are converting the text in the context of some `ContentEntityObject` (for example, within a page or blog post),
then you can call `contentEntityObject.toPageContext()` to retrieve its `RenderContext`. Otherwise, pass in a `new PageContext()`.
For example:

``` java
    public String someMethod() {
        String wiki = "## Some wiki";
        String xhtml = "";
        try {
            xhtml = xhtmlContent.convertWikiToView(wiki, new DefaultConversionContext(new PageContext()), new ArrayList<>());
        } catch (XMLStreamException | XhtmlException e) {
            e.printStackTrace();
        }
        return xhtml;
    }
```
