---
aliases:
- /server/confluence/2031824.html
- /server/confluence/2031824.md
category: reference
confluence_id: 2031824
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031824
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031824
date: '2017-12-08'
legacy_title: "Spring Component Module \u2013 Old Style"
platform: server
product: confluence
subcategory: modules
title: "Spring Component module \u2013 old style"
---
# Spring Component module – old style

<table>
<colgroup>
<col style="width: 20%" />
<col style="width: 80%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 2.2 and later</p></td>
</tr>
<tr class="even">
<td><p>Deprecated:</p></td>
<td><p>Confluence 2.10 - use the new <a href="/server/confluence/component-module">Component Module Type</a> instead</p></td>
</tr>
</tbody>
</table>

## Old-Style Plugin Module Type

We recommend that you use the [new plugin module type](/server/confluence/component-module), rather than the old-style Spring Component described below. Confluence still supports the earlier module type, but the new OSGi-based plugin framework fixes a number of bugs and limitations experienced by the old-style plugin modules.

## Purpose of this Module Type

A Spring module allows you to use standard <a href="http://www.springframework.org/" class="external-link">Spring</a> XML configuration tags.

A Spring module appears in `atlassian-plugin.xml` like this:

``` xml
<spring name="Space Cleaner Job" key="spaceCleanerJob" class="org.springframework.scheduling.quartz.JobDetailBean">
        ... any standard spring configuration goes here...
</spring>
```

The above is equivalent to the following configuration in `applicationContext.xml`:

``` xml
<bean id="spaceCleanerJob" class="org.springframework.scheduling.quartz.JobDetailBean">
        ...
</bean>
```

## Ordering of Components

If you declare a Spring component that refers to another Spring component, you must ensure the referred component is declared first. For example:

``` xml
<spring name="Bean A" key="beanA" class="org.springframework.transaction.interceptor.TransactionProxyFactoryBean">
    ...
</spring>

<spring name="Bean B" key="beanB" alias="soapServiceDelegator" class="org.springframework.aop.framework.ProxyFactoryBean">
    <property name="target">
        <ref local="beanA"/>
    </property>
    ...
</spring>
```

Notice that `beanB` refers to `beanA` and that `beanA` is declared *before beanB*. If you don't do it in this order, Confluence will complain that `beanA` does not exist.

##### RELATED TOPICS

[Component Module](/server/confluence/component-module)  
[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)  
<a href="#installing-a-plugin" class="unresolved">Installing a Plugin</a>

