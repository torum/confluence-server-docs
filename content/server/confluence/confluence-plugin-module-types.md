---
aliases:
- /server/confluence/confluence-plugin-module-types-2031663.html
- /server/confluence/confluence-plugin-module-types-2031663.md
category: reference
confluence_id: 2031663
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=2031663
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=2031663
date: '2017-12-08'
legacy_title: Confluence Plugin Module Types
platform: server
product: confluence
subcategory: modules
title: Confluence plugin module types
---
# Confluence plugin module types

Confluence supports the following types of plugin modules:

<table>
<colgroup>
<col style="width: 25%" />
<col style="width: 15%" />
<col style="width: 25%" />
<col style="width: 35%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Module Type</p></th>
<th><p>Since version...</p></th>
<th><p>Documentation</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>index-recoverer</td>
<td>5.8</td>
<td><a href="/server/confluence/index-recoverer-module">Index Recoverer Module</a></td>
<td>Recover out of date or invalid plugin index on start up of a node in a cluster.</td>
</tr>
<tr class="even">
<td>about-page-panel</td>
<td>5.2</td>
<td><a href="/server/confluence/about-page-panel-module">About Page Panel Module</a></td>
<td>Adds information to the 'About Confluence' dialog in the Confluence user interface.</td>
</tr>
<tr class="odd">
<td>blueprint</td>
<td>5.1</td>
<td><a href="/server/confluence/blueprint-module">Blueprint Module</a></td>
<td>Defines a way to create new pages based on user input and pre-defined content.</td>
</tr>
<tr class="even">
<td><p>codeformatter</p></td>
<td><p>2.2</p></td>
<td><p><a href="/server/confluence/code-formatting-module">Code Formatting Module</a></p></td>
<td><p>Adds new languages to the {code} macro</p></td>
</tr>
<tr class="odd">
<td><p>colour-scheme</p></td>
<td><p>1.3</p></td>
<td><p><a href="/server/confluence/theme-module">Theme Module</a></p></td>
<td><p>A colour-scheme for a theme</p></td>
</tr>
<tr class="even">
<td><p>component</p></td>
<td><p>2.10</p></td>
<td><p><a href="/server/confluence/component-module">Component Module</a></p></td>
<td><p>Adds components to Confluence's component system. This is the newer and recommended version of the <code>component</code> module type.</p></td>
</tr>
<tr class="odd">
<td><p>component</p></td>
<td><p>1.4</p></td>
<td><p><a href="/server/confluence/component-module-old-style">Component Module - Old Style</a></p></td>
<td><p>Adds components to Confluence's component system. This is the earlier version of the <code>component</code> module type.</p></td>
</tr>
<tr class="even">
<td><p>component-import</p></td>
<td><p>2.10</p></td>
<td><p><a href="/server/confluence/component-import-module">Component Import Module</a></p></td>
<td><p>Accesses Java components shared by other plugins.</p></td>
</tr>
<tr class="odd">
<td>cql field</td>
<td>5.9</td>
<td><a href="https://developer.atlassian.com/confdev/confluence-plugin-guide/confluence-plugin-module-types/cql-field-module">CQL Field Module</a></td>
<td>Adds fields to the Confluence Query Language</td>
</tr>
<tr class="even">
<td>cql function</td>
<td>5.9</td>
<td><a href="https://developer.atlassian.com/confdev/confluence-plugin-guide/confluence-plugin-module-types/cql-function-module">CQL Function Module</a></td>
<td>Adds functions to the Confluence Query Language</td>
</tr>
<tr class="odd">
<td><p>decorator</p></td>
<td><p>2.5</p></td>
<td><p><a href="/server/confluence/decorator-module">Decorator Module</a></p></td>
<td><p>Adds decorators without using a <a href="/server/confluence/theme-module">Theme Plugin</a></p></td>
</tr>
<tr class="even">
<td>device-type-renderer</td>
<td>4.3.3</td>
<td><a href="/server/confluence/device-type-renderer-module">Device Type Renderer Module</a></td>
<td>Adds a renderer for a particular device type.</td>
</tr>
<tr class="odd">
<td><p>extractor</p></td>
<td><p>1.4</p></td>
<td><p><a href="/server/confluence/extractor-module">Extractor Module</a></p></td>
<td><p>Adds information to the Confluence search index</p></td>
</tr>
<tr class="even">
<td><p>gadget</p></td>
<td><p>3.1</p></td>
<td><p><a href="/server/confluence/gadget-plugin-module">Gadget Plugin Module</a></p></td>
<td><p>Atlassian gadgets provide a new way to include external content into a Confluence wiki page.</p></td>
</tr>
<tr class="odd">
<td><p>job-config</p></td>
<td><p>5.10</p></td>
<td><p><a href="/server/confluence/job-config-module">Job Config Module</a></p></td>
<td><p>Adds scheduled tasks to Confluence using atlassian-scheduler.</p></td>
</tr>
<tr class="even">
<td><p>keyboard-shortcut</p></td>
<td><p>3.4</p></td>
<td><p><a href="/server/confluence/keyboard-shortcut-module">Keyboard Shortcut Module</a></p></td>
<td><p>defines a keyboard shortcut within Confluence.</p></td>
</tr>
<tr class="odd">
<td><p>language</p></td>
<td><p>2.2</p></td>
<td><p><a href="/server/confluence/language-module">Language Module</a></p></td>
<td><p>Adds language translations to Confluence</p></td>
</tr>
<tr class="even">
<td><p>layout</p></td>
<td><p>1.3</p></td>
<td><p><a href="/server/confluence/theme-module">Theme Module</a></p></td>
<td><p>A layout (decorator) definition for a theme</p></td>
</tr>
<tr class="odd">
<td><p>lifecycle</p></td>
<td><p>2.3</p></td>
<td><p><a href="/server/confluence/lifecycle-module">Lifecycle Module</a></p></td>
<td><p>Schedule tasks to be run on application startup and shutdown</p></td>
</tr>
<tr class="even">
<td><p>listener</p></td>
<td><p>1.4</p></td>
<td><p><a href="/server/confluence/event-listener-module">Event Listener Module</a></p></td>
<td><p>A component that can respond to events occurring in the Confluence server</p></td>
</tr>
<tr class="odd">
<td><p>lucene-boosting-strategy</p></td>
<td><p>3.0</p></td>
<td><p><a href="/server/confluence/lucene-boosting-strategy-module">Lucene Boosting Strategy Module</a></p></td>
<td><p>Tweaks document scores in search results in Confluence.</p></td>
</tr>
<tr class="even">
<td><p>macro</p></td>
<td><p>1.3</p></td>
<td><p><a href="/server/confluence/macro-module">Macro Module</a></p></td>
<td><p>A macro used in wiki to HTML conversions (e.g {color}). Outputs HTML that can be embedded in a page or layout. Can retreive user, page and space info, or external content (eg RSS)</p></td>
</tr>
<tr class="odd">
<td><p>module-type</p></td>
<td><p>2.10</p></td>
<td><p><a href="/server/confluence/module-type-module">Module Type Module</a></p></td>
<td><p>Dynamically adds new plugin module types to the plugin framework, generally building on other plugin modules.</p></td>
</tr>
<tr class="even">
<td><p>path-converter</p></td>
<td><p>2.8</p></td>
<td><p><a href="/server/confluence/path-converter-module">Path Converter Module</a></p></td>
<td><p>Allows you to install custom URL schemes as a part of your plugin, i.e. you can have 'pretty' URLs.</p></td>
</tr>
<tr class="odd">
<td><p>rest</p></td>
<td><p>3.1</p></td>
<td><p><a href="/server/confluence/rest-module">REST Module</a></p></td>
<td><p>Exposes services and data entities as REST APIs.</p></td>
</tr>
<tr class="even">
<td><p>rpc-soap</p></td>
<td><p>1.4</p></td>
<td><p><a href="/server/confluence/rpc-module">RPC Module</a></p></td>
<td><p>Deploys a SOAP service within Confluence</p></td>
</tr>
<tr class="odd">
<td><p>rpc-xmlrpc</p></td>
<td><p>1.4</p></td>
<td><p><a href="/server/confluence/rpc-module">RPC Module</a></p></td>
<td><p>Deploys an XML-RPC service within Confluence</p></td>
</tr>
<tr class="even">
<td><p>servlet</p></td>
<td><p>1.4</p></td>
<td><p><a href="/server/confluence/servlet-module">Servlet Module</a></p></td>
<td><p>A standard Java servlet deployed within a Confluence plugin</p></td>
</tr>
<tr class="odd">
<td><p>servlet-context-listener</p></td>
<td><p>2.10</p></td>
<td><p><a href="/server/confluence/servlet-context-listener-module">Servlet Context Listener Module</a></p></td>
<td><p>Deploys Java Servlet context listeners as a part of your plugin.</p></td>
</tr>
<tr class="even">
<td><p>servlet-context-param</p></td>
<td><p>2.10</p></td>
<td><p><a href="/server/confluence/servlet-context-parameter-module">Servlet Context Parameter Module</a></p></td>
<td><p>Sets parameters in the Java Servlet context shared by your plugin's servlets, filters, and listeners.</p></td>
</tr>
<tr class="odd">
<td><p>servlet-filter</p></td>
<td><p>2.10</p></td>
<td><p><a href="/server/confluence/servlet-filter-module">Servlet Filter Module</a></p></td>
<td><p>Deploys Java Servlet filters as a part of your plugin, specifying the location and ordering of your filter.</p></td>
</tr>
<tr class="even">
<td>space-blueprint</td>
<td>5.3</td>
<td><a href="/server/confluence/space-blueprint-module">Space Blueprint Module</a></td>
<td>Defines a way to create new spaces based on user input and pre-defined content.</td>
</tr>
<tr class="odd">
<td><p>spring</p></td>
<td><p>2.2</p></td>
<td><p><a href="/server/confluence/spring-component-module-old-style">Spring Component Module - Old Style</a></p></td>
<td><p>Add a Spring component. Unlike component plugins these allow the use of full Spring configuration XML</p></td>
</tr>
<tr class="even">
<td><p>theme</p></td>
<td><p>1.3</p></td>
<td><p><a href="/server/confluence/theme-module">Theme Module</a></p></td>
<td><p>A custom look-and-feel for a Confluence site or space</p></td>
</tr>
<tr class="odd">
<td><p>usermacro</p></td>
<td><p>2.3</p></td>
<td><p><a href="/server/confluence/user-macro-module">User Macro Module</a></p></td>
<td><p>Allows a simple macro to be created in the plugin XML file, with no Java coding necessary</p></td>
</tr>
<tr class="even">
<td><p>velocity-context-item</p></td>
<td><p>1.4</p></td>
<td><p><a href="/server/confluence/velocity-context-module">Velocity Context Module</a></p></td>
<td><p>Adds helper objects to Confluence's Velocity context</p></td>
</tr>
<tr class="odd">
<td><p>web-item</p></td>
<td><p>2.2</p></td>
<td><p><a href="/server/confluence/web-ui-modules">Web UI Modules</a></p></td>
<td><p>Adds links or tabs to the Confluence UI</p></td>
</tr>
<tr class="even">
<td><p>web-resource</p></td>
<td><p>2.8</p></td>
<td><p><a href="/server/confluence/including-javascript-and-css-resources">Including Javascript and CSS resources</a></p></td>
<td><p>Allows you to include Javascript and CSS resources</p></td>
</tr>
<tr class="odd">
<td><p>web-resource-transformer</p></td>
<td><p>3.4</p></td>
<td><p><a href="/server/confluence/web-resource-transformer-module">Web Resource Transformer Module</a></p></td>
<td><p>Web Resource Transformer plugin modules allow you to manipulate static web resources before they are batched and delivered to the browser</p></td>
</tr>
<tr class="even">
<td><p>web-section</p></td>
<td><p>2.2</p></td>
<td><p><a href="/server/confluence/web-ui-modules">Web UI Modules</a></p></td>
<td><p>Adds sections of links to the Confluence UI</p></td>
</tr>
<tr class="odd">
<td><p>xwork</p></td>
<td><p>1.4</p></td>
<td><p><a href="/server/confluence/xwork-webwork-module">XWork-WebWork Module</a></p></td>
<td><p>XWork/Webwork actions and views bundled with a plugin, enabling user interaction</p></td>
</tr>
</tbody>
</table>

##### RELATED TOPICS

[Writing Confluence Plugins](/server/confluence/writing-confluence-plugins)<br>
<a href="https://confluence.atlassian.com/display/UPM/Installing+Add-ons" class="external-link">Installing a Plugin, in the UPM Administrator's Guide</a>
